package gov.doe.jgi.pi.pps.clarity.scriptexecution

import gov.doe.jgi.pi.pps.clarity.scriptexecution.grails.PoolingService
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.webtransaction.util.RequestUtils

class PoolingController {

    PoolingService poolingService

    def submit() {
        RequestUtils.setNoCache(response)
        ClarityWebTransaction webTransaction = new ClarityWebTransaction('pooling-controller').performTransaction {
            poolingService.submit()
        }
        render(contentType:"application/json",encoding:"UTF-8",status:webTransaction.statusCode) { webTransaction.jsonResponse }
    }

}

