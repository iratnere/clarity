package gov.doe.jgi.pi.pps.clarity.scriptexecution

class UrlMappings {

    static mappings = {
        "/register-process-execution"(controller:'clarityProcessExecution'){
            action = [GET:"postOnly", PUT:"postOnly", DELETE:"postOnly", POST:"register"]
        }
    }
}
