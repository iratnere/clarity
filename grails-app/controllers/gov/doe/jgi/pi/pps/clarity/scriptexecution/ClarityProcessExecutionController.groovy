package gov.doe.jgi.pi.pps.clarity.scriptexecution

import gov.doe.jgi.pi.pps.clarity.scriptexecution.grails.ProcessExecutionService
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransactionSuccessRecorder
import gov.doe.jgi.pi.pps.util.exception.MethodNotAllowedException

class ClarityProcessExecutionController {

	ProcessExecutionService processExecutionService

	def postOnly() {
		response.addHeader('Allow', 'POST')
		throw new MethodNotAllowedException()
	}
	
	def register() {
		ClarityWebTransaction webTransaction = new ClarityWebTransaction('clarity-process-script').setLoggingEnabled().performTransaction(new ClarityWebTransactionSuccessRecorder(enabled: true)) {
			processExecutionService.processScriptParams()
		}
		render(contentType:'application/json',encoding:'UTF-8',status:webTransaction.statusCode, text:webTransaction.jsonResponse?.toString())
	}
	
}
