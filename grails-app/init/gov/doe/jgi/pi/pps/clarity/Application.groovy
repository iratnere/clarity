package gov.doe.jgi.pi.pps.clarity

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import groovy.transform.CompileStatic
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.context.ApplicationPidFileWriter
import org.springframework.boot.web.context.WebServerPortFileWriter

@CompileStatic
@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        //GrailsApp.run(Application, args)
        GrailsApp app = new GrailsApp(Application.class)
        app.addListeners(new ApplicationPidFileWriter())
        app.addListeners(new WebServerPortFileWriter())
        app.run(args)

    }
}