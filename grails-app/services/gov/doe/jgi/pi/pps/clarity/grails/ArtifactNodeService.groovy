package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.util.HttpUtils
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.gorm.transactions.Transactional
import groovy.sql.Sql
import org.grails.web.json.JSONObject
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException

@Transactional
class ArtifactNodeService {

    DataSource dataSource

    /*
    collect container ids with
    the 'Label' UDF value equals "$DATE_RA_$counter"
    return contaner LIMS IDs
    example: https://clarity-dev01.jgi-psf.org/api/v2/containers/?udf.Label=17-Jul-12_RA_1
     */
    List<String> collectPlateContainerLimsIds(int counter, Date date = new Date()) {
        NodeConfig nodeConfig = ClarityWebTransaction.requireCurrentTransaction().nodeManager.nodeConfig
        def formatDate = date?.format("yy-MMM-dd") //17-Jun-30
        def query = "?udf.Label=$formatDate" + "_RA_$counter"
        String url = "${nodeConfig.containersUrl}${query}"
        def prcIds = new XmlSlurper().parseText(HttpUtils.httpGet(nodeConfig, url)).container.collect {
            it.'@limsid' as String
        }
        prcIds
    }

    List<String> getDescendantList(String limsid) {
        Sql query = new Sql(dataSource)
        String findDescendants = buildDescendantListQuery()
        def descendants = query.rows(findDescendants, ['LIMSID': limsid])
        descendants.descendant
    }

    private final static String buildDescendantListQuery() {
        return """
            SELECT descendants.luid AS descendant
            FROM ARTIFACT descendants
            INNER JOIN ARTIFACT_ANCESTOR_MAP aam
            ON descendants.artifactid = aam.artifactid
            INNER JOIN ARTIFACT art
            ON aam.ancestorartifactid = art.artifactid
            WHERE descendants.artifacttypeid = 2            -- Child is an analyte
            AND art.luid = :LIMSID                          -- Script input to query
            ORDER BY descendants.artifactid                 -- Oldest child first
            """
    }

    private String buildQueueDateQuery(List<String> artifactIds, String protocolName, String workflowName){
        String artifactIdsStr = "'", delim = ''
        for(String artifactId : artifactIds){
            artifactIdsStr = artifactIdsStr + delim + artifactId
            delim = "', '"
        }
        artifactIdsStr += "'"
        return "select art.LUID as ARTIFACTID, MIN(st.CREATEDDATE) as QUEUEDATE " +
                " from STAGETRANSITION st, ARTIFACT art, STAGE s, PROTOCOLSTEP ps, LABPROTOCOL lp, WORKFLOWSECTION ws, LABWORKFLOW lw " +
                " where st.STAGEID = s.STAGEID"+
                " and s.STEPID = ps.STEPID"+
                " and ps.PROTOCOLID  = lp.PROTOCOLID"+
                " and s.MEMBERSHIPID = ws.SECTIONID"+
                " and ws.WORKFLOWID = lw.WORKFLOWID"+
                " and st.ARTIFACTID = art.ARTIFACTID"+
                " and WORKFLOWRUNID > 0"+
                " and COMPLETEDBYID is null"+
                " and lw.WORKFLOWNAME = '${workflowName}'"+
                " and lp.PROTOCOLNAME = '${protocolName}'"+
                " and art.LUID in (${artifactIdsStr})"+
                " group by art.LUID".replaceAll("\n","")
    }

    Map getArtifactsQueueDate(List<String> artifactIds, String protocolName, String workflowName){
        String query = buildQueueDateQuery(artifactIds, protocolName, workflowName)
        JdbcTemplate jt = new JdbcTemplate(dataSource)
        return jt.query(query, new ResultSetExtractor<Object>() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                JSONObject obj = new JSONObject()
                while (rs.next()) {
                    obj.put(rs.getString('ARTIFACTID'), rs.getDate('QUEUEDATE'))
                }
                return obj
            }
        })
    }

    /*
    http://frow.jgi-psf.org:8080/api/v2/processes/?type==LC+Library+Creation&inputartifactlimsid=2-561306
     */
    static List<String> getProcessIdsForInputArtifactId(String inputartifactlimsid, def prcType) {
        NodeConfig nodeConfig = ClarityWebTransaction.requireCurrentTransaction().nodeManager.nodeConfig
        def encType = URLEncoder.encode(prcType.value,"UTF-8")
        def query = "?type=$encType&inputartifactlimsid=$inputartifactlimsid"
        String url = "${nodeConfig.processesUrl}${query}"
        def prcIds = new XmlSlurper().parseText(HttpUtils.httpGet(nodeConfig, url)).process.collect {
            it.'@limsid' as String
        }
        prcIds
    }

    /*
    //http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/?attach-to-category=ProcessType&name=Kapa+Library+Prep+Kit&attach-to-name=LC+Library+Creation
     */
    static List<String> getUdfUris(NodeConfig nodeConfig,
                                   String udfName,
                                   String attachToName="LC Library Creation",
                                   String attachToCategory="ProcessType"
                                   ) {
        def encUdfName = URLEncoder.encode(udfName,"UTF-8")
        def encAttachToName = URLEncoder.encode(attachToName,"UTF-8")
        def encAttachToCategory = URLEncoder.encode(attachToCategory,"UTF-8")
        def query = "?attach-to-category=$encAttachToCategory&name=$encUdfName&attach-to-name=$encAttachToName"
        String url = "${nodeConfig.configurationUrl}udfs/${query}"
        def udfUris = new XmlSlurper().parseText(HttpUtils.httpGet(nodeConfig, url)).udfconfig.collect {
            it.'@uri' as String
        }
        udfUris
    }

    /*
https://clarity-dev01.jgi-psf.org/api/v2/projects?name=1283085
*/
    static String getProjectIdByName(String name) {
        NodeConfig nodeConfig = ClarityWebTransaction.requireCurrentTransaction().nodeManager.nodeConfig
        def projectName = URLEncoder.encode(name,"UTF-8")
        def query = "?name=$projectName"
        String url = "${nodeConfig.projectsUrl}${query}"
        def projectIds = new XmlSlurper().parseText(HttpUtils.httpGet(nodeConfig, url)).project.collect {
            it.'@limsid' as String
        }
        assert projectIds
        assert projectIds.size() == 1
        projectIds[0]
    }
/*
https://clarity-dev01.jgi-psf.org/api/v2/samples?name=228859
 */
    static def getSampleIdsByName(String name) {
        NodeConfig nodeConfig = ClarityWebTransaction.requireCurrentTransaction().nodeManager.nodeConfig
        def sampleName = URLEncoder.encode(name,"UTF-8")
        def query = "?name=$sampleName"
        String url = "${nodeConfig.samplesUrl}${query}"
        def sampleIds = new XmlSlurper().parseText(HttpUtils.httpGet(nodeConfig, url)).sample.collect {
            it.'@limsid' as String
        }
        assert sampleIds
        sampleIds
    }


}
