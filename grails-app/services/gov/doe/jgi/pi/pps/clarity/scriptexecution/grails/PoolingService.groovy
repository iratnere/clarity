package gov.doe.jgi.pi.pps.clarity.scriptexecution.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling.PoolingFactory
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import grails.gorm.transactions.Transactional

@Transactional
class PoolingService {
    ArtifactIndexService artifactIndexService

    def submit() {
        ClarityWebTransaction webTransaction = ClarityWebTransaction.currentTransaction
        try {
            webTransaction.logger.info "Indexes submitted for pooling: \n${webTransaction.jsonSubmission.toString(2)}"
            def indexesForPooling = webTransaction.jsonSubmission.'data'
            int dop = indexesForPooling.'dop'
            if(!dop || dop < 2){
                throw new IllegalArgumentException("Dop is required and should be >= 2")
            }
            def indexes = indexesForPooling.'indexes'
            def poolCount
            List poolPrepBeans
            if(indexes) {
                poolPrepBeans = indexesForPooling.'indexes'?.collect { new LibraryInformation(indexName: it) }
                poolCount = poolPrepBeans.size()/dop
            }
            else {
                poolPrepBeans = getBeansForSet(indexesForPooling.'index-set-name')
                if(!poolPrepBeans)
                    throw new IllegalArgumentException("index set name ${indexesForPooling.'index-set-name'} does not exist in the database")
                poolCount = indexesForPooling.'pools-count'
                if(!poolCount)
                    throw new IllegalArgumentException("pools-count field is mandatory with index-set-name field")
            }
            if (poolPrepBeans) {
                if(poolPrepBeans.find{!it.isDualIndex})
                    throw new IllegalArgumentException("Please use this service to pool dual indexes only")
                indexesForPooling.'min-pools' = poolIndexes(dop, poolCount as int, poolPrepBeans)
            }
            else
                throw new IllegalArgumentException("Please provide a list of indexes or the index set name.")
            webTransaction.statusCode = 200
            webTransaction.jsonResponse.'data' = indexesForPooling
        }
        catch(Exception e){
            webTransaction.statusCode = 422
            webTransaction.jsonResponse.'error' = e.message
        }
    }

    List<List<LibraryInformation>> poolIndexes(int dop, int poolCount, List<LibraryInformation> poolPrepBeans){
        if(!poolPrepBeans[0].indexSequence.contains("-"))
            return poolPrepBeans.collect{it.indexName}
        Pooling pooling = PoolingFactory.getPoolingTechnique(LibraryInformation.DUAL_INDEX, poolPrepBeans)
        pooling.degreeOfPooling = dop
        pooling.makePools()
        Map map = [:].withDefault {[]}
        poolPrepBeans.each{ LibraryInformation li ->
            if(li.poolNumber > 0)
                map[li.poolNumber] << "${li.indexName}:${li.plateLocation}"
        }
        return map.values() as List
    }

    List<LibraryInformation> getBeansForSet(String indexSetName) {
        if(!artifactIndexService)
            artifactIndexService = ClarityWebTransaction.currentTransaction.requireApplicationBean(ArtifactIndexService.class)
        Map<String, List<Index>> indexes = artifactIndexService.getIndexes([indexSetName])
        List<LibraryInformation> members = []
        indexes[indexSetName]?.each{ Index index ->
            members = new LibraryInformation(indexName: index.indexName, indexSequence: index.indexSequence, plateLocation: index.plateLocation)
        }
        return members
    }
}
