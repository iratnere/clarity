package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.core.GrailsApplication
import org.springframework.beans.factory.DisposableBean

import javax.sql.DataSource
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ConfigurationService implements DisposableBean {

    GrailsApplication grailsApplication
    DataSource dataSource_clarity

    static int defaultThreadPoolSize = 10
    private ExecutorService clarityExecutorService = Executors.newFixedThreadPool(defaultThreadPoolSize)

    private synchronized ExecutorService getThreadPool() {
        clarityExecutorService
    }

    @Override
    public void destroy() throws Exception {
        clarityExecutorService?.shutdownNow()
    }

    //CAREFUL, specifying too large a thread pool size could put too much strain on clarity!!
    synchronized resetThreadPool(int threadPoolSize) {
        clarityExecutorService?.shutdownNow()
        clarityExecutorService = Executors.newFixedThreadPool(threadPoolSize)
    }

    NodeConfig getClarityNodeConfig() {
        NodeConfig nodeConfig = new NodeConfig()
        nodeConfig.geneusUrl = grailsApplication.config.clarity.url
        nodeConfig.geneusUser = grailsApplication.config.clarity.user
        nodeConfig.geneusPassword = grailsApplication.config.clarity.password
        nodeConfig.sftpUrl = grailsApplication.config.clarity.sftp.url
        nodeConfig.sftpUser = grailsApplication.config.clarity.sftp_user
        nodeConfig.sftpPassword = grailsApplication.config.clarity.sftp_password
        nodeConfig.clarityDataSource = dataSource_clarity
        nodeConfig.executorService = threadPool
        String tr = grailsApplication.config.getProperty('nodeManager.httpStackTrace')
        //nodeConfig.httpStackTrace = grailsApplication.config.getProperty('nodeManager.httpStackTrace')?.equalsIgnoreCase('true')
        if (tr)
            nodeConfig.httpStackTrace = tr.equalsIgnoreCase('true')
        nodeConfig
    }

    NodeConfig userNodeConfig(String username, String password) {
        NodeConfig nodeConfig = new NodeConfig()
        nodeConfig.geneusUrl = grailsApplication.config.clarity.url
        nodeConfig.geneusUser = username
        nodeConfig.geneusPassword = password
        nodeConfig.sftpUrl = grailsApplication.config.clarity.sftp.url
        nodeConfig.sftpUser = grailsApplication.config.clarity.sftp_user
        nodeConfig.sftpPassword = grailsApplication.config.clarity.sftp_password
        nodeConfig.clarityDataSource = dataSource_clarity
        nodeConfig.executorService = threadPool
        nodeConfig.httpStackTrace = grailsApplication.config.getProperty('nodeManager.httpStackTrace')?.equalsIgnoreCase('true')
        nodeConfig
    }

}
