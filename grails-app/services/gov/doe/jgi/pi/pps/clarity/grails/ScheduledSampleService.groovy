package gov.doe.jgi.pi.pps.clarity.grails

import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovy.json.JsonBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.apache.log4j.Logger
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException

@Transactional
class ScheduledSampleService {

    Logger logger = Logger.getLogger(ScheduledSampleService.class.name)

    GrailsApplication grailsApplication
    JdbcTemplate jdbcTemplate
    DataSource dataSource

    def createScheduledSamplesInClarity(List<Long> sowItemIds, Long contactId) {
        def sowItemSampleMap = null
        if(!sowItemIds)
            return sowItemSampleMap
        String scheduledSampleUrl = "${grailsApplication.config.getProperty('scheduledSample.url')}"
        ClarityWebTransaction.logger.info "${scheduledSampleUrl}"
        def jsonRequest = new JsonBuilder()
        jsonRequest {
            'sow-item-ids'(sowItemIds)
            'submitted-by'(contactId)
        }
        ClarityWebTransaction.logger.info "${jsonRequest.toPrettyString()}"
        HTTPBuilder http = new HTTPBuilder(scheduledSampleUrl)
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = jsonRequest.toPrettyString()
            response.success = {resp, reader ->
                sowItemSampleMap = reader
            }
            response.failure = {resp ->
                throw new RuntimeException( "${resp.status} : ${resp.statusLine.reasonPhrase}")
            }
        }
        return sowItemSampleMap
    }

    def getSowItemMetadataForSample(Long pmoSampleId, Boolean returnPeu=false, Boolean validateQueue = false){
        getBatchSowItemMetadataForSample([pmoSampleId], returnPeu, validateQueue)
    }

    JSONObject getBatchSowItemMetadataForSample(Collection sampleIds, Boolean returnPeu=false, Boolean validateQueue = false){
        def jsonRequest = new JsonBuilder()
        jsonRequest {
            'sample-ids'(sampleIds)
            'return-peu'(returnPeu)
            'validate-queue'(validateQueue)
        }

        JSONObject sowItemMap = new JSONObject()
        String sowItemsUrl = "${grailsApplication.config.getProperty('sowItems.url')}"
        HTTPBuilder http = new HTTPBuilder(sowItemsUrl)
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = jsonRequest.toPrettyString()
            response.success = { resp, reader ->
                logger.info "web-service=sow-items status=success resp=${resp}:\n${reader}"
                JSONArray samples = new JSONArray(reader)
                samples?.each { def sample ->
                    String sampleIdString = sample?.'sample-id' as String
                    if (!sampleIdString?.isLong()) {
                        return
                    }
                    JSONArray sowItemList = new JSONArray()
                    sowItemMap[sampleIdString.toLong()] = sowItemList
                    sample.'sow-items'?.each { sowItem ->
                        if (sowItem) {
                            sowItemList.add(new JSONObject(sowItem))
                        }
                    }
                }
            }
            response.failure = {resp, reader ->
                logger.error "web-service=sow-items status=failure resp=${resp}:\n${reader}"
                throw new RuntimeException("Errors: ${resp.status} : ${reader}")
            }
        }
        ClarityWebTransaction.logger.info "service=${ScheduledSampleService.class.simpleName} returning:\n${sowItemMap.toString(2)}"
        sowItemMap
    }

    def getAllDescendantIds(List<String> scheduledSampleAnalyteIds){
        String query = """select art2.luid as ss, art.luid as artLimsId
from CLARITYLIMS.ARTIFACT_ANCESTOR_MAP artmap, claritylims.artifact art, claritylims.artifact art2,
claritylims.artifact_sample_map amap
where artmap.ANCESTORARTIFACTID = art2.artifactid
and amap.ARTIFACTID=art2.artifactid
and artmap.artifactid=art.artifactid
and art.artifacttypeid=2
and art2.artifacttypeid=2
and art2.luid in ('${scheduledSampleAnalyteIds.join("','")}')
order by artLimsId"""
        jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                def obj = [:].withDefault {[]}
                while (rs.next()) {
                    obj[rs.getString('ss')] << rs.getString('artLimsId')
                }
                return obj
            }
        })
    }

    Map addSowItem(def submissionJson){
        Map sampleSow = [:]
        String url = "${grailsApplication.config.getProperty('addSowItem.url')}"
        JSONArray samples = new JSONArray(postToSowService(url, submissionJson))
        samples?.each { def sample ->
            sampleSow[sample?.'sample-id'] = sample?.'sow-item-id'
                }
        return sampleSow
    }

    void editSow(Long submittedBy, List<ScheduledSample> scheduledSamples){
        String url = "${grailsApplication.config.getProperty('sowItemsEdit.url')}"
        def ssGroups = scheduledSamples.groupBy {it.udfDegreeOfPooling + it.udfRunMode}
        ssGroups.values().each{List<ScheduledSample> ssg ->
            def jsonRequest = new JsonBuilder()
            jsonRequest {
                "submitted-by"(submittedBy)
                "sow-items-edit" {
                    "sow-item-ids"(ssg.collect { it.sowItemId })
                    'degree-of-pooling'(ssg[0].udfDegreeOfPooling)
                    'run-mode'(ssg[0].udfRunMode)
                }
            }
            postToSowService(url, jsonRequest.toPrettyString())
        }
    }

    def postToSowService(String url, String jsonSubmission){
        HTTPBuilder http = new HTTPBuilder(url)
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = jsonSubmission
            response.success = { resp, reader ->
                return reader
            }
            response.failure = {resp, reader ->
                throw new RuntimeException("Errors: ${resp.status} : ${reader}")
            }
        }
    }
}
