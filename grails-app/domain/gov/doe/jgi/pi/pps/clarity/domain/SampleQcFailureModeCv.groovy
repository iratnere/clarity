package gov.doe.jgi.pi.pps.clarity.domain

class SampleQcFailureModeCv {

    static mapping = {
        table name:'dt_sample_qc_failure_mode_cv', schema: 'uss'
        id column: 'SAMPLE_QC_FAILURE_MODE_ID', generator:'assigned'
        failureMode column: 'sample_qc_failure_mode'
        version false
    }

    static constraints = {
        failureMode(nullable:false, maxSize:32)
        active(nullable:false,maxSize:1,inList:['Y','N'])
    }

    String failureMode
    String active

    String toString() {
        return failureMode
    }
}
