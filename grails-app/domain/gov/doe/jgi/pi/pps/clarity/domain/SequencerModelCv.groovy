package gov.doe.jgi.pi.pps.clarity.domain

import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv

class SequencerModelCv {

	static final String NOVASEQ = 'NovaSeq'

	static transients = ['isSequel','isPacBio','isIlluminaHiSeqHO']

	static fetchMode = [platformCv: 'eager']

	static mapping = {
		table name:'dt_sequencer_model_cv', schema: 'uss'
		id column:'sequencer_model_id', generator:'assigned'
		platformCv column: 'platform_id'
		version false
	}
	
	static constraints = {
		sequencerModel(nullable:false, maxSize:30, unique:true)
	}

	String sequencerModel
    PlatformCv platformCv
	String active

	String getPlatform() {
		return platformCv.platform
	}

	boolean getIsSequel() {
		(sequencerModel == SequencerModelTypeCv.SEQUEL.value || sequencerModel == SequencerModelTypeCv.SEQUEL_II.value)
	}

	boolean getIsPacBio() {
		platformCv?.isPacBio
	}

	boolean getIsIllumina() {
		platformCv?.isIllumina
	}

	boolean getIsIlluminaMiSeq() {
		sequencerModel == SequencerModelTypeCv.MISEQ.value
	}

	boolean getIsIlluminaNovaSeq() {
		sequencerModel.contains(NOVASEQ)
	}

    boolean getIsIlluminaHiSeqRapid() {
        sequencerModel == SequencerModelTypeCv.HISEQ_RAPID.value
    }

    boolean getIsIlluminaNextSeqHO() {
        sequencerModel == SequencerModelTypeCv.NEXTSEQ_HO.value
    }

    boolean getIsNextSeq() {
        return isIlluminaNextSeqHO || isIlluminaNextSeqMO
    }

    boolean getIsIlluminaNextSeqMO() {
        sequencerModel == SequencerModelTypeCv.NEXTSEQ_MO.value
    }

	boolean getIsIlluminaHiSeqHO() {
		sequencerModel == SequencerModelTypeCv.HISEQ_HO.value
	}

	String toString() {
		sequencerModel
	}

	String getFlowcellType() {
		if(!isIlluminaNovaSeq)
			return ""
		String[] sequencerModelSplit = sequencerModel.split("\\s+")
		if(sequencerModelSplit.size() <= 1)
			return ""
		return sequencerModelSplit[1]
	}
}