package gov.doe.jgi.pi.pps.clarity.domain

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.dto.RunModeDto
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode

import java.util.regex.Matcher
import java.util.regex.Pattern

class RunModeCv implements Comparator<RunModeCv> {

	static transients = ['isActive']
	static fetchMode = [sequencerModel:'eager', 'sequencingQueue':'eager']

	static mapping = {
		table name:'dt_run_mode_cv', schema: 'uss'
		id column:'run_mode_id', generator:'assigned'
		sequencingQueue column:'clarity_lims_queue_id'
		version false
	}

	static constraints = {
		runMode(nullable:false, maxSize:30, unique:true)
	}

	String runMode
    SequencerModelCv sequencerModel
    ClarityLimsQueueCv sequencingQueue

	String active
	BigInteger readLengthBp
	BigInteger readTotal

	boolean getIsActive() {
		active == 'Y'
	}

	String getSequencingWorkflowName(){
		sequencingQueue?.clarityLimsQueueName
	}

	WorkflowNode getSequencingWorkflowNode() {
		String qName = sequencingWorkflowName
		if (qName) {
			return ClarityWebTransaction.requireCurrentTransaction().requireNodeManager().getWorkflowNodeByName(qName)
		}
		return null
	}

	String getSequencingWorkflowUri() {
		sequencingWorkflowNode?.workflowUri
	}

	@Override
	int compare(RunModeCv a, RunModeCv b) {
		return a.runMode.compareToIgnoreCase(b.runMode)
	}

    RunModeDto toDto() {
		new RunModeDto(this)
	}

	boolean getIsPacBio(){
		return this.sequencerModel.isPacBio
	}

    Stage getTargetWorkflowForLibrary(){
		if(isPacBio) {
			if(this.sequencerModel.isSequel)
				return Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING
		}
		return Stage.LIBRARY_QPCR
	}

	String toString() {
		runMode
	}

	static getRunModeForFlowcellType(RunModeCv runMode, String flowcellType) {
		if(!runMode.sequencerModel.isIlluminaNovaSeq)
			return runMode
		String regex = "([A-Za-z]+\\s+)([A-Za-z]+\\s+)([A-Za-z0-9]*\\s*)([0-9]+\\s+X\\s+[0-9]+)"
		Pattern p = Pattern.compile(regex)
		Matcher m = p.matcher(runMode.runMode)
		if (m.matches()) {
			String platform = m.group(1)
			String sequencer = m.group(2)
			String flowcell = m.group(3)
			String reads = m.group(4)
			if(flowcell == flowcellType)
				return runMode
			String runModeName = "$platform$sequencer$flowcellType $reads"
            RunModeCv newRunMode = RunModeCv.findByRunMode(runModeName)
			if(newRunMode)
				return newRunMode
		}
		throw new IllegalArgumentException("Could not find runmode for flowcelltype $flowcellType")
	}
}