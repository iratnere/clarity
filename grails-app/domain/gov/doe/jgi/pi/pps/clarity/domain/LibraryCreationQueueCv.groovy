package gov.doe.jgi.pi.pps.clarity.domain

class LibraryCreationQueueCv {
	static belongsTo = LibraryCreationSpecsCv
	static hasMany = [libraryCreationSpecs: LibraryCreationSpecsCv]
	static mapping = {
		table name:'dt_library_creation_queue_cv', schema: 'uss'
		id column:'library_creation_queue_id', generator:'assigned'
		plateTargetVolumeLibTrialUl column:'plate_target_vol_lib_trial_ul'
		tubeTargetVolumeLibTrialUl column:'tube_target_vol_lib_trial_ul'
		plateTargetMassLibTrialNg column:'plate_target_mass_lib_trial_ng'
		tubeTargetMassLibTrialNg column:'tube_target_mass_lib_trial_ng'
		libraryCreationSpecs joinTable: 'dt_m2m_lc_queue_specs', column: 'library_creation_queue_id'
		version false
	}


	static constraints = {
		libraryCreationQueue(nullable:false, maxSize:100, unique:true)
		active(nullable:false,maxSize:1,inList:['Y','N'])
	}

	String active
	String libraryCreationQueue
	BigDecimal plateTargetVolumeLibTrialUl
	BigDecimal tubeTargetVolumeLibTrialUl
	BigDecimal plateTargetMassLibTrialNg
	BigDecimal tubeTargetMassLibTrialNg
}