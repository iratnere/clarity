package gov.doe.jgi.pi.pps.clarity.domain

class SampleAliquotFailureModeCv {

    static mapping = {
        table name:'dt_sample_aliquot_fail_mode_cv', schema: 'uss'
        id column: 'SAMPLE_ALIQUOT_FAILURE_MODE_ID', generator:'assigned'
        version false
    }

    static constraints = {
        failureMode(nullable:false, maxSize:32)
        active(nullable:false,maxSize:1,inList:['Y','N'])
    }

    String failureMode
    String active

    String toString() {
        return failureMode
    }
}
