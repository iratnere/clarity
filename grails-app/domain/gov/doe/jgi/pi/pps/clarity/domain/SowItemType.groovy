package gov.doe.jgi.pi.pps.clarity.domain

class SowItemType {

    static mapping = {
        table name:'dt_sow_item_type_cv', schema: 'uss'
        id column:'sow_item_type_id'
        version false
    }

    static constraints = {
        sowItemType(maxSize:50)
        active(nullable:false,maxSize:1,inList:['Y','N'])
    }

    String sowItemType
    String active

    public String toString() {
        sowItemType
    }
}
