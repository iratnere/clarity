package gov.doe.jgi.pi.pps.clarity.domain

class LibraryNames {

    static mapping = {
        table name:'dt_library_names', schema: 'uss'
        id column: 'LIBRARY_ID', generator: 'assigned'
        version false
    }

    static constraints = {
        geneusLimsId(nullable:true, unique:true, maxSize:32)
        libraryName(nullable:false, unique:true, maxSize:16)
        libraryType(nullable:true)
        dateGranted(nullable:true)
    }

    String geneusLimsId
    String libraryName
    String libraryType
    Date dateGranted
}
