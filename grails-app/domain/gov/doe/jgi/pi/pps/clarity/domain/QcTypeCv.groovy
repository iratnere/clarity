package gov.doe.jgi.pi.pps.clarity.domain

class QcTypeCv {

	static mapping = {
		table name:'dt_qc_type_cv', schema: 'uss'
		id column:'qc_type_id', generator:'assigned'
		qcType column:'qc_type'
		version false
	}

	String qcType
	String active
}