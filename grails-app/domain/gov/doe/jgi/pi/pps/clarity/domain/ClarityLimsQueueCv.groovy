package gov.doe.jgi.pi.pps.clarity.domain

class ClarityLimsQueueCv implements Comparator<ClarityLimsQueueCv> {

	static transients = ['isActive']

	static mapping = {
		table name:'dt_clarity_lims_queue_cv', schema: 'uss'
		id column:'clarity_lims_queue_id', generator:'assigned'
		version false
	}

	static constraints = {
		clarityLimsQueueName(nullable:false, maxSize:100, unique:true, blank:false)
		active(nullable:false,maxSize:1,inList:['Y','N'])
	}

	String clarityLimsQueueName
	String active

	boolean getIsActive() {
		active == 'Y'
	}

	@Override
	int compare(ClarityLimsQueueCv a, ClarityLimsQueueCv b) {
		int returnCode = a.isActive <=> b.isActive
		if (!returnCode) {
			returnCode = a.clarityLimsQueueName.compareToIgnoreCase(b.clarityLimsQueueName)
		}
		returnCode
	}

	String toString() {
		clarityLimsQueueName
	}

}