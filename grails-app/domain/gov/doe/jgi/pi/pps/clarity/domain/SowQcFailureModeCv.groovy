package gov.doe.jgi.pi.pps.clarity.domain

class SowQcFailureModeCv {

    static mapping = {
        table name:'dt_sow_item_qc_failure_mode_cv', schema: 'uss'
        id column: 'SOW_ITEM_QC_FAILURE_MODE_ID', generator:'assigned'
        failureMode column: 'SOW_ITEM_QC_FAILURE_MODE'
        version false
    }

    static constraints = {
        failureMode(nullable:false, maxSize:32)
        active(nullable:false,maxSize:1,inList:['Y','N'])
    }

    String failureMode
    String active

    String toString() {
        return failureMode
    }
}
