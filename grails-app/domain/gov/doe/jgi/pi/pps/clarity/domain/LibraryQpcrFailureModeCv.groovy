package gov.doe.jgi.pi.pps.clarity.domain

class LibraryQpcrFailureModeCv {

    static mapping = {
        table name: 'dt_library_qpcr_fail_mode_cv', schema: 'uss'
        id column: 'LIBRARY_QPCR_FAILURE_MODE_ID', generator:'assigned'
        version false
    }

    static constraints = {
        failureMode(nullable:false, maxSize:32)
        active(nullable:false,maxSize:1, inList:['Y','N'])
    }

    String failureMode
    String active

    String toString() {
        return failureMode
    }
}
