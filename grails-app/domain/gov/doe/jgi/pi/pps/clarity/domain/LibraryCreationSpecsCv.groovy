package gov.doe.jgi.pi.pps.clarity.domain

class LibraryCreationSpecsCv {
	static hasMany = [libraryCreationQueues: LibraryCreationQueueCv]
	static mapping = {
		table name:'dt_library_creation_specs_cv', schema: 'uss'
		id column:'library_creation_specs_id', generator:'assigned'
		libraryCreationQueues joinTable: 'dt_m2m_lc_queue_specs', column: 'library_creation_specs_id'
		version false
	}

	static constraints = {
		libraryCreationSpecs(nullable:false, maxSize:100, unique:true)
		active(nullable:false,maxSize:1,inList:['Y','N'])
	}

	String active
	String libraryCreationSpecs

	String toString() {
		return libraryCreationSpecs
	}

}