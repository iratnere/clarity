package gov.doe.jgi.pi.pps.clarity.domain

import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv

class PlatformCv {

    static transients = ['isPacBio']

    static mapping = {
        table name:'dt_platform_cv', schema: 'uss'
        id column:'platform_id', generator:'assigned'
        version false
    }

    String platform
    String active

    boolean getIsPacBio() {
        platform == PlatformTypeCv.PACBIO.value
    }

    boolean getIsIllumina() {
        platform == PlatformTypeCv.ILLUMINA.value
    }

}
