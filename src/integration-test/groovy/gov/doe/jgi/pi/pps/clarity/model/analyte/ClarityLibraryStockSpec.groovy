package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration
import spock.lang.Shared
import spock.lang.Specification

@Integration
@Rollback
class ClarityLibraryStockSpec extends Specification {

    ClarityWebTransaction webTransaction
    NodeManager clarityNodeManager
    @Shared
    def plateTransferLibraryId = '2-1691342'
    @Shared
    def libraryStockId = '2-1686028'
    ClarityLibraryStock libraryStock
    ClarityLibraryStock copyLibraryStock

    def setup() {
        webTransaction = new ClarityWebTransaction().setNodeManager().setTestMode().build()
        clarityNodeManager = webTransaction.requireNodeManager()
        libraryStock = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(libraryStockId)) as ClarityLibraryStock
        copyLibraryStock = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(plateTransferLibraryId)) as ClarityLibraryStock
    }

    def cleanup() {
        webTransaction?.close()
    }

    void "test getUdfSmrtbellTemplatePrepKit"() {
        expect:"ClarityLibraryStock"
            libraryStock
            copyLibraryStock
        when:
            def udfSmrtbellTemplatePrepKit = libraryStock.udfSmrtbellTemplatePrepKit
        then:
            udfSmrtbellTemplatePrepKit == '8888'
            udfSmrtbellTemplatePrepKit == copyLibraryStock.udfSmrtbellTemplatePrepKit
    }

    void "test getUdfPacBioSmrtbellTemplatePrepKit"() {
        setup:
            def prepKit = "099999999"
        ClarityLibraryStock ls = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode('2-1686028')) as ClarityLibraryStock
            ls.artifactNode.parentProcessNode.setUdf(ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT.udf, prepKit)
        ClarityLibraryStock copyLs = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode('2-1691342')) as ClarityLibraryStock
        when:
            def udfPacBioSmrtbellTemplatePrepKit = ls.udfPacBioSmrtbellTemplatePrepKit
        then:
            udfPacBioSmrtbellTemplatePrepKit == prepKit
            udfPacBioSmrtbellTemplatePrepKit == copyLs.udfPacBioSmrtbellTemplatePrepKit
    }

    void "test udfLpActualWithSof"() {
        expect:"ClarityLibraryStock"
            libraryStock
            BigDecimal actualFl = 22.33
        when:
            libraryStock.udfLpActualWithSof = actualFl
        then:
            libraryStock.udfLpActualWithSof == actualFl
    }
}
