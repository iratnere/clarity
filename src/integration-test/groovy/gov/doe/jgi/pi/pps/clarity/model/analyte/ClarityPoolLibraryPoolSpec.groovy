package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPoolLibraryPool
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration
import spock.lang.Specification

@Integration
@Rollback
class ClarityPoolLibraryPoolSpec extends Specification {

    NodeManager clarityNodeManager
    ClarityWebTransaction webTransaction

    def setup() {
        webTransaction = new ClarityWebTransaction().setNodeManager().setTestMode().build()
        clarityNodeManager = webTransaction.requireNodeManager()
    }

    def cleanup() {
        webTransaction?.close()
    }

    void "test ClarityPoolLibraryPool"() {
        setup:
            def poolOfPoolsExome = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode('2-1560332'))
        expect:
            poolOfPoolsExome instanceof ClarityPoolLibraryPool
    }
}
