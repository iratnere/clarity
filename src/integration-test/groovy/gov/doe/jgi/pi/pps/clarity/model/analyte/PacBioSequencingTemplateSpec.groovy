package gov.doe.jgi.pi.pps.clarity.model.analyte


import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration
import spock.lang.Specification

@Integration
@Rollback
class PacBioSequencingTemplateSpec extends Specification {

    ClarityWebTransaction webTransaction
    NodeManager clarityNodeManager

    PacBioSequencingTemplate pacBioPool
    PacBioSequencingTemplate pacBioLibrary

    def setup() {
        webTransaction = new ClarityWebTransaction().setNodeManager().setTestMode().build()
        clarityNodeManager = webTransaction.requireNodeManager()
        def processId = '24-907826' //PacBio Sequel II

        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        List<PacBioSequencingTemplate> templates = processNode.getOutputAnalytes().collect{
            AnalyteFactory.analyteInstance(it) as PacBioSequencingTemplate
        }
        pacBioPool = templates.find{ PacBioSequencingTemplate sequencingTemplate ->
            Analyte library = sequencingTemplate.parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
            library instanceof ClarityLibraryPool
        } as PacBioSequencingTemplate
        pacBioLibrary = templates.find{ PacBioSequencingTemplate sequencingTemplate ->
            Analyte library = sequencingTemplate.parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
            library instanceof ClarityLibraryStock
        } as PacBioSequencingTemplate
    }

    def cleanup() {
        webTransaction?.close()
    }

    void "test getIsClarityLibraryPool template created from pool"() {
        expect:
            pacBioPool
            pacBioLibrary
            pacBioPool.isClarityLibraryPool
            !pacBioLibrary.isClarityLibraryPool
    }
}
