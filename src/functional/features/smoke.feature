Feature: Smoke Test

  Background:
  http://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html

  Scenario: check application health
    When the client requests GET /sp-health
    Then the status is 200
    And the response should be JSON:
    """
  {
  "status": "UP",
  "components": "<some info>"
  }
    """
