import support.Http
import wslite.http.HTTPClientException

import static support.Http.getPruMetadata

this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

client = Http.primeRestClient()


When(~'^the client requests GET /physical-runs/physical-run-units\\?(.+)=(.+)$') { String key, String value ->
    try {
        response = getPruMetadata(client, key, value)
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}