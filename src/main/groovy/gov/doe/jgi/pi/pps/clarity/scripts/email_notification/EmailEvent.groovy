package gov.doe.jgi.pi.pps.clarity.scripts.email_notification

import org.codehaus.jackson.map.ObjectMapper

/**
 * Created by lvishwas on 1/22/2015.
 */
class EmailEvent {
    public static final String ENTITY_TYPE_PROPOSAL = 'Proposal'

    String entity_type = ENTITY_TYPE_PROPOSAL
    Set<Long> entity_id
    String subject
    String from
    String to
    String cc
    String bcc
    String body

    private class Email_event{
        EmailEvent email_event
    }

    String marshallJson(){
        if(!this.entity_id)
            return ''
        Email_event ee = new Email_event(email_event: this)
        ObjectMapper mapper = new ObjectMapper()
        mapper.writeValueAsString(ee)
    }

    @Override
    String toString() {
        def info = """
entity_type     $entity_type
entity_id       $entity_id
subject         $subject
from            $from
to              $to
cc              $cc
bcc             $bcc
body
$body
"""
        return info
    }
}
