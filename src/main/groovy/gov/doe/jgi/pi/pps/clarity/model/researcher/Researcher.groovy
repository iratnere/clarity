package gov.doe.jgi.pi.pps.clarity.model.researcher

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ResearcherNode
import org.grails.web.json.JSONObject

/**
 * Created by duncanscott on 5/1/15.
 */
class Researcher {

    final ResearcherNode researcherNode

    protected Researcher(ResearcherNode researcherNode) {
        this.researcherNode = researcherNode
    }

    boolean getAccountLocked() {
        return researcherNode.accountLocked
    }

    String getLimsId() {
        return researcherNode.id
    }

    String getLastName() {
        researcherNode.lastName
    }

    String getFirstName() {
        researcherNode.firstName
    }

    String getUsername() {
        return researcherNode.username
    }

    String getFullName() {
        return "$firstName $lastName".trim()
    }

    String getLastFirstName() {
        return "$lastName, $firstName".trim()
    }

    Long getContactId() {
        return researcherNode.getUdfAsBigInteger(ClarityUdf.RESEARCHER_CONTACT_ID.udf)?.toLong()
    }

    JSONObject toJson() {
        JSONObject json = new JSONObject()
        json.'contact-id' = contactId
        json.'full-name' = fullName
        json.'first-name' = firstName
        json.'last-name' = lastName
        json.'limsid' = limsId
        json
    }

}
