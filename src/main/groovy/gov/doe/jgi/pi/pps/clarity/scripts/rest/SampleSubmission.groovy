package gov.doe.jgi.pi.pps.clarity.scripts.rest

import grails.converters.JSON

/**
 * Created by tlpaley on 5/3/17.
 * //https://docs.google.com/document/d/1Y3AQwpVP1EmmRuy24NfODvZVfr6JxHBZxw0i5_ZGE98/edit
 */
class SampleSubmission {

    SampleSubmission() {
        registerSubmission()
    }
    def sampleId
    def label
    def barcode
    def plateLocation
    def poolNumber
    String controlType
    def destContainerName
    def destContainerLocation

    static def registerSubmission() {
        JSON.registerObjectMarshaller (SampleSubmission) { SampleSubmission submission ->
            def output = [:]
            output['sample-id'] = submission.sampleId
            output['sample-name'] = "sample${submission.sampleId}"
            output['tube-plate-label'] = submission.label
            output['source-container-barcode'] = submission.barcode
            output['plate-location'] = submission.plateLocation

            output['pool-number'] = submission.poolNumber
            output['control-type'] = submission.controlType
            output['destination-container-name'] = submission.destContainerName
            output['destination-container-location'] = submission.destContainerLocation

            if ('positive'.equalsIgnoreCase(submission.controlType)) {
                output['control-organism-name'] = 'Control Organism Name'
                output['control-organism-tax-id'] = 26900
            }
            output['internal-collaborator-sample-name'] = '74-.OASymb.TypeA2B.Gma1.01.H4.S93_B10_078'
            output['collaborator-concentration-ngul'] = 100
            output['collaborator-volume-ul'] = 100
            output['sample-isolated-from'] = 'stuff'
            output['sample-format'] = 'Aqueous'
            output['storage-solution'] = 'Water'
            output['dnase-treated'] = 'Y'
            output['starting-culture-axenic-strain-pure'] = 'Y'
            output['biosafety-material-category'] = 'Bacteria'
            output['sample-isolation-method'] = 'ethanol extraction'
            output['sample-collection-date'] = '2010-02-27'
            output['latitude-of-sample-collection'] = 23
            output['longitude-of-sample-collection'] = 32
            output['altitude-or-depth-of-sample-collection'] = -23
            output['collection-isolation-site-or-growth-conditions'] = 'optimal temperature 20C'

            output['location-of-sample-collection'] = 'Mexico'
            output['ribosomal-seq-one-type'] = '16S'
            output['ribosomal-seq-one'] = 'GGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGCAACGTGATACGATCGAGAGGTACTTGC'

            return output
        }
    }
    /* WIP ERRORS
Row 1: Estimated genome size is required for samples with this sequencing product.
   Row 1: Collection site or growth conditions is required for Single Cell sequencing product samples.
   Row 1: Latitude is required for Single Cell sequencing product samples.
   Row 1: Longitude is required for Single Cell sequencing product samples.
Row 1: Isolation information is required for Single Cell sequencing product samples.
   Row 1: Altitude or depth is required for Single Cell sequencing product samples.
   Row 1: Control Type is required for Internal Single Cell samples.
   Row 1: Pool Number is required for Internal Single Cell samples.
   Row 1: Destination Container Name is required for Internal Single Cell samples.
   Row 1: Destination Container Location is required for Internal Single Cell samples.
   Row 1: Biosafety material category must be Animal, Archaea, Bacteria, Fungi,Plant, Plasmid, Protist, Synthetic Construct, or Virus
   Row 1: Biosafety material category can't be blank
   Row 1: Pathogenicity can't be blank
   Row 1: Risk group level must indicate either level 1 or level 2.
   Row 1: Collabor volume ul can't be blank
   Row 1: Collabor volume ul is not a number
   Row 1: Collabor concentration ngul can't be blank
   Row 1: Collabor concentration ngul is not a number
   Row 1: Tube plate label can't be blank
   Row 1: Sample container type is not included in the list
   Row 1: Sample format can't be blank
   Row 1: Dnase treated must indicate either Y or N.
   Row 1: Starting culture axenic strain pure must indicate either Y or N.
   Row 1: Purity evidence can't be blank
   Row 1: Sample collection year can't be blank
   Row 1: Sample collection year is not a number
   Row 1: Sample collection month can't be blank
   Row 1: Sample collection month is not included in the list
   Row 1: Sample collection day can't be blank
   Row 1: Sample collection day is not a number
Row 1: Dna isolation method can't be blank
   Row 1: Sample isolated from can't be blank
*/

}
