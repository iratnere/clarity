package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService

import java.util.regex.Matcher
import java.util.regex.Pattern

class Index {
    int datastoreId
    String indexSet
    String indexName
    String indexSequence
    String plateLocation
    int indexSetId

    List<String> getKey(Map handShakeIdToSetName) {
        List<String> keyOptions = handShakeIdToSetName[indexSet]
        List<String> validKeys = []
        if (keyOptions.size() == 1)
            return keyOptions
        for (String option : keyOptions) {
            Index index
            if (!ArtifactIndexService.isIndexPlateBarcode(option)) {
                index = parseTubeIndexBarcode(option)
            } else {
                index = parsePlateIndexBarcode(option)
            }
            if (compare(index))
                validKeys << option
        }
        return validKeys
    }

    static Index parsePlateIndexBarcode(String indexSetName) {
        String masterPlateName = ""
        if(ArtifactIndexService.isContainerBarcode(indexSetName)){
            masterPlateName = indexSetName.split("-")[0]
        }
        return new Index(indexSet: masterPlateName)
    }

    static Index parseTubeIndexBarcode(String indexSetName) {
        String regex = "([0-9]+)([A-Z])([0-9]+)"
        Pattern r = Pattern.compile(regex)
        Matcher m = r.matcher(indexSetName)
        Index index = null
        if (m.find()) {
            index = new Index()
            index.indexSet = m.group(1)
            index.plateLocation = "${m.group(2)}:${m.group(3) as int}"
        }
        return index
    }

    boolean compare(Index index) {
        if (index.indexSet != indexSet)
            return false
        if (index.plateLocation && plateLocation) {
            if (index.plateLocation != plateLocation)
                return false
        }
        return true
    }
}