package gov.doe.jgi.pi.pps.webtransaction.transaction

import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonConverter
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import gov.doe.jgi.pi.pps.webtransaction.exception.MessageResolver
import gov.doe.jgi.pi.pps.webtransaction.grails.WebTransactionService
import gov.doe.jgi.pi.pps.webtransaction.util.ExternalId
import gov.doe.jgi.pi.pps.webtransaction.util.IdGenerator
import grails.core.GrailsApplication
import grails.util.GrailsUtil
import grails.util.Holders
import grails.validation.ValidationErrors
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONElement
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.event.LoggingEvent
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.validation.Errors
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError

/**
 * Created by dscott on 2/2/2016.
 */
class WebTransactionDelegate implements WebTransaction {

    final OnDemandCache<Locale> cachedLocale = new OnDemandCache<>()
    final OnDemandCache<MessageSource> cachedMessageSource = new OnDemandCache<>()
    final OnDemandCache<GrailsApplication> cachedGrailsApplication = new OnDemandCache<>()
    final OnDemandCache<String> cachedLoggerName = new OnDemandCache<>()

    boolean extractJsonSubmssion = true

    JSONElement jsonSubmission
    final JSONObject jsonResponse = new JSONObject()
    final JSONObject logData = new JSONObject()
    final Map<String,String> webTransactionHeaders = [:]

    void generateJsonResponse() {
        WebTransactionUtil.generateResponse(this)
    }

    JSONArray messages = new JSONArray()
    JSONArray errorMessages = new JSONArray()
    final List<LoggingEvent> loggingEvents = []

    private final ConfigObject data = new ConfigObject()

    ConfigObject getData() {
        return data
    }

    final Map<String, WebTransactionChildSession> childSessions = [:]

    private boolean testMode = false
    boolean extractJsonSubmission = true
    boolean closed = false
    boolean opened = false
    boolean loggingEnabled = false

    String transactionId

    ExternalId externalId
    Date startTime
    Date endTime
    Integer statusCode = 500
    String submittedBy
    String action

    WebTransactionRecorder webTransactionRecorder

    GrailsApplication getGrailsApplication() {
        return cachedGrailsApplication.fetch {
            Holders.grailsApplication
        }
    }

    void setGrailsApplication(GrailsApplication grailsApplication) {
        cachedGrailsApplication.forceCache(grailsApplication)
    }

    MessageSource getMessageSource() {
        return cachedMessageSource.fetch {
            grailsApplication.mainContext.messageSource
        }
    }

    void setMessageSource(MessageSource messageSource) {
        cachedMessageSource.forceCache(messageSource)
    }

    Locale getLocale() {
        return cachedLocale.fetch {
            LocaleContextHolder.getLocale()
        }
    }

    void setLocale(Locale locale) {
        cachedLocale.forceCache(locale)
    }

    @Override
    WebTransactionDelegate setTestMode() {
        this.testMode = true
        return this
    }

    @Override
    boolean getTestMode() {
        return testMode
    }

    WebTransactionDelegate() {
        this.transactionId = IdGenerator.nextId()
        this.startTime = new Date()
    }

    WebTransactionDelegate(GrailsApplication grailsApplication) {
        this()
        this.grailsApplication = grailsApplication
    }

    WebTransactionDelegate(Locale locale, MessageSource messageSource, GrailsApplication grailsApplication) {
        this()
        this.locale = locale
        this.messageSource = messageSource
        this.grailsApplication = grailsApplication
    }

    WebTransactionDelegate(JSONElement jsonSubmission) {
        this()
        this.jsonSubmission = jsonSubmission
        this.extractJsonSubmssion = false
    }

    WebTransactionDelegate(Locale locale, MessageSource messageSource, JSONElement jsonSubmission) {
        this()
        this.locale = locale
        this.messageSource = messageSource
        this.jsonSubmission = jsonSubmission
        this.extractJsonSubmssion = false
    }

    WebTransaction addChildSession(String key, WebTransactionChildSession childSession) {
        Object existingSession = childSessions[key]
        if (!existingSession) {
            synchronized(childSessions) {
                existingSession = childSessions[key]
                if (!existingSession) {
                    childSessions[key] = childSession
                    transactionLogger.debug "${this}: added child session ${childSession} under key \"${key}\""
                }
            }
        }
        if (existingSession) {
            throw new WebException([code:'webTransaction.childSessionKey.notUnique', args:[key]], 500)
        }
        return this
    }

    @Override
    WebTransactionChildSession getChildSession(String key) {
        childSessions[key]
    }

    WebTransactionChildSession requireChildSession(String key) {
        WebTransactionChildSession child = getChildSession(key)
        if (child == null) {
            throw new WebException([code:'webTransaction.noChildSession', args:[key]], 500)
        }
        return child
    }

    @Override
    WebTransactionChildSession removeChildSession(String key) {
        childSessions.remove(key)
    }

    JSONArray getLoggerMessages() {
        JSONArray events = new JSONArray()
        loggingEvents.each { LoggingEvent event ->
            events << JsonConverter.loggingEventToJson(event)
        }
        return events
    }

    @Override
    void addLoggingEvent(LoggingEvent loggingEvent) {
        if (!closed) {
            loggingEvents.add(loggingEvent)
        }
    }

    void exit() {
        throw new WebTransactionExit(this.statusCode)
    }

    void exit(Integer statusCode) {
        this.statusCode = statusCode
        exit()
    }

    void exitWithErrorMessage(Object message, Integer statusCode = null) {
        if (statusCode) {
            this.statusCode = statusCode
        }
        addErrorMessage(message)
        exit()
    }

    String getLoggerName() {
        return cachedLoggerName.fetch() {
            StringBuilder sb = new StringBuilder(this.class.name)
            sb << '.'
            sb << transactionId
            if (externalId) {
                sb << '.'
                sb << "${externalId}"
            }
            return sb.toString()
        }
    }

    Logger getTransactionLogger() {
        String lName = loggerName
        return LoggerFactory.getLogger((String) lName)
    }

    void addErrorMessage(Object message, String defaultMessage = null) {
        JSONObject localeMessage  = MessageResolver.resolveLocaleMessageJson(messageSource, locale, message, defaultMessage)
        if (localeMessage) {
            errorMessages << localeMessage
            transactionLogger.error "${localeMessage.message}"
        }
    }

    void addErrorMessages(Iterable messages) {
        messages?.each{
            addErrorMessage(it)
        }
    }

    void addExceptionErrorMessage(Throwable t, Map msgMap = null) {
        if (t instanceof ValidationErrors) {
            addValidationErrors(t, msgMap)
        } else {
            MessageResolver.resolveExceptionMessagesJson(messageSource, locale, t)?.each { JSONObject errorMessage ->
                msgMap?.each{ key,value ->
                    errorMessage.put(key?.toString(), value?.toString())
                }
                errorMessages << errorMessage
                transactionLogger.error "${errorMessage.message}", GrailsUtil.sanitizeRootCause(t)
            }
        }
    }

    void addExceptionErrorMessage(Throwable t, Map msgMap, Integer statusCode) {
        addExceptionErrorMessage(t,msgMap)
        this.statusCode = statusCode
    }

    void addValidationErrors(ValidationErrors errors, Map msgMap = null) {
        if (errors) {
            transactionLogger.error "${errors}"
        }
        errors?.allErrors?.each() { ObjectError objectError ->
            String code = objectError.code
            List<String> args = objectError.arguments*.toString()

            String key = null
            String entity = null
            String index = null

            if (msgMap) {
                key = msgMap['key']?.toString()
                entity = msgMap['entity']?.toString()
                index = msgMap['index']?.toString()
            }

            if (!key) {
                if (objectError instanceof FieldError) {
                    key = ((FieldError) objectError).field
                }
            }
            if (!entity) {
                entity = objectError.objectName
            }
            JSONObject localeMessage  = MessageResolver.resolveLocaleMessageJson(messageSource, locale, [code:code, args:args, key:key, entity:entity, index:index])
            if (localeMessage) {
                errorMessages << localeMessage
            }
        }
    }

    void addErrors(Errors errors, Map msgMap = null){
        if (errors) {
            transactionLogger.error "${errors}"
        }
        errors?.allErrors?.each() { ObjectError objectError ->
            String code = objectError.code
            List<String> args = objectError.arguments*.toString()

            String key = null
            String entity = null
            String index = null

            if (msgMap) {
                key = msgMap['key']?.toString()
                entity = msgMap['entity']?.toString()
                index = msgMap['index']?.toString()
            }

            if (!key) {
                if (objectError instanceof FieldError) {
                    key = ((FieldError) objectError).field
                }
            }
            if (!entity) {
                entity = objectError.objectName
            }
            JSONObject localeMessage = MessageResolver.resolveLocaleMessageJson(messageSource, locale, [code:code, args:args, key:key, entity:entity, index:index])
            if (localeMessage) {
                errorMessages << localeMessage
            }
        }
    }

    String getStartTimeString() {
        //startTime?.format(dateFormatString)//format is not supported by groovy v.2.5*
        startTime
    }

    String getEndTimeString() {
        //endTime?.format(dateFormatString)
        endTime
    }

    WebTransaction performTransaction(Closure action) {
        performTransaction(null, action)
    }

    WebTransaction performTransaction(WebTransactionRecorder webTransactionRecorder, Closure action) {
        try {
            WebTransactionService webTransactionService = (WebTransactionService) requireApplicationBean(WebTransactionService)
            webTransactionService.performTransaction(this, webTransactionRecorder, action)
        } finally {
            this.close()
            transactionLogger.debug "${this}: closed with status code ${statusCode}\n${jsonResponse?.toString(2)}"
        }
        return this
    }

    Object getApplicationBean(Class beanClass) {
        if (!grailsApplication) {
            exitWithErrorMessage([code:'grailsApplication.notInjected'], 500)
        }
        String beanName = beanClass.simpleName.replaceFirst('.',{it.toLowerCase()})
        return grailsApplication.mainContext.getBean(beanName)
    }

    Object requireApplicationBean(Class beanClass) {
        Object applicationBean = getApplicationBean(beanClass)
        if (!applicationBean) {
            exitWithErrorMessage([code:'applicationBean.notFound',args:[beanClass?.simpleName]], 500)
        }
        return applicationBean
    }

    @Override
    synchronized boolean getOpened() {
        return this.opened
    }

    @Override
    synchronized void open() {
        if (!getOpened()) {
            transactionLogger.debug "${this} opening"
            this.opened = true
            WebTransactionUtil.setCurrentTransaction(this)
        }
    }

    synchronized void close() {
        if(!closed) {
            WebTransactionUtil.close(this)
            closed = true
        }
    }

    void finalize() throws Throwable {
        transactionLogger.debug "${this} finalizing"
        close()
    }

    String toString() {
        return "${WebTransaction.class.simpleName}:${transactionId}${externalId?':'+externalId.toString():''}"
    }

}
