package gov.doe.jgi.pi.pps.clarity.scripts.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

abstract class FractionalLanePooling implements Pooling{
    int startPoolNumber
    List<LibraryInformation> members
    List<Analyte> sagPools

    FractionalLanePooling(List<LibraryInformation> members){
        this.members = members
        sagPools = members.findAll { it.isSAGPool }
    }

    @Override
    void makePools() {
        ClarityWebTransaction.logger.debug("Starting PoolingNovaSeq makePools")
        def poolNumberToSum = [:]
        members.sort{-it.libraryPercentage}.eachWithIndex{ LibraryInformation candidateBean, index ->
            if (index == 0) {
                candidateBean.poolNumber = startPoolNumber
                poolNumberToSum[startPoolNumber] = candidateBean.libraryPercentage as BigDecimal
            }
            else {
                int candidateNumber = findPoolNumber(members, poolNumberToSum, candidateBean) as int
                candidateBean.poolNumber = candidateNumber
                poolNumberToSum[candidateNumber] = getSum(members, candidateNumber)
            }
        }
        startPoolNumber = members.collect{ it.poolNumber }.max() + 1
        ClarityWebTransaction.logger.debug("PoolingNovaSeq makePools completed: nextPoolNumber $startPoolNumber")
    }

    static def findPoolNumber(List<LibraryInformation> members, def poolNumberToSum, LibraryInformation candidateBean) {
        BigInteger pool = poolNumberToSum.find{number,sum ->
            isPoolMember(members, number, candidateBean)
        }?.key
        if (pool != null) {
            return pool
        }
        return (poolNumberToSum.keySet().max() + 1)
    }

    static boolean isPoolMember(List<LibraryInformation> members, def poolNumber, LibraryInformation candidateBean) {
        BigDecimal threshold = members[0].threshold ? members[0].threshold : Pooling.DEFAULT_THRESHOLD
        BigDecimal libraryPercentage = candidateBean.libraryPercentage as BigDecimal
        return ((getSum(members, poolNumber) + libraryPercentage) <= threshold
                && isDistinctIndex(members, poolNumber, candidateBean.indexSequences)
                    && !has2SagPools(members, poolNumber, candidateBean)
        )
    }

    static BigDecimal getSum(List<LibraryInformation> members, def poolNumber) {
        def sum = members.findAll{ it.poolNumber == poolNumber }*.libraryPercentage.sum()
        return sum ? sum : 0
    }

    static boolean isDistinctIndex(List<LibraryInformation> members, def poolNumber, def indexSequences) {
        def indexes = []
        members.findAll { it.poolNumber == poolNumber }.each { LibraryInformation member ->
            member.indexSequences.each { String sequence ->
                indexes.addAll(ArtifactIndexService.splitSequences(sequence))
            }
        }
        return !(indexSequences.intersect(indexes))
    }

    static boolean has2SagPools(List<LibraryInformation> members, int poolNumber, LibraryInformation candidateBean) {
        //PPS-5253 - 2 SAG Pools should not be pooled together while pooling Internal single cell pools with NovaSeq
        if(!candidateBean.isSAGPool)
            return false
        List<LibraryInformation> existingSagPools = members.findAll { it.poolNumber == poolNumber && it.isSAGPool }
        if(existingSagPools)
            return true
        return false
    }



    @Override
    int completePoolPrepBeans(int poolNumber) {
        startPoolNumber = poolNumber
        makePools()
        return startPoolNumber
    }

    Map<String, List<LibraryInformation>> getIndexMembersMap(){
        Map<String, List<LibraryInformation>> indexMembers = [:].withDefault {[]}
        members.each{ LibraryInformation li ->
            def indexToCompare = getIndexToCompare(li)
            if(indexToCompare instanceof String)
                indexToCompare = [indexToCompare]
            indexToCompare.each { String index ->
                ArtifactIndexService.splitSequences(index).each {
                    indexMembers[it] << li
                }
            }
        }
        return indexMembers
    }
}