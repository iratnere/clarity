package gov.doe.jgi.pi.pps.clarity.cv

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive

/*
HiSeq
HiSeq-1TB
HiSeq-HO
HiSeq-Rapid
HiSeq2500
MiSeq
MinION
NextSeq-HO
NextSeq-MO
NovaSeq
PromethION
Sequel
Sequel II
X10
 */

enum SequencerModelTypeCv {
	HISEQ('HiSeq', PlatformTypeCv.ILLUMINA),
//	HISEQ_1TB('HiSeq-1TB', PlatformTypeCv.ILLUMINA), //PPS-5150: inactivate discontinued run modes
	HISEQ_HO('HiSeq-HO', PlatformTypeCv.ILLUMINA),
	HISEQ_RAPID('HiSeq-Rapid', PlatformTypeCv.ILLUMINA),
	HISEQ_2500('HiSeq2500', PlatformTypeCv.ILLUMINA),
	MISEQ('MiSeq', PlatformTypeCv.ILLUMINA),
	MINION('MinION', PlatformTypeCv.OXFORD),
	NEXTSEQ_HO('NextSeq-HO', PlatformTypeCv.ILLUMINA),
	NEXTSEQ_MO('NextSeq-MO', PlatformTypeCv.ILLUMINA),
	NOVASEQ('NovaSeq', PlatformTypeCv.ILLUMINA), //will be deprecated
	NOVASEQ_S4('NovaSeq S4', PlatformTypeCv.ILLUMINA),
	NOVASEQ_SP('NovaSeq SP', PlatformTypeCv.ILLUMINA),
	NOVASEQ_S2('NovaSeq S2', PlatformTypeCv.ILLUMINA),
	PROMETHION('PromethION', PlatformTypeCv.OXFORD),
	SEQUEL('Sequel', PlatformTypeCv.PACBIO),
	SEQUEL_II('Sequel II', PlatformTypeCv.PACBIO),
	X10('X10', PlatformTypeCv.ILLUMINA)

	final String value
	final PlatformTypeCv platformTypeCv

	SequencerModelTypeCv(String value, PlatformTypeCv platform) {
		this.value = value
		this.platformTypeCv = platform
	}

	String getValue() {
		return this.value
	}
	
	String toString() {
		return value
	}
	
	private static final EnumConverterCaseInsensitive<SequencerModelTypeCv> converter = new EnumConverterCaseInsensitive(SequencerModelTypeCv)
	
	static SequencerModelTypeCv toEnum(value) {
		return converter.toEnum(value)
	}

}
