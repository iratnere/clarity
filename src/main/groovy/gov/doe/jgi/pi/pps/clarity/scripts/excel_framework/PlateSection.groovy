package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference

import java.util.logging.Logger

/**
 * This section corresponds to plate section in the excel file
 * One has to specify the data type the plate section shall hold while creating the instance. Also populate the Map
 * The Map takes plate location (Example: 'A2')  string as key.
 * store: Converts plate location to cell address and prints the value to that cell
 * load: Reads each cell of the plate section and populates the map by calculating plate location from cell address
 * 
 * @author lvishwas
 */
class PlateSection extends Section {
	private Map<String, Object> values
	private CellTypeEnum cellType
	CellStyle wellCellStyle = null, cornerCellStyle = null
    CellStyle redCellStyleCached
    List<String> failedQcPlateLocations = []
    int rowsTotal = 8
    int colTotal = 12

    static final Logger logger = Logger.getLogger(PlateSection.class.name)

	PlateSection(int parentSheetIndex, CellTypeEnum dataType, String startAddress, String endAddress = null, boolean isPlate384 = false){
		super(parentSheetIndex,null,startAddress,endAddress)
		cellType = dataType
        if (isPlate384) {
            rowsTotal = 16
            colTotal = 24
        }
        calculateLastCell()
	}
	
	PlateSection(int parentSheetIndex, CellTypeEnum dataType, CellRangeAddress sectionRangeAddress){
		super(parentSheetIndex,null,sectionRangeAddress)
		cellType = dataType
		calculateLastCell()
	}
	
	PlateSection(PlateSection srcSection, int parentSheetIndex, CellTypeEnum dataType, String startAddress){
		super(srcSection,parentSheetIndex,null,startAddress)
		cellType = dataType
		calculateLastCell()
	}

    void addDataValues(Map<String, Object> data){
        values = data
    }

	boolean isPlateWell(Cell cell){
		String cellReference = calculatePlateLocation(cell.rowIndex, cell.columnIndex)
		if(cellReference)
			return true
		return false
	}

	boolean isCornerCell(Cell cell){
		String cellReference = calculatePlateLocation(cell.rowIndex, cell.columnIndex)
		if(cellReference in ['A1', 'A12', 'H1', 'H12']){
			return true
		}
		return false
	}

	CellStyle getCellStyle(Cell cell){
		if(isPlateWell(cell)) {
			if (isCornerCell(cell)) {
				if (!cornerCellStyle)
					cornerCellStyle = workbook.createCellStyle()
				return cornerCellStyle
			} else {
				if(!wellCellStyle)
					wellCellStyle = workbook.createCellStyle()
				return wellCellStyle
			}
		}
		return workbook.createCellStyle()
	}
	
	private CellReference calculateCellAddress(String plateLocation){
		CellReference plateAddress = new CellReference(plateLocation)
		int targetRow = startCell.row + 1 + plateAddress.col
		int targetCol = startCell.col + 1 + plateAddress.row
		CellRangeAddress validRange = new CellRangeAddress(startCell.row, lastCell.row, startCell.col,  lastCell.col)
		if(validRange.isInRange(targetRow, targetCol))
			return new CellReference(targetRow,targetCol)
		else
			throw new IllegalArgumentException("Plate location ${plateLocation} not within valid range")
	}

    private String calculatePlateLocation(int rowIndex, int colIndex){
        int plateCol = rowIndex - startCell.row - 1
        int plateRow = colIndex - startCell.col - 1
        if(plateRow < 0 || plateCol < 0){
            return null
        }
        CellReference plateAddress = new CellReference(plateRow, plateCol)
        return plateAddress.formatAsString()
    }

    void calculateLastCell(){
        lastCell = new CellReference(startCell.row+rowsTotal, startCell.col+colTotal)
    }

    void setData(def data){
        if(!data instanceof Map)
            throw new IllegalArgumentException("Expecting Map of plate location to value")
        values = data
    }
	
	@Override
	void store() {
		logger.fine "Writing section: ${this.sectionKey}"
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		calculateLastCell()
		values?.each{ String plateLocation, Object value ->
			CellReference targetCellAddress = calculateCellAddress(plateLocation)
			Row row = sheet.getRow(targetCellAddress.row)?sheet.getRow(targetCellAddress.row):sheet.createRow(targetCellAddress.row)
			Cell cell = row.getCell(targetCellAddress.col)?row.getCell(targetCellAddress.col):row.createCell(targetCellAddress.col)
            if (plateLocation in failedQcPlateLocations) {
                cell.setCellStyle(redCellStyle)
            }
			writeCell(cell, cellType, value)
		}
	}

    CellStyle getRedCellStyle() {
        if (!redCellStyleCached) {
            redCellStyleCached = workbook.createCellStyle()
            redCellStyleCached.setFillPattern(FillPatternType.SOLID_FOREGROUND)
            redCellStyleCached.setFillForegroundColor(Font.COLOR_RED)
            return redCellStyleCached
        }
        return redCellStyleCached
    }

    @Override
	void load(boolean validate=true) {
		logger.fine "Reading section: ${this.sectionKey}"
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		int currentRow = startCell.row + 1
		calculateLastCell()
		values = [:]
		while(currentRow <= lastCell.row) {
            Row row = sheet.getRow(currentRow)
            int currentColumn = startCell.col + 1
            while (currentColumn <= lastCell.col){
                Cell cell = row?.getCell(currentColumn)
                Object value = cell ? readCell(cell, cellType) : null
                if (value) {
                    values[calculatePlateLocation(currentRow, currentColumn)] = value
                }
                currentColumn++
            }
			currentRow++
		}
	}
	
	String getSectionConfig(String sectionDelim, String fieldDelim){
		StringBuilder sb = new StringBuilder()
		sb.append(sectionDelim)
		sb.append(this.class.name).append(fieldDelim)
		sb.append(fieldDelim).append(this.cellType.name()).append(fieldDelim)
		sb.append(parentSheetIndex).append(fieldDelim)
		sb.append(startCell.formatAsString()).append(fieldDelim)
		if(lastCell)sb.append(lastCell.formatAsString())
		sb.append(fieldDelim)
		return sb.toString()
	}
	
	def getData(){
		return values
	}

	String toString(){
		StringBuilder sb = new StringBuilder()
		sb.append("Plate Section: ").append(sectionKey).append("\n")
		getData().each{ String location, Object value ->
			sb.append(location).append(":").append(value.toString()).append("\n")
		}
		return sb.toString()
	}

	CellReference getCellAddress(String plateLocation){
		if(sourceSection)
			return calculateCellAddressFromSourceSection(plateLocation)
		return calculateCellAddress(plateLocation)
	}

}
