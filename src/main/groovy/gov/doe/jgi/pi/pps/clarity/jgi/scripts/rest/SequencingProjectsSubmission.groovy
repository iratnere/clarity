package gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest

import grails.converters.JSON

/**
 * Created by tlpaley on 5/3/17.
 */
class SequencingProjectsSubmission {

    SequencingProjectsSubmission() {
        registerSubmission()
    }
    def submittedBy
    List<SequencingProjectSubmission> sequencingProjects = []

    static def registerSubmission() {
        JSON.registerObjectMarshaller (SequencingProjectsSubmission) { SequencingProjectsSubmission submission ->
            def output = [:]
            output['submitted-by-cid'] = submission.submittedBy
            output['sequencing-projects'] = submission.sequencingProjects

            return output
        }
    }
}
