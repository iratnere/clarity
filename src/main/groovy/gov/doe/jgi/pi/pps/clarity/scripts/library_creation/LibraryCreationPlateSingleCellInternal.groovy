package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.CsvBuilder
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.FileUploadUtil
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcEchoTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcKeyValueResultsBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateSingleCellTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.SampleIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import org.apache.poi.hssf.usermodel.DVConstraint
import org.apache.poi.hssf.usermodel.HSSFDataValidation
import org.apache.poi.ss.usermodel.DataValidation
import org.apache.poi.ss.usermodel.DataValidationConstraint
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.util.CellRangeAddressList

import java.lang.reflect.Field

/**
 * Created by tlpaley on 4/1/15.
 */
class LibraryCreationPlateSingleCellInternal extends LibraryCreation {

    final static String TEMPLATE_NAME = 'resources/templates/excel/LcPlateSingleCell'
    private final static String TABLE_CLASS_NAME = LcPlateSingleCellTableBean.class.simpleName
    private final static  BigDecimal DEFAULT_LIBRARY_VOLUME_UL = 0.5
    private final static  BigInteger DEFAULT_LIBRARY_VOLUME_NL = (DEFAULT_LIBRARY_VOLUME_UL * 1000) as BigInteger
    //final static String DEFAULT_PROCESS_PRINTER = 'ECHO_BCode8'

    Map<String, List<ClarityLibraryStock>> containerToAnalytesCached = [:]

    LibraryCreationPlateSingleCellInternal(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputPlate(process)
    }

    @Override
    void updateInputsAndSamples() {}

    @Override
    Stage getDefaultStage() {
        Stage.POOL_CREATION
    }

    @Override
    String getLcTableBeanClassName() {
        TABLE_CLASS_NAME
    }

    @Override
    void updateLibraryStockUdfs() {
        Map<String, List<ClarityLibraryStock>> containerToAnalytes = getContainerToAnalytesMap()
        containerToAnalytesMap.keySet().each { containerLimsId ->
            process.sheetIndex = process.excelWorkbook.getSheetIndex(containerLimsId)
            List<Analyte> outputAnalytes = containerToAnalytes[containerLimsId]
            transferBeansDataToClarityLibraryStocks(outputAnalytes)
            output.updateLibraryIndexUdf(outputAnalytes)
        }
        process.updateNonWorksheetUdfs()
    }

    void transferBeansDataToClarityLibraryStocks(List<ClarityLibraryStock> outputAnalytes) {
        outputAnalytes[0].containerUdfNotes = process.resultsBean.lcPlateNotes
        outputAnalytes[0].containerUdfLibraryQcResult = process.resultsBean.plateResult?.value
        outputAnalytes[0].containerUdfLibraryQcFailureMode = process.resultsBean.failureMode?.value
        String indexSet = process.resultsBean.indexContainerBarcode
        outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcPlateSingleCellTableBean lcPlateTableBean = (LcPlateSingleCellTableBean) process.getLcTableBean(clarityLibraryStock.id)
            clarityLibraryStock.udfConcentrationNgUl = lcPlateTableBean.libraryConcentration
            clarityLibraryStock.udfActualTemplateSizeBp = lcPlateTableBean.libraryTemplateSize
            clarityLibraryStock.udfVolumeUl = lcPlateTableBean.libraryVolume
            clarityLibraryStock.udfIndexContainerBarcode = indexSet
            clarityLibraryStock.udfIndexName = lcPlateTableBean.libraryIndexName
            clarityLibraryStock.udfLibraryMolarityQcPm = convertLibraryMolarity(lcPlateTableBean.libraryMolarityNm)
            clarityLibraryStock.udfNumberPcrCycles = process.resultsBean.numberPcrCycles
            clarityLibraryStock.udfNotes = lcPlateTableBean.lcNotes
            clarityLibraryStock.udfLibraryQcResult = lcPlateTableBean.libraryQcResult?.value
            clarityLibraryStock.systemQcFlag = lcPlateTableBean.libraryQcResult?.value == Analyte.PASS
            clarityLibraryStock.udfLibraryQcFailureMode = lcPlateTableBean.libraryFailureMode?.value
            //set qPCR molarity
            clarityLibraryStock.udfLibraryMolarityPm = convertLibraryMolarity(lcPlateTableBean.libraryMolarityNm)
        }
        process.resultsBeanCached = null
        process.libraryStockBeansCached = null
    }

    static BigDecimal convertLibraryMolarity(BigDecimal libraryMolarityNm){
        return (libraryMolarityNm != null ? (libraryMolarityNm * 1000) : null)
    }

    Map<String, List<ClarityLibraryStock>> getContainerToAnalytesMap() {
        if (containerToAnalytesCached) {
            return containerToAnalytesCached
        }
        process.outputAnalytes.each {
            String containerLimsId = it.containerId
            if (!containerToAnalytesCached[it.containerId])
                containerToAnalytesCached[containerLimsId] = []
            containerToAnalytesCached[containerLimsId] << (ClarityLibraryStock) it
        }
        containerToAnalytesCached
    }

    ExcelWorkbook populateLibraryCreationSheet(String templateName){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(templateName, process.testMode)
        Map<String, List<ClarityLibraryStock>> containerToAnalytes = getContainerToAnalytesMap()

        KeyValueSection sourceKvResultSection = (KeyValueSection) getKeyValueResultSection()
        sourceKvResultSection.workbook = excelWorkbook

        //Read style from first 2 table's rows
        OneDSection sourceHeader = new OneDSection(0, CellTypeEnum.STRING, 'A26', 'K26')
        sourceHeader.workbook = excelWorkbook
        OneDSection sourceFirstRow = new OneDSection(0, CellTypeEnum.STRING, 'A27', 'K27')
        sourceFirstRow.workbook = excelWorkbook

        OneDSection sourceColorColumn = new OneDSection(0, CellTypeEnum.STRING, 'H2', 'H4')
        sourceColorColumn.workbook = excelWorkbook
        OneDSection sourceColorInfoColumn = new OneDSection(0, CellTypeEnum.STRING, 'I2', 'I4')
        sourceColorInfoColumn.workbook = excelWorkbook

        OneDSection sourceIndexSet = new OneDSection(0, CellTypeEnum.FORMULA, 'D27', 'D410')
        sourceIndexSet.workbook = excelWorkbook

        containerToAnalytes.keySet().sort().each { containerLimsId ->
            List<ClarityLibraryStock> outputAnalytes = containerToAnalytes[containerLimsId]
            List<Analyte> inputAnalytes = outputAnalytes.collect{it.parentAnalyte}

            List<Section> sheetSections = []
            excelWorkbook.addSheet(containerLimsId)
            def sheetIndex = excelWorkbook.getSheetIndex(containerLimsId)

            KeyValueSection kvResultSection = new KeyValueSection(sourceKvResultSection, sheetIndex, LcKeyValueResultsBean.class, 'A2')
            LcKeyValueResultsBean keyValueBean = populateLcKeyValueResultsBean(inputAnalytes, outputAnalytes)
            kvResultSection.setData(keyValueBean)
            sheetSections << kvResultSection

            //Apply style from first table's row
            OneDSection header = new OneDSection(sourceHeader, sheetIndex, CellTypeEnum.STRING, 'A26')
            sheetSections << header

            OneDSection color = new OneDSection(sourceColorColumn, sheetIndex, CellTypeEnum.STRING, 'H2')
            sheetSections << color
            OneDSection colorInfo = new OneDSection(sourceColorInfoColumn, sheetIndex, CellTypeEnum.STRING, 'I2')
            sheetSections << colorInfo

            TableSection tableSection = new TableSection(sheetIndex, LcPlateSingleCellTableBean.class, 'A26', 'K122')
            tableSection.setData(populateWells(outputAnalytes))
            sheetSections << tableSection
            // Copy Indexes Formulas column
            OneDSection indexSet = new OneDSection(sourceIndexSet, sheetIndex, CellTypeEnum.FORMULA, 'D27')
            sheetSections << indexSet

            excelWorkbook.addSections(sheetSections)
            updateWorksheetStyle(excelWorkbook.getSheet(containerLimsId))
        }
        updateWorkbookStyle(excelWorkbook)
        return excelWorkbook
    }

    def getSelectedTabName() {
        return containerToAnalytesMap.keySet().sort().first()
    }

    static LcPlateSingleCellTableBean createLcTableBean(ClarityLibraryStock clarityLibraryStock, String well) {
        LibraryStockFailureModesService libraryStockFailureModesService = ClarityWebTransaction.requireCurrentTransaction()
                .requireApplicationBean(LibraryStockFailureModesService) as LibraryStockFailureModesService
        LcPlateSingleCellTableBean bean = new LcPlateSingleCellTableBean()
        bean.well = well
        bean.libraryQcResult = libraryStockFailureModesService.dropDownPassFail
        bean.libraryFailureMode = libraryStockFailureModesService.dropDownFailureModes
        if (clarityLibraryStock) {
            bean.libraryLimsId = clarityLibraryStock.id
            bean.libraryName = clarityLibraryStock.name
            bean.libraryVolume = DEFAULT_LIBRARY_VOLUME_UL
        }
        return bean
    }

    /**
     * A Function to get 384 well from Fragment Analyzer 96 well.
     * left upper corner of the 384 quadrant
     * Library plate wells A1, C1, E1...O21  go into FA plate wells A1, B1, C1... H11
     * @return      FA well converted to the 384 well as String
     */
    static String convertFragmentAnalyzerWell(def row, Integer column) {
        def convertedRow = (2*(row.hashCode())-65) as Character
        def convertedColumn = 2*column-1
        return "$convertedRow$convertedColumn"
    }

    /**
     * A Function to get 96 index well from the 384 well.
     * left upper corner of the 384 quadrant
     * Library plate 384 wells A1, C1, E1...O21 go into 96 index plate wells A:1, B:1, C:1... H:11
     * @return      FA well converted to the 384 well as String
     */
    static String convert384Well(String row, Integer column) {
        def convertedRow = ((row.hashCode() + 65) / 2) as Character
        def convertedColumn = (column + 1) / 2
        return "$convertedRow:$convertedColumn"
    }

    static List<LcPlateSingleCellTableBean> populateWells(List<Analyte> outputAnalytes) {
        List<LcPlateSingleCellTableBean> tableBeansArray = []
        (1..12).each { column ->
            ('A'..'H').each { row ->
                def well = convertFragmentAnalyzerWell(row, column)
                ClarityLibraryStock clarityLibraryStock = (ClarityLibraryStock) outputAnalytes.find {
                    it.artifactNode.location?.replaceAll(':', '') == well
                }
                tableBeansArray << createLcTableBean(clarityLibraryStock, well)
            }
        }
        return tableBeansArray
    }

    static Section getKeyValueResultSection() {
        Section kvSection = new KeyValueSection(0,LcKeyValueResultsBean.class, 'A2','B20')
        return kvSection
    }

    static Section getTableSection(){
        Section tableSection = new TableSection(0, LcPlateSingleCellTableBean.class, 'A26', 'K27')
        return tableSection
    }

    LcKeyValueResultsBean populateLcKeyValueResultsBean(List<Analyte> inputAnalytes, List<Analyte> outputAnalytes) {
        LibraryStockFailureModesService libraryStockFailureModesService = ClarityWebTransaction.requireCurrentTransaction()
                .requireApplicationBean(LibraryStockFailureModesService) as LibraryStockFailureModesService
        Analyte inputAnalyte = inputAnalytes[0]
        ScheduledSample claritySample = (ScheduledSample)inputAnalyte.claritySample
        LcKeyValueResultsBean bean = new LcKeyValueResultsBean()
        bean.materialCategory = claritySample.sequencingProject.udfMaterialCategory
        bean.inputPlateBarcode = inputAnalytes.collect{it.containerNode.name}?.unique()?.join(',')
        bean.outputPlateBarcode = outputAnalytes[0].containerNode.name
        bean.plateName = outputAnalytes.collect{it.getContainerUdfLabel()}?.unique()?.join(',')
        bean.numberSamples = inputAnalytes.size()
        bean.libraryCreationQueue = inputAnalyte.libraryCreationQueue?.libraryCreationQueue
        bean.lcInstructions = claritySample.udfLcInstructions
        bean.smNotes = claritySample.udfNotes
        bean.claimant = process.researcher?.fullName
        //bean.targetTemplateSize = claritySample.udfTargetTemplateSizeBp
        bean.freezerPath = process.getFreezerPath()
        //bean.targetAliquotMass = inputAnalyte.containerUdfFinalAliquotMassNg
        //bean.targetAliquotVolume = inputAnalyte.containerUdfFinalAliquotVolumeUl
        bean.failureMode = libraryStockFailureModesService.dropDownFailureModes
        bean.plateResult = libraryStockFailureModesService.dropDownPassFail
        bean.libraryIndexSet = process.dropDownIndexSet
        bean
    }

    static void updateWorksheetStyle(Sheet destSheet) {
        CellRangeAddressList addressList = new CellRangeAddressList(26, 409, 4, 7)
        DVConstraint dvConstraint = DVConstraint.createNumericConstraint(
                DataValidationConstraint.ValidationType.DECIMAL,
                DataValidationConstraint.OperatorType.GREATER_OR_EQUAL,
                "0",
                null
        )
        DataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint)
        destSheet.addValidationData(dataValidation)

        destSheet.setColumnWidth(0, 6098)
        destSheet.setColumnWidth(1, 6098)
        destSheet.setColumnWidth(10, 6098)
        (0..10).each { indexColumn ->
            if (indexColumn in [0,1,10])
                destSheet.setColumnWidth(indexColumn, 6098)
            else
                destSheet.setColumnWidth(indexColumn, 4098)
        }
    }

    void updateWorkbookStyle(ExcelWorkbook excelWorkbook) {
        def sheetIndex = excelWorkbook.getSheetIndex(selectedTabName)
        excelWorkbook.workbook.setActiveSheet(sheetIndex)
        excelWorkbook.workbook.setSelectedTab(sheetIndex)
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        return populateLibraryCreationSheet(TEMPLATE_NAME)
    }

    @Override
    Section getTableSectionLibraries(){
        return null
    }

    @Override
    void moveToNextWorkflow() {
        if (invalidLibraryStock) {
            process.postErrorMessage("Library Stock $invalidLibraryStock.id: invalid analyte and/or container 'Library QC Result' udf")
        }
        process.routeArtifactNodes(process.lcAdapter.defaultStage, passedLibraryStocks*.artifactNode)
        process.routeArtifactNodes(Stage.ABANDON_QUEUE, abandonLibraryStocks*.artifactNode)
    }

    Analyte getInvalidLibraryStock() {
        return process.outputAnalytes.find{
            ClarityLibraryStock ls = it as ClarityLibraryStock
            !ls.validQcResult
        }
    }

    List<Analyte> getPassedLibraryStocks() {
        return process.outputAnalytes.findAll{
            ClarityLibraryStock ls = it as ClarityLibraryStock
            ls.containerUdfLibraryQcResult == Analyte.PASS && ls.udfLibraryQcResult == Analyte.PASS
        }
    }

    List<Analyte> getAbandonLibraryStocks() {
        return process.outputAnalytes.findAll {
            ClarityLibraryStock ls = it as ClarityLibraryStock
            ls.containerUdfLibraryQcResult == Analyte.FAIL ||
                    (ls.containerUdfLibraryQcResult == Analyte.PASS && ls.udfLibraryQcResult == Analyte.FAIL)
        }
    }

    @Override
    void updateLcaUdfs() {
        def libraryQcResult = process.outputAnalytes[0].containerUdfLibraryQcResult
        process.outputAnalytes.each{
            ClarityLibraryStock output = it as ClarityLibraryStock
            ScheduledSample scheduledSample = (ScheduledSample)output.claritySample
            scheduledSample.incrementUdfLcAttempt()
            if (libraryQcResult == Analyte.FAIL) {
                scheduledSample.incrementUdfLcFailedAttempt()
            }
        }
    }

    @Override
    void automaticPlacement() {
        def destContainerNameToContainerIdMap = createDestContainers()
        OutputPlacements outputPlacements = new OutputPlacements()
        // For destination containers, iterate through its corresponding rows and add their placement to the placement node
        process.outputAnalytes.each {
            ClarityLibraryStock outputAnalyte = it as ClarityLibraryStock
            String containerId = destContainerNameToContainerIdMap[(outputAnalyte.claritySample as ScheduledSample).udfDestContainerName]
            ContainerLocation containerLocation = new ContainerLocation(
                    containerId,
                    convertToContainerNodeLocation((outputAnalyte.claritySample as ScheduledSample).udfDestContainerLocation)
            )
            outputPlacements << new OutputPlacement(artifactId: outputAnalyte.id, containerLocation: containerLocation)
        }
        process.processNode.placementsNode.setOutputPlacements(outputPlacements).httpPost()
        ClarityWebTransaction.logger.info "Automatic Placement Script has completed successfully. ${process.outputAnalytes?.size()} samples have been transferred into ${destContainerNameToContainerIdMap.values()} container."
    }

    static String convertToContainerNodeLocation(String location) {
        return "${location.substring(0, 1)}:${location.substring(1, location.length())}"
    }

    Map<String,String> createDestContainers() {
        NodeManager clarityNodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        def destContainerNameToContainerIdMap = [:]
        def numberOutputContainers = countNumberOutputContainers()
        String containerId = process.processNode.placementsNode.selectedContainerIds[0] //first available output plate container
        destContainerNameToContainerIdMap[destContainerNames[0]] = containerId
        if (numberOutputContainers > 1) {
            List<ContainerNode> outputContainerNodes = clarityNodeManager.createContainersBulk(ContainerTypes.WELL_PLATE_384, numberOutputContainers - 1)
            outputContainerNodes.eachWithIndex { ContainerNode containerNode, index ->
                destContainerNameToContainerIdMap[destContainerNames[++index]] = containerNode.id
            }
        }
        destContainerNameToContainerIdMap as Map<String,String>
    }

    def countNumberOutputContainers() {
        def numberOutputContainers = destContainerNames?.size()
        if (!numberOutputContainers || numberOutputContainers < 1) {
            process.postErrorMessage("Input samples have an invalid '${ClarityUdf.SAMPLE_DEST_CONTAINER_NAME.udf}': $destContainerNames.")
        }
        numberOutputContainers
    }

    List getDestContainerNames() {
        return process.inputAnalytes.collect{ it.claritySample.udfDestContainerName }?.unique()
    }

    @Override
    void validateInputAnalyteClass(def inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each { analyte ->
            if (!(analyte instanceof SampleAnalyte)) {
                process.postErrorMessage("Input Analyte '$analyte.name' has an invalid analyte class: '${analyte.class?.simpleName}'. Expected class: 'SampleAnalyte'.")
            }
        }
    }

    @Override
    void validateInputsContainerType(List<ContainerNode> containerNodes = process.getContainerNodes(process.inputAnalytes)) {
        Set<ContainerTypes> containerTypes = containerNodes.collect {it.containerTypeEnum}.unique()
        if (containerTypes.size() != 1) {
            process.postErrorMessage("""
                Inputs can only be in the containers of the same type.
                Please select ${ContainerTypes.WELL_PLATE_384.value}.
            """)
        }
        if (containerTypes.find{true} == ContainerTypes.TUBE) {
            process.postErrorMessage("""
                Invalid input container type ${ContainerTypes.TUBE.value}
            """)
        }
    }

    @Override
    void validateOutputContainers() {
        List<ContainerNode> outputContainerNodes = process.getContainerNodes(process.outputAnalytes)
        outputContainerNodes.each { ContainerNode containerNode ->
            if (!containerNode.isPlate) {
                process.postErrorMessage("""
                    Output plate does not pass custom validation.
                    Invalid output container type '${containerNode.containerType}'.
                    Please select ${ContainerTypes.WELL_PLATE_96.value} as the output container.
                """)
            }
        }
        checkLibraryLocations(expectedSortedContainerNodePlacements, outputSortedContainerNodePlacements)
    }

    static final def ERROR_MSG_PLACEMENTS = """
Output plates do not pass custom validation.
Library plate location does not match to the original sample submission's plate location.
Please do not rearrange samples on the output plate.
"""

    void checkLibraryLocations(List<List<String>> expectedSortedContainerNodePlacements,
                               List<List<String>> outputSortedContainerNodePlacements) {
        if (expectedSortedContainerNodePlacements.sort(false) != outputSortedContainerNodePlacements.sort(false))
            process.postErrorMessage(ERROR_MSG_PLACEMENTS)
    }

    Map<String, List<String>> getDestContainerNameToLocationsMap() {
        Map<String, List<String>> destContainerNameToLocationsMap = [:].withDefault { new ArrayList<String>() }
        process.outputAnalytes.each{
            ClarityLibraryStock outputAnalyte = it as ClarityLibraryStock
            ScheduledSample scheduledSample = outputAnalyte.claritySample as ScheduledSample
            destContainerNameToLocationsMap
                    .get(scheduledSample.udfDestContainerName)
                    .add(convertToContainerNodeLocation(scheduledSample.udfDestContainerLocation))
        }
        return destContainerNameToLocationsMap
    }

    def getExpectedSortedContainerNodePlacements(def locationsMap = destContainerNameToLocationsMap) {
        return locationsMap.collect{ String containerName, List<String> locations ->
            locations.sort()
        }
    }

    def getOutputSortedContainerNodePlacements() {
        List<ContainerNode> outputContainerNodes = process.getContainerNodes(process.outputAnalytes)
        return outputContainerNodes.collect { ContainerNode containerNode ->
            containerNode.getLocationContentsIds().keySet().sort()
        }
    }

    @Override
    void validateBatch() {
        def batchId = validateSamplesBatchId()
        validateAllSamplesAreInBatch(batchId)
    }

    void validateAllSamplesAreInBatch(
            def batchId,
            List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>
    ) {
        def sampleLimsIds = findSampleLimsIdsByUdf(ClarityUdf.SAMPLE_BATCH_ID.value, batchId as String)
        if (sampleLimsIds.size() != inputAnalytes.size()) {
            process.postErrorMessage("""
Not all batch samples are selected as inputs.
Please select all samples with $ClarityUdf.SAMPLE_BATCH_ID.value '$batchId'.
            """)
        }
    }

    /*
    https://tempest.jgi-psf.org/api/v2/samples?udf.Sow+Item+Type=Single+Cell+Internal
    nextPage https://tempest.jgi-psf.org/api/v2/samples?udf.Sow Item Type=Single+Cell+Internal&start-index=500
    */
    static List<String> findSampleLimsIdsByUdf(
            String udfName,
            String udfValue,
            NodeConfig nodeConfig = ClarityWebTransaction.requireCurrentTransaction().nodeManager.nodeConfig
    ) {
        List<String> sampleIds = []
        SampleParameterValue spv = SampleParameter.UDF.setValue(udfValue).setUdf(udfName) as SampleParameterValue
        SampleIdIterator sampleIdIterator = new SampleIdIterator(nodeConfig, [spv])
        while (sampleIdIterator.nextPage()) {
            sampleIds.addAll(sampleIdIterator.idList)
        }
        sampleIds
    }

    def validateSamplesBatchId(List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>) {
        List batchIds = inputAnalytes.collect { it.claritySample.udfBatchId }.unique()
        if (batchIds.size() != 1) {
            process.postErrorMessage("""
Inputs should have the same '$ClarityUdf.SAMPLE_BATCH_ID.value'.
Please select the samples with the same $ClarityUdf.SAMPLE_BATCH_ID.value.
            """)
        }
        def batchId = batchIds[0]
        if (!batchId || batchId < 1) {
            process.postErrorMessage("Invalid $ClarityUdf.SAMPLE_BATCH_ID.value: $batchId.")
        }
        batchId
    }

    @Override
    void removeCornerBeans(List beansList) {
    }

    @Override
    void createPrintLabelsFile() {
        Map<String, List<ClarityLibraryStock>> containerToAnalytes = getContainerToAnalytesMap()
        List<Analyte> printAnalytes = containerToAnalytes.keySet().collect { containerLimsId ->
            containerToAnalytes[containerLimsId].first()
        }
        process.writeLabels(printAnalytes)
    }

    @Override
    void updateOutputContainers() {
        //output plate container udf “Label” set to the input sample 'Destination Container Name' udf value + '_' + 'Batch Id' udf value
        Map<String, List<ClarityLibraryStock>> containerToAnalytes = getContainerToAnalytesMap()
        containerToAnalytes.keySet().each { containerLimsId ->
            List<ClarityLibraryStock> outputAnalytes = containerToAnalytes[containerLimsId]
            ScheduledSample sample = (ScheduledSample) outputAnalytes[0].claritySample
            outputAnalytes[0].containerUdfLabel = "${sample.udfDestContainerName}"
        }
    }

    @Override
    void uploadExtraFiles() {
        Map<String, List<ClarityLibraryStock>> containerToAnalytes = getContainerToAnalytesMap()
        containerToAnalytes.keySet().eachWithIndex { containerLimsId, int index ->
            List<Analyte> outputAnalytes = containerToAnalytes[containerLimsId]
            uploadEchoFile(++index, outputAnalytes)
        }
    }

    void uploadEchoFile(int index, List<ClarityLibraryStock> outputAnalytes) {
        NodeManager clarityNodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        ArtifactNode fileNode = process.getFileNode("${LibraryCreationProcess.SCRIPT_GENERATED_ECHO_FILE}$index")
        ClarityWebTransaction.logger.info "Uploading script generated file ${fileNode.id}..."
        //ExcelWorkbook echoWorkbook = populateEchoSheet()
        //echoWorkbook.store(process.nodeManager.nodeConfig, fileNode.id)
        String echoFileContents = buildEchoFile(outputAnalytes)
        Node node = FileUploadUtil.uploadTextFile(
                fileNode.id,
                echoFileContents,
                clarityNodeManager.nodeConfig,
                '.csv',
                process.testMode
        )
        ClarityWebTransaction.logger.info "Uploaded Echo File to $node"
    }

    static private String buildEchoFile(List<ClarityLibraryStock> outputAnalytes) {
        CsvBuilder builder = new CsvBuilder(beanHeaders.size(), false)
        appendEchoFileHeader(builder)
        appendEchoFileData(builder, outputAnalytes)
        builder.done()
        return builder.toString()
    }

    static private void appendEchoFileHeader(CsvBuilder builder) {
        builder.append(getBeanHeaders())
    }

    static def getBeanHeaders() {
        return LcEchoTableBean.declaredFields
                .findAll { Field field -> field.isAnnotationPresent(FieldMapping.class) }
                .collect { Field field -> field.getAnnotation(FieldMapping.class).header()?.trim() }
    }

    static private void appendEchoFileData(CsvBuilder builder, List<ClarityLibraryStock> outputAnalytes) {
        outputAnalytes
                .collect {
                    createLcEchoTableBean(it)
                }
                .sort{ LcEchoTableBean b1, LcEchoTableBean b2 ->
                    b1.sourcePlateName <=> b2.sourcePlateName ?: b1.sourceColumn <=> b2.sourceColumn ?: b1.sourceRow <=> b2.sourceRow
                }
                .each{ LcEchoTableBean bean ->
                    builder.append(createEchoFileRow(bean))
                }
    }

    static def createEchoFileRow(LcEchoTableBean bean) {
         return LcEchoTableBean.declaredFields
                 .findAll { Field field -> field.isAnnotationPresent(FieldMapping.class) }
                 .collect { Field field -> bean."${field.name}"?.toString()?.trim() }
    }

    static LcEchoTableBean createLcEchoTableBean(ClarityLibraryStock clarityLibraryStock) {
        Analyte sampleAnalyte = clarityLibraryStock.parentAnalyte
        LcEchoTableBean bean = new LcEchoTableBean()
        bean.sourceRow = convertRowToBigInteger(sampleAnalyte.containerLocation.row)
        bean.sourcePlateName = sampleAnalyte.containerUdfLabel
        bean.sourcePlateBarcode = sampleAnalyte.containerName
        bean.destinationPlateName = clarityLibraryStock.containerUdfLabel
        bean.destinationPlateBarcode = clarityLibraryStock.containerName
        bean.sourceColumn = sampleAnalyte.containerLocation.column as BigInteger
        bean.destinationRow = convertRowToBigInteger(clarityLibraryStock.containerLocation.row)
        bean.destinationColumn = clarityLibraryStock.containerLocation.column as BigInteger
        bean.transferVolumeNl = DEFAULT_LIBRARY_VOLUME_NL
        bean
    }

/*
    ExcelWorkbook populateEchoSheet(){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(ECHO_TEMPLATE_NAME, process.testMode)
        excelWorkbook.addSection(echoTableSection)
        return excelWorkbook
    }

    Section getEchoTableSection() {
        Section tableSection = new TableSection(0, LcEchoTableBean.class, 'A1', null, true)
        def tableBeansArray = []
        process.outputAnalytes.eachWithIndex { ClarityLibraryStock clarityLibraryStock, index ->
            SampleAnalyte sampleAnalyte = clarityLibraryStock.parentAnalyte

            LcEchoTableBean bean = new LcEchoTableBean()
            bean.sourcePlateName = sampleAnalyte.containerName
            bean.sourcePlateBarcode = sampleAnalyte.containerUdfLabel
            bean.sourceRow = convertRowToBigInteger(sampleAnalyte.containerLocation.row)
            bean.sourceColumn = sampleAnalyte.containerLocation.column as BigInteger
            bean.destinationColumn = clarityLibraryStock.containerLocation.column as BigInteger
            bean.destinationRow = convertRowToBigInteger(clarityLibraryStock.containerLocation.row)
            bean.destinationPlateBarcode = clarityLibraryStock.containerUdfLabel
            bean.destinationPlateName = clarityLibraryStock.containerName
            bean.transferVolumeNl = DEFAULT_LIBRARY_VOLUME_NL
            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }
*/

    static BigInteger convertRowToBigInteger(String row) {
        if (!row || !(row in ('A'..'P')))
            return null
        return (row.charAt(0)-'A'.charAt(0) + 1)
    }

    String getTemplate(){
        return TEMPLATE_NAME
    }

    int getTemplateIndex() {
        return 8
    }
}