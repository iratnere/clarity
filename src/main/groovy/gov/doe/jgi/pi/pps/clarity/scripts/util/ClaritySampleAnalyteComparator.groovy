package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes

class ClaritySampleAnalyteComparator implements Comparator<SampleAnalyte> {
    static final String MICROBIAL_SCIENTIFIC_PROGRAM = 'Microbial'
    final static List<String> predefinedOrderByProgram = Collections.unmodifiableList([MICROBIAL_SCIENTIFIC_PROGRAM])
    final static List<String> predefinedOrderByContainer = Collections.unmodifiableList([ContainerTypes.WELL_PLATE_96.value, ContainerTypes.TUBE.value])

    @Override
	int compare(SampleAnalyte a, SampleAnalyte b) {
		int result = customSorter(a.containerType, b.containerType, predefinedOrderByContainer)
		if(result == 0) {
            if (ContainerTypes.TUBE.value.equalsIgnoreCase(a.containerType)) {
                result = (a.name <=> b.name)
            } else {
                result = (a.containerName <=> b.containerName)
            }
			if (result == 0) {
				// on plate we need to order by columns: A1,B1,...H1, A2,B2,...,H2, A3,B3...
                def aSourceLocation = a.worksheetSourceLocation
                def bSourceLocation = b.worksheetSourceLocation
				def aRow = aSourceLocation?.getAt(0)
				def aCol = aSourceLocation?.drop(1)
				def bRow = bSourceLocation?.getAt(0)
				def bCol = bSourceLocation?.drop(1)
				result = (aCol?.toBigInteger() <=> bCol?.toBigInteger())
				if (result == 0) {
					result = (aRow <=> bRow)
					if (result == 0 && (a.claritySample instanceof ScheduledSample) && (b.claritySample instanceof ScheduledSample)) {
						result = sortbySowItemType(a.claritySample, b.claritySample)
					}
					return result
				}
				return result
			}
			return result
		}
		return result
	}

    static def customSorter = { a, b, order ->
        order.indexOf(b) <=> order.indexOf(a)
    }

    static def customSorterByTargetInsertSize = { aTargetInsertSizeKb, bTargetInsertSizeKb ->
        if (aTargetInsertSizeKb == bTargetInsertSizeKb)
            return 0
        else if (aTargetInsertSizeKb < bTargetInsertSizeKb)
            return 1
        else
            return -1
    }

    static int sortbySowItemType(ScheduledSample ss1, ScheduledSample ss2) {
        int result = customSorter(ss1.udfScientificProgram, ss2.udfScientificProgram, predefinedOrderByProgram)
        if (result == 0) {
            result = (SowItemTypeCv.toSortIndex(ss1.udfSowItemType) <=> SowItemTypeCv.toSortIndex(ss2.udfSowItemType))
            if (result == 0) {
                if (SowItemTypeCv.FRAGMENT.value == ss1.udfSowItemType) {
                    return (PlatformTypeCv.toSortIndex(ss1.platform) <=> PlatformTypeCv.toSortIndex(ss2.platform))
                } else if (SowItemTypeCv.LMP.value == ss1.udfSowItemType) {
                    return customSorterByTargetInsertSize(ss1.udfTargetInsertSizeKb, ss2.udfTargetInsertSizeKb)
                }
            }
            return result
        }
        return  result
    }

}
