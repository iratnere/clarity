package gov.doe.jgi.pi.pps.clarity.scripts.grails


import grails.gorm.transactions.Transactional
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.ResultSetExtractor
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.namedparam.SqlParameterSource

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException

@Transactional
class AliquotCreationService {

    DataSource dataSource
    NamedParameterJdbcTemplate jdbcTemplate

    Map<String, KapaWellConfig> retrieveKapaPlateConfiguration(String categoryName) {
        assert categoryName
        Map<String, String> result = new HashMap<>()
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("categoryName", categoryName)
        if (!jdbcTemplate)
            jdbcTemplate = new NamedParameterJdbcTemplate(dataSource)
        Collection<KapaWellConfig> kapaPlate = jdbcTemplate.query(KAPA_SQL, parameters,
                new RowMapper<KapaWellConfig>() {
                    @Override
                    KapaWellConfig mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new KapaWellConfig(
                                rs.getString('tagNameLocation'),
                                rs.getString('tagSequence'))
                    }
                })
        kapaPlate.each { KapaWellConfig kapaWellConfig ->
            result[kapaWellConfig.location] = kapaWellConfig
        }

        return result
    }

    final static KAPA_SQL = """
SELECT rt.NAME as tagNameLocation, rtrim(replace(rt.metadata,
'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>')
as tagSequence
FROM CLARITYLIMS.REAGENTTYPE rt
join CLARITYLIMS.REAGENTCATEGORY rc on rc.REAGENTCATEGORYID = rt.REAGENTCATEGORYID
WHERE rc.NAME = :categoryName
"""

    List getKapaPlateNames() {
        String query = """select distinct rc.name as NAME
from reagentcategory rc, reagenttype rt
where Rc.Reagentcategoryid = Rt.Reagentcategoryid
and Rt.Specialtype = 'Kapa'
and Rt.Isvisible = 1
and Rc.Isvisible = 1"""
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<String> sets = []
                while (rs.next()) {
                    sets << "${rs.getString('NAME')}"
                }
                return sets
            }
        })
    }
}
