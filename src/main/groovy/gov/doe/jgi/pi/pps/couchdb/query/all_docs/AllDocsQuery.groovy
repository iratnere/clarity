package gov.doe.jgi.pi.pps.couchdb.query.all_docs

import gov.doe.jgi.pi.pps.couchdb.util.CouchDb

/**
 * Created by duncanscott on 6/9/15.
 */
class AllDocsQuery {
    /*
    Query Parameters:

    conflicts (boolean) – Includes conflicts information in response. Ignored if include_docs isn’t true. Default is false.
    descending (boolean) – Return the documents in descending by key order. Default is false.
    endkey (string) – Stop returning records when the specified key is reached. Optional.
    end_key (string) – Alias for endkey param.
    endkey_docid (string) – Stop returning records when the specified document ID is reached. Optional.
    end_key_doc_id (string) – Alias for endkey_docid param.
    include_docs (boolean) – Include the full content of the documents in the return. Default is false.
    inclusive_end (boolean) – Specifies whether the specified end key should be included in the result. Default is true.
    key (string) – Return only documents that match the specified key. Optional.
    limit (number) – Limit the number of the returned documents to the specified number. Optional.
    skip (number) – Skip this number of records before starting to return the results. Default is 0.
    stale (string) – Allow the results from a stale view to be used, without triggering a rebuild of all views within the encompassing design doc. Supported values: ok and update_after. Optional.
    startkey (string) – Return records starting with the specified key. Optional.
    start_key (string) – Alias for startkey param.
    startkey_docid (string) – Return records starting with the specified document ID. Optional.
    start_key_doc_id (string) – Alias for startkey_docid param.
    update_seq (boolean) – Response includes an update_seq value indicating which sequence id of the underlying database the view reflects. Default is false.
     */
    CouchDb couchDb

    Boolean conflicts
    Boolean descending
    String endkey
    String endkeyDocid
    Boolean includeDocs
    Boolean inclusiveEnd
    String key
    Number limit
    Number skip
    String stale
    String startkey
    String startkeyDocid
    Boolean includeUpdateSeq

    Map<String,Object> getParams() {
        Map<String,Object> p = [:]
        if (conflicts != null) {
            p['conflicts'] = conflicts
        }
        if (descending != null) {
            p['descending'] = descending
        }
        if (endkey != null) {
            p['endkey'] = endkey
        }
        if (endkeyDocid != null) {
            p['endkey_docid'] = endkeyDocid
        }
        if (includeDocs != null) {
            p['include_docs'] = includeDocs
        }
        if (inclusiveEnd != null) {
            p['inclusive_end'] = inclusiveEnd
        }
        if (key != null) {
            p['key'] = key
        }
        if (limit != null) {
            p['limit'] = limit
        }
        if (skip != null) {
            p['skip'] = skip
        }
        if (stale != null) {
            p['stale'] = stale
        }
        if (startkey != null) {
            p['startkey'] = startkey
        }
        if (startkeyDocid != null) {
            p['startkey_docid'] = startkeyDocid
        }
        if (includeUpdateSeq != null) {
            p['update_seq'] = includeUpdateSeq
        }
        return p
    }

}
