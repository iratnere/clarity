package gov.doe.jgi.pi.pps.clarity.scripts.rest

import grails.converters.JSON
import org.grails.web.json.JSONObject

/**
 * Created by tlpaley on 11/22/17.
 */
class LaneFraction {

    LaneFraction() {
        registerSubmission()
    }
    def libraryLimsId
    def sowItemId
    def sequencingOptimizationFactor
    def totalNumberOfReads
    def numberOfReadsBp
    def laneFraction

    static def registerSubmission() {
        JSON.registerObjectMarshaller (LaneFraction) { LaneFraction submission ->
            def output = [:]
            output['library-lims-id'] = submission.libraryLimsId
            return output
        }
    }

    JSONObject toJson(){
        JSONObject jsonObject = new JSONObject()
        jsonObject."library-lims-id" = libraryLimsId
        jsonObject."sow-item-id" = sowItemId
        jsonObject."sequencing-optimization-factor" = sequencingOptimizationFactor
        jsonObject."total-number-of-reads" = totalNumberOfReads
        jsonObject."number-of-reads-bp" = numberOfReadsBp
        jsonObject."lane-fraction" = laneFraction
        return jsonObject
    }
}
