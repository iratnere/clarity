package gov.doe.jgi.pi.pps.clarity.dto

/**
 * Created by dscott on 7/28/2016.
 */
class ProjectDto extends GeneusNodeDto implements Comparable<ProjectDto> {

    @Override
    int compareTo(ProjectDto o) {
        int result = 0
        if (this.name?.isBigInteger() && o.name?.isBigInteger()) {
            result = this.name.toBigInteger().compareTo(o.name.toBigInteger())
        }
        if (result) {
            return result
        }
        return super.compareTo(o)
    }

}
