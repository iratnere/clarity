package gov.doe.jgi.pi.pps.clarity.cv

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive


enum IdTypeCv {
	SAMPLE_LIMSID('sample LIMSID', 'sample'),
	ARTIFACT_LIMSID('artifact LIMSID', 'artifact'),
	CONTAINER_LIMSID('container LIMSID', 'container'),
	CONTAINER_BARCODE('container barcode', 'container'),
	UNDETERMINED('undetermined / mixture', 'mixture'),
	PROJECT_LIMSID('project LIMSID', 'project'),
	PROJECT_NAME('project name (PMO project ID)', 'project'),
	PROCESS_LIMSID('process LIMSID', 'process'),
	ARTIFACT_NAME('artifact name', 'artifact'),
	SAMPLE_NAME('sample name', 'sample')

	final String value
	final String entityType

	IdTypeCv(String value, String entityType) {
		this.value = value
		this.entityType = entityType
	}
	
	static List<IdTypeCv> toList() {
		List<IdTypeCv> statusList = []
		for (IdTypeCv cv : values()) {
			statusList << cv
		}
		return statusList
	}

	
	public String toString() {
		return value
	}
	
	private static final EnumConverterCaseInsensitive<IdTypeCv> converter = new EnumConverterCaseInsensitive(IdTypeCv)
	
	public static IdTypeCv toEnum(value) {
		if (value instanceof IdTypeCv) {
			return value
		}
		return converter.toEnum(value)
	}

}
