package gov.doe.jgi.pi.pps.clarity.scripts.rest

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import grails.converters.JSON
import org.grails.web.json.JSONObject

/**
 * Created by tlpaley on 11/22/17.
 */
class Libraries {

    Libraries() {
        registerSubmission()
    }

    Libraries(json, List<Analyte> inputAnalytes) {
        inputAnalytes.each { Analyte analyte ->
            BigDecimal laneFraction = 0
            def sowItemId = null
            def sequencingOptimizationFactor
            def totalNumberOfReads
            def numberOfReadsBp = 0
            if(analyte.libraryStockIds.size() > 1)
                sowItemId = 0 // PPS-5053: pools have sow-item-id = 0 on DW request.
            analyte.libraryStockIds.each { String id ->
                def lf = json.'libraries'.find{it.'library-lims-id' == id }
                laneFraction += lf.'lane-fraction'
                if(sowItemId == null)
                    sowItemId = lf.'sow-item-id'
                sequencingOptimizationFactor = lf.'sequencing-optimization-factor'
                totalNumberOfReads = lf.'total-number-of-reads'
                numberOfReadsBp += lf.'number-of-reads-bp'
            }
            libraries.add(new LaneFraction(
                    libraryLimsId: analyte.id,
                    sowItemId: sowItemId,
                    sequencingOptimizationFactor: sequencingOptimizationFactor,
                    totalNumberOfReads: totalNumberOfReads,
                    numberOfReadsBp: numberOfReadsBp,
                    laneFraction: laneFraction
            ))
        }
    }
    List<LaneFraction> libraries = []

    static def registerSubmission() {
        JSON.registerObjectMarshaller (Libraries) { Libraries submission ->
            def output = [:]
            output['libraries'] = submission.libraries
            return output
        }
    }

    JSONObject toJson() {
        JSONObject jsonObject = new JSONObject()
        jsonObject."libraries" = libraries.collect{it.toJson()}
        return jsonObject
    }
}
