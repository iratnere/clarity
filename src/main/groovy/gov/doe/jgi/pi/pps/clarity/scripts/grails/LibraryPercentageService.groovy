package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.Libraries
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import org.springframework.http.HttpStatus

@Transactional
class LibraryPercentageService {

    GrailsApplication grailsApplication

    def laneFractionsResponseJson(List libraryLimsIds){
        def responseJson = callHttp(laneFractionsSubmissionUrl(), Method.POST, libraryLimsIds)
        responseJson
    }

    JSON getLaneFractionsJson(List libraryLimsIds) {
        Libraries submission = new Libraries()
        submission.libraries = libraryLimsIds.collect{ new LaneFraction(libraryLimsId: it) }
        submission as JSON
    }

    String laneFractionsSubmissionUrl() {
        return grailsApplication.config.laneFraction.url
    }

//http://claritydev1.jgi-psf.org/pps-1/lane-fraction
    def callHttp(String url, def method, List libraryLimsIds){
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        def json = getLaneFractionsJson(libraryLimsIds)
        def responseJson = null
        ClarityWebTransaction.logger.info "http request URL: ${url}"
        ClarityWebTransaction.logger.info "submission: ${json}"
        HTTPBuilder http = new HTTPBuilder(url)
        http.request( method, ContentType.JSON ) { req ->
            json ? body = json.toString() : null
            response.success = { HttpResponseDecorator resp, respJson ->
                ClarityWebTransaction.logger.info "response json: ${respJson}"
                responseJson = respJson
            }
            response.failure = { HttpResponseDecorator resp, respJson ->
                def errorMsg = StringBuilder.newInstance()
                errorMsg << "$url service response status: [${resp.status}]" << ClarityProcess.WINDOWS_NEWLINE
                def respError = JsonUtil.toJson(respJson)
                ClarityWebTransaction.logger.error("$url service response: status [${resp.status}], body [${respError}]")
                if (resp.status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
                    respError.'errors'.each{
                        if (it.'index' != null) {
                            def index = it.'index' as int
                            def libraryLimsId = libraryLimsIds[index]
                            def libraryName = nodeManager.getArtifactNode(libraryLimsId)?.name
                            errorMsg << "$libraryName/$libraryLimsId: $it.message" << ClarityProcess.WINDOWS_NEWLINE
                        }
                    }
                }
                throw new WebException(errorMsg.toString(), resp.status)
            }
        }
        responseJson
    }

}
