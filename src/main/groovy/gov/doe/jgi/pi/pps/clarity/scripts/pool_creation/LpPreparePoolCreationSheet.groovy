package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryNameReservationService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * This class is meant to populate Pool creation sheet based on the pools created in the previous action
 * for requirements refer 4c (vi) of the Clarity Library Pooling Requirements document
 * https://docs.google.com/a/lbl.gov/document/d/1itCb6lXyP_IM0Fl-o-8tMDdsVq-WgdmekOmN53JMfg4/edit#
 *
 * Created of the ticket PPS-1769
 * Created by lvishwas on 1/6/15.
 */
class LpPreparePoolCreationSheet extends ActionHandler {

    private static final String POOL_CREATION_TEMPLATE = 'resources/templates/excel/PoolingSheet.xls'

    List<PoolCreationTableBean> getPoolCreationBeansForPools(){
        ClarityWebTransaction.logger.info "Populating Pool Creation sheet with pool(s) ${process.outputAnalytes}"
        List<PoolCreationTableBean> poolMemberBeans = []
        int rowNum = 1
        //PPS-4757 - Output pools are being sorted by pool number
        def outputAnalytes = process.outputAnalytes.sort{a,b->a.name <=>b.name}
        LibraryNameReservationService libraryNameReservationService = (LibraryNameReservationService) ClarityWebTransaction.currentTransaction.requireApplicationBean(LibraryNameReservationService.class)
        libraryNameReservationService.assignLibraryNames(outputAnalytes)
        outputAnalytes.eachWithIndex{ Analyte pool, int poolIndex->
            ClarityWebTransaction.logger.info "PoolNode: ${pool.id}"
            List<Analyte> poolMembers = pool.getPoolMembers()
            //PPS-4757 - Member libraries in the pool are sorted by library names and before assigning source position
            poolMembers?.sort{ Analyte a, Analyte b->
                return a.name <=> b.name
            }
            poolMembers.each{ Analyte libraryStock ->
                PoolCreationTableBean bean = new PoolCreationTableBean()
                bean.populateBean(libraryStock, pool)
                bean.poolNumber = rowNum
                def indexes = LibraryInformation.getIndexName(libraryStock, process.artifactIndexService)
                bean.indexName = indexes?.size() == 1?indexes[0]:''
                bean.destPosition = poolIndex+1
                bean.srcPosition = libraryStock.isOnPlate?libraryStock.containerLocation.rowColumn.replace(':', ''):rowNum
                //PPS-4708 - Pool fraction needs to be carried over to the pool creation worksheet
                bean.libraryPercentage = libraryStock.udfLibraryPercentageWithSOF
                poolMemberBeans << bean
                rowNum++
            }
        }
        return poolMemberBeans
    }

    ExcelWorkbook populatePoolCreationSheet(List<PoolCreationTableBean> tableBeansList, boolean testMode=false){
        ExcelWorkbook poolCreationWorkbook = new ExcelWorkbook(POOL_CREATION_TEMPLATE,testMode)
        Section tableSection = new TableSection(0,PoolCreationTableBean.class, 'A1',null, true)
        tableSection.setData(tableBeansList)
        poolCreationWorkbook.addSection(tableSection)
        return poolCreationWorkbook
    }

    void execute(){
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        def fileNode = processNode.outputResultFiles.find { it.name.contains(PoolCreation.SCRIPT_GENERATED_POOL_CREATION_SHEET)}
        ExcelWorkbook poolCreationWorksheet = populatePoolCreationSheet(poolCreationBeansForPools,process.testMode)
        ClarityWebTransaction.logger.info "Uploading script generated file ${fileNode.id}..."
        Node responseNode = poolCreationWorksheet.store(nodeManager.nodeConfig, fileNode.id)
        ClarityWebTransaction.logger.info "${responseNode}"
        updateOutputsUdfs()
        process.nodeManager.httpPutDirtyNodes()
    }

    void updateOutputsUdfs(List<ClarityLibraryPool> outputAnalytes = process.outputAnalytes) {
        outputAnalytes.each{ ClarityLibraryPool clarityLibraryPool ->
            //PPS-5102 - Flowcell type will be read from sequencer model of parent libraries
            RunModeCv runModeCv = clarityLibraryPool.poolMembers.first().runModeCv
            clarityLibraryPool.udfRunMode = runModeCv.runMode
        }
    }
}
