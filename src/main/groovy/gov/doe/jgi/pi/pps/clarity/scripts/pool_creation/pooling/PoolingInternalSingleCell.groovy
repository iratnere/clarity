package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Sheet

class PoolingInternalSingleCell implements Pooling {
    static final def SECTION_STARTCELL_ROW = 1
    static final def SECTION_STARTCELL_CELL = 13
    static final String SECTION_MESSAGE = 'Internal Single Cell pools, please do not modify'

    List<LibraryInformation> members
    int startPoolNumber

    PoolingInternalSingleCell(List<LibraryInformation> members){
        this.members = members
    }

    @Override
    void makePools() {
        members.each {
            it.poolNumber = startPoolNumber
        }
        startPoolNumber++
    }

    @Override
    int completePoolPrepBeans(int poolNumber) {
        startPoolNumber = poolNumber
        makePools()
        return startPoolNumber
    }

    @Override
    String getMessage() {
        return SECTION_MESSAGE
    }

    @Override
    CellStyle getCellStyle(Sheet sheet) {
        def row = sheet.getRow(SECTION_STARTCELL_ROW)
        def cell = row.getCell(SECTION_STARTCELL_CELL)
        CellStyle cellStyle = cell.getCellStyle()
        return cellStyle
    }

    @Override
    List getActionBeans() {
        return null
    }

    @Override
    List getSummaryBeans() {
        return null
    }

    @Override
    List getCalculationBeans() {
        return null
    }

    int getPoolNumber(){
        members.first().poolNumber
    }

    @Override
    def getIndexToCompare(LibraryInformation member) {
        return member.indexSequence
    }

    @Override
    Map validate() {
        Map<String,List<String>> findings = [:].withDefault {[]}
        String errorMessage = validateSingleCellInternalPool(members, poolNumber)
        if (errorMessage)
            findings[ERROR] << errorMessage
        //PPS-4879: Single Cell pool validation does not check for duplicate indexes
        Map<String, List<String>> indexAnalytes = indexMembersMap
        def repIndexes = indexAnalytes.findAll{it.value.size() > 1}
        if(repIndexes)
            findings[ERROR] << "pool #$poolNumber has duplicate indexes: $repIndexes"
        return findings
    }

    static String validateSingleCellInternalPool(def pool, def poolNumber) {
        def errorMsg = "Single Cell Internal pool #$poolNumber is not valid." << "$ClarityProcess.WINDOWS_NEWLINE"//StringBuilder.newInstance()
        def defaultSize = errorMsg.length()
        if (poolNumber < 1) {
            errorMsg << "Pool number value should be greater than zero.$ClarityProcess.WINDOWS_NEWLINE"
            return errorMsg as String
        }
        def batchIds = [] as Set
        def poolNumbers = [] as Set
        pool.each { LibraryInformation bean ->
            poolNumbers.add(bean.analyte.claritySample.udfPoolNumber)
            batchIds.add(bean.analyte.claritySample.udfBatchId)
        }
        if (batchIds?.size() != 1)
            errorMsg << "Pool members should have the same ${ClarityUdf.SAMPLE_BATCH_ID}.$ClarityProcess.WINDOWS_NEWLINE"
        if (poolNumbers?.size() != 1)
            errorMsg << "Pool members should have the same ${ClarityUdf.SAMPLE_POOL_NUMBER}.$ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg.length() > defaultSize) {
            return errorMsg as String
        }
        return null
    }

    Map<String, List<LibraryInformation>> getIndexMembersMap(){
        Map<String, List<LibraryInformation>> indexMembers = [:].withDefault {[]}
        members.each{ LibraryInformation li ->
            def indexToCompare = getIndexToCompare(li)
            if(indexToCompare instanceof String)
                indexToCompare = [indexToCompare]
            indexToCompare.each { String index ->
                ArtifactIndexService.splitSequences(index).each {
                    indexMembers[it] << li
                }
            }
        }
        return indexMembers
    }
}
