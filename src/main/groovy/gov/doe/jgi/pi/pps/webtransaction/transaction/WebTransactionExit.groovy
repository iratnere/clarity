package gov.doe.jgi.pi.pps.webtransaction.transaction

import gov.doe.jgi.pi.pps.util.exception.HttpStatusCodeReporter


class WebTransactionExit extends RuntimeException implements HttpStatusCodeReporter {

	private Integer httpStatusCode
	
	public WebTransactionExit(Integer httpStatusCode) {
		this.httpStatusCode = httpStatusCode
	}
	
	@Override
	public Integer getHttpStatusCode() {
		return httpStatusCode
	}
	
}
