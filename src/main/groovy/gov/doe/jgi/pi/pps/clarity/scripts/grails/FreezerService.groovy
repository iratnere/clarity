package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovy.util.slurpersupport.GPathResult
import groovy.xml.MarkupBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Propagation

import java.text.DateFormat
import java.text.SimpleDateFormat

@Transactional(propagation = Propagation.SUPPORTS)
class FreezerService {

	static Logger logger = LoggerFactory.getLogger(FreezerService.class.name)
	static final DateFormat freezerDateFormat = new SimpleDateFormat("yyyy-MM-dd")

    GrailsApplication grailsApplication //injected

	static String generateLookupXml(Collection<String> containerBarcodes) {
		def writer = new StringWriter()
		def xml = new MarkupBuilder(writer)
		xml.containers {
			containerBarcodes?.unique()?.findAll{it}?.each { String containerBarcode ->
				container(barcode:containerBarcode)
			}
		}
		return writer.toString()
	}
	
	static String generateCheckOutXml(Collection<String> containerBarcodes, String operatorFirstName, String operatorLastName) {
		def writer = new StringWriter()
		def xml = new MarkupBuilder(writer)
		xml.'freezer-transaction' {
			'operator-first-name'(operatorFirstName)
			'operator-last-name'(operatorLastName)
			containers {
				containerBarcodes?.unique()?.findAll{it}?.each { String containerBarcode ->
					container(barcode:containerBarcode)
				}
			}
		}
		return writer.toString()
	}
	
	static String generateCheckInXml(Collection<FreezerContainer> freezerContainers, String operatorFirstName, String operatorLastName, String freezerPurpose = null) {
		def writer = new StringWriter()
		def xml = new MarkupBuilder(writer)
		xml.'freezer-transaction' {
			'operator-first-name'(operatorFirstName)
			'operator-last-name'(operatorLastName)
			'freezer-purpose'(freezerPurpose)
			containers {
				freezerContainers.each { FreezerContainer freezerContainer ->
					container(barcode:freezerContainer.barcode,'container-class':freezerContainer.containerClass?:'tube')
				}
			}
		}
		return writer.toString()
	}
	
	static List<FreezerContainer> xmlToFreezerContainers(GPathResult xml) {
		List<FreezerContainer> containers = []
		xml?.container?.each { containerNode ->
            FreezerContainer container = new FreezerContainer()
			containers << container
			
			container.barcode = containerNode.'@barcode'
			container.contentsType = containerNode.'contents-type'?.text()
			container.checkInStatus = containerNode.'check-in-status'?.text()
			container.operatorFirstName = containerNode.'operator-first-name'?.text()
			container.operatorLastName = containerNode.'operator-last-name'?.text()
			container.holderName = containerNode.'holder-name'?.text()
			container.location = containerNode.'location'?.text()
			container.errorMessage = containerNode.'error-message'?.text()
			container.containerClass = containerNode.'container-class'?.text()
			container.containerState = containerNode.'container-state'?.text()
			
			String transactionDate = containerNode.'transaction-date'?.text()
			if (transactionDate) {
				container.transactionDate = freezerDateFormat.parse(transactionDate)
			}
		}
		return containers
	}
	
	static void logErrors(xml) {
		logger.error "freezer error:\n${xml}"
	}
	
	Map<String, FreezerContainer> freezerContainersToMap(Collection<FreezerContainer> freezerContainers) {
		Map<String, FreezerContainer> containerMap = [:]
		freezerContainers?.each{ FreezerContainer freezerContainer ->
			containerMap[freezerContainer.barcode] = freezerContainer
		}
		return containerMap
	}
	
    List<FreezerContainer> lookup(Collection<String> containerBarcodes) {
		List<FreezerContainer> containers = []
		if (!containerBarcodes) {
			return containers
		}
		String containerXml = generateLookupXml(containerBarcodes)
		String url = grailsApplication.config.freezerService.url
		String path = 'lookup.xml'
		HTTPBuilder http = new HTTPBuilder(url)
		logger.info "posting XML to ${url}${path} with contentType ${ContentType.XML}:\n${containerXml}"
		http.request( Method.POST, ContentType.XML ) {
			uri.path = path
			body = containerXml
			response.success = { resp, gpath ->
				containers = xmlToFreezerContainers(gpath)
			}
			response.failure = { resp, gpath ->
				logErrors(gpath)
				throw new WebException([code:'freezer.error', args:['look-up']], resp.status)
			}
		}
		return containers
    }
	
	List<FreezerContainer> checkout(Collection<String> containerBarcodes, String operatorFirstName, String operatorLastName) {
		List<FreezerContainer> containers = []
		if (!containerBarcodes) {
			return containers
		}
		String checkoutXml = generateCheckOutXml(containerBarcodes, operatorFirstName, operatorLastName)
		HTTPBuilder http = new HTTPBuilder(grailsApplication.config.freezerService.url)
		http.request( Method.POST, ContentType.XML ) {
			uri.path = 'checkout'
			body = checkoutXml
			response.success = { resp, gpath ->
				containers = xmlToFreezerContainers(gpath)
			}
			response.failure = { resp, gpath ->
				logErrors(gpath)
				throw new WebException([code:'freezer.error', args:['check-out']], resp.status)
			}
		}
		return containers
	}
	
	List<FreezerContainer> checkIn(Collection<FreezerContainer> freezerContainers, String operatorFirstName, String operatorLastName, String freezerPurpose = null) {
		List<FreezerContainer> containers = []
		if (!freezerContainers) {
			return containers
		}
		String checkinXml = generateCheckInXml(freezerContainers, operatorFirstName, operatorLastName, freezerPurpose)
		HTTPBuilder http = new HTTPBuilder(grailsApplication.config.freezerService.url)
		http.request( Method.POST, ContentType.XML ) {
			uri.path = 'checkin'
			body = checkinXml
			response.success = { resp, gpath ->
				containers = xmlToFreezerContainers(gpath)
			}
			response.failure = { resp, gpath ->
				logErrors(gpath)
				throw new WebException([code:'freezer.error', args:['check-in']], resp.status)
			}
		}
		return containers
	}

    /**
     * check out the inputs from the freezers(if the sample is in the old freezer system not in SAM)
     * @return
     */
    List<FreezerContainer> freezerCheckoutInputAnalytes(ClarityProcess process) {
        return checkout(process.processNode.inputAnalytes*.containerNode.unique()*.name, process.researcher?.firstName, process.researcher?.lastName)
    }

    List<FreezerContainer> freezerLookupInputAnalytes(ClarityProcess process) {
        return lookup(process.processNode.inputAnalytes*.containerNode.unique()*.name)
    }

}
