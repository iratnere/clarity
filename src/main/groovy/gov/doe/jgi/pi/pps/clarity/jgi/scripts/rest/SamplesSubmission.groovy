package gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest

import grails.converters.JSON

/**
 * Created by tlpaley on 5/3/17.
 */
class SamplesSubmission {

    SamplesSubmission() {
        registerSubmission()
    }
    def submittedBy
    List<SampleSubmission> samples = []

    static def registerSubmission() {
        JSON.registerObjectMarshaller (SamplesSubmission) { SamplesSubmission submission ->
            def output = [:]
            output['submitted-by-cid'] = submission.submittedBy
            output['samples'] = submission.samples

            return output
        }
    }
}
