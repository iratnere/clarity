package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

class DropDownList {
	
	private String[] controlledVocabulary
	private String value
	private boolean showErrorBox = true//PPS-4733 - To allow custom values in drop down lists
	
	String[] getControlledVocabulary(){
		return controlledVocabulary
	}
	
	void setControlledVocabulary(String[] dropDownListValues){
		this.controlledVocabulary = dropDownListValues
	}
	
	public void setValue(String selectedValue){
		this.value = selectedValue
	}
	
	public String getValue(){
		return value
	}

	public void setShowErrorBox(boolean allowCustomValues){
		this.showErrorBox = allowCustomValues
	}

	public boolean getShowErrorBox(){
		return showErrorBox
	}
} 
