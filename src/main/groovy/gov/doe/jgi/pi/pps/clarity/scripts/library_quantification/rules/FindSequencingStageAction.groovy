package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.Action

/**
 * Created by datjandra on 7/7/2015.
 */
class FindSequencingStageAction implements Action {

    @Override
    String perform(def subject) {
        if (!subject) {
            throw new IllegalArgumentException('Subject must not be null')
        }
        if (!(subject instanceof Analyte)) {
            throw new IllegalArgumentException('Analyte type is required')
        }
        if (subject.hasProperty('udfRunMode')) {
            RunModeCv runMode = subject.runModeCv
            return runMode.sequencingWorkflowUri
        }
        throw new IllegalArgumentException('UDF run mode getter is missing')
    }

    @Override
    boolean isNoOp() {
        return false
    }
}
