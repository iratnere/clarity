package gov.doe.jgi.pi.pps.clarity.scripts.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Sheet

abstract class BasicPoolingAlgorithm implements PoolingByDOP{
    int poolsCount = 0
    int startNumber
    Integer degreeOfPooling = null
    List<LibraryInformation> members

    BasicPoolingAlgorithm(List<LibraryInformation> members){
        this.members = members
    }

    int getDop(){
        if(degreeOfPooling == null)
            degreeOfPooling = members.first().dop
        return degreeOfPooling
    }

    boolean getCanPool(){
        return members.size() >= dop
    }

    def getIndexToCompare(LibraryInformation member){
        return member.indexSequences
    }

    @Override
    void makePools() {
        int maxPoolCount = members.size()/dop
        def unusedMembers = []
        int startIndex = 0
        for(int i = 0; i < maxPoolCount; i++){
            def poolMembers = []
            while(startIndex < members.size()){
                LibraryInformation member = members[startIndex]
                def usedIndexes = []
                poolMembers.each { LibraryInformation li ->
                    getIndexToCompare(li).each {
                        usedIndexes.addAll(ArtifactIndexService.splitSequences(it))
                    }
                }
                if(getIndexToCompare(member).collect{ArtifactIndexService.splitSequences(it)}.flatten().intersect(usedIndexes)){
                    unusedMembers << member
                }
                else{
                    poolMembers << member
                }
                startIndex++
                if(poolMembers.size() == dop){
                    makePool(poolMembers)
                    startIndex = partialSort(members, unusedMembers)
                    break
                }
            }
        }
    }

    private int partialSort(List members, List unusedMembers) {
        members.removeAll(unusedMembers)
        int startIndex = poolsCount*dop
        startNumber++
        members.addAll(startIndex,unusedMembers)
        unusedMembers.clear()
        startIndex
    }

    private void makePool(def poolMembers){
        if(startNumber == 0)
            startNumber++
        poolMembers.each{
            it.poolNumber = startNumber
        }
        poolsCount++
        poolMembers.clear()
    }

    @Override
    int completePoolPrepBeans(int startPoolNumber){
        if(!canPool)
            return startPoolNumber
        this.startNumber = startPoolNumber
        makePools()
        return startNumber
    }

    String getMessage(){
        return ""
    }

    CellStyle getCellStyle(Sheet sheet){
        def row = sheet.getRow(REGULAR_CELL_ROW)
        def cell = row.getCell(REGULAR_CELL)
        CellStyle cellStyle = cell.getCellStyle()
        return cellStyle
    }

    List getActionBeans(){
        return []
    }

    List getSummaryBeans(){
        return []
    }

    List getCalculationBeans(){
        return []
    }

    Map validate(){
        Map<String,List<String>> findings = [:].withDefault {[]}
        Map<String, List<String>> indexAnalytes = indexMembersMap
        def repIndexes = indexAnalytes.findAll{it.value.size() > 1}
        if(repIndexes)
            findings[ERROR] << "pool #$poolNumber has duplicate indexes: $repIndexes"
        return findings
    }

    int getPoolNumber(){
        members.first().poolNumber
    }

    Map<String, List<LibraryInformation>> getIndexMembersMap(){
        Map<String, List<LibraryInformation>> indexMembers = [:].withDefault {[]}
        members.each{ LibraryInformation li ->
            def indexToCompare = getIndexToCompare(li)
            if(indexToCompare instanceof String)
                indexToCompare = [indexToCompare]
            indexToCompare.each { String index ->
                ArtifactIndexService.splitSequences(index).each {
                    indexMembers[it] << li
                }
            }
        }
        return indexMembers
    }
}
