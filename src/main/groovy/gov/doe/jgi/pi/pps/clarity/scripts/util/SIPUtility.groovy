package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

class SIPUtility {
    static String updateGroupQcResult(List groupSamples, BigDecimal replicatesFailurePercentAllowed, BigDecimal controlsFailurePercentAllowed) {
        int sampleFailCount = 0, controlFailCount = 0, sampleCount = 0, controlCount = 0
        String result = Analyte.PASS
        groupSamples.each { def analyte ->
            if(analyte.isControlSample) {
                controlCount++
                if(!analyte.passedQc)
                    controlFailCount++
            }
            else {
                sampleCount++
                if(!analyte.passedQc)
                    sampleFailCount++
            }
        }
        if(sampleCount > 0) {
            BigDecimal percentFailedSamples = ((BigDecimal) sampleFailCount) / sampleCount * 100
            if (percentFailedSamples > replicatesFailurePercentAllowed)
                result = Analyte.FAIL
        }
        if(controlsFailurePercentAllowed && controlCount > 0) {
            BigDecimal percentFailedControls = ((BigDecimal) controlFailCount) / controlCount * 100
            if (percentFailedControls > controlsFailurePercentAllowed)
                result = Analyte.FAIL
        }
        if(result == Analyte.FAIL) {
            groupSamples.each { def analyte ->
                analyte.passOnGroupFailure()
            }
        }
        return result
    }
}
