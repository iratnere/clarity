package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.config.Action
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.grails.RoutingService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import grails.gorm.transactions.Transactional
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException

@Transactional
class ProcessEditService {

    def grailsApplication
    DataSource dataSource
    JdbcTemplate jdbcTemplate
    RoutingService routingService

    def getTrackerIds(String processId, List<String> artifactLimsIds)
    {
        if(!artifactLimsIds)
            return null
        ClarityWebTransaction.logger.info "Retrieving trackerIds for processId ${processId} and artifactIds ${artifactLimsIds}"
        String artifactId = 'ARTIFACTID'
        String trackerId = 'TRACKER_ID'
        String query = "select p.processid as PROCESS_ID, atf.artifactid as ${artifactId}, pio.trackerid as ${trackerId}" +
                " from processiotracker pio, artifact atf, process p" +
                " where pio.INPUTARTIFACTID=atf.artifactid" +
                " and p.processid=pio.processid" +
                " and atf.luid in ('${artifactLimsIds.join("','")}')" +
                " and p.luid='${processId}'"
        ClarityWebTransaction.logger.info "Executing query ${query}"
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                def obj = [:].withDefault {[]}
                while (rs.next()) {
                    obj[rs.getLong(artifactId)] << rs.getLong(trackerId)
                }
                return obj
            }
        })
    }

    void deleteOutputMappings(List<Long> trackerIds){
        if(!trackerIds)
            return
        ClarityWebTransaction.logger.info "Deleting output mappings records for trackerIds ${trackerIds}"
        String query = "delete from outputmapping where TRACKERID in (${trackerIds.join(',')})"
        ClarityWebTransaction.logger.info "Executing query ${query}"
        jdbcTemplate.execute(query)
    }

    void deleteProcessIOTracker(List<Long> artifactIds, List<Long> trackerIds){
        if(!artifactIds || !trackerIds)
            return
        ClarityWebTransaction.logger.info "Deleting Process IO Tracker records for artifactIds ${artifactIds} and trackerIds ${trackerIds}"
        String query = "delete from processiotracker where INPUTARTIFACTID in (${artifactIds.join(',')}) and trackerid in (${trackerIds.join(',')})"
        ClarityWebTransaction.logger.info "Executing query ${query}"
        jdbcTemplate.execute(query)
    }

    def getTransitionIds(List<Long> artifactIds, String workflowName, String protocolName){
        if(!artifactIds)
            return null
        ClarityWebTransaction.logger.info "Retrieving transitionIds for artifactIds ${artifactIds}, workflowName ${workflowName} and protocolName ${protocolName}"
        String transactionIdStr = 'TRANSACTION_ID'
        String artifactIdStr = 'ARTIFACT_LIMS_ID'
        String query = "select art.luid as ${artifactIdStr}, st.transitionid as ${transactionIdStr} " +
                "from WORKFLOWSECTION ws, labworkflow w, protocolstep ps, processtype pt,stage s, stagetransition st, artifact art  " +
                "where ws.WORKFLOWID = w.WORKFLOWID  " +
                "and ws.PROTOCOLID=ps.PROTOCOLID " +
                "and pt.typeid=ps.PROCESSTYPEID " +
                "and s.MEMBERSHIPID=ws.SECTIONID " +
                "and st.STAGEID=s.stageid " +
                "and w.WORKFLOWNAME='${workflowName}' " +
                "and pt.displayname='${protocolName}' " +
                "and st.ARTIFACTID = art.ARTIFACTID "+
                "and st.artifactid in (${artifactIds.join(',')})"
        ClarityWebTransaction.logger.info "Executing query ${query}"
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                def obj = [:].withDefault {[]}
                while (rs.next()) {
                    obj[rs.getString(artifactIdStr)] << rs.getLong(transactionIdStr)
                }
                return obj
            }
        })
    }

    void updateStageTransition(List<Long> transitionIds, long actionId){
        if(!transitionIds)
            return
        ClarityWebTransaction.logger.info "Updating state transition records for trackerIds ${transitionIds} to ${actionId}"
        String query = "update stagetransition " +
                "set actionid = ${actionId} " +
                "where transitionid in (${transitionIds.join(',')})"
        ClarityWebTransaction.logger.info "Executing query ${query}"
        jdbcTemplate.execute(query)
    }

    void routeToSequencingQueue(ClarityProcess process, def analytes){
        Map<String, List<String>> routeMap = [:].withDefault {[]}
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        List<RoutingRequest> requests = []
        requests.addAll(RoutingRequest.generateRequestsToRouteArtifactIdsToUri(ClarityWorkflow.POOL_CREATION.uri,analytes*.id,Routing.Action.unassign))
        analytes?.each{ ArtifactNode artifactNode ->
            Analyte analyte = AnalyteFactory.analyteInstance(artifactNode)
            //analyte.removeFromWorkflow(ClarityWorkflow.POOL_CREATION)
            routeMap[process.getUriToRoute(analyte.runModeCv)] << analyte.artifactNode.id
        }
        routeMap.each{String stageUri, List<String> poolIds ->
            ClarityWebTransaction.logger.info "Routing Analytes ${poolIds} to ${stageUri}"
            requests.addAll(RoutingRequest.generateRequestsToRouteArtifactIdsToUri(stageUri, poolIds))
        }
        routingService.submitRoutingRequests(requests)
    }

    def editProcess(ClarityProcess process, List<Analyte> repeatAnalytes, List<Analyte> continueAnalytes, Stage stage){
        if(!repeatAnalytes && !continueAnalytes)
            return
        jdbcTemplate = new JdbcTemplate(dataSource)
        jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);
        try {
            List<String> repeatAnalyteIds = repeatAnalytes?.collect { it.id }
            List<String> continueAnalyteIds = continueAnalytes?.collect { it.id }
            def analyteIds = repeatAnalyteIds
            analyteIds.addAll(continueAnalyteIds)
            ClarityWebTransaction.logger.info "Routing analytes ${continueAnalyteIds} to Sequencing queue"
            routeToSequencingQueue(process, continueAnalytes)
            def trackerIds = getTrackerIds(process.processNode.id, analyteIds)
            ClarityWebTransaction.logger.info "Found trackerIds ${trackerIds}"
            def transactionIds = getTransitionIds(trackerIds.keySet() as List, stage.workflow.value, process.stepsNode.protocolNode.name)
            ClarityWebTransaction.logger.info "Found transactionIds ${transactionIds}"

            def updateTranstionIds = []
            repeatAnalyteIds?.each { String limsId ->
                updateTranstionIds.addAll(transactionIds[limsId])
            }
            updateStageTransition(updateTranstionIds, Action.REPEAT.actionId)
            updateTranstionIds = []
            continueAnalyteIds?.each { String limsId ->
                updateTranstionIds.addAll(transactionIds[limsId])
            }
            updateStageTransition(updateTranstionIds, Action.STEPACTION.actionId)
            deleteOutputMappings(trackerIds?.values()?.flatten())
            deleteProcessIOTracker(trackerIds?.keySet() as List, trackerIds?.values().flatten())
        }
        catch(Exception e){
            jdbcTemplate.getDataSource().connection.rollback()
            process.postErrorMessage(e.message)
            return
        }
        jdbcTemplate.getDataSource().connection.commit()
    }
}
