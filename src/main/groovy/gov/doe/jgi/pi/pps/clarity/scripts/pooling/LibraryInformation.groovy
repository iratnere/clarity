package gov.doe.jgi.pi.pps.clarity.scripts.pooling

import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv
import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import org.apache.poi.ss.usermodel.CellStyle

class LibraryInformation {
    @FieldMapping(header = 'Analyte Lims ID', cellType = CellTypeEnum.STRING)
    public def analyteLimsId
    @FieldMapping(header = 'Container ID', cellType = CellTypeEnum.STRING)
    public def containerId
    @FieldMapping(header = 'Library Name', cellType = CellTypeEnum.STRING)
    public def libraryName
    @FieldMapping(header = 'Queue Date', cellType = CellTypeEnum.DATE)
    public def queueDate
    @FieldMapping(header = 'DOP', cellType = CellTypeEnum.NUMBER)
    public def dop
    @FieldMapping(header = 'Index', cellType = CellTypeEnum.STRING)
    public def indexName
    @FieldMapping(header = 'Platform', cellType = CellTypeEnum.STRING)
    public def platform
    @FieldMapping(header = 'Sequencer Model', cellType = CellTypeEnum.STRING)
    public def sequencerModel
    @FieldMapping(header = 'Run Mode', cellType = CellTypeEnum.STRING)
    public def runMode
    @FieldMapping(header = 'Material Type', cellType = CellTypeEnum.STRING)
    public def materialType
    @FieldMapping(header = 'Amplified', cellType = CellTypeEnum.STRING)
    public def amplified
    @FieldMapping(header = 'Library Molarity (pm)', cellType = CellTypeEnum.NUMBER)
    public def concentration
    @FieldMapping(header = 'Library Percentage with SOF (%)', cellType = CellTypeEnum.NUMBER)
    public def libraryPercentage
    @FieldMapping(header = 'Pool Number', cellType = CellTypeEnum.NUMBER, isCustomCellStyle = true)
    public def poolNumber
    @FieldMapping(header = 'Plate Map', cellType = CellTypeEnum.STRING)
    public def plateMap

    CellStyle customCellStyle
    public String indexSequence
    private String indexSeqBits
    public Analyte analyte
    public String plateLocation
    public def allIndexes
    private String poolKey = null
    private String validKey = null
    public BigDecimal threshold

    ArtifactIndexService service

    public static final String I_TAG = 'iTag'
    public static final String EXOME_CAPTURE = 'ExomeCapture'
    public static final String DUAL_INDEX = 'isDual'
    public static final String INTERNAL_SINGLE_CELL = 'InternalSingleCell'
    public static final String FRACTIONAL_LANE = 'FractionalLane'
    public static final String INDEX_NAME_SUFFIX = "IndexName_${Analyte.INDEX_NAME_NA}"
    public static final String PACBIO = 'PacBio'
    public static final String POOL_BY_DOP = 'PoolByDOP'

    void populateBean(Analyte analyte, ClaritySample sample, ArtifactIndexService artifactIndexService) {
        analyteLimsId = analyte.id
        containerId = analyte.containerId
        libraryName = analyte.name
        def dop = analyte.udfDegreeOfPooling
        this.dop = dop ? dop : null
        platform = analyte.platform
        sequencerModel = analyte.sequencer
        runMode = analyte.udfRunMode
        materialType = sample.sequencingProject.udfMaterialCategory
        concentration = analyte.udfLibraryMolarityPm
        this.analyte = analyte
        setIndexes(getIndexName(analyte, artifactIndexService))
        amplified = getAmplified(analyte)
        plateMap = getPlateLabel(analyte)
    }

    def getAmplified(Analyte analyte) {
        Analyte poolMember = analyte
        if (analyte instanceof ClarityLibraryPool) {
            def poolMembers = analyte.poolMembers
            if (poolMembers.size() > 0)
                poolMember = poolMembers.first()
        }
        def pcrCycles = poolMember.udfNumberPcrCycles
        String amplified = 'N'
        if (pcrCycles > 0)
            amplified = 'Y'
        amplified
    }

    String getPlateLabel(analyte) {
        if (analyte instanceof ClarityLibraryStock)
            return analyte.isOnPlate ? analyte.containerUdfLabel : ''
        if ((analyte instanceof ClarityLibraryPool) && analyte.isExomeCapture) {
            return analyte.poolMembers[0].containerUdfLabel
        }
        return ''
    }

    static List<String> getIndexName(Analyte analyte, ArtifactIndexService artifactIndexService) {
        List<String> indexName = []
        if (analyte instanceof ClarityLibraryStock)
            indexName << analyte.udfIndexName
        if (analyte instanceof ClarityLibraryPool && analyte.getIsItag()) {
            def poolMembers = analyte.poolMembers
            Analyte poolMember
            if (poolMembers.size() > 0)
                poolMember = poolMembers.first()
            def indexId = poolMember?.udfIndexName
            if (!artifactIndexService)
                artifactIndexService = (ArtifactIndexService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(ArtifactIndexService)
            indexName << artifactIndexService.getIndexSets(indexId)?.replace('iTag_Set', '')
        }
        if (analyte instanceof ClarityLibraryPool) {
            def poolMembers = analyte.poolMembers
            poolMembers?.each { poolMember ->
                indexName << poolMember?.udfIndexName
            }
        }
        return indexName
    }

    void setIndexes(List<String> indexes) {
        indexName = indexes?.size() == 1 ? indexes[0] : ''
        indexes.removeAll(Analyte.INDEX_NAME_NA)
        indexes.removeAll('')
        allIndexes = indexes
    }

    String getPoolingKey(){
        if(!poolKey) {
            if ((analyte?.isInternalSingleCell) && (analyte instanceof ClarityLibraryStock))
                return "${INTERNAL_SINGLE_CELL} ${analyte.claritySample.udfPoolNumber} ${analyte.containerId} ${SowItemTypeCv.SINGLE_CELL_INTERNAL.value}"
            StringBuilder sb = new StringBuilder()
            String delimiter = ' '
            if(!isNovaSeq) {
                if(analyte instanceof ClarityLibraryPool)
                    sb.append(POOL_BY_DOP).append(delimiter) //PPS-5253 - Pooling suggestions are not made while pooling pools by DOP
                else
                    sb.append(dop).append(delimiter)
            }
            sb.append(validationKey)
            poolKey = sb.toString()
        }
        return poolKey
    }

    boolean getIsNovaSeq(){
        return sequencerModel?.contains(SequencerModelTypeCv.NOVASEQ.value)
    }

    boolean getIsPacBio() {
        return PlatformTypeCv.PACBIO.value.equals(platform)
    }

    String getValidationKey() {
        if(!validKey) {
            StringBuilder sb = new StringBuilder()
            String delimiter = ' '
            if ((analyte?.isInternalSingleCell) && (analyte instanceof ClarityLibraryStock)) {
                sb.append(INTERNAL_SINGLE_CELL)
                if(indexName && indexSequence)
                    sb.append(indexName?"${delimiter}index length-${indexSequence?.length()}":"")
                return sb.toString()
            }
            if(isNovaSeq)
                sb.append(FRACTIONAL_LANE).append(delimiter)
            sb.append(platform).append(delimiter)
            sb.append(sequencerModel).append(delimiter).append(runMode).append(delimiter)
            if (isItag)
                sb.append(I_TAG).append(delimiter)
            if (isExomeCapture)
                sb.append(EXOME_CAPTURE).append(delimiter)
            //PPS-1567 Need not verify dual color rule for iTag and exome pool of pools
            if (indexName) {
                if (indexName == Analyte.INDEX_NAME_NA)
                    sb.append("${INDEX_NAME_SUFFIX}").append(delimiter)
                //PPS-4112 Do not rely on Index name to identify Dual indexes
                else if (isDualIndex)
                    sb.append(DUAL_INDEX).append(delimiter)
                if(!isPacBio) {
                    //PPS-4785 -> Libraries with different index lengths should not be pooled together
                    sb.append("index length-${ArtifactIndexService.splitSequences(indexSequence)[0]?.length()}").append(delimiter)
                }

            }
            else {
                if(analyte instanceof ClarityLibraryPool && allIndexes instanceof Map){
                    String iName = allIndexes.keySet().first()
                    String iSequence = allIndexes[iName]
                    if(getIsDualIndex(iName, iSequence)){
                        sb.append(DUAL_INDEX).append(delimiter)
                    }
                    sb.append("index length-${iSequence?.length()}").append(delimiter)
                }
            }
            if(isPacBio)
                sb.append(PACBIO)
            validKey = sb.toString()
        }
        return validKey
    }

    public LibraryInformation() {}

    public LibraryInformation(String indexName) {
        ArtifactIndexService service = ClarityWebTransaction.currentTransaction.requireApplicationBean(ArtifactIndexService.class)
        indexSequence = service.getSequences([indexName])[indexName]
        if (!indexSequence)
            throw new RuntimeException("Index Id $indexName not valid")
        this.indexName = indexName
    }

    public boolean getIsDualIndex(String indexName=this.indexName, String indexSequence=this.indexSequence) {
        //PPS-4632 - dual color validation" in PacBio pooling should be removed
        if (isItag)
            return false
        if (!service)
            service = ClarityWebTransaction.currentTransaction.requireApplicationBean(ArtifactIndexService.class)
        return service.isDualIndex(indexName, indexSequence)
    }

    public List<String> getIndexNames() {
        if (analyte?.isItag)
            return []
        if (indexName && indexName != Analyte.INDEX_NAME_NA)
            return [indexName]
        if (!indexName) {
            if (allIndexes instanceof Map)
                return allIndexes.keySet() as List
            allIndexes.removeAll("$Analyte.INDEX_NAME_NA")
            allIndexes.removeAll("A")
            allIndexes.removeAll("B")
            allIndexes.removeAll('')
            return allIndexes
        }
        return []
    }

    public List<String> getIndexSequences() {
        if(!allIndexes) {
            if(indexSequence)
                return [indexSequence]
            ArtifactIndexService artifactIndexService = ClarityWebTransaction.currentTransaction?.requireApplicationBean(ArtifactIndexService.class)
            Map sequences = artifactIndexService?.getSequences(indexNames)
            return sequences? sequences.values() as List : []
        }
        if (allIndexes instanceof Map)
            return allIndexes.values() as List
        return []
    }

    public void setUpIndexes(Map<String, String> indexMap) {
        List<String> indexes
        if(allIndexes){
            if(allIndexes instanceof Map)
                indexes = allIndexes.keySet() as List
            else
                indexes = allIndexes
        } else
            indexes = getIndexNames()
        allIndexes = [:]
        indexes.each { String index ->
            allIndexes[index] = indexMap[index]
        }
        if (indexName && indexName != Analyte.INDEX_NAME_NA)
            indexSequence = indexMap[indexName]
    }

    public boolean getIsInternalSingleCell() {
        //To make the tests work
        return analyte?.isInternalSingleCell
    }

    public boolean getIsItag() {
        //To make the tests work
        return analyte?.isItag
    }

    public boolean getIsExomeCapture() {
        //To make the tests work
        return analyte?.isExomeCapture
    }

    public boolean isPacBio() {
        return poolingKey.contains(PACBIO)
    }

    public void passOnLibraryPercentageSOF() {
        //PPS-4708 - Pool fraction needs to be carried over to the pool creation worksheet
        if (libraryPercentage != null && analyte) {
            analyte.artifactNode.readOnly = false
            analyte.udfLibraryPercentageWithSOF = libraryPercentage
        }
    }

    String getIndexSequenceBits() {
        if (!indexSeqBits) {
            indexSeqBits = ArtifactIndexService.splitSequences(indexSequence).join("+")
            indexSeqBits = indexSeqBits.replaceAll("-", "")
            indexSeqBits = indexSeqBits.replaceAll("A|C", "0")
            indexSeqBits = indexSeqBits.replaceAll("G|T", "1")
        }
        return indexSeqBits
    }

    boolean checkValueAt(int value, int index) {
        String[] indexBitsSplit = indexSequenceBits.split("\\+")
        return indexBitsSplit.collect{"${it.charAt(index)}"}.contains("$value")
    }

    String toString() {
        return "$libraryName\t$indexName\t$indexSequence"
    }

    String toErrorMessage() {
        return "$libraryName/$analyteLimsId"
    }

    boolean getIsSAGPool() {
        return (analyte instanceof ClarityLibraryPool && analyte.isSAG)
    }
}
