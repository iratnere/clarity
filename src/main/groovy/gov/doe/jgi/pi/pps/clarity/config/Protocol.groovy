package gov.doe.jgi.pi.pps.clarity.config

import gov.doe.jgi.pi.pps.clarity_node_manager.util.HttpUtils
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import gov.doe.jgi.pi.pps.util.util.OnDemandCacheMapped

enum Protocol {
	SQ_SEQUENCING_ILLUMINA_MISEQ('SQ Sequencing Illumina MiSeq'),
	SQ_SEQUENCING_ILLUMINA_HISEQ_2500('SQ Sequencing Illumina HiSeq 2500'),
	SQ_SEQUENCING_ILLUMINA_NEXTSEQ('SQ Sequencing Illumina NextSeq'),
	SQ_CLUSTER_GENERATION_HISEQ_HO('SQ Cluster Generation HiSeq-HO'),
	SQ_CLUSTER_GENERATION_1TB('SQ Cluster Generation 1Tb'),

	final String value
	
	Protocol(String value) {
		this.value = value
	}
	
	private static OnDemandCacheMapped<String, Map<String, String>> cachedProtocolUris = new OnDemandCacheMapped<>()
	private OnDemandCacheMapped<String, Node> cachedProtocolNodes = new OnDemandCacheMapped<>()
	private static OnDemandCache<Map<String, Protocol>> cachedValueMap = new OnDemandCache<Map<String, Protocol>>()
	
	static Map<String, Protocol> getValueMap() {
		return cachedValueMap.fetch {
			Protocol.values().inject([:]){ Map<String, Protocol> nameProtocol, Protocol protocol ->
				if (nameProtocol.containsKey(protocol.value)) {
					throw new RuntimeException("Configuration error: duplicate name \"${protocol.value}\" for protocol ${nameProtocol[protocol.value].name()} and ${protocol.name()}.")
				}
				nameProtocol[protocol.value] = protocol
				nameProtocol
			}
		}
	}
	
	static Protocol toEnum(String value) {
		return valueMap[value]
	}
	
	static Map<String, String> protocolUris(NodeConfig nodeConfig) {
		return cachedProtocolUris.fetch(nodeConfig.configurationProtocolsUrl) { String protocolsListUrl ->
            Node workflowsListNode = HttpUtils.httpGetAsNode(nodeConfig, protocolsListUrl)
			Map<String,String> namesToUris = [:]
			workflowsListNode.'protocol'.each {
			   namesToUris[it.@name] = it.@uri
			}
			return namesToUris
		}
	}
	
	String uri(NodeConfig nodeConfig) {
		return protocolUris(nodeConfig)?.get(value)
	}

    Node node(NodeConfig nodeConfig) {
		cachedProtocolNodes.fetch(uri(nodeConfig)) { String protocolUri ->
			HttpUtils.httpGetAsNode(nodeConfig, protocolUri)
		}
	}
	
	List<Node> stepNodes(NodeConfig nodeConfig)	{
		node(nodeConfig)?.'steps'?.'step'
	}

}
/*
<protcnf:protocol uri="http://frow.jgi-psf.org:8080/api/v2/configuration/protocols/5" index="7" name="LC Illumina Exome Capture">
  <steps>
    <step protocol-uri="http://frow.jgi-psf.org:8080/api/v2/configuration/protocols/5" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/protocols/5/steps/11" name="LC Library Creation">
      <protocol-step-index>1</protocol-step-index>
      <process-type uri="http://frow.jgi-psf.org:8080/api/v2/processtypes/33">LC Library Creation</process-type>
      <permitted-containers>
        <container-type>96 well plate</container-type>
        <container-type>Tube</container-type>
      </permitted-containers>
      <permitted-reagent-categories/>
      ...
 */