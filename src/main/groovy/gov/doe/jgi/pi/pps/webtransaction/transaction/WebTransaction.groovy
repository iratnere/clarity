package gov.doe.jgi.pi.pps.webtransaction.transaction

import gov.doe.jgi.pi.pps.webtransaction.util.ExternalId
import grails.core.GrailsApplication
import grails.validation.ValidationErrors
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONElement
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.event.LoggingEvent
import org.springframework.context.MessageSource
import org.springframework.validation.Errors

interface WebTransaction {

	static final String STATUS = 'status'
	static final String WEB_TRANSACTION = 'web-transaction'

	static final String START_TIME = 'start-time'
	static final String END_TIME = 'end-time'
	static final String START_TIME_ARRAY = 'start-time-array'
	static final String END_TIME_ARRAY = 'end-time-array'
	static final String TRANSACTION_ID = 'transaction-id'
	static final String EXTERNAL_ID = 'external-id'
	static final String dateFormatString = 'yyyy-MM-dd HH:mm:ss z'

    JSONElement getJsonSubmission()
	void setJsonSubmission(JSONElement jsonElement)

    JSONObject getJsonResponse()
	void generateJsonResponse()

    JSONObject getLogData()

	boolean getOpened()
	void open()
	boolean getClosed()
	void close()

    JSONArray getMessages()

    JSONArray getErrorMessages()

    JSONArray getLoggerMessages()
    List<LoggingEvent> getLoggingEvents()
	void addLoggingEvent(LoggingEvent loggingEvent)

    ConfigObject getData()

	Map<String,String> getWebTransactionHeaders()

	String getAction()
	void setAction(String action)

	boolean getLoggingEnabled()
	void setLoggingEnabled(boolean enabled)

    boolean getExtractJsonSubmission()
	void setExtractJsonSubmission(boolean doExtract)

	WebTransactionRecorder getWebTransactionRecorder()
	void setWebTransactionRecorder(WebTransactionRecorder webTransactionRecorder)

	String getTransactionId()

	ExternalId getExternalId()
	void setExternalId(ExternalId externalId)

	Date getStartTime()
	void setStartTime(Date date)

    Date getEndTime()
	void setEndTime(Date date)

	String getStartTimeString()
	String getEndTimeString()

    Integer getStatusCode()
	void setStatusCode(Integer statusCode)

	String getSubmittedBy()
	void setSubmittedBy(String submittedBy)

    GrailsApplication getGrailsApplication()
	void setGrailsApplication(GrailsApplication grailsApplication)

    MessageSource getMessageSource()
	void setMessageSource(MessageSource messageSource)

	Locale getLocale()
	void setLocale(Locale locale)

	boolean getTestMode()

    WebTransaction setTestMode()

    WebTransaction addChildSession(String key, WebTransactionChildSession childSession)
	WebTransactionChildSession getChildSession(String key)
	WebTransactionChildSession requireChildSession(String key)
	Map<String,WebTransactionChildSession> getChildSessions()
	WebTransactionChildSession removeChildSession(String key)

	void exit()
	void exit(Integer statusCode)
	String getLoggerName()

    Logger getTransactionLogger()

	void addErrorMessage(Object message)
	void addErrorMessage(Object message, String defaultMessage)
	void addErrorMessages(Iterable messages)

	void addExceptionErrorMessage(Throwable t)
	void addExceptionErrorMessage(Throwable t, Map msgMap)
    void addExceptionErrorMessage(Throwable t, Map msgMap, Integer statusCode)

	void addValidationErrors(ValidationErrors errors)
	void addValidationErrors(ValidationErrors errors, Map msgMap)

	void addErrors(Errors errors)
	void addErrors(Errors errors, Map msgMap)

	void exitWithErrorMessage(Object message)
	void exitWithErrorMessage(Object message, Integer statusCode)

    WebTransaction performTransaction(Closure action)

    WebTransaction performTransaction(WebTransactionRecorder webTransactionRecorder, Closure action)

	Object getApplicationBean(Class beanClass)
	Object requireApplicationBean(Class beanClass)

	
}
