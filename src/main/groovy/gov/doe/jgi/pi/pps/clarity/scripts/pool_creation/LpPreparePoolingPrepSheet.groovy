package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * This class is meant to make pooling suggestions and populate pre-pooling sheet accordingly
 * for requirements refer 4b of the Clarity Library Pooling Requirements document
 * https://docs.google.com/a/lbl.gov/document/d/1itCb6lXyP_IM0Fl-o-8tMDdsVq-WgdmekOmN53JMfg4/edit#
 *
 * Created of the ticket PPS-1769
 * Created by lvishwas on 12/18/2014.
 */
class LpPreparePoolingPrepSheet extends ActionHandler {

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        PoolCreation poolCreation = process as PoolCreation
        poolCreation.threshold = PoolCreation.DEFAULT_THRESHOLD
        //populating the pooling prep worksheet and uploading the same
        def fileNode = processNode.outputResultFiles.find{
            it.name.contains(PoolCreation.SCRIPT_GENERATED_POOLING_PREP_SHEET)
        }
        poolCreation.uploadPoolingPrepSheet(poolCreation.sortedBeans, fileNode)
    }

}