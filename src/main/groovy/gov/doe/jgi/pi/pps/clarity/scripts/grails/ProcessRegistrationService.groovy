package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import grails.util.GrailsUtil
import org.reflections.Reflections
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.lang.reflect.Constructor

@Transactional
class ProcessRegistrationService {

    Logger logger = LoggerFactory.getLogger(ProcessRegistrationService.class.name)

    static final reflectionsBasePackage = 'gov.doe.jgi.pi.pps.clarity'

    private final Map<ProcessType,Constructor<ClarityProcess>> registeredProcesses = [:]

    Constructor<ClarityProcess> processConstructor(ProcessType processType) {
        synchronized (registeredProcesses) {
            return registeredProcesses[processType]
        }
    }

    void registerAllProcesses() {
        Reflections reflections = new Reflections(reflectionsBasePackage)
        reflections.getSubTypesOf(ClarityProcess.class).sort{it.simpleName.toLowerCase()}.each { Class<? extends ClarityProcess> processClass ->
            logger.warn "registering process: ${processClass.name}"
            try {
                registerProcessClass(processClass)
            } catch (t) {
                logger.error "error registering: ${processClass.name}", GrailsUtil.sanitizeRootCause(t)
            }
        }
    }

    void registerProcessClass(Class<? extends ClarityProcess> processClass) {
        if (!processClass) {
            logger.error "NULL process class for registration"
            return
        }
        if (processClass.processTypes) {
            processClass.processTypes?.each { ProcessType processType ->
                registerProcess(processType, processClass)
            }
        } else {
            registerProcess(processClass?.processType,processClass)
        }
    }

    private void registerProcess(ProcessType processType, Class<ClarityProcess> clarityProcessClass) {
        if (!processType) {
            logger.error "NULL process type for registration of class ${clarityProcessClass.name}"
            return
        }
        synchronized (registeredProcesses) {
            Constructor<ClarityProcess> constructor = clarityProcessClass?.getConstructor(ProcessNode)
            if (!constructor) {
                throw new RuntimeException("no constructor obtained for ${processType.name()} [${processType.value}] from process class [${clarityProcessClass?.name}]")
            }
            Constructor<ClarityProcess> existingConstructor = registeredProcesses[processType]
            if (existingConstructor && existingConstructor.declaringClass != constructor.declaringClass) {
                throw new RuntimeException("unable to register [${clarityProcessClass.name}]: process type ${processType.name()} [${processType}] is already associated with process [${existingConstructor.declaringClass.name}]")
            }
            registeredProcesses[processType] = constructor
        }
    }

    void logRegisteredProcesses() {
        registeredProcesses.findAll(){it.key}.sort{it.key.value.toLowerCase()}.each{ ProcessType processType, Constructor constructor ->
            logger.warn "REGISTERED PROCESS ${processType.name()} [${processType.value}] : [${constructor?.declaringClass?.simpleName}]"
        }
    }

    void outputRegisteredProcesses(OutputStream os) {
        registeredProcesses.findAll(){it.key}.sort{it.key.value.toLowerCase()}.each{ ProcessType processType, Constructor constructor ->
            os << "REGISTERED PROCESS ${processType.name()} [${processType.value}] : [${constructor?.declaringClass?.simpleName}]\n"
        }
    }

    String outputRegisteredProcesses() {
        StringBuilder sb = new StringBuilder()
        List<ProcessType> processTypes = registeredProcesses.keySet().sort{it.value.toLowerCase()}
        int maxProcessNameLength = Collections.max(processTypes.collect{it.value.size()})
        processTypes.each{ ProcessType processType ->
            Constructor constructor = registeredProcesses[processType]
            sb << "${processType.value} "
            sb << '.' * (maxProcessNameLength - processType.value.size())
            sb << "... ${constructor?.declaringClass?.simpleName}\n"
        }
        sb.toString()
    }
}
