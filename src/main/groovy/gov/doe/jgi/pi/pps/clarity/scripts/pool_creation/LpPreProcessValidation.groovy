package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPoolLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * Created by lvishwas on 2/27/15.
 */
class LpPreProcessValidation extends ActionHandler {

    String validateInputType(Analyte analyte){
        if(!(analyte instanceof ClarityLibraryStock) && !(analyte instanceof ClarityLibraryPool))
            return "Invalid input ${analyte}. Expecting only Library Stocks and Library Pools as input.\n"
        if(analyte instanceof ClarityPoolLibraryPool)
            return "Invalid input ${analyte}. Pooling does not yet support pools of pools as inputs.\n"
        return ""
    }

    String validateInputAttributes(Analyte analyte){
        if(analyte instanceof ClarityLibraryPool)
            return ""
        ClarityLibraryStock library = (ClarityLibraryStock) analyte
        String indexName = library.udfIndexName
        if(!indexName || indexName == ArtifactIndexService.INDEX_NAME_N_A)
            return "Invalid input ${analyte}. Only library stocks with valid index can be pooled.\n"
        return ""
    }

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        String errorMsg = ''
        process.inputAnalytes.each{ Analyte analyte ->
            errorMsg += validateInputType(analyte)
            errorMsg += validateInputAttributes(analyte)
        }
        if(errorMsg){
            process.postErrorMessage(errorMsg)
        }
    }
}
