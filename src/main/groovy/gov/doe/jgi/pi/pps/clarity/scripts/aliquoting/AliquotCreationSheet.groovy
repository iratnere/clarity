package gov.doe.jgi.pi.pps.clarity.scripts.aliquoting

import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ClaritySampleAnalyteComparator
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager

class AliquotCreationSheet {
    static final String TEMPLATE_NAME = 'resources/templates/excel/SampleAliquotStarlet'
    protected final static Comparator COMPARATOR = new ClaritySampleAnalyteComparator()

    NodeManager nodeManager
    AliquotCreationSheetParams details
    boolean testMode

    AliquotCreationSheet(AliquotCreationSheetParams params, ClarityProcess process) {
        this.details = params
        this.nodeManager = process.nodeManager
        this.testMode = process.testMode
    }

    ExcelWorkbook getAliquotCreationSheet(){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(TEMPLATE_NAME, testMode)
        excelWorkbook.addSection(tableSection)
        details.outputPlateContainer ? excelWorkbook.addSection(keyValuePlateResultSection) : null
        return excelWorkbook
    }

    Section getKeyValuePlateResultSection() {
        Section kvSection = new KeyValueSection(1,AcKeyValuePlateResultsBean.class, 'A7','B18')
        AcKeyValuePlateResultsBean bean = acKeyValuePlateResultsBean
        kvSection.setData(bean)
        return kvSection
    }

    AcKeyValuePlateResultsBean getAcKeyValuePlateResultsBean(LibraryCreationQueueCv queue = details.plateLcQueue) {
        AcKeyValuePlateResultsBean bean = new AcKeyValuePlateResultsBean()
        bean.aliquotStatus = details.dropDownPassFail
        bean.targetAliquotMassNg = queue ? queue.plateTargetMassLibTrialNg : 0.0 //custom aliquots have no queue
        bean.totalVolumeUl = queue ? queue.plateTargetVolumeLibTrialUl : 0.0
        bean
    }

    Section getTableSection(def beans = aliquotCreationBeans) {
        Section tableSection = new TableSection(0,SampleAnalyteTableBean.class, 'A3',null, true)
        beans.sort{a,b -> COMPARATOR.compare(a.sampleAnalyte, b.sampleAnalyte)}
        tableSection.setData(beans)
        return tableSection
    }

    List<SampleAnalyteTableBean> getAliquotCreationBeans(){
        List<FreezerContainer> freezerContainers = details.freezerContainers
        List<SampleAnalyteTableBean> tableBeans = []
        Boolean isPlate = details.outputPlateContainer
        details.analytes.each{ Analyte analyte ->
            SampleAnalyte sampleAnalyte = analyte.parentAnalyte
            PmoSample rootSample = analyte instanceof SampleQcHamiltonAnalyte?sampleAnalyte.claritySample:analyte.parentPmoSamples.find{ true }
            ScheduledSample scheduledSample = analyte instanceof SampleQcHamiltonAnalyte? rootSample.scheduledSamples.first(): (ScheduledSample) sampleAnalyte.claritySample
            LibraryCreationQueueCv lcQueue = isPlate ? details.plateLcQueue : analyte.libraryCreationQueue
            def freezerContainer = freezerContainers?.find { FreezerContainer fc -> fc.barcode == sampleAnalyte.containerNode.name}
            SampleAnalyteTableBean bean = new SampleAnalyteTableBean()
            bean.freezerPath = freezerContainer?.location
            bean.sampleId = scheduledSample?.name
            bean.sampleLimsId = rootSample?.id
            bean.sowItemId = scheduledSample?.sowItemId
            bean.sourceBarcode = rootSample?.sampleArtifactNode?.containerNode?.name
            bean.sourceLabware = sampleAnalyte?.containerNode.containerType
            bean.sourceLocation = sampleAnalyte?.worksheetSourceLocation
            bean.sampleConcentrationNgUl = rootSample?.udfConcentration
            bean.initialSampleVolumeUl = rootSample?.udfVolumeUl
            bean.libraryQueue = lcQueue?.libraryCreationQueue
            bean.hmwgDnaEval = rootSample?.udfSampleHMWgDNAYN
            bean.qcComments = rootSample?.udfSampleQcNotes
            bean.destinationBarcode = isPlate ? analyte.containerNode?.id : ''
            bean.destinationLabware = analyte.containerNode.containerType
            bean.destinationLocation = isPlate ? analyte.artifactNode.location?.minus(':') : null
            bean.targetAliquotMassNg = scheduledSample.udfTargetAliquotMassNg
            bean.claimant = details.researcher?.fullName
            bean.freezerStatus = freezerContainer?.checkInStatus
            bean.smQcInstruction = scheduledSample.udfSmInstructions
            bean.smNotes = scheduledSample.udfNotes
            def finalAliquotVolumeUl = getFinalAliquotVolumeUl(lcQueue)
            bean.targetMaxVolumeUl = finalAliquotVolumeUl
            bean.finalAliquotVolumeUl = finalAliquotVolumeUl
            bean.aliquotLimsId = analyte.id
            bean.failureMode = details.dropDownFailureModes
            bean.aliquotStatus = details.dropDownPassFail
            bean.sampleAnalyte = sampleAnalyte
            tableBeans << bean
        }
        tableBeans
    }

    def getFinalAliquotVolumeUl(LibraryCreationQueueCv lcQueue) {
        if (!lcQueue)
            return 0.0 //custom aliquots
        if (details.outputPlateContainer)
            return lcQueue.plateTargetVolumeLibTrialUl
        else
            return lcQueue.tubeTargetVolumeLibTrialUl
    }
}
