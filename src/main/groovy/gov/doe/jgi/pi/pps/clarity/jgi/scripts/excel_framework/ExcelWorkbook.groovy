package gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework

import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.FilePoster
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.apache.poi.hssf.model.InternalSheet
import org.apache.poi.hssf.record.DVRecord
import org.apache.poi.hssf.record.aggregates.DataValidityTable
import org.apache.poi.hssf.usermodel.HSSFEvaluationWorkbook
import org.apache.poi.hssf.usermodel.HSSFOptimiser
import org.apache.poi.ss.formula.EvaluationWorkbook
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddressList
import org.apache.poi.ss.util.CellReference

import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.util.logging.Logger

/**
 * This class represents excel workbook. This class holds all the sections that a workbook consists of...
 * This class can be used to both store and load data from/to excel file. 
 * 
 *  The user has to create an instance of the ExcelWorkbook, add all the sections that have to be updated in the file
 *  Then call store/load method on the instance
 * @author lvishwas
 */
class ExcelWorkbook {
	Workbook workbook //POI workbook instance, holds the input workbook
	Map<String, Section> sections // holds the list of sections of the workbook
	String templateName
	boolean testMode
	static final Logger logger = Logger.getLogger(ExcelWorkbook.class.name)
	private static final String RESOURCES_FOLDER = "../../scripts-plugin/src/resources/templates/excel"
	Map<String[], Cell> dataValidations = [:]
	
	/**
	 * This class write the excel configuration sheet while writing excel file  
	 * reads the same while loading the excel sheet
	 * 
	 * @author lvishwas
	 */
	class ExcelConfig{

		static final String EXCEL_CONFIG_SHEET = 'Excel Configuration'
		static final String MESSAGE = 'Please do not modify/remove this sheet'
		static final CellReference MESSAGE_CELL = new CellReference('A1')
		static final CellReference CONFIG_CELL = new CellReference('A2')
		static final String SECTION_DELIM = ";"
		static final String FIELD_DELIM = ","
		static final int SECTION_TYPE_INDEX = 0
		static final int BEAN_CLASS_INDEX = 1
		static final int CELL_TYPE_INDEX = 2
		static final int SHEET_NUMBER_INDEX = 3
		static final int FIRST_CELL_INDEX = 4
		static final int LAST_CELL_INDEX = 5

		void writeExcelConfig(){
			Sheet sheet = ExcelWorkbook.this.getSheet(EXCEL_CONFIG_SHEET)
			if(!sheet) {
                ExcelWorkbook.this.addSheet(EXCEL_CONFIG_SHEET)
                def configSheetIndex = ExcelWorkbook.this.getSheetIndex(EXCEL_CONFIG_SHEET)
                ExcelWorkbook.this.workbook.setSheetVisibility(configSheetIndex, SheetVisibility.HIDDEN)
            }
			sheet = ExcelWorkbook.this.getSheet(EXCEL_CONFIG_SHEET)
			writeMessage(sheet)
			writeConfig(sheet)			
		}
		
		void readExcelConfig(){
			Sheet sheet = ExcelWorkbook.this.getSheet(EXCEL_CONFIG_SHEET)
			if(!sheet)
				return 
			String config = sheet.getRow(CONFIG_CELL.row).getCell(CONFIG_CELL.col).getStringCellValue()
			config.split(SECTION_DELIM).each{ String sectionConfig ->
                ExcelWorkbook.this.addSection(getSectionInstance(sectionConfig))
			}
		}
		
		private Section getSectionInstance(String sectionConfig){
			Class[] cArgs = new Class[4]
			String[] args = sectionConfig.split(FIELD_DELIM)
			String sectionType = args[SECTION_TYPE_INDEX]
			cArgs[0] = int.class
			int sheetIndex = Integer.parseInt(args[SHEET_NUMBER_INDEX])
			def type
			if(args[BEAN_CLASS_INDEX]){
				cArgs[1] = Class.class
				type = Class.forName(args[BEAN_CLASS_INDEX])
			}else{
				cArgs[1] = CellTypeEnum.class
				type = CellTypeEnum.valueOf(args[CELL_TYPE_INDEX])
			}
			cArgs[2] = String.class
			String startCell = args[FIRST_CELL_INDEX]
			cArgs[3] = String.class
			String lastCell = args.length == 6?args[LAST_CELL_INDEX]:null
			Constructor constructor = Class.forName(sectionType).getConstructor(cArgs)
			return constructor.newInstance(sheetIndex, type, startCell, lastCell)
		}
		
		private void writeConfig(Sheet sheet){
			Cell configCell = sheet?.createRow(CONFIG_CELL.row).createCell(CONFIG_CELL.col)
			String config = ''
            ExcelWorkbook.this.sections.each{ String key, Section section ->
				config += section.getSectionConfig(SECTION_DELIM, FIELD_DELIM)
			}
            ClarityWebTransaction.logger.info "Excel config: ${config.substring(1)}"
			configCell.setCellValue(config.substring(1))
		}
		
		private void writeMessage(Sheet sheet){
			Cell messageCell = sheet?.createRow(MESSAGE_CELL.row).createCell(MESSAGE_CELL.col)
			messageCell.setCellStyle(getMessageCellStyle(ExcelWorkbook.this.workbook))
			messageCell.setCellValue(MESSAGE)
		}
		
		private CellStyle getMessageCellStyle(Workbook workbook){
			CellStyle style = workbook.createCellStyle()
			Font font = workbook.createFont()
			font.setColor(Font.COLOR_RED)
			font.setBold(true)
			style.setFont(font)
			return style
		}
	}

	List<String> getSupportedExtensions(){
		return ['.xls', '.xlt', '.xlm']
	}

	String getFileNameWithExtension(){
		for(String ex : supportedExtensions){
			if(templateName.endsWith(ex))
				return templateName
		}
		if(templateName.contains('.xl'))
			throw new IllegalArgumentException("File format not supported. Supported formats : $supportedExtensions")
		return "$templateName${supportedExtensions.first()}"
	}

	void openExcelFile(){
		try{
			String fileName = fileNameWithExtension
			File file = getFileLocally(fileName)
			if(file)
				workbook = getWorkbook(new FileInputStream(file))
			else
				workbook = getWorkbook(getTemplateUrl(fileName).openStream())
		} catch(Exception e){
			workbook = downloadWorkbook(templateName)
		}
		if (!workbook)
		throw new FileNotFoundException("File ${templateName} not found")
	}

	protected ExcelWorkbook(){}

	ExcelWorkbook(InputStream inputStream, boolean testMode= false){
		this.testMode = testMode
		sections = [:]
		workbook = getWorkbook(inputStream)
	}

	/**
	 *  It takes an input file to instantiate this class. 
	 * Input file is the template file name during serialization and excel file for deserialization.
	 * @param templateName
	 */
	ExcelWorkbook(String inputFile, boolean testMode = false){
		sections = [:]
		this.templateName = inputFile
		this.testMode = testMode
        ClarityWebTransaction.logger.info "Opening workbook ${inputFile}"
		openExcelFile()
	}

	Workbook downloadWorkbook(String artifactId) {
		NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
		InputStream inputStream = null
		try {
			inputStream = nodeManager.downloadFile(artifactId)
			return getWorkbook(inputStream)
		}
		catch(Exception e){
			throw e
		}
		finally {
			inputStream?.close()
		}
		return null
	}

	File getFileLocally(String filePath){
		File templateFile = new File(filePath)
        ClarityWebTransaction.logger.info "File path: ${templateFile.canonicalPath}"
		String testModeFilePath
		if(!templateFile.exists()) {
			testModeFilePath = "../../scripts-plugin/src/main/" + filePath
			templateFile = new File(testModeFilePath)
		}
        ClarityWebTransaction.logger.info "File path: ${templateFile.canonicalPath}"
		if(templateFile.exists())
			return templateFile
		return null
	}

    private URL getTemplateUrl(String path) {
		path = path.endsWith(supportedExtensions.first())?path:path+supportedExtensions.first()
		String resourcePath = path.replace("resources/","")
        ClarityWebTransaction.logger.info "Resource path: ${this.class.getClassLoader().getResource(resourcePath)}"
		URL url =  this.class.getClassLoader().getResource(resourcePath)
        if (!url) {
            throw new WebException([code:'template.notFound', args:[templateName]],422)
        }
        return url
    }

	void addSection(Section section){
		if(!section)
			throw new RuntimeException("Check the file format, it does not match with the template.")
		sections[section.getSectionKey()] = section
		section.workbook = this
	}

	void addSections(List<Section> sectionList){
		sectionList.each{ Section section ->
			addSection(section)
		}
	}

	void removeSection(Section section){
		sections.remove(section.getSectionKey())
	}

	Sheet getSheet(int index){
		if(workbook.numberOfSheets == index)
			return workbook.createSheet()
		if(workbook.numberOfSheets < index)
			return null
		workbook.getSheetAt(index)
	}
	
	Sheet getSheet(String sheetName){
		workbook.getSheet(sheetName)
	}
	
	int getSheetIndex(String sheetName){
		return workbook.getSheetIndex(sheetName)
	}
	
	void addSheet(String sheetName){
		workbook.createSheet(sheetName)
	}

	void addSheet(String sheetName, int index){
		Sheet sheet = workbook.createSheet(sheetName)
		workbook.setSheetOrder(sheetName, index)
	}

	CreationHelper getCreationHelper(){
		workbook.getCreationHelper()
	}

	CellStyle createCellStyle(){
		workbook.createCellStyle()
	}

    Workbook getWorkbook(InputStream templateFile){
        Workbook workbook = WorkbookFactory.create(templateFile)
        workbook?.setForceFormulaRecalculation(true)
        workbook
    }

	void optimiseStyles(){
		if(workbook.numCellStyles > 3000)
			HSSFOptimiser.optimiseCellStyles(workbook)
	}

	/**
	 * This method stores data to a file 
	 * The file name is the artifact limsId
	 * @param nodeConfig
	 * @param limsId
	 * @return
	 */
	Node store(NodeConfig nodeConfig, String limsId, boolean writeConfig=true, String tFileName = null){
        File tempFile = null
        Node responseNode = null
        FileOutputStream fileOutputStream = null
        try{
			sections.each{ String key, Section section->
				if(section.hasSourceSection())
					section.copySection()
				optimiseStyles()
			}
            sections.each{ String key, Section section->
                ClarityWebTransaction.logger.info "Storing ${section.sectionKey}....."
				if(section.data)
                	section.store()
            }
			if(writeConfig)
				new ExcelConfig().writeExcelConfig()
			String tempFileName = tFileName ? tFileName : limsId
            tempFile = new File(tempFileName+supportedExtensions.first())
            fileOutputStream = new FileOutputStream(tempFile)
            workbook.write(fileOutputStream)
            fileOutputStream.flush()
            responseNode = testMode?null:FilePoster.attachFileReturnContentLocationNode(nodeConfig, limsId, tempFile)
        }catch(Exception e) {
			ClarityWebTransaction.logger.error(e.message, e)	// print stack trace in logs
		}finally {
            fileOutputStream?.close()
            if(!testMode)
				tempFile?.delete()
			return responseNode
        }
	}

	/**
	 * This method reads data from a file to a excel workbook
	 */
	void load(boolean validate = true){
		new ExcelConfig().readExcelConfig()
		sections.each{ String key, Section section ->
			section.load(validate)
		}
	}

	String toString(){
		StringBuilder sb = new StringBuilder()
		sections.values().each{ Section section ->
			sb.append(section.toString()).append("\n")
		}
		return sb.toString()
	}

	DataValidation createDropdownList(Sheet sheet, CellRangeAddressList addressList, String[] values){
		logger.fine "Creating Dropdown list at ${addressList} with values ${values}"
		DataValidationHelper dvHelper = sheet.dataValidationHelper
		String str = values.join(',')
		DataValidationConstraint dataValidation
		if(str.size() > 255){
			Sheet hiddenSheet = workbook.getSheet("hidden")
			int startRow = 0
			Name namedCell
			if(hiddenSheet) {
				namedCell = workbook.getName("hidden")
				startRow = dataValidations[values.join(',')].rowIndex
			} else {
				addSheet("hidden")
				hiddenSheet = getSheet("hidden")
				startRow = hiddenSheet.getLastRowNum() + 1
				for (int i = startRow; i < values.size(); i++) {
					String name = values[i]
					Row row = hiddenSheet.createRow(i)
					Cell hiddenCell = row.createCell(0)
					hiddenCell.setCellValue(name)
				}
				namedCell = workbook.createName()
				namedCell.setNameName("hidden")
				dataValidations[values.join(',')] = hiddenSheet.getRow(startRow).getCell(0)
			}
			workbook.setSheetVisibility(workbook.getSheetIndex(hiddenSheet), SheetVisibility.HIDDEN)
			dataValidation = dvHelper.createFormulaListConstraint('hidden!$A$'+ startRow + ':$A$' + "${values.size()}")
		}
		else
			dataValidation = dvHelper.createExplicitListConstraint(values)
		return dvHelper.createValidation(dataValidation, addressList)
	}

	def getDVRecords(Sheet sheet){
		Field sheetField = Sheet.class.getDeclaredField("_sheet")
		sheetField.setAccessible(true)
		Object internalSheet = sheetField.get(sheet)
		InternalSheet is = (InternalSheet) internalSheet
		DataValidityTable dvTable = is.getOrCreateDataValidityTable()
		Field dvtField = DataValidityTable.class.getDeclaredField("_validationList")
		dvtField.setAccessible(true)
		Object records = dvtField.get(dvTable)
		return (ArrayList<DVRecord>) records
	}

    EvaluationWorkbook getEvaluationWorkbook(){
        return HSSFEvaluationWorkbook.create(workbook)
    }

	List<String> getSheetNames() {
		List<String> sheetNames = []
		for(int i=0; i<workbook.numberOfSheets; i++) {
			sheetNames << workbook.getSheetName(i)
		}
		return sheetNames
	}
}

