package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.domain.LibraryNames
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import grails.gorm.transactions.Transactional
import org.springframework.transaction.annotation.Propagation

@Transactional
class LibraryNameReservationService {
    Integer librariesLoadSize = 100

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    synchronized Map<String,String> reserveLibraryNames(Collection<String> limsIds, String libraryType) {
        Map<String,String> libraryLimsIds = [:]
        ClarityWebTransaction.logger.info "reserving library names for IDs ${limsIds}"
        if (!limsIds) {
            return libraryLimsIds
        }
        Set<String> uniqueIds = []
        Integer startSize = limsIds.unique().size()
        Date reservationDate = new Date()
        LinkedList<LibraryNames> libraryNamesBuffer = new LinkedList<>()

        try {
            limsIds?.each { String limsId ->
                if (uniqueIds.contains(limsId)) {
                    return
                }
                uniqueIds << limsId
                String libraryName = reserveLibraryName(limsId, libraryType, reservationDate, libraryNamesBuffer)
                libraryLimsIds[limsId] = libraryName
            }
        } catch (NoMoreNamesException e) {
            throw new BundleAwareException([code: 'libraryNames.insufficient', args: [startSize]])
        }
        return libraryLimsIds
    }

    private String reserveLibraryName(String limsId, String libraryType, Date reservationDate, LinkedList<LibraryNames> libraryNamesBuffer) {
        LibraryNames libraryName = LibraryNames.findByGeneusLimsId(limsId)
        if (libraryName) {
            libraryName.dateGranted = reservationDate
            libraryName.libraryType = libraryType
            return libraryName.save(flush: true, failOnError: true).libraryName
        }

        if (!libraryNamesBuffer) {
            libraryNamesBuffer.addAll(loadUnusedLibraryNames(librariesLoadSize))
            if (!libraryNamesBuffer) {
                throw new NoMoreNamesException()
            }
        }

        libraryName = libraryNamesBuffer.removeFirst()
        libraryName.dateGranted = reservationDate
        //this double checking of library reservation status should be redundant if this service
        //is the sole means of modifying gov.doe.jgi.pi.pps.clarity.domain.LibraryNames records.
        libraryName.save(flush: true, failOnError: true)
        libraryName.refresh()
        if (libraryName.geneusLimsId || libraryName.libraryType) {
            throw new BundleAwareException([code: 'libraryName.concurrentAccess', args: [libraryName.id]])
        }

        libraryName.geneusLimsId = limsId
        libraryName.libraryType = libraryType
        libraryName.save(flush: true, failOnError: true)
        return  libraryName.libraryName
    }

    private List<LibraryNames> loadUnusedLibraryNames(Integer numberOfLibraries) {
        ClarityWebTransaction.logger.trace "loading ${numberOfLibraries} libraries"
        LibraryNames.createCriteria().list {
            and {
                isNull('geneusLimsId')
                isNull('libraryType')
                isNull('dateGranted')
            }
            maxResults(numberOfLibraries)
            order("id","asc")
        }
    }

    private class NoMoreNamesException extends RuntimeException {}

    //analytes: List<Analyte> OR List<ArtifactNode>
    void assignLibraryNames(List analytes){
        String libraryType = (!analytes || analytes?.isEmpty()) ? null : analytes.first().class.simpleName
        ClarityWebTransaction.logger.info "library type '$libraryType'"
        Collection<String> limsIds = analytes.collect{ it.id }

        //reserve library names and associate them with the analytes
        Map<String,String> limsId2libraryName = reserveLibraryNames(limsIds, libraryType)
        if (!limsId2libraryName) {
            throw new RuntimeException("Can't reserve library names for analyte ids '$limsIds' and library type '$libraryType'.")
        }
        // Rename the input analyte on the API with the four-letter name derived from the DB
        analytes.each {
            it.name = limsId2libraryName[it.id]
            ClarityWebTransaction.logger.info "renamed '${it?.id}' analyte to '${it?.name}'"
        }
    }
}
