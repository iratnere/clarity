package gov.doe.jgi.pi.pps.clarity.scripts.rest

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming
import groovy.transform.Canonical

@Canonical
@JsonNaming(PropertyNamingStrategy.KebabCaseStrategy.class)
class SamplesFractionationModel {

    Long submittedBy
    List<SampleFractionModel> fractionSamples = []
}
