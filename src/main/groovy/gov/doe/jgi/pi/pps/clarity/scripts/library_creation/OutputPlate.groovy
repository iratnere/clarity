package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes

/**
 * Created by tlpaley on 1/3/16.
 */
class OutputPlate implements OutputType {

    final static String TABLE_CLASS_NAME = LcPlateTableBean.class.simpleName
    LibraryCreationProcess process

    OutputPlate(LibraryCreationProcess process) {
        this.process = process
    }

    @Override
    void validate() {
        validateNumberOfOutputContainers()
    }

    def validateNumberOfOutputContainers(
            List<ContainerNode> outputContainerNodes = process.getContainerNodes(process.outputAnalytes)
    ) {
        def outputPlateContainers = outputContainerNodes.findAll { it.isNinetySixWellPlate }
        if (outputPlateContainers && outputPlateContainers.size() != 1) {
            process.postErrorMessage("""
                Output plate does not pass custom validation.
                Number of output containers with value '${outputPlateContainers.size()}' exceeds maximum value 1.
                Please select a single ${ContainerTypes.WELL_PLATE_96.value} as the output container.
            """)
        }
    }

    @Override
    void update() {
        updateOutputPlateContainers()
    }

    def updateOutputPlateContainers() {
        //output plate container udf “Label” set to the input plate “Label” udf
        process.outputAnalytes[0].containerUdfLabel = process.inputAnalytes[0].containerUdfLabel
    }

    @Override
    void print() {
        process.writeLabels(process.outputAnalytes[0] as List)
    }

    @Override
    void removeCorners(List beansList) {
        beansList.removeAll{it.well in LibraryCreationProcess.PLATE_CORNERS}
    }

    @Override
    void moveOutputToNextWorkflow(List<ClarityLibraryStock> analytes) { //Plate Illumina, Plate SmallRna, Plate PacBio
        def libraryQcResult = analytes[0].containerUdfLibraryQcResult
        if (libraryQcResult == Analyte.PASS) {
            List<ClarityLibraryStock> failedStocks = analytes.findAll { ClarityLibraryStock stock -> stock.udfLibraryQcResult == Analyte.FAIL }
            List<ClarityLibraryStock> passedStocks = analytes.findAll { ClarityLibraryStock stock -> stock.udfLibraryQcResult == Analyte.PASS }
            process.routeArtifactNodes(process.lcAdapter.defaultStage, passedStocks*.artifactNode)
            process.routeArtifactNodes(Stage.ABANDON_QUEUE, failedStocks*.artifactNode)
            return
        }
        if (libraryQcResult == Analyte.FAIL) {
            process.processCompleteFailure(process.outputAnalytes)
            return
        }
        process.postErrorMessage("Library Stock Plate: invalid Library QC Result '$libraryQcResult'")
    }

    //Plate Illumina, Plate SmallRna
    @Override
    void transferBeansDataToClarityLibraryStocks() {
        process.outputAnalytes[0].containerUdfLibraryQcResult = process.resultsBean.plateResult?.value
        process.outputAnalytes[0].containerUdfLibraryQcFailureMode = process.resultsBean.failureMode?.value
        Boolean passed = process.resultsBean.plateResult?.value == Analyte.PASS
        String indexSet = process.resultsBean.indexContainerBarcode
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcPlateTableBean lcPlateTableBean = (LcPlateTableBean) process.getLcTableBean(clarityLibraryStock.id, passed)
            clarityLibraryStock.udfConcentrationNgUl = lcPlateTableBean.libraryConcentration
            clarityLibraryStock.udfActualTemplateSizeBp = lcPlateTableBean.libraryTemplateSize
            clarityLibraryStock.udfVolumeUl = lcPlateTableBean.libraryVolume
            clarityLibraryStock.udfIndexContainerBarcode = indexSet
            clarityLibraryStock.udfIndexName = lcPlateTableBean.libraryIndexName
            clarityLibraryStock.udfLibraryMolarityQcPm = lcPlateTableBean.libraryMolarity
            clarityLibraryStock.udfNumberPcrCycles = process.resultsBean.numberPcrCycles
            clarityLibraryStock.udfLibraryQcResult = process.resultsBean.plateResult?.value
            clarityLibraryStock.systemQcFlag = passed
            clarityLibraryStock.udfLibraryQcFailureMode = process.resultsBean.failureMode?.value
        }
    }

    ExcelWorkbook populateLibraryCreationSheet(String templateName, Section tableSectionLibraries){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(templateName, process.testMode)
        Section kvResultSection = process.getKeyValueResultSection()
        excelWorkbook.addSection(kvResultSection)
        excelWorkbook.addSection(tableSectionLibraries)

        return excelWorkbook
    }

    @Override
    void updateLibraryIndexUdf(List<ClarityLibraryStock> libraryStocks){
        List indexNames = process.getIndexNames(libraryStocks)
        if (indexNames) {
            processIndexNames(libraryStocks, indexNames)
        } else {
            processIndexContainerBarcodes(libraryStocks)
        }
    }

    void processIndexNames(List<ClarityLibraryStock> libraryStocks, List indexNames){
        libraryStocks.each {
            // if 'Index Name' provided then set it to 'Index Container Barcode': PPS-4904
            it.udfIndexContainerBarcode = it.udfIndexName
        }
        validateLibraryIndexUdfBatch(libraryStocks)
        validateIndexNames(libraryStocks, indexNames)
    }

    void processIndexContainerBarcodes(List<ClarityLibraryStock> libraryStocks){
        List indexContainerBarcodes = process.getIndexContainerBarcodes(libraryStocks)
        ArtifactIndexService artifactIndexService = ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(ArtifactIndexService) as ArtifactIndexService
        Map<String, List<Index>> barcodeToIndexes = artifactIndexService.getIndexes(indexContainerBarcodes)
        validateLibraryIndexesSize(libraryStocks, barcodeToIndexes)
        libraryStocks.each{ libraryStock ->
            Index index = findIndex(libraryStock, barcodeToIndexes)
            libraryStock.udfIndexName = index?.indexName
        }
        validateLibraryIndexUdfBatch(libraryStocks)
    }

    static final def getBarcodeKey(String indexContainerBarcode, Set<String> barcodeKeySet){
        return barcodeKeySet?.find{indexContainerBarcode == it}
    }

    static final Index findIndex(ClarityLibraryStock libraryStock, Map<String, List<Index>> barcodeToIndexes){
        String indexContainerBarcode = libraryStock.udfIndexContainerBarcode
        if (!barcodeToIndexes || !indexContainerBarcode)
            return null
        String key = getBarcodeKey(indexContainerBarcode, barcodeToIndexes.keySet())
        List<Index> indexes = barcodeToIndexes[key]
        if (!indexes)
            return null
        String location = getConvertedLocation(libraryStock)
        return indexes.find{it.plateLocation == location}//it.indexSet == key &&
    }

    static final String getConvertedLocation(ClarityLibraryStock libraryStock) {
        ContainerLocation location = libraryStock.containerLocation
        if (libraryStock.containerType == ContainerTypes.WELL_PLATE_384.value) {
            String row = location.rowColumn.split(':')[0]
            String column = location.rowColumn.split(':')[1]
            return LibraryCreationPlateSingleCellInternal.convert384Well(row, column as Integer)
        }
        return location.rowColumn
    }

    def validateLibraryIndexesSize(List<ClarityLibraryStock> libraryStocks, Map<String, List<Index>> barcodeToIndexes) {
        def barcode = libraryStocks[0].udfIndexContainerBarcode
        if (!barcodeToIndexes || !barcodeToIndexes[barcode]) {
            def completeErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << "Invalid Library Index Set '$barcode'"
            process.postErrorMessage(completeErrorMsg as String)
        }
        if (libraryStocks.size() > barcodeToIndexes[barcode].size()) {
            def completeErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE
            completeErrorMsg << "Not enough indexes for the input plate." << ClarityProcess.WINDOWS_NEWLINE
            completeErrorMsg << "Library Index Set plate '$barcode' has only '${barcodeToIndexes[barcode].size()}' indexes." << ClarityProcess.WINDOWS_NEWLINE
            completeErrorMsg << "Input plate has '${libraryStocks.size()}' libraries."
            process.postErrorMessage(completeErrorMsg as String)
        }
    }

    def validateLibraryIndexUdfBatch(List<ClarityLibraryStock> libraryStocks) {
        def errorMsg = StringBuilder.newInstance()
        libraryStocks.each {
            if (!it.udfIndexName) {
                errorMsg << "Library Stock ($it.name): Unspecified ${ClarityUdf.ANALYTE_INDEX_NAME.udf} '$it.udfIndexName'"
                errorMsg << ClarityProcess.WINDOWS_NEWLINE
            }
        }
        if (errorMsg?.length()) {
            def completeErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << errorMsg
            process.postErrorMessage(completeErrorMsg as String)
        }
    }

    def validateIndexNames(List<ClarityLibraryStock> libraryStocks, List indexNames) {
        ArtifactIndexService artifactIndexService = ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(ArtifactIndexService) as ArtifactIndexService
        Map<String, String> indexToSequence = artifactIndexService.getSequences(indexNames)
        def errorMsg = StringBuilder.newInstance()
        libraryStocks.each {
            if (!indexToSequence[it.udfIndexName]) {
                errorMsg << "Library Stock ($it.name): Invalid ${ClarityUdf.ANALYTE_INDEX_NAME.udf} '$it.udfIndexName'"
                errorMsg << ClarityProcess.WINDOWS_NEWLINE
            }
        }
        if (errorMsg?.length()) {
            def completeErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << errorMsg
            process.postErrorMessage(completeErrorMsg as String)
        }
    }

}
