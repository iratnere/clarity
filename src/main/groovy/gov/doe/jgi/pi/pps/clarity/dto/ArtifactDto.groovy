package gov.doe.jgi.pi.pps.clarity.dto

/**
 * Created by dscott on 3/4/2016.
 */
class ArtifactDto extends GeneusNodeDto {

    LocationDto location
    List<SampleDto> parentSamples = []
    Boolean isSample
    Boolean isAnalyte

    String analyteClass
    String parentProcessType
    String runMode
    String newRunMode

    Set<ArtifactStageDto> activeStages = []
    Set<ArtifactStageDto> newStages = []

    List<ArtifactStageDto> getSortedActiveStages() {
        activeStages.sort()
    }

    List<ArtifactStageDto> getSortedNewStages() {
        newStages.sort()
    }

    ArtifactDto() {}

    ContainerDto getContainer() {
        location?.container
    }

    SampleDto getParentSample() {
        if (parentSamples.size() == 1) {
            return parentSamples[0]
        }
        return null
    }

    SampleDto getSample() {
        SampleDto sample = null
        if (isSample) {
            sample = parentSamples[0]
        }
        sample
    }

    List<ArtifactStageDto> getSequencingQueuesInProgress() {
        activeSequencingQueues?.findAll{it.status == 'IN_PROGRESS'} ?: []
    }

    List<ArtifactStageDto> getSequencingQueuesQueued() {
        activeSequencingQueues?.findAll{it.status == 'QUEUED'} ?: []
    }

    List<ArtifactStageDto> activeSequencingQueues
    List<ArtifactStageDto> newSequencingQueues

    String sequencingQueueUri
    Long newRunModeId

}
