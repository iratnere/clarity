package gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference

import java.lang.reflect.Field
import java.util.logging.Logger

/**
 * This section corresponds to header/key-value section in the excel file
 * One needs to define a bean class the maps key to a field in the class
 * store: Create a bean class instance and the class shall print the value if the field against the corresponding header
 * load: This class reads key-value section and populates bean class instance accordingly
 * 
 * Created by lvishwas on 10/4/14.
 */
class KeyValueSection extends Section{
    Map<String, Closure> headerFieldMap
    private Object beanObject
    static final Logger logger = Logger.getLogger(KeyValueSection.class.name)

	KeyValueSection(int parentSheetIndex, Class cls, String startAddress, String endAddress = null){
		super(parentSheetIndex,cls, startAddress,endAddress)
	}
	
	KeyValueSection(int parentSheetIndex, Class cls, CellRangeAddress sectionRangeAddress){
		super(parentSheetIndex,cls,sectionRangeAddress)
	}
	
	KeyValueSection(KeyValueSection srcSection, int parentSheetIndex, Class cls, String startAddress){
		super(srcSection,parentSheetIndex,cls,startAddress)
	}

    Field getFieldForHeader(String header){
        if(!headerFieldMap){
            populateHeaderFieldMap()
        }
        return headerFieldMap[header]
    }

    void populateHeaderFieldMap(){
        Field[] fields = beanClass.declaredFields;
        headerFieldMap = [:]
        fields.each{ Field field ->
            if(field.isAnnotationPresent(FieldMapping.class)){
                FieldMapping annotation = field.getAnnotation(FieldMapping.class);
                headerFieldMap[annotation.header()?.trim()] = field
            }
        }
    }

	void setData(def data){
		beanObject = data
	}
	
    @Override
    void store() {
        logger.fine "Writing section: ${this.sectionKey}"
        Sheet sheet = workbook.getSheet(parentSheetIndex)
        int startRow = startCell.row
        Row row = sheet.getRow(startRow)
        while(row){
            String header = readCell(row.getCell(startCell.col),CellTypeEnum.STRING).trim()
            Field field = getFieldForHeader(header)
            if(field){
                Cell cell = row.getCell(startCell.col + 1)?row.getCell(startCell.col + 1):row.createCell(startCell.col + 1)
                writeCell(cell, ((FieldMapping)field.annotations[0]).cellType(),field.get(beanObject))
            }
			++startRow
			if(lastCell && startRow > lastCell.row)
				break
			row = sheet.getRow(startRow)
        }
    }

    @Override
    void load(boolean validate=true) {
        logger.fine "Reading section: ${this.sectionKey}"
        Sheet sheet = workbook.getSheet(parentSheetIndex)
        int startRow = startCell.row
        Row row = sheet.getRow(startRow);
        beanObject = beanClass.newInstance()
        while(row){
            String header = readCell(row.getCell(startCell.col),CellTypeEnum.STRING).trim()
            Field field = getFieldForHeader(header)
            if(field){
                Cell cell = row.getCell(startCell.col + 1)
                FieldMapping fieldMapping = field.getAnnotation(FieldMapping.class)
                field.set(beanObject, readCell(cell, fieldMapping.cellType()))
            }
			++startRow
			if(lastCell && startRow > lastCell.row)
				break
            row = sheet.getRow(startRow);
        }
        if(beanObject&&validate)
            validateBean(beanObject)
    }
	
	String getSectionConfig(String sectionDelim, String fieldDelim){
		StringBuilder sb = new StringBuilder()
		sb.append(sectionDelim)
		sb.append(this.class.name).append(fieldDelim)
		sb.append(this.beanClass.name).append(fieldDelim).append(fieldDelim)
		sb.append(parentSheetIndex).append(fieldDelim)
		sb.append(startCell.formatAsString()).append(fieldDelim)
		if(lastCell)sb.append(lastCell.formatAsString())
		sb.append(fieldDelim)
		return sb.toString()
	}
	
	def getData(){
		return beanObject
	}

    String toString(){
        StringBuilder sb = new StringBuilder()
        sb.append("Key-Value Section: ").append(sectionKey).append("\n")
        sb.append(getData().toString()).append("\n")
        return sb.toString()
    }

    CellReference getCellAddress(String headerStr){
        if(sourceSection)
            return calculateCellAddressFromSourceSection(headerStr)
        CellReference cellReference = null
        Sheet sheet = workbook.getSheet(parentSheetIndex)
        int currRow = startCell.row
        Row row = sheet.getRow(currRow)

        while(row){
            Cell cell = row.getCell(startCell.col)
            String header = readCell(cell, CellTypeEnum.STRING)?.trim()
            if(header == headerStr){
                cellReference = new CellReference(cell.rowIndex, cell.columnIndex)
                break
            }
            row = sheet.getRow(++currRow)
        }
        return cellReference
    }
}