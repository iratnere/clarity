package gov.doe.jgi.pi.pps.clarity.scripts.freezer

class FreezerContainer {
	String barcode
	String contentsType
	String checkInStatus
	Date transactionDate
	String operatorFirstName
	String operatorLastName
	String holderName
	String location
	String errorMessage
	String containerClass
	String containerState
	
	String getOperatorFullName() {
		if (operatorFirstName && operatorLastName) {
			return "${operatorFirstName} ${operatorLastName}"
		}
		return operatorFirstName?:operatorLastName
	}
}
