package gov.doe.jgi.pi.pps.clarity.model.analyte


import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import groovy.transform.PackageScope

/**
 * Created by dscott on 4/22/2014.
 * connected to scheduled sample artifact node
 */
class SampleAnalyte extends Analyte {

    @PackageScope
    SampleAnalyte(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
        validateIsSample()
    }

    @Override
    String getPrintLabelText() {
        // if the sample analyte represents a scheduled sample then print the pmo sample's (root sample) label
        PmoSample claritySample = this.parentPmoSamples.find{true}
        if (isOnPlate) {
            return new LabelBean(
                    pos_1_1: containerName,
                    pos_1_2: claritySample.containerUdfLabel,
                    pos_1_4: claritySample.sequencingProject.udfMaterialCategory,
                    pos_1_5: containerName
            ) as String
        }
        return new LabelBean(
                pos_1_1: claritySample.pmoSampleIdAsString,
                pos_1_2: sampleNode.id,
                pos_1_3: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: claritySample.pmoSampleIdAsString
        ) as String
    }

    String getWorksheetSourceLocation() {
        return (containerNode.isPlate ? artifactNodeInterface?.location?.minus(':') : null)
    }

    @Override
    LibraryCreationQueueCv getLibraryCreationQueue() {
        return claritySample.libraryCreationQueue
    }

    boolean getIsDNA(){
        if(claritySample.sequencingProject.udfMaterialCategory == MaterialCategoryCv.DNA.materialCategory)
            return true
        return false
    }
}
