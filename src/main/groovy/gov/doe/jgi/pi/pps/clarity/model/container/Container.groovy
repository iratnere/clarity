package gov.doe.jgi.pi.pps.clarity.model.container

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

/**
 * Created by dscott on 3/3/2016.
 */
class Container {

    private final ContainerNode containerNode

    Container(ContainerNode containerNode) {
        this.containerNode = containerNode
    }

    ContainerNode getContainerNode() {
        return containerNode
    }

    String getId() {
        containerNode.id
    }

    String getName() {
        containerNode.name
    }

    List<Analyte> getAnalytes() {
        containerNode.contentsArtifactNodes.collect { ArtifactNode artifactNode ->
            AnalyteFactory.analyteInstance(artifactNode)
        }
    }

    Map<ContainerLocation, Analyte> getContainerLocationAnalytes() {
        Map<ContainerLocation, Analyte> locationAnalyte = [:]
        containerNode.containerLocationArtifactNodes().each { ContainerLocation containerLocation, ArtifactNode artifactNode ->
            locationAnalyte[containerLocation] = AnalyteFactory.analyteInstance(artifactNode)
        }
        locationAnalyte
    }

}
