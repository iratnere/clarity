package gov.doe.jgi.pi.pps.clarity.scripts.rest

import grails.converters.JSON
import org.apache.commons.lang.RandomStringUtils

/**
 * Created by tlpaley on 5/3/17.
 */
class SequencingProjectSubmission {

    SequencingProjectSubmission() {
        registerSubmission()
    }
    Long strategyId
    Long sequencingProductId
    def submittedBy

    static def registerSubmission() {
        JSON.registerObjectMarshaller (SequencingProjectSubmission) { SequencingProjectSubmission submission ->
                def output = [:]
            output['existing-jgi-taxonomy-id'] = 1492
            output['sequencing-project-name'] = "SP ${RandomStringUtils.randomAlphanumeric(4)}${new Date().time}"
            output['expected-number-of-samples'] = 1

            output['final-deliverable-project-id'] = 1091346
            output['sequencing-product-id'] = submission.sequencingProductId
            output['sequencing-strategy-id'] = submission.strategyId

            output['sequencing-project-manager-cid'] = submission.submittedBy
            output['sequencing-project-comments-and-notes'] = "sequencing-project-comments-and-notes"
            output['eligible-for-public-release'] = "Y"
            output['embargo-days'] = 180
            output['sequencing-project-contact-cid'] = submission.submittedBy
            output['sample-contact-cid'] = submission.submittedBy
            output['auto-schedule-sow-items'] = "Y"

            return output
        }
    }
}
