package gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode

enum FreezerSheetField {
	BARCODE('Barcode',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return (freezerContainer?.barcode)?:analyte?.containerNode?.name
			}
		),
	FREEZER_PATH('Freezer Path',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				def location = freezerContainer?.location
				if (!location) {
					return freezerContainer?.errorMessage
				}
				return location
			}
		),
	LIMSID('LIMS ID',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return analyte?.id
			}
		),
	ANALYTE_NAME('Analyte Name',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return analyte?.name
			}
		),
	CONCENTRATION('Concentration (ng/uL)',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return analyte?.artifactNode?.getUdfAsBigDecimal(ClarityUdf.ANALYTE_CONCENTRATION_NG_UL.udf)
			}
		),
	SAMPLE_ALIQUOT('Sample Aliquot',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				SampleNode sampleNode = analyte?.artifactNode?.sampleNode
				if (sampleNode) {
					ArtifactNode sampleAliquot = analyte?.artifactNode?.sampleNode?.artifactNode//sampleNode.artifactNode
					return "${sampleNode.id} \"${sampleNode.name}\" : ${sampleAliquot?.containerName}"
				}
				return null
			}
		),
	OWNER('Owner',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return freezerContainer?.operatorFullName
			}
		),
	TRANSACTION_DATE('Transaction Date',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return freezerContainer?.transactionDate
			}
		),
	STATUS('Freezer Status',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				return freezerContainer?.checkInStatus
			}
		),
	ERROR('Error',
			{ FreezerContainer freezerContainer, Analyte analyte = null ->
				if (freezerContainer) {
					return freezerContainer.errorMessage
				}
				return 'no freezer container'
			}
		),
	// index/freezer combo sheet
	LABEL('Label',
		{ FreezerContainer freezerContainer, Analyte analyte = null ->
			return analyte?.containerNode?.name
		}
	),
	NUMBER('#',
		{ FreezerContainer freezerContainer, Analyte analyte = null ->
			return null
		}
	),
	DEGREE_OF_POOLING('Degree of pooling',
		{ FreezerContainer freezerContainer, Analyte analyte = null ->
			return null
		}
	),
	CONTAINER_ID('Container ID',
		{ FreezerContainer freezerContainer, Analyte analyte = null ->
			return analyte?.containerNode?.id
		}
	),
	PLATE_NAME('Plate Name',
		{ FreezerContainer freezerContainer, Analyte analyte = null ->
			return analyte?.container?.name
		}
	)

    FreezerSheetValueGenerator valueGenerator
	
	FreezerSheetField(String header, Closure valueGenerator) {
		this.valueGenerator = new FreezerSheetValueGenerator(header:header,valueGenerator:valueGenerator)
	}
	
	static List<FreezerSheetValueGenerator> getDefaultAnalyteFreezerSheetProperties() {
		return [BARCODE, LIMSID, ANALYTE_NAME, FREEZER_PATH, CONCENTRATION, SAMPLE_ALIQUOT, OWNER, TRANSACTION_DATE, STATUS, ERROR].collect{it.valueGenerator}
	}
	
	static List<FreezerSheetValueGenerator> getPrepareFlowcellAnalyteFreezerSheetProperties() {
		return [BARCODE, LIMSID, ANALYTE_NAME, FREEZER_PATH, CONCENTRATION, OWNER, TRANSACTION_DATE, STATUS, ERROR].collect{it.valueGenerator}
	}
	
	static List<FreezerSheetValueGenerator> getDefaultLibraryStockFreezerSheetProperties() {
		return [BARCODE, LABEL, LIMSID, ANALYTE_NAME, CONCENTRATION, SAMPLE_ALIQUOT,
				OWNER, FREEZER_PATH, TRANSACTION_DATE, STATUS, ERROR].collect{it.valueGenerator}
	}
	
	static List<FreezerSheetValueGenerator> getDefaultContainerFreezerSheetProperties() {
		return [BARCODE, FREEZER_PATH, OWNER, TRANSACTION_DATE, STATUS, ERROR].collect{it.valueGenerator}
	}

	static List<FreezerSheetValueGenerator> getTubesIndexFreezerSheetProperties() {
		return [NUMBER,	ANALYTE_NAME, DEGREE_OF_POOLING, LABEL,
				FREEZER_PATH, //STATUS, OWNER, ERROR,
				CONCENTRATION
			].collect{it.valueGenerator}
	}
}
