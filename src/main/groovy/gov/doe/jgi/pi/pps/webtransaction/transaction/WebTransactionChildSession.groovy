package gov.doe.jgi.pi.pps.webtransaction.transaction

abstract class WebTransactionChildSession<K> {
	
	abstract void openSession(WebTransaction webTransaction)
	abstract void closeSession()
	abstract K getSession()

	void finalize() throws Throwable {
		try {
			WebTransactionUtil.staticLogger.debug "finalizing session ${this}"
		} catch (ignore) {
		} finally {
			closeSession()
		}
	}

}
