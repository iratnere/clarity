package gov.doe.jgi.pi.pps.clarity.welladdress

/**
 * Created by dscott on 6/20/2014.
 */
abstract class WellAddress {

    Integer rowIndex
    Integer colIndex

    abstract String getRow();
    abstract String getColumn();
    abstract boolean getIsCorner();

    final int hashCode() {
        return 37 * rowIndex + colIndex
    }

    final boolean equals(Object other) {
        if (!other) {
            return false
        }
        if (this.class != other.class) {
            return false
        }
        if (this.rowIndex != ((WellAddress) other).rowIndex) {
            return false
        }
        if (this.colIndex != ((WellAddress) other).colIndex) {
            return false
        }
        return true
    }

    final String getWellName() {
        return "${row}${column}"
    }

	final String getClarityLocation() {
		return "${row}:${column}"	
	}
	
    final String toString() {
        getWellName()
    }
}