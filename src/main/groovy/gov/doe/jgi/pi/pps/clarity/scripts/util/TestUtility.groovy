package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ContainerIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ProcessIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ContainerParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ContainerParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ProcessParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ProcessParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerStates
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.QueueNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import org.grails.web.json.JSONObject
import org.springframework.http.HttpStatus

class TestUtility {
    NodeManager clarityNodeManager

    TestUtility(NodeManager clarityNodeManager){
        this.clarityNodeManager = clarityNodeManager
    }

    List<Analyte> getAnalytesInStage(Stage stage, int analyteCount=1){
        QueueNode queue = clarityNodeManager.getQueueNode(StageUtility.getStepConfigurationNode(clarityNodeManager, stage).stepId)
        if(queue.queueArtifacts)
            return queue.queueArtifacts[0..(analyteCount-1)].collect{ AnalyteFactory.analyteInstance(it.artifactNode)}
        return []
    }

    List<ContainerNode> getContainerInStage(Stage stage, ContainerTypes containerType, int count = 1){
        QueueNode queue = clarityNodeManager.getQueueNode(StageUtility.getStepConfigurationNode(clarityNodeManager, stage).stepId)
        List containers = []
        if(queue.queueArtifacts){
            for(def queueArtifacts: queue.queueArtifacts.collate(100)){
                if(containerType == ContainerTypes.TUBE){
                    List tubes = queue.queueArtifacts.findAll{it.containerLocation.rowColumn == "1:1"}.collect{it.containerNode}
                    if(tubes.size() <= count){
                        containers.addAll(tubes)
                    }
                    else{
                        while(containers.size() < count){
                            containers << tubes[0]
                            tubes.remove(0)
                        }
                    }
                    if(containers.size() >= count)
                        return containers
                }
                else{
                    def plates = queue.queueArtifacts.findAll{it.containerLocation.rowColumn != "1:1"}?.groupBy {it.containerNode}
                    plates = plates.keySet() as List
                    if(plates.size() <= count){
                        containers.addAll(plates)
                    }
                    else{
                        while(containers.size() < count){
                            containers << plates[0]
                            plates.remove(0)
                        }
                    }
                    if(containers.size() >= count)
                        return containers
                }
            }
        }
        return containers
    }

    List<ContainerNode> getContainers(ContainerTypes containerType, int count){
        List<String> containerIds = []
        List<ContainerParameterValue> parameterValues = []
        parameterValues << ContainerParameter.TYPE.setValue(containerType.value)
        ContainerIdIterator iterator = new ContainerIdIterator(clarityNodeManager.nodeConfig, parameterValues)
        int index = 0
        while(iterator.hasNext() && index++ < count){
            containerIds << iterator.next()
        }
        return clarityNodeManager.getContainerNodes(containerIds)
    }

    Map prepareStatusMap(Long pmoSampleId, SampleStatusCv status){
        Map map = [:]
        map[pmoSampleId] = status.value
        return map
    }

    List<String> getProcessIds(String processType, int count = 1, String firstName=null, String lastName = null){
        List<ProcessParameterValue> paramsValue = []
        paramsValue << ProcessParameter.TYPE.setValue(processType)
        if(firstName)
            paramsValue << ProcessParameter.TECH_FIRST_NAME.setValue(firstName)
        if(lastName)
            paramsValue << ProcessParameter.TECH_LAST_NAME.setValue(lastName)
        ProcessIdIterator iterator = new ProcessIdIterator(clarityNodeManager.nodeConfig, paramsValue, 10)
        iterator.hasNext()
        List<String> processIds = iterator.idList.reverse()
        if (count > processIds.size()) {
            throw new RuntimeException("Cannot find '$count' $processType processes")
        }
        processIds[0..count-1]
    }

    void executeAction(ProcessType processType, String action = null, String processId = null){
        if(!processId){
            processId = getProcessIds(processType.value).first()
        }
        ClarityProcess process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
        List<ActionHandler> actions = []
        if(!action){
            actions.add(process.actionHandlers.values())
        }
        else{
            actions << process.getActionHandler(action)
        }
        actions.each{ ActionHandler actionHandler ->
            actionHandler.process = process
            actionHandler.execute()
        }
    }

    List<ContainerNode> getEmptyContainers(ContainerTypes containerType, int count = 1) {
        List<String> containerIds = []
        List<ContainerParameterValue> parameterValues = []
        parameterValues << ContainerParameter.TYPE.setValue(containerType.value)
        parameterValues << ContainerParameter.STATE.setValue(ContainerStates.EMPTY)
        ContainerIdIterator iterator = new ContainerIdIterator(clarityNodeManager.nodeConfig, parameterValues)
        def nextPageUrl
        while(iterator.nextPage()){
            if(iterator.nextPageUrl)
                nextPageUrl = iterator.nextPageUrl
        }
        iterator.nextPageUrl = nextPageUrl
        if(iterator.hasNext()) {
            while(iterator.idList.size() > count)
                iterator.next()
            containerIds = iterator.idList
        }
        return clarityNodeManager.getContainerNodes(containerIds)
    }

    List<String> createSamplesInClarity(String url, int samplesCount, Long sequencingProductId, ContainerTypes containerTypes, boolean autoSchedule, int researcherContactId=2633) {
        List<String> sampleLimsIds = []
        int samplePerBatch = 92
        while(samplesCount > 0) {
            JSONObject json = new JSONObject()
            json.'sample-container-type' = containerTypes.value
            json.'number-samples' = samplesCount > samplePerBatch ? samplePerBatch : samplesCount
            json.'sequencing-product-id' = sequencingProductId
            json.'auto-schedule-sow-items' = autoSchedule ? 'Y' : 'N'
            json.'sequencing-project-manager-id' = researcherContactId
            def responseJson = makePostCall(url, json.toString(2))
            sampleLimsIds.addAll((new JSONObject(responseJson)).'clarity-lims-ids')
            samplesCount -= samplePerBatch
        }
        return sampleLimsIds
    }

    String makePostCall(String url, String jsonSubmission) {
        HTTPBuilder http = new HTTPBuilder(url)
        ClarityWebTransaction.logger.info "Url: $url"
        ClarityWebTransaction.logger.info "Json:\n$jsonSubmission"
        String jsonResponse = null
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = jsonSubmission
            response.success = {resp, reader ->
                jsonResponse = JsonUtil.toJsonObject(reader).toString()
            }
            response.failure = { HttpResponseDecorator resp, respJson ->
                ClarityWebTransaction.logger.info "Response:\n $respJson"
                throw new WebException(respJson."errors".collect{it."message"}.join("\n"), HttpStatus.valueOf(respJson."status").value())
            }
        }
        return jsonResponse
    }
}
