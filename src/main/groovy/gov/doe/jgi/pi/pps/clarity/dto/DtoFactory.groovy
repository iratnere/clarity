package gov.doe.jgi.pi.pps.clarity.dto

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UdfUtil
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import gov.doe.jgi.pi.pps.util.util.OnDemandCacheMapped
import groovy.util.logging.Slf4j
import org.codehaus.groovy.runtime.StackTraceUtils

/**
 * Created by dscott on 4/13/2016.
 */
@Slf4j
class DtoFactory {

    NodeManager nodeManager

    private final Map<String, ArtifactDto> artifacts = [:]
    private final Map<String, SampleDto> samples = [:]
    private final Map<String, LocationDto> locations = [:]
    private final Map<String, ContainerDto> containers = [:]
    private final Map<String, ProjectDto> projects = [:]
    private final Map<String, ProcessDto> processes = [:]

    private OnDemandCache<List<UserDefinedField>> cachedProjectUdfs = new OnDemandCache<>()
    private OnDemandCacheMapped<String, List<UserDefinedField>> cachedProcessUdfs = new OnDemandCacheMapped<>()

    void clear() {
        artifacts.clear()
        samples.clear()
        locations.clear()
        containers.clear()
        projects.clear()
        processes.clear()
        cachedProjectUdfs.clear()
        cachedProcessUdfs.clear()
    }

    DtoFactory(NodeManager nodeManager) {
        this.nodeManager = nodeManager
    }

    ContainerDto containerIdToContainerDto(String containerId, boolean ignoreErrors = true, boolean loadSamples = true) {
        ContainerDto containerDto = containers[containerId]
        if (containerDto) {
            return containerDto
        }
        try {
            ContainerNode containerNode = nodeManager.getContainerNode(containerId)
            containerDto = containerNodeToContainerDto(containerNode,loadSamples)
        } catch (t){
            if (!ignoreErrors) {
                log.error "error translating container ID ${containerId} to ContainerDto", StackTraceUtils.sanitizeRootCause(t)
                throw new RuntimeException(t)
            }
        }
        containerDto
    }

    List<ArtifactDto> artifactIdsToArtifactDtos(Collection<String> artifactIds) {
        Map<String, ArtifactDto> artifactMap = [:]
        List<String> missingIds = []
        artifactIds?.each { String artifactId ->
            if (artifactMap.containsKey(artifactId)) {
                return
            }
            ArtifactDto artifactDto = artifacts[artifactId]
            artifactMap[artifactId] = artifactDto
            if (!artifactDto) {
                missingIds << artifactId
            }
        }
        if (missingIds) {
            List<ArtifactNode> artifactNodes = nodeManager.getArtifactNodes(missingIds)
            artifactNodes?.each { ArtifactNode artifactNode ->
                ArtifactDto artifactDto = artifactNodeToArtifactDto(artifactNode)
                artifactMap[artifactNode.id] = artifactDto
            }
        }
        artifactMap.values().toList()
    }

    ArtifactDto artifactIdToArtifactDto(String artifactId, boolean loadSamples = true) {
        ArtifactDto artifactDto = artifacts[artifactId]
        if (artifactDto) {
            return artifactDto
        }
        try {
            ArtifactNode artifactNode = nodeManager.getArtifactNode(artifactId)
            artifactDto = artifactNodeToArtifactDto(artifactNode,loadSamples)
        } catch (ignore) {}
        artifactDto
    }

    ProcessDto processIdToProcessDto(String processId) {
        ProcessDto processDto = processes[processId]
        if (processDto) {
            return processDto
        }
        try {
            ProcessNode processNode = nodeManager.getProcessNode(processId)
            processDto = processNodeToProcessDto(processNode)
        } catch (ignore) {}
        processDto
    }

    SampleDto sampleIdToSampleDto(String sampleId) {
        SampleDto sampleDto = samples[sampleId]
        if (sampleDto) {
            return sampleDto
        }
        try {
            SampleNode sampleNode = nodeManager.getSampleNode(sampleId)
            sampleDto = sampleNodeToSampleDto(sampleNode)
        } catch (ignore) {}
        sampleDto
    }

    ProjectDto projectIdToProjectDto(String projectId) {
        ProjectDto projectDto = projects[projectId]
        if (projectDto) {
            return projectDto
        }
        try {
            ProjectNode projectNode = nodeManager.getProjectNode(projectId)
            projectDto = projectNodeToProjectDto(projectNode)
        } catch (ignore) {}
        projectDto
    }

    ContainerDto containerNodeToContainerDto(ContainerNode containerNode, boolean loadSamples = true) {
        ContainerDto containerDto = containers[containerNode.id]
        if (containerDto) {
            return containerDto
        }
        containerDto = new ContainerDto()
        containers[containerNode.id] = containerDto

        containerDto.limsid = containerNode.id
        containerDto.name = containerNode.name
        containerDto.type = containerNode.containerType

        containerNode.containerLocationArtifactNodes().each{ ContainerLocation location, ArtifactNode artifactNode ->
            ArtifactDto artifactDto = artifactNodeToArtifactDto(artifactNode,loadSamples)
            LocationDto locationDto = containerLocationToLocationDto(location,artifactDto,loadSamples)
            containerDto.addLocation(locationDto)
        }
        containerDto.setUdfs(containerNode.udfs)
        containerDto
    }

    private LocationDto containerLocationToLocationDto(ContainerLocation containerLocation, ArtifactDto artifactDto, boolean loadSamples = true) {
        String key = LocationDto.containerLocationKey(containerLocation)
        LocationDto locationDto = locations[key]
        if (locationDto) {
            return locationDto
        }
        locationDto = new LocationDto()
        locations[key] = locationDto
        if (containerLocation) {
            locationDto.containerId = containerLocation.containerId
            locationDto.row = containerLocation.row
            locationDto.column = containerLocation.column
        }
        locationDto.artifact = artifactDto
        artifactDto.location = locationDto
        if (containerLocation) {
            locationDto.container = containerIdToContainerDto(containerLocation.containerId,false, loadSamples)
        } else {
            locationDto.container = new ContainerDto()
        }
        locationDto
    }

    ProcessDto processNodeToProcessDto(ProcessNode processNode) {
        ProcessDto processDto = processes[processNode.id]
        if (processDto) {
            return processDto
        }
        processDto = new ProcessDto()
        processes[processNode.id] = processDto
        processDto.limsid = processNode.id
        processDto.name = processNode.processType
        List<UserDefinedField> processUdfs = []
        processUdfs.addAll(cachedProcessUdfs.fetch(processNode.processType) {String processType -> UdfUtil.processTypeUdfs(nodeManager.nodeConfig,processType)})
        processUdfs.addAll(processNode.udfs)
        processDto.setUdfs(processUdfs)
        processDto
    }

    ProjectDto projectNodeToProjectDto(ProjectNode projectNode) {
        ProjectDto projectDto = projects[projectNode.id]
        if (projectDto) {
            return projectDto
        }
        projectDto = new ProjectDto()
        projects[projectNode.id] = projectDto
        projectDto.limsid = projectNode.id
        projectDto.name = projectNode.name
        List<UserDefinedField> projectUdfs = []
        projectUdfs.addAll(cachedProjectUdfs.fetch{UdfUtil.projectUdfs(nodeManager.nodeConfig)})
        projectUdfs.addAll(projectNode.udfs)
        projectDto.setUdfs(projectUdfs)
        projectDto
    }

    SampleDto sampleNodeToSampleDto(SampleNode sampleNode) {
        SampleDto sampleDto = samples[sampleNode.id]
        if (sampleDto) {
            return sampleDto
        }
        sampleDto = new SampleDto()
        samples[sampleNode.id] = sampleDto
        sampleDto.limsid = sampleNode.id
        sampleDto.name = sampleNode.name
        sampleDto.artifact = artifactIdToArtifactDto(sampleNode.artifactId)
        sampleDto.setUdfs(sampleNode.udfs)
        sampleDto
    }

    ArtifactDto artifactNodeToArtifactDto(ArtifactNode artifactNode, boolean loadSamples = true) {
        ArtifactDto artifactDto = artifacts[artifactNode.id]
        if (artifactDto) {
            return artifactDto
        }
        artifactDto = new ArtifactDto()
        artifacts[artifactNode.id] = artifactDto
        artifactDto.limsid = artifactNode.id
        artifactDto.name = artifactNode.name
        artifactNode.getActiveStages(true).each { ArtifactWorkflowStage artifactWorkflowStage ->
            artifactDto.activeStages << new ArtifactStageDto(artifactWorkflowStage)
        }
        artifactDto.setUdfs(artifactNode.udfs)
        if (loadSamples) {
            artifactNode.sampleIds.each { String sampleId ->
                SampleDto sampleDto = sampleIdToSampleDto(sampleId)
                artifactDto.parentSamples << sampleDto
            }
        }
        artifactDto.isSample = artifactNode.isSample
        artifactDto.isAnalyte = artifactNode.isAnalyte
        artifactDto.parentProcessType = artifactNode.parentProcessType
        Analyte analyte = AnalyteFactory.analyteInstance(artifactNode)
        artifactDto.analyteClass = analyte.class.name
        containerLocationToLocationDto(artifactNode.containerLocation,artifactDto,loadSamples)
        artifactDto
    }

    void close() {
        clear()
        nodeManager = null
    }

}
