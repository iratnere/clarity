package gov.doe.jgi.pi.pps.clarity.scripts.aliquoting

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class SampleAnalyteTableBean {
	@FieldMapping(header = 'Sample Tube Freezer Path', cellType = CellTypeEnum.STRING) //CellTypeEnum.STRING, isRowHeader = true)
	public String freezerPath
	@FieldMapping(header = 'ITS Sample ID', cellType = CellTypeEnum.NUMBER)
	public def sampleId
	@FieldMapping(header = 'Sample Lims ID', cellType = CellTypeEnum.STRING)
	public String sampleLimsId
	@FieldMapping(header = 'Sow Item ID', cellType = CellTypeEnum.NUMBER)
	public def sowItemId
	@FieldMapping(header = 'Source Barcode', cellType = CellTypeEnum.STRING)
	public String sourceBarcode
	@FieldMapping(header = 'Source Labware', cellType = CellTypeEnum.STRING)
	public String sourceLabware
	@FieldMapping(header = 'Source Location', cellType = CellTypeEnum.STRING)
	public String sourceLocation
	@FieldMapping(header = 'Library Queue', cellType = CellTypeEnum.STRING)
	public String libraryQueue
	@FieldMapping(header = 'HMW gDNA', cellType = CellTypeEnum.STRING)
	public String hmwgDnaEval
	@FieldMapping(header = 'QC Comments', cellType = CellTypeEnum.STRING)
	public String qcComments
	@FieldMapping(header = 'Destination Barcode', cellType = CellTypeEnum.STRING)
	public String destinationBarcode
	@FieldMapping(header = 'Destination Labware', cellType = CellTypeEnum.STRING)
	public String destinationLabware
	@FieldMapping(header = 'Destination Location', cellType = CellTypeEnum.STRING)
	public String destinationLocation
	@FieldMapping(header = 'Claimant', cellType = CellTypeEnum.STRING)
	public String claimant
	@FieldMapping(header = 'Freezer Status', cellType = CellTypeEnum.STRING)
	public String freezerStatus
	@FieldMapping(header = 'SM QC instruction from PM', cellType = CellTypeEnum.STRING)
	public String smQcInstruction
	@FieldMapping(header = 'SM Notes', cellType = CellTypeEnum.STRING)
	public String smNotes
	@FieldMapping(header = 'Aliquot Lims ID', cellType = CellTypeEnum.STRING)
	public String aliquotLimsId

	@FieldMapping(header='Fluorometer Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal sampleConcentrationNgUl
	@FieldMapping(header = 'Initial Sample  Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal initialSampleVolumeUl
	@FieldMapping(header='Target Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal targetAliquotMassNg
    @FieldMapping(header='Target Max Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal targetMaxVolumeUl
	@FieldMapping(header = 'Final Aliquot Volume(ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal finalAliquotVolumeUl

	@FieldMapping(header='Aliquot Status', cellType = CellTypeEnum.DROPDOWN)
	public def aliquotStatus
	@FieldMapping(header='Failure Mode', cellType = CellTypeEnum.DROPDOWN)
	public def failureMode

	@FieldMapping(header='Initial Sample Mass (ng)', cellType = CellTypeEnum.FORMULA)
	public def initialSampleMassNg
	@FieldMapping(header='Available Volume (ul)', cellType = CellTypeEnum.FORMULA)
	public def availableVolumeUl
	@FieldMapping(header='Available Mass (ng)', cellType = CellTypeEnum.FORMULA)
	public def availableMassNg
	@FieldMapping(header='Target Aliquot Volume (ul)', cellType = CellTypeEnum.FORMULA)
	public def targetAliquotVolumeUl
	@FieldMapping(header='Adjusted Aliquot Volume (ul)', cellType = CellTypeEnum.FORMULA)
	public def adjustedAliquotVolumeUl
	@FieldMapping(header = 'Cumulative Used Volume (ul) - DO NOT EDIT', cellType = CellTypeEnum.FORMULA)
	public def usedVolumeUl
	@FieldMapping(header = 'Final Aliquot Mass(ng)', cellType = CellTypeEnum.FORMULA)
	public def finalAliquotMassNg

    SampleAnalyte sampleAnalyte
	Boolean passed

	def validate() {
        def errorMsg = "Please correct the errors below and upload spreadsheet again.$ClarityProcess.WINDOWS_NEWLINE Data Tab Sample Aliquot($aliquotLimsId):"<<' '
        String labResult = aliquotStatus?.value
        if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
            return  errorMsg << "invalid Aliquot Status '$labResult'"
        }
        passed = labResult == Analyte.PASS
        if (!passed && !failureMode?.value) {
           return  errorMsg << 'unspecified Failure Mode' << ClarityProcess.WINDOWS_NEWLINE
        }
		if (adjustedAliquotVolumeUl == null || parseToBigDecimalError(adjustedAliquotVolumeUl)) {
           return  errorMsg << "unspecified Adjusted Aliquot Volume (ul)" << ClarityProcess.WINDOWS_NEWLINE
		}
		if (passed && failureMode?.value) {
           return  errorMsg << "both set failure mode '$failureMode.value' and status '$labResult'" << ClarityProcess.WINDOWS_NEWLINE
        }
		if (passed && !destinationBarcode) {
           return  errorMsg << 'unspecified Destination Barcode' << ClarityProcess.WINDOWS_NEWLINE
		}
        if (finalAliquotMassNg == null || parseToBigDecimalError(finalAliquotMassNg)) {
           return  errorMsg << "unspecified Final Aliquot Mass(ng)" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (finalAliquotVolumeUl == null) {
           return  errorMsg << "unspecified Final Aliquot Volume(ul)" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (passed) {
            if (finalAliquotVolumeUl == 0.0) {
                return errorMsg << "Final Aliquot Volume(ul) cannot be zero" << ClarityProcess.WINDOWS_NEWLINE
            }
        }
       return null
	}

	static def parseToBigDecimalError(def object) {
        try{
            object as BigDecimal
        } catch (e) {
            return e
        }
        return null
    }

}
