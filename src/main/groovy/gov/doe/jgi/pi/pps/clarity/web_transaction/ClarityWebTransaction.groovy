package gov.doe.jgi.pi.pps.clarity.web_transaction

import gov.doe.jgi.pi.pps.clarity.grails.ClarityCouchdbService
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.couchdb.grails.CouchDbService
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.DateUtil
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import gov.doe.jgi.pi.pps.webtransaction.transaction.*
import gov.doe.jgi.pi.pps.webtransaction.util.ExternalId
import grails.core.GrailsApplication
import grails.validation.ValidationErrors
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONElement
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.event.LoggingEvent
import org.springframework.context.MessageSource
import org.springframework.validation.Errors

class ClarityWebTransaction implements WebTransaction {

	private static final staticLogger = LoggerFactory.getLogger(ClarityWebTransaction.class.name)

    OnDemandCache<String> cachedSubmissionUri = new OnDemandCache<>()

	final WebTransaction delegate

	ClarityWebTransaction(String action) {
		this()
		setAction(action)
	}
	
	ClarityWebTransaction() {
		this.delegate = new WebTransactionDelegate()
	}

	ClarityWebTransaction(GrailsApplication grailsApplication) {
		this.delegate = new WebTransactionDelegate(grailsApplication)
	}

	ClarityWebTransaction(WebTransaction webTransaction) {
		this.delegate = webTransaction
	}

	ClarityWebTransaction(Locale locale, MessageSource messageSource, GrailsApplication grailsApplication) {
		this.delegate = new WebTransactionDelegate(locale, messageSource, grailsApplication)
	}

	ClarityWebTransaction(JSONElement jsonSubmission) {
		this.delegate = new WebTransactionDelegate(jsonSubmission)
	}

	ClarityWebTransaction(Locale locale, MessageSource messageSource, JSONElement jsonSubmission) {
		this.delegate = new WebTransactionDelegate(locale, messageSource, jsonSubmission)
	}

    ClarityWebTransaction build() {
		delegate.open()
		return this
	}

	static ClarityWebTransaction getCurrentTransaction() {
		WebTransaction webTransaction = WebTransactionUtil.currentTransaction
		if (webTransaction) {
			return new ClarityWebTransaction(webTransaction)
		}
		return null
	}

	static ClarityWebTransaction requireCurrentTransaction() {
		WebTransaction webTransaction = WebTransactionUtil.requireCurrentTransaction()
		return new ClarityWebTransaction(webTransaction)
	}

	synchronized ClarityWebTransaction setNodeManager(username, password) {
		//addChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value, new ClarityProcessSession(testMode))
        NodeManagerSession nodeManagerSession = new NodeManagerSession(username,password)
		addChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value, nodeManagerSession)
		return this
	}

	synchronized ClarityWebTransaction setNodeManager(boolean testMode = false) {
		//addChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value, new ClarityProcessSession(testMode))
        NodeManagerSession nodeManagerSession = (NodeManagerSession) getChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value)
		if (!nodeManagerSession) {
			nodeManagerSession = new NodeManagerSession(testMode)
			addChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value, nodeManagerSession)
		}
		return this
	}

	Map<String,String> getWebTransactionHeaders() {
		delegate.webTransactionHeaders
	}

	String getAction() {
		delegate.action
	}

	void setAction(String action) {
		delegate.action = action
	}

	NodeManager getNodeManager() {
		NodeManagerSession nodeManagerSession = null
		try {
			nodeManagerSession = (NodeManagerSession) getChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value)
		} catch (ignore) {}
		if (!nodeManagerSession) {
			setNodeManager()
			nodeManagerSession = (NodeManagerSession) getChildSession(WebTransactionChildSessionKeys.NODE_MANAGER.value)
		}
		if (!nodeManagerSession.isOpen) {
			nodeManagerSession.openSession(this)
		}
		nodeManagerSession.session
	}

	NodeManager requireNodeManager() {
		NodeManager nodeManager = getNodeManager()
		if (!nodeManager) {
			throw new WebException([code:'webTransaction.nodeManager.notSet', args:[this]],422)
		}
		nodeManager
	}

	@Override
    JSONElement getJsonSubmission() {
		delegate.jsonSubmission
	}

	@Override
	void setJsonSubmission(JSONElement jsonElement) {
		delegate.setJsonSubmission(jsonElement)
	}

	@Override
    JSONObject getJsonResponse() {
		delegate.jsonResponse
	}

    @Override
    void generateJsonResponse() {
        delegate.generateJsonResponse()
    }

	@Override
    JSONObject getLogData() {
		delegate.logData
	}

	@Override
	boolean getOpened() {
		return delegate.opened
	}

	@Override
	void open() {
		delegate.open()
	}

	@Override
    JSONArray getMessages() {
		delegate.messages
	}

	@Override
    JSONArray getErrorMessages() {
		delegate.errorMessages
	}

    ClarityWebTransaction setLoggingEnabled() {
		delegate.loggingEnabled = true
        return this
    }

    boolean getLoggingEnabled() {
		delegate.loggingEnabled
    }

	void setLoggingEnabled(boolean enabled) {
		delegate.loggingEnabled = enabled
	}

    JSONArray getLoggerMessages() {
		return delegate.loggerMessages
	}

	@Override
	List<LoggingEvent> getLoggingEvents() {
		delegate.loggingEvents
	}

    @Override
    void addLoggingEvent(LoggingEvent loggingEvent) {
        delegate.addLoggingEvent(loggingEvent)
    }

    @Override
    ConfigObject getData() {
		delegate.data
	}

    ClarityWebTransaction setTestMode() {
		delegate.setTestMode()
		return this
	}

	@Override
	boolean getExtractJsonSubmission() {
		delegate.extractJsonSubmission
	}

	@Override
	void setExtractJsonSubmission(boolean doExtract) {
		delegate.setExtractJsonSubmission(doExtract)
	}

	@Override
    WebTransactionRecorder getWebTransactionRecorder() {
		delegate.webTransactionRecorder
	}

	@Override
	void setWebTransactionRecorder(WebTransactionRecorder webTransactionRecorder) {
		delegate.webTransactionRecorder = webTransactionRecorder
	}

	@Override
	boolean getClosed() {
		delegate.closed
	}

	@Override
	String getTransactionId() {
		delegate.transactionId
	}

	String getSubmissionUri() {
		cachedSubmissionUri.fetch {
			ClarityCouchdbService.CouchDatabase couchdb = ClarityCouchdbService.CouchDatabase.WEB_TRANSACTION
            CouchDbService couchDbService = (CouchDbService) requireApplicationBean(CouchDbService)
			URL url = new URL(couchDbService.couchDbUrl(couchdb.name))
			"http://${url.host}:${url.port}/${couchdb.name}/${transactionId}".toString()
		}
	}

	@Override
    ExternalId getExternalId() {
		delegate.externalId
	}

	@Override
	void setExternalId(ExternalId externalId) {
		delegate.externalId = externalId
	}

	@Override
	Date getStartTime() {
		delegate.startTime
	}

	@Override
	void setStartTime(Date date) {
		delegate.setStartTime(date)
	}

	@Override
	Date getEndTime() {
		delegate.endTime
	}

	@Override
	void setEndTime(Date date) {
		delegate.setEndTime()
	}

	@Override
	String getStartTimeString() {
		delegate.startTimeString
	}

	@Override
	String getEndTimeString() {
		delegate.endTimeString
	}

	@Override
	Integer getStatusCode() {
		delegate.statusCode
	}

	@Override
	void setStatusCode(Integer statusCode) {
		delegate.setStatusCode(statusCode)
	}

	@Override
	String getSubmittedBy() {
		delegate.submittedBy
	}

	@Override
	void setSubmittedBy(String submittedBy) {
		delegate.setSubmittedBy(submittedBy)
	}

	@Override
    GrailsApplication getGrailsApplication() {
		delegate.grailsApplication
	}

	@Override
	void setGrailsApplication(GrailsApplication grailsApplication) {
		delegate.setGrailsApplication(grailsApplication)
	}

	@Override
    MessageSource getMessageSource() {
		delegate.messageSource
	}

	@Override
	void setMessageSource(MessageSource messageSource) {
		delegate.setMessageSource(messageSource)
	}

	@Override
	Locale getLocale() {
		delegate.locale
	}

	@Override
	void setLocale(Locale locale) {
		delegate.setLocale(locale)
	}

	@Override
	boolean getTestMode() {
		return delegate.testMode
	}

    ClarityWebTransaction addChildSession(String key, WebTransactionChildSession childSession) {
		delegate.addChildSession(key, childSession)
		return this
	}

	@Override
    WebTransactionChildSession getChildSession(String key) {
		delegate.getChildSession(key)
	}

	@Override
    WebTransactionChildSession requireChildSession(String key) {
		delegate.requireChildSession(key)
	}

	@Override
	Map<String, WebTransactionChildSession> getChildSessions() {
		return delegate.childSessions
	}

	@Override
    WebTransactionChildSession removeChildSession(String key) {
		delegate.removeChildSession(key)
	}

	@Override
	void exit() {
		delegate.exit()
	}

	@Override
	void exit(Integer statusCode) {
		delegate.exit(statusCode)
	}

	@Override
	String getLoggerName() {
		delegate.loggerName
	}

	@Override
    Logger getTransactionLogger() {
		delegate?.transactionLogger
	}

	static Logger getLogger() {
		(currentTransaction?.transactionLogger) ?: staticLogger
	}

	@Override
	void addErrorMessage(Object message) {
		delegate.addErrorMessage(message)
	}

	@Override
	void addErrorMessage(Object message, String defaultMessage) {
		delegate.addErrorMessage(message,defaultMessage)
	}

	@Override
	void addErrorMessages(Iterable messages) {
		delegate.addErrorMessages(messages)
	}

	@Override
	void addExceptionErrorMessage(Throwable t) {
		delegate.addExceptionErrorMessage(t)
	}

	@Override
	void addExceptionErrorMessage(Throwable t, Map msgMap) {
		delegate.addExceptionErrorMessage(t,msgMap)
	}

	@Override
	void addExceptionErrorMessage(Throwable t, Map msgMap, Integer statusCode) {
		delegate.addExceptionErrorMessage(t,msgMap,statusCode)
	}

	@Override
	void addValidationErrors(ValidationErrors errors) {
		delegate.addValidationErrors(errors)
	}

	@Override
	void addValidationErrors(ValidationErrors errors, Map msgMap) {
		delegate.addValidationErrors(errors,msgMap)
	}

	@Override
	void addErrors(Errors errors) {
		delegate.addErrors(errors)
	}

	@Override
	void addErrors(Errors errors, Map msgMap) {
		delegate.addErrors(errors,msgMap)
	}

	@Override
	void exitWithErrorMessage(Object message) {
		delegate.exitWithErrorMessage(message)
	}

	@Override
	void exitWithErrorMessage(Object message, Integer statusCode) {
		delegate.exitWithErrorMessage(message,statusCode)
	}

    ClarityWebTransaction performTransaction(Closure action) {
		delegate.performTransaction(action)
		return this
	}

    ClarityWebTransaction performTransaction(WebTransactionRecorder webTransactionRecorder, Closure action) {
		delegate.performTransaction(webTransactionRecorder,action)
		return this
	}

	@Override
	Object getApplicationBean(Class beanClass) {
		delegate.getApplicationBean(beanClass)
	}

	@Override
	Object requireApplicationBean(Class beanClass) {
		delegate.requireApplicationBean(beanClass)
	}

	@Override
	void close() {
		delegate?.close()
	}

	void addRoutings(Collection<RoutingRequest> routingRequests, Map activeStagesBefore) {
		if (!routingRequests) {
			return
		}
        JSONArray routingList = delegate.jsonResponse.'routings'
		if (!routingList) {
			routingList = new JSONArray()
			delegate.jsonResponse.'routings' = routingList
			delegate.jsonResponse.'active-stages-before-routings' = new JSONObject()
		}
        JSONObject activeStageJson = delegate.jsonResponse.'active-stages-before-routings'
        JsonUtil.toJsonObject(activeStagesBefore).each { key, val ->
			if (!activeStageJson.containsKey(key)) {
				activeStageJson[key] = val
			}
		}
        JSONObject routingEvent = new JSONObject()
		routingList << routingEvent
		routingEvent.'routing-date' = DateUtil.dateToJsonArray()
		//routingEvent.'active-stages-after' = JsonUtil.toJsonObject(activeStagesAfter)
		JSONArray routings = new JSONArray()
		routingEvent.'routing-requests' = routings
		routingRequests.each { RoutingRequest routingRequest ->
			if (routingRequest) {
				routings << routingRequest.toJson()
			}
		}

	}

}
