package gov.doe.jgi.pi.pps.webtransaction.exception

import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource

public class MessageResolver {
	
	static Logger logger = LoggerFactory.getLogger(MessageResolver.class.name)
	
	static String resolveLocaleMessage(MessageSource messageSource, Locale locale, Object message, String defaultMessage = null) {
		String messageString = null
		if (message) {			
			if (message instanceof Map) {
				String mapMessage = message['message']?.toString()
				if (!mapMessage) {
					mapMessage = defaultMessage
				}
				String code = message['code']?.toString()
				Object args = message['args']
				List argumentList = []
				if (code) {
					if (args?.respondsTo('collect')) {
						argumentList += args.collect { it?.toString() }
					} else if (args?.respondsTo('toList')) {
						argumentList += args.toList().collect { it?.toString() }
					}
					//getMessage(String code, Object[] args, String defaultMessage, Locale locale)
					messageString = messageSource.getMessage(code, argumentList.toArray(), mapMessage?:code, locale)
				} else {
					messageString = mapMessage
				}
			}
			if (!messageString) {
				messageString = defaultMessage
			}
			if (!messageString) {
				messageString = message.toString()
			}
		}
		logger.trace "returning message: ${(messageString != null)?'"' + messageString + '"':'null'}"
		return messageString
	}
	
	static JSONObject resolveLocaleMessageJson(MessageSource messageSource, Locale locale, Object message, String defaultMessage = null) {
		JSONObject json = new JSONObject()
		if (message) {
			json['message'] = resolveLocaleMessage(messageSource, locale, message, defaultMessage)
			if (message instanceof Map) {
				message.each { Object key, Object val ->
					String keyString = key?.toString()
					if (keyString && !(keyString in ['code','args','message'])) {
						json[keyString] = val
					}
				}
			}
		}
		return json
	}
	
	static JSONArray resolveExceptionMessagesJson(MessageSource messageSource, Locale locale, Throwable t) {
		JSONArray json = new JSONArray()
		if (t) {
			if (t instanceof MessageListException) {
				((MessageListException) t).messages.each{ Map message ->
                    JSONObject localeMessage = resolveLocaleMessageJson(messageSource, locale, message, t.toString())
					if (localeMessage) {
						json << localeMessage
					}
				}
			} else if (t instanceof BundleAwareException) {
				JSONObject localeMessage = resolveLocaleMessageJson(messageSource, locale, ((BundleAwareException) t).messageBundleParams, t.toString())
				if (localeMessage) {
					json << localeMessage
				}
			} else if (t) {
				json << resolveLocaleMessageJson(messageSource, locale, t)
			}
		}
		return json
	}
	
	static List<String> resolveExceptionMessages(MessageSource messageSource, Locale locale, Throwable t) {
		List<String> messages = []
		if (t instanceof MessageListException) {
			((MessageListException) t).messages.each{ Map message ->
				String localeMessage = resolveLocaleMessage(messageSource, locale, message, t.toString())
				if (localeMessage) {
					messages << localeMessage
				}
			}
		} else if (t instanceof BundleAwareException) {
			String localeMessage = resolveLocaleMessage(messageSource, locale, ((BundleAwareException) t).messageBundleParams, t.toString())
			if (localeMessage) {
				messages << localeMessage
			}
		} else if (t) {
			messages << t.toString()
		}
		return messages
	}
	
	static String exceptionToString(MessageSource messageSource, Locale locale, Throwable t) {
		List<String> messages = []
		if (t instanceof MessageListException) {
			((MessageListException) t).messages.each{ Map message ->
				String localeMessage = resolveLocaleMessage(messageSource, locale, message, t.toString())
				if (localeMessage) {
					messages << localeMessage
				}
			}
		} else if (t instanceof BundleAwareException) {
			String localeMessage = resolveLocaleMessage(messageSource, locale, ((BundleAwareException) t).messageBundleParams, t.toString())
			if (localeMessage) {
				messages << localeMessage
			}
		} else if (t) {
			messages << t.toString()
		}
		if (messages.size() == 1) {
			return messages[0]
		} else {
			return messages.join(',\n')
		}
	}
	
}
