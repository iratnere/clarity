package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

/**
 * Created by datjandra on 7/7/2015.
 */
enum AttributeKeys {
    ANALYTE_STATUS,
    ANALYTE_STATUS_LIST,
    CONTAINER_CLASS,
    DOP,
    PLATE_STATUS,
    LC_ATTEMPT,
    ITAG,
    EXOME,
    SINGLE_CELL_INTERNAL,
    EXTERNAL,
    ANALYTE_CLASS,
    SEQUENCER_MODEL
}
