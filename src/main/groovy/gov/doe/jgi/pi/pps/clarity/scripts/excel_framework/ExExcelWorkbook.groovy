package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

import org.apache.poi.ss.formula.EvaluationWorkbook
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook
import org.apache.poi.xssf.usermodel.XSSFSheet

class ExExcelWorkbook extends ExcelWorkbook{

    List<String> getSupportedExtensions(){
        return ['.xlsx', '.xlsm', '.xltx', '.xltm']
    }

    ExExcelWorkbook(String inputFile, boolean testMode = false) {
        super()
        sections = [:]
        this.testMode = testMode
        templateName = inputFile
        openExcelFile()
    }

    ExExcelWorkbook(InputStream inputStream, boolean testMode = false) {
        super(inputStream, testMode)
    }

    def getDVRecords(Sheet sheet){
        XSSFSheet xssfSheet = (XSSFSheet) sheet
        return xssfSheet.dataValidations
    }

    void optimiseStyles(){
        if(workbook.numCellStyles > 64000)
            throw new IllegalStateException("Exceeded maximum styles count")
    }

    EvaluationWorkbook getEvaluationWorkbook(){
        return XSSFEvaluationWorkbook.create(workbook)
    }
}
