package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import grails.gorm.transactions.Transactional
import groovy.json.JsonBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import org.grails.web.json.JSONObject
import org.slf4j.Logger

@Transactional
class RqcPacBioService {

    Logger logger = Logger.getLogger(this.class.name)
    final static String RESPONSE_CODE = 'response-code'
    final static String RESPONSE_BODY = 'response-body'

    def grailsApplication

    void callPacBioSequencingCompleteService(def process) {
        def contactId = process.researcher?.contactId
        def libraryNames = getLibraryNames(process)
        def jsonSubmission = getJsonSubmission(contactId, libraryNames)
        String url = "${grailsApplication.config.getProperty('rqcPacBioReady.url')}"
        HTTPBuilder http = new HTTPBuilder(url)
        http.request(Method.POST, ContentType.JSON) { req ->
            body = jsonSubmission.toPrettyString()
            response.success = { HttpResponseDecorator resp, respJson ->
                JSONObject jsonResponse = buildResponseJson(jsonSubmission, resp, respJson, uri)
                submitToCouchdb(jsonResponse)
            }

            response.failure = { HttpResponseDecorator resp, respJson ->
                def json = JsonUtil.toJson(respJson)
                logger.error("Rqc service response: status [${resp.status}], body [${json?.toString(2)}]")
                def errorMsg
                if (resp.status >= 500)
                    errorMsg = 'cannot reach RQC website'
                else if (resp.status == 422) {
                    errorMsg = json?.'errors'?.collect { it.'message' }?.join('\n')
                }
                errorMsg ?: 'unexpected response from RQC website'
                process.postErrorMessage(errorMsg)
            }
        }
    }

    def getLibraryNames(def process){
        List<PacBioBindingComplex> inputAnalytes = process.inputAnalytes
        def libraryNames = []
        inputAnalytes.each { PacBioBindingComplex bindingComplex ->
            Analyte parentAnalyte = bindingComplex.parentAnalyte.parentAnalyte //return ClarityLibraryStock or ClarityLibraryPool
            if (parentAnalyte instanceof ClarityLibraryStock){
                libraryNames.add(parentAnalyte.name)
            }
            if (parentAnalyte instanceof ClarityLibraryPool){
                libraryNames.addAll((parentAnalyte as ClarityLibraryPool).poolMembers*.name)
            }
        }
        libraryNames
    }

    JSONObject buildResponseJson(submissionBody, HttpResponseDecorator resp, respJson, uri) {
        Date notificationTime = new Date() //resp.headers['Date']
        JSONObject responseJson = new JSONObject()
        responseJson['success'] = resp.success
        responseJson['notification-time'] = notificationTime.format('yyyy-MM-dd HH:mm:ss z')
        responseJson['submission-url'] = uri //resp.context['http.request'].URI
        responseJson['submission-content-type'] = resp.contentType
        responseJson['submission-method'] = resp.context['http.request']?.method
        responseJson['submission-body'] = submissionBody
        responseJson[RESPONSE_CODE] = resp.status
        responseJson[RESPONSE_BODY] = """
            Rqc service response ${resp.status}:
            ${JsonUtil.toJson(respJson)?.toString(2)}
        """
        logger.info"Complete PacBio Sequencing service response: ${responseJson?.toString(2)}"
        responseJson
    }

    def getJsonSubmission(def contactId, List<String> libraries) {
        def jsonSubmission = new JsonBuilder()
        jsonSubmission {
            'submitted-by-cid'(contactId)
            'sequencing-complete-library-names'(libraries.collect { "$it" })
        }
        logger.info "PacBio Complete Submission ${jsonSubmission.toPrettyString()}"
        jsonSubmission
    }

    def submitToCouchdb(JSONObject couchDbRecord) {
        /* TODO fix this - where is CouchDbEntity parameter to insert?
        CouchDb couchDb = ClarityCouchdbService.CouchDatabase.PROCESS_SCRIPT.couchDb
        couchDbService.insert
        */
    }
}
