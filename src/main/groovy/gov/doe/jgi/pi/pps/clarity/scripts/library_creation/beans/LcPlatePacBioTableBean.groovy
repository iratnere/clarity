package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LcPlatePacBioTableBean {
	@FieldMapping(header = 'Well', cellType = CellTypeEnum.STRING, isRowHeader = true)
	public String well
	@FieldMapping(header = 'Library LIMS ID', cellType = CellTypeEnum.STRING)
	public String libraryLimsId
	@FieldMapping(header = 'Library Name', cellType = CellTypeEnum.STRING)
	public String libraryName
	@FieldMapping(header = 'Index', cellType = CellTypeEnum.STRING)
	public def libraryIndexName
	@FieldMapping(header='Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal aliquotMass
	@FieldMapping(header='Library Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryConcentration
	@FieldMapping(header='Library Template Size (bp)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryTemplateSize
	@FieldMapping(header='Library Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryVolume
	@FieldMapping(header='Library Molarity (pM)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryMolarity
	@FieldMapping(header='Pool Number', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolNumber
	@FieldMapping(header = 'LC - Notes', cellType = CellTypeEnum.STRING)
	public String lcNotes

	@FieldMapping(header='Library QC (Pass/Fail)', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList libraryQcResult
	@FieldMapping(header='Library Failure Mode', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList libraryFailureMode

    Boolean passed

	def validate() {
        def errorMsg = StringBuilder.newInstance()
		String labResult = libraryQcResult?.value
		if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
			errorMsg << "invalid Library QC Result '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
			return buildErrorMessage(errorMsg)
		}
		passed = labResult == Analyte.PASS
		if (!passed && !libraryFailureMode?.value) {
			errorMsg << "unspecified Library QC Failure Mode $ClarityProcess.WINDOWS_NEWLINE"
			return buildErrorMessage(errorMsg)
		}
		if (!passed)
			return null
		//validate passed libraries
		if (libraryFailureMode?.value) {
			errorMsg << "incorrect failure mode '$libraryFailureMode.value' or status '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
		}
        if (!libraryConcentration) {
			errorMsg << 'unspecified Library Concentration (ng/ul)' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (!libraryTemplateSize) {
			errorMsg << 'unspecified Library Template Size (bp)' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (!libraryVolume) {
			errorMsg << 'unspecified Library Volume (ul)' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (!libraryMolarity) {
			errorMsg << "unspecified Library Molarity (pM)" << ClarityProcess.WINDOWS_NEWLINE
		}
        return buildErrorMessage(errorMsg)
    }

    def buildErrorMessage(def errorMsg) {
        def startErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << "Plate Info Section $ClarityProcess.WINDOWS_NEWLINE"
		startErrorMsg << "Library Stock($libraryName):$ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

	void populateRequiredFields(ClarityProcess process, int passCount){
		LibraryStockFailureModesService libraryStockFailureModesService = (LibraryStockFailureModesService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(LibraryStockFailureModesService)
		libraryQcResult = libraryStockFailureModesService.dropDownPassFail
		libraryFailureMode = libraryStockFailureModesService.dropDownFailureModes
		if(passCount > 0)
			libraryQcResult.value = Analyte.PASS
		else{
			libraryQcResult.value = Analyte.FAIL
			libraryFailureMode.value = libraryFailureMode.controlledVocabulary[0]
		}
		libraryConcentration = 1
		libraryTemplateSize = 1
		libraryVolume = 10
		libraryMolarity = 2
		libraryIndexName = ArtifactIndexService.INDEX_NAME_N_A
	}

	boolean getRequiresIndex(){
		return libraryIndexName && !(libraryIndexName?.equalsIgnoreCase(ArtifactIndexService.INDEX_NAME_N_A))
	}

	void setIndexName(String indexName){
		libraryIndexName = indexName
	}
}
