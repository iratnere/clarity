package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryPoolFailureModesService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPoolTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

/**
 * Created by tlpaley on 4/1/15.
 */
@Deprecated
class LibraryCreationItag extends LibraryCreation {

    final static String TEMPLATE_NAME = 'resources/templates/excel/LcItag'
    private final static def DEFAULT_POOL_NUMBER = 1
    OutputPool outputPool

    LibraryCreationItag(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputPlate(process)
        outputPool = new OutputPool(process) //common methods for Exome/iTags
    }

    @Override
    String getLcTableBeanClassName() {
        output.TABLE_CLASS_NAME
    }

    @Override
    void  updateLibraryPoolUdfs() {
        outputPool.processLibraryPools()
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        outputPool.populateLibraryCreationSheet(TEMPLATE_NAME, getTableSectionPools(), getTableSectionLibraries())
    }

    @Override
    void  updateLibraryStockUdfs() {
        outputPool.updateLibraryStockUdfs()
        output.updateLibraryIndexUdf(process.outputAnalytes as List<ClarityLibraryStock>)
    }

    @Override
    Section getTableSectionLibraries(){
        Section tableSection = new TableSection(0,LcPlateTableBean.class,'A26', 'J122')
        def tableBeansArray = []
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcPlateTableBean bean = new LcPlateTableBean()
            bean.well = clarityLibraryStock.artifactNode.location?.replaceAll(':','')
            bean.libraryLimsId = clarityLibraryStock.id
            bean.libraryName = clarityLibraryStock.name
            bean.aliquotMass = clarityLibraryStock.parentAnalyte?.massNg
            bean.poolNumber = DEFAULT_POOL_NUMBER
            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }

    @Override
    void moveToNextWorkflow() {
        List<ClarityLibraryPool> passedPools = outputPool.clarityLibraryPools.findAll { it.udfLibraryQcResult == Analyte.PASS }
        if (passedPools) {
            process.routeAnalytes(defaultStage,passedPools)
            //move any failed libraries, libraries included in failed pools or libraries not included in pools to Abandon queue
            List<ClarityLibraryStock> failedStocks = process.outputAnalytes.findAll { it.udfLibraryQcResult == Analyte.FAIL }
            process.routeAnalytes(Stage.ABANDON_QUEUE, failedStocks)
        } else {
            List<ClarityLibraryPool> failedPools = outputPool.clarityLibraryPools?.findAll { it.udfLibraryQcResult == Analyte.FAIL }
            process.routeArtifactIds(Stage.ABANDON_QUEUE,failedPools*.id)
            //PPS-3121: submit ABANDON RoutingRequests because of the DB triggers
            //first move to Stage.ABANDON_QUEUE then move to Stage.ALIQUOT_CREATION_DNA
            process.processCompleteFailure(process.outputAnalytes)
        }
        process.postInfoMessage(buildInfoMessage() as String)
    }

    @Override
    void createPoolPrintLabelsFile() {
        process.writeLabels(outputPool.clarityLibraryPools)
        process.writeLabels(outputPool.clarityLibraryPools)
    }

    Section getTableSectionPools() {
        LibraryPoolFailureModesService libraryPoolFailureModesService = (LibraryPoolFailureModesService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(LibraryPoolFailureModesService)
        Section tableSection = new TableSection(0, LcPoolTableBean.class, 'L26', 'R27')
        def tableBeansArray = []
        LcPoolTableBean bean = new LcPoolTableBean()
        bean.poolResult = libraryPoolFailureModesService.dropDownPassFail
        bean.failureMode = libraryPoolFailureModesService.dropDownFailureModes
        tableBeansArray << bean
        tableSection.setData(tableBeansArray)
        return tableSection
    }

    @Override
    void updateLcaUdfs() {
        outputPool.updateLibraryCreationAttemptUdfs()
    }

    def buildInfoMessage() {
        def infoMsg = "Library pools have been created.$ClarityProcess.WINDOWS_NEWLINE" << "Pool Name/Pool Barcode/LS Plate Barcode/LS Plate Name$ClarityProcess.WINDOWS_NEWLINE"
        def lsPlateBarcode = process.outputAnalytes[0].containerNode.name
        def lsLabel = process.outputAnalytes[0].getContainerUdfLabel()
        outputPool.clarityLibraryPools.each{
            infoMsg << "$it.name / ${it.containerNode.name} / $lsPlateBarcode / $lsLabel$ClarityProcess.WINDOWS_NEWLINE"
        }
        infoMsg
    }

    String getTemplate(){
        return TEMPLATE_NAME
    }

    int getTemplateIndex() {
        return 8
    }
}
