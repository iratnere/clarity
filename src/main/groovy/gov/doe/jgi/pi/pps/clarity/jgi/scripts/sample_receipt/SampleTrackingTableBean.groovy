package gov.doe.jgi.pi.pps.clarity.jgi.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.SampleTrackingService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import gov.doe.jgi.pi.pps.clarity.model.researcher.ResearcherFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage

/**
 * Created by datjandra on 11/10/2015.
 */
class SampleTrackingTableBean {

    @FieldMapping(header="Container Barcode", cellType= CellTypeEnum.STRING)
    public String containerBarcode

    @FieldMapping(header="Status", cellType= CellTypeEnum.STRING)
    public String status

    @FieldMapping(header="Operator Id", cellType= CellTypeEnum.NUMBER)
    public BigDecimal operatorId

    @FieldMapping(header="Date", cellType= CellTypeEnum.STRING)
    public String date

    static final List<String> VALID_STATUSES = [SampleTrackingService.STATUS_SHIPPED, SampleTrackingService.STATUS_TRASHED, SampleTrackingService.STATUS_ONSITE]
    private final static Map<String, Stage> STAGE_LOOKUP = [
            (SampleTrackingService.STATUS_ONSITE) : Stage.ON_SITE,
            (SampleTrackingService.STATUS_TRASHED): Stage.TRASH,
            (SampleTrackingService.STATUS_SHIPPED): Stage.SHIP_OFFSITE
    ]
    List<ContainerNode> beanContainers
    List<Analyte> beanAnalytes = []
    List<Long> pmoSampleIds = []
    List<RoutingRequest> routingRequests = []
    Researcher researcher

    String validateBean(NodeManager nodeManager){
        String errors = ''
        if(!containerBarcode)
            errors += "Barcode is a required field.\n"
        if(!(status in VALID_STATUSES))
            errors += "Invalid status ${status}.\n"
        if(operatorId == null)
            errors += "Operator Id is required.\n"
        researcher = ResearcherFactory.researcherForContactId(nodeManager, operatorId?.longValue())
        if(!researcher)
            errors += "Invalid operator Id $operatorId.\n"
        return errors
    }

    String getBarcode(){
        return containerBarcode.minus(~/\.[0-9]*/)
    }

    void setBeanContainers(List<ContainerNode> containerNodes){
        beanContainers = containerNodes
        beanContainers.each{ContainerNode container ->
            container.contentsArtifactNodes.each{ArtifactNode artifactNode ->
                Analyte analyte = AnalyteFactory.analyteInstance(artifactNode)
                beanAnalytes << analyte
                addPmoSampleId(analyte)
                addRoutingRequests(analyte.artifactNode)
            }
        }
    }

    void addPmoSampleId(Analyte analyte){
        if(analyte.claritySample instanceof PmoSample)
            pmoSampleIds << analyte.claritySample.pmoSampleId
    }

    void addRoutingRequests(ArtifactNode analyte){
        if(status != SampleTrackingService.STATUS_ONSITE){
            analyte.activeStages.each { ArtifactWorkflowStage stage ->
                routingRequests << new RoutingRequest(artifactId: analyte.id, routingUri: stage.uri, action: Routing.Action.unassign)
            }
        }
    }

    boolean getIsContainerInClarity(){
        return !beanContainers?.isEmpty()
    }

    void updateContainerUdfs(){
        beanContainers?.each{ContainerNode containerNode ->
            containerNode.setUdf(ClarityUdf.CONTAINER_LOCATION.udf, status)
            containerNode.setUdf(ClarityUdf.CONTAINER_LOCATION_DATE.udf, date)
        }
    }

    Stage getStage(){
        STAGE_LOOKUP.get(status)
    }
}
