package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.PoolCreation
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling.PoolingFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager

class PrePoolingWorksheet {
    ExcelWorkbook prePoolingWorkbook
    Map<String, List<LibraryInformation>> prePoolingBeanGroups
    Map<String, Pooling> groupPooling
    Map<String, List> sectionDataMap
    Map<String, LaneFraction> libraryNameToFraction
    Map<String, Analyte> inputAnalytes = [:]

    private static final String POOL_TABLE = "Pool Table"
    private static final String MESSAGE = "Message"
    private static final String ACTIONS = "Actions Table"
    private static final String CALCULATIONS = "Calculations Table"
    private static final String SUMMARY = "Summary Table"

    static final String POOL_SECTION_START_ADDRESS = 'A3'
    static final String ACTION_SECTION_START_ADDRESS = 'Q1'
    static final String ACTION_SECTION_END_ADDRESS = 'R2'
    static final String SUMMARY_SECTION_START_ADDRESS = 'Q3'
    static final String SUMMARY_SECTION_END_ADDRESS = 'S100'
    static final String CALCULATION_SECTION_START_ADDRESS = 'A4'
    static final String MESSAGE_SECTION_STARTCELL = 'N2'

    PrePoolingWorksheet(String templateName, List<LibraryInformation> prePoolingLibInfoList, Map<String, LaneFraction> libraryNameToFraction, boolean testMode = false){
        if(!prePoolingLibInfoList)
            return
        prePoolingWorkbook = new ExcelWorkbook(templateName, testMode)
        this.libraryNameToFraction = libraryNameToFraction
        makeGroups(prePoolingLibInfoList)
        sectionDataMap = [:].withDefault {[]}
    }

    PrePoolingWorksheet(String prePoolingFileLimsId, List<Analyte> analytes){
        prePoolingWorkbook = new ExcelWorkbook(prePoolingFileLimsId)
        prePoolingWorkbook.load()
        groupPooling = [:]
        analytes.each{
            inputAnalytes[it.id] = it
        }
    }
    PrePoolingWorksheet(){}

    void makeGroups(List<LibraryInformation> beans) {
        prePoolingBeanGroups = [:].withDefault {[]}
        groupPooling = [:]
        //Grouping analytes by DOP, platform, sequencer model, run mode, single and dual indexes, itag, index sequence size
        beans.each { LibraryInformation bean ->
            def poolingKey = bean.poolingKey
            prePoolingBeanGroups[poolingKey] << bean
        }
        prePoolingBeanGroups.each{String key, List<LibraryInformation> beanLists ->
            groupPooling[key] = PoolingFactory.getPoolingTechnique(key, beanLists, libraryNameToFraction)
            beanLists.each { LibraryInformation bean ->
                bean.customCellStyle = groupPooling[key].getCellStyle(prePoolingWorkbook.getSheet(0))
            }
        }

    }

    List<Section> getAllSections(){
        if(!sectionDataMap || !sectionDataMap[POOL_TABLE])
            prepareDataForSections()
        return [poolingTableSection, messageSection, actionsSection, summarySection, calculationsSection]
    }

    private void prepareDataForSections(){
        int poolNumber = 1
        Map<String, List<LibraryInformation>> pools = [:]
        prePoolingBeanGroups.each{String groupKey, List<LibraryInformation> members ->
            Pooling pooling = groupPooling[groupKey]
            poolNumber = pooling.completePoolPrepBeans(poolNumber)
            members = members.sort({ LibraryInformation bean1, LibraryInformation bean2 ->
                //PPS-4757 Clarity worksheets: PoolingPrep and Pool Creation - sort by Library Name
                //the lab operator prefers to see the pooling prep and pooling sheet ordered by library name even if the pool numbers will be out of order, intertwined and it will be hard to see a pool constituents at a glance.
                //PPS-4757 after diane's comment - Prepooling sheet is sorted by pool numbers within each group and then by library name
                if (bean1.poolNumber != bean2.poolNumber)
                    return bean1.poolNumber <=> bean2.poolNumber
                return bean1.libraryName <=> bean2.libraryName
            })
            def unpooled = members.findAll{it.poolNumber == null}
            if(unpooled){
                pools[groupKey] = members-unpooled
                pools[groupKey].addAll(unpooled)
            }
            else{
                pools[groupKey] = members
            }
            def beans
            sectionDataMap[POOL_TABLE].addAll(pools[groupKey])
            if(!sectionDataMap[ACTIONS]) {
                beans = pooling.actionBeans
                if(beans)
                    sectionDataMap[ACTIONS].addAll(beans)
            }
            beans = pooling.summaryBeans
            if(beans)
                sectionDataMap[SUMMARY].addAll(beans)
            beans = pooling.calculationBeans
            if(beans)
                sectionDataMap[CALCULATIONS].addAll(beans)
            if(!sectionDataMap[MESSAGE]) {
                beans = pooling.message
                if(beans)
                    sectionDataMap[MESSAGE].addAll(beans)
            }
        }
    }

    Section getPoolingTableSection(){
        Section tableSection = new TableSection(0, LibraryInformation.class, POOL_SECTION_START_ADDRESS,null, true)
        tableSection.setData(sectionDataMap[POOL_TABLE])
        return tableSection
    }

    Section getActionsSection(){
        Section section = new TableSection(0, ActionBean.class, ACTION_SECTION_START_ADDRESS, ACTION_SECTION_END_ADDRESS)
        section.setData(sectionDataMap[ACTIONS])
        return section
    }

    Section getSummarySection(){
        Section section = new TableSection(0, PoolSummaryTableBean.class, SUMMARY_SECTION_START_ADDRESS, SUMMARY_SECTION_END_ADDRESS)
        section.setData(sectionDataMap[SUMMARY])
        return section
    }

    Section getCalculationsSection(){
        Section section = new TableSection(1, CalculationsBean.class, CALCULATION_SECTION_START_ADDRESS)
        section.setData(sectionDataMap[CALCULATIONS])
        return section
    }

    Section getMessageSection(){
        Section oneDSection = new OneDSection(0, CellTypeEnum.STRING, MESSAGE_SECTION_STARTCELL, MESSAGE_SECTION_STARTCELL)
        oneDSection.setData(sectionDataMap[MESSAGE])
        return oneDSection
    }

    ExcelWorkbook upload(NodeManager nodeManager, String fileName){
        prePoolingWorkbook.addSections(allSections)
        prePoolingWorkbook.store(nodeManager.nodeConfig,fileName)
        return prePoolingWorkbook
    }

    Map<Integer, List<Analyte>> readPoolsFromPrePoolingSheet(ArtifactIndexService artifactIndexService, ClarityProcess process=null){
        List<LibraryInformation> poolPrepTableBeans = (List<LibraryInformation>) prePoolingWorkbook.sections.values()?.find {
            it.beanClass?.simpleName == LibraryInformation.class.simpleName
        }?.getData()
        def poolBeans = [:].withDefault {[]}
        def pools = [:].withDefault {[]}
        poolPrepTableBeans.each { LibraryInformation bean ->
            bean.analyte = inputAnalytes[bean.analyteLimsId]
            if(bean.poolNumber && bean.poolNumber > 0) {
                poolBeans[bean.poolNumber] << bean
                pools[bean.poolNumber] << bean.analyte.artifactNode
                if(bean.poolNumber > 0) {
                    //PPS-4708 - Pool fraction needs to be carried over to the pool creation worksheet
                    bean.passOnLibraryPercentageSOF()
                }
            }
            else {
                int poolNumber = bean.poolNumber == null ? PoolCreation.REPEAT_POOL_NUMBER: bean.poolNumber
                poolBeans[poolNumber] << bean
                pools[poolNumber] << bean.analyte.artifactNode
            }
        }
        if(process) {
            Map<String,List<String>> messages = validateAllPools(artifactIndexService, poolBeans)
            if(messages[Pooling.ERROR])
                process.postErrorMessage(messages[Pooling.ERROR]?.join("\n"))
            else if(messages[Pooling.WARNING])
                process.postWarningMessage(messages[Pooling.WARNING]?.join("\n"))
        }
        pools
    }

    Map<String,List<String>> validateAllPools(ArtifactIndexService artifactIndexService, Map poolBeans){
        Map<String,List<String>> messages = [:].withDefault {[]}
        def poolNumbers = poolBeans.keySet().findAll{it > 0}
        if(!poolNumbers){
            messages[Pooling.ERROR] << "You must create at least one pool."
        }
        else{
            for(Map.Entry<Integer, List<LibraryInformation>> entry: poolBeans){
                if(entry.key <= 0)
                    continue
                def poolKeys = [:].withDefault {[]}
                setIndexSequences(artifactIndexService, entry.value)
                entry.value.each{
                    poolKeys[it.validationKey] << it.analyteLimsId
                }
                if(poolKeys.size() > 1) {
                    if((entry.key > 0))
                        messages[Pooling.ERROR] << "Pool #${entry.key} is inconsistent. Libraries in this pool ${poolKeys.values() as List} cannot be pooled: $poolKeys"
                    continue
                }
                //PPS-5253: 2 SAG pools cannot be pools together
                def memberSagPools = entry.value.findAll{ it.isSAGPool }
                if(memberSagPools && memberSagPools.size() > 1) {
                    messages[Pooling.ERROR] << "Pool #${entry.key} is inconsistent. SAG pools $memberSagPools cannot be pooled together."
                }
                Pooling pooling = PoolingFactory.getPoolingTechnique(poolKeys.keySet().first(), entry.value)
                Map findings = pooling.validate()
                if(findings[Pooling.ERROR])
                    messages[Pooling.ERROR].addAll(findings[Pooling.ERROR])
                if(findings[Pooling.WARNING])
                    messages[Pooling.WARNING].addAll(findings[Pooling.WARNING])
            }
        }
        return messages
    }

    void setIndexSequences(ArtifactIndexService artifactIndexService, List<LibraryInformation> beans){
        if (!beans)
            return
        List<String> allIndexes = []
        beans.each{ bean ->
            bean.analyte = inputAnalytes[bean.analyteLimsId]
            def indexNames
            if(bean.indexName)
                indexNames = [bean.indexName]
            else
             indexNames = bean.getIndexName(bean.analyte, artifactIndexService)
            if(indexNames.size() >= 1 && indexNames[0] != Analyte.INDEX_NAME_NA) {
                bean.setIndexes(indexNames)
                allIndexes << indexNames
            }
        }
        allIndexes = allIndexes.flatten().unique()
        allIndexes.removeAll('A')
        allIndexes.removeAll('B')
        allIndexes.removeAll(Analyte.INDEX_NAME_NA)
        if(allIndexes) {
            Map<String, String> indexMap = artifactIndexService.getSequences(allIndexes)
            beans.each { LibraryInformation bean ->
                bean.setUpIndexes(indexMap)
            }
        }
    }

    Section readActionSection(){
        return prePoolingWorkbook.sections.values()?.find {
            it.beanClass?.simpleName == ActionBean.class.simpleName
        }
    }
}
