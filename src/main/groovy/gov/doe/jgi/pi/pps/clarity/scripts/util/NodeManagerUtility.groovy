package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.ArtifactIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.GenericIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ArtifactParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.ArtifactParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import org.apache.commons.lang3.NotImplementedException

import java.rmi.NoSuchObjectException

class NodeManagerUtility {
    NodeManager clarityNodeManager

    NodeManagerUtility(NodeManager nodeManager){
        clarityNodeManager = nodeManager
    }

    List<PacBioBindingComplex> getBindingComplexes(Analyte library){
        if(!(library instanceof ClarityLibraryStock) && !(library instanceof ClarityLibraryPool)){
            throw new NotImplementedException("This method can only accept ClarityLibraryStock and ClarityLibraryPool object types")
        }
        List<ArtifactParameterValue> parameters = []
        parameters << ArtifactParameter.NAME.setValue("lb ${library.name}")
        String rLabel = library.reagentLabel
        if (rLabel) {
            parameters << ArtifactParameter.REAGENT_LABEL.setValue(Analyte.REAGENT_LABEL_PREFIX + rLabel)
        }

        ArtifactIdIterator artifactIdIterator = new ArtifactIdIterator(clarityNodeManager.nodeConfig, parameters)
        return clarityNodeManager.getArtifactNodes(artifactIdIterator.findAll()).collect { ArtifactNode artifactNode ->
            AnalyteFactory.analyteInstance(artifactNode)
        }.findAll { it instanceof PacBioBindingComplex }.toList()
    }

    List<PacBioAnnealingComplex> getAnnealingComplexes(Analyte library) {
        if(!(library instanceof ClarityLibraryStock) && !(library instanceof ClarityLibraryPool)){
            throw new NotImplementedException("This method can only accept ClarityLibraryStock and ClarityLibraryPool object types")
        }
        List<ArtifactParameterValue> parameters = []
        parameters << ArtifactParameter.NAME.setValue("la ${library.name}")
        String rLabel = library.reagentLabel
        if (rLabel) {
            parameters << ArtifactParameter.REAGENT_LABEL.setValue(Analyte.REAGENT_LABEL_PREFIX + rLabel)
        }
        ArtifactIdIterator artifactIdIterator = new ArtifactIdIterator(clarityNodeManager.nodeConfig, parameters)
        return clarityNodeManager.getArtifactNodes(artifactIdIterator.findAll()).collect { ArtifactNode artifactNode ->
            AnalyteFactory.analyteInstance(artifactNode)
        }.findAll { it instanceof PacBioAnnealingComplex }.toList()
    }

    List<String> lookUpSamplesBySipGroupName(String groupName) {
        NodeManager nodeManager = ClarityWebTransaction.currentTransaction.nodeManager
        String sampleQueryUrl = nodeManager.nodeConfig.geneusUrl + 'samples?udf.' +
                URLEncoder.encode(ClarityUdf.SAMPLE_GROUP_NAME.value, "UTF-8") + '=' +
                URLEncoder.encode(groupName, "UTF-8")
        Iterator<String> sampleIdIterator = new GenericIdIterator(nodeManager.nodeConfig, sampleQueryUrl, 'sample')
        Set<String> sampleIds = [] as Set
        while (sampleIdIterator.hasNext()) {
            sampleIds << sampleIdIterator.next()
        }
        return sampleIds as List
    }

    List<String> lookUpScheduledSamplesByName(List<String> sampleNames) {
        List<ScheduledSample> scheduledSamples
        if (sampleNames) {
            String query = sampleNames.collect{"name=$it"}.join("&")
            String url = "${clarityNodeManager.nodeConfig.samplesUrl}?${URLEncoder.encode(query,'UTF-8')}"
            String xml = HttpUtils.httpGet(clarityNodeManager.nodeConfig, url);
            Node root = new XmlParser().parseText(xml)
            List<String> sampleLimsIds = root?.'sample'?.'@limsid'
            if(!sampleLimsIds)
                throw new NoSuchObjectException("Samples not found: $sampleNames")
            List<ClaritySample> samples = clarityNodeManager.getSampleNodes(sampleLimsIds).collect{ SampleFactory.sampleInstance(it) }
            scheduledSamples = samples.findAll { it instanceof ScheduledSample}
        }
        return scheduledSamples.collect { it.sampleNode.artifactId }
    }
}
