package gov.doe.jgi.pi.pps.util.util

import java.util.concurrent.ConcurrentHashMap

/**
 * Created by dscott on 3/4/14.
 */
class OnDemandCacheMapped<K,V> {

    private final Map<K,OnDemandCache<V>> cacheMap = new ConcurrentHashMap<>().withDefault{ new OnDemandCache<V>() }

    OnDemandCacheMapped() {}

    V fetch(K input, Closure fetchClosure) {
        return cacheMap[input].fetch {
            fetchClosure?.call(input)
        }
    }

    synchronized void clear(K key) {
        cacheMap.remove(key)
    }

    synchronized void clear() {
        cacheMap.clear()
    }
}
