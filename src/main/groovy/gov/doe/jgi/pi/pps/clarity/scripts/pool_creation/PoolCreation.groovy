package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.LibraryPoolFailureModeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.grails.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryPercentageService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.Libraries
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.FileUploadUtil
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.ActionBean
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PrePoolingWorksheet
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import groovy.json.JsonBuilder

class PoolCreation extends ClarityProcess {

    static ProcessType processType = ProcessType.LP_POOL_CREATION

	static final String SCRIPT_GENERATED_POOL_CREATION_SHEET = 'Download Pool Creation Worksheet'
	static final String UPLOADED_POOL_CREATION_SHEET = 'Upload Pool Creation Worksheet'
	static final String SCRIPT_GENERATED_POOLING_PREP_SHEET = 'Download PoolingPrep Worksheet'
	static final String UPLOADED_POOLING_PREP_SHEET = 'Upload PoolingPrep Worksheet'
    static final String GENERATED_FRACTIONAL_LANE_POOLING = 'FractionalLanePooling'
	private static final int REPEAT_POOL_NUMBER = -1
    private static final int CONTINUE_POOL_NUMBER = 0

    static final BigDecimal DEFAULT_THRESHOLD = 100.0
    static final BigDecimal DEFAULT_SEQ_OPT_FACTOR = 10
	static final String PROCESS_ACTION_REPEAT = 'Repeat'
    static final String PROCESS_ACTION_DONE = 'Done'
	static final def PROCESS_ACTIONS = [PROCESS_ACTION_REPEAT, PROCESS_ACTION_DONE]
    static final def FILE_NAMES = ['PoolingPrep 2', 'PoolingPrep 3','PoolingPrep 4','PoolingPrep 5']

    static final String SECTION_STARTCELL = 'N2' // == row 1 cell 13
    static final def SECTION_STARTCELL_ROW = 1
    static final def SECTION_STARTCELL_CELL = 13
    static final def REGULAR_CELL_ROW = 3 // == row 3 cell 11
    static final def REGULAR_CELL = 11
    static final String SECTION_MESSAGE = 'Internal Single Cell pools, please do not modify'
    static final String POOLING_PREP_TEMPLATE = 'resources/templates/excel/PoolingPrepWorksheet.xls'

    boolean testMode = false
    ArtifactIndexService artifactIndexService
    ArtifactNodeService artifactNodeService
    LibraryPercentageService libraryPercentageService
	def unpooledInputs = []
    PrePoolingWorksheet workbookCached
    Map<String, LaneFraction> libraryNameToFractionCached
    def threshold
    Libraries laneFractionCached

    static final String[] EXCLUDED_FAILURE_MODES = ['Abandoned Work']
    static final String[] getActiveLibraryPoolFailureModes() {
        return LibraryPoolFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }
    static final String[] getValidLibraryPoolFailureModes() {
        return getActiveLibraryPoolFailureModes()?.minus(EXCLUDED_FAILURE_MODES)
    }
    static final DropDownList getDropDownFailureModes() {
        DropDownList dropDownList = new DropDownList()
        dropDownList.setControlledVocabulary(validLibraryPoolFailureModes)
        return dropDownList
    }

    static DropDownList getDropDownPassFail(){
        return new DropDownList(controlledVocabulary: Analyte.PASS_FAIL_LIST)
    }

    PrePoolingWorksheet getPoolingPrepWorkbook() {
        if (!workbookCached) {
            def fileNode = processNode.outputResultFiles.find {
                it.name.contains(UPLOADED_POOLING_PREP_SHEET)
            }
            if (!fileNode || !fileNode.id) {
                postErrorMessage("File node ${UPLOADED_POOLING_PREP_SHEET} not found")
            }
            workbookCached = new PrePoolingWorksheet(fileNode.id, inputAnalytes)
        }
        return workbookCached
    }

	PoolCreation(ProcessNode processNode) {
		super(processNode)
        actionHandlers = [
                'PreProcessValidation': LpPreProcessValidation,
                'PreparePoolingPrepSheet': LpPreparePoolingPrepSheet,
                'ProcessPoolingPrepSheet': LpProcessPoolPrepWorksheet,
                'PoolInputs': LpPoolCreation,
                'PreparePoolCreationSheet': LpPreparePoolCreationSheet,
                'PrintLabels': PrintLabels,
                'ProcessPoolCreationSheet': LpProcessPoolCreationSheet,
                'RouteToNextWorkflow': LpRouteToWorkflow
        ]
        artifactIndexService = (ArtifactIndexService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(ArtifactIndexService)
        libraryPercentageService = (LibraryPercentageService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(LibraryPercentageService)
    }

	/**
	 * This method reads the uploaded pooling prep sheet
	 * This method ignores libraries that are not pooled
	 * @param workbookLimsId
	 * @return
	 */
	def getPoolPrepMemberBeans(boolean validatePool=false){
        if (!workbookCached) {
            poolingPrepWorkbook
        }
        Map<Integer, List<Analyte>> pools
        if(validatePool)
            pools = workbookCached.readPoolsFromPrePoolingSheet(artifactIndexService, this)
        else
            pools = workbookCached.readPoolsFromPrePoolingSheet(artifactIndexService)
        return pools
	}

	def getPoolCreationMemberBeans(String workbookName, boolean validate = true){
		def fileNode = processNode.outputResultFiles.find { it.name.contains(workbookName)}
		if (!fileNode || !fileNode.id) {
			postErrorMessage("File node (${workbookName}) not found")
		}
        ExcelWorkbook workbook = new ExcelWorkbook(fileNode.id, testMode)
		workbook.load()
		def beansList = workbook.sections.values()[0].getData()
		def pools = [:].withDefault {[]}
        def poolKeys = [:].withDefault {[] as Set}
		String errors=''
		beansList.each{ PoolCreationTableBean bean ->
            bean.analyte = inputAnalytes.find{it.id == bean.sampleLimsId}
			if(validate)
				errors += bean.validateBean()
			pools[bean.poolName] << bean
            poolKeys[bean.poolName] << bean.key
		}
        def inconsistentPools = poolKeys.findAll{it.value.size() > 1}
        if(inconsistentPools)
            errors += "Inconsistent data for pools - $inconsistentPools"
		if(errors)
			postErrorMessage(errors)
		pools
	}

	def getAnalytesToPrintLabels(){
		processNode.outputResultFiles.each{
			it.httpRefresh()
		}
		List<Analyte> analytesToPrint = []
		def pools = getPoolCreationMemberBeans(SCRIPT_GENERATED_POOL_CREATION_SHEET,false)
		pools.each { poolName, poolMembers ->
            ClarityLibraryPool pool = (ClarityLibraryPool) getOutputAnalytes(poolMembers.first().analyte).first()
			pool.name = poolName
			analytesToPrint << pool
		}
		return analytesToPrint
	}

    def getActionBean(){
        Section actionSection = poolingPrepWorkbook.readActionSection()
        if(!actionSection || !actionSection.data)
            return null
        ActionBean bean = actionSection.data?.first()
        def errorMsg = bean.validate()
        if (errorMsg)
            postErrorMessage(errorMsg as String)
        return bean
    }

    def getSortedBeans() {
        //This sorting ensures that the analytes with the oldest queue date get pooled first
        List<String> allIndexes = []
        List<LibraryInformation> poolBeans = []
        inputAnalytes.each{
            LibraryInformation bean = populatePoolBeans(it)
            allIndexes.addAll(bean.getIndexNames())
            poolBeans << bean
        }
        Map<String, String> indexMap = artifactIndexService.getSequences(allIndexes.unique())
        if(poolBeans.size() == 1){
            poolBeans[0].setUpIndexes(indexMap)
            return poolBeans
        }
        def sortedBeans = poolBeans.sort({ LibraryInformation bean1, LibraryInformation bean2 ->
            bean1.setUpIndexes(indexMap)
            bean2.setUpIndexes(indexMap)
            if (bean1.dop != bean2.dop)
                return bean1.dop <=> bean2.dop
            if (bean1.platform != bean2.platform)
                return bean1.platform <=> bean2.platform
            if (bean1.sequencerModel != bean2.sequencerModel)
                return bean1.sequencerModel <=> bean2.sequencerModel
            if (bean1.runMode != bean2.runMode)
                return bean1.runMode <=> bean2.runMode
            if (bean1.queueDate != bean2.queueDate)
                return bean1.queueDate <=> bean2.queueDate
            if (bean1.materialType != bean2.materialType)
                return bean1.materialType <=> bean2.materialType
            if (bean1.amplified != bean2.amplified)
                return bean1.amplified <=> bean2.amplified
            if (bean1.libraryName != bean2.libraryName)
                return bean1.libraryName <=> bean2.libraryName
            return bean1.indexName <=> bean2.indexName
        })
        return sortedBeans
    }

    LibraryInformation populatePoolBeans(Analyte analyte){
        LibraryInformation bean = new LibraryInformation()
        ClaritySample sample = analyte.claritySample
        bean.populateBean(analyte, sample, artifactIndexService)
        bean.queueDate = getQueueDate(analyte)
        def laneFraction = libraryNameToFraction[analyte.name]
        bean.libraryPercentage = laneFraction ? (laneFraction.laneFraction * 100) : null
        bean.threshold = threshold
        bean
    }

    Map analyteQueueDate = [:]

    java.sql.Date today
    Date getCurrentDate() {
        if (!today) {
            today = new java.sql.Date(System.currentTimeMillis())
        }
        return today
    }

    def getQueueDate(Analyte analyte){
        if (inputAnalytes.size() >= 1000) {
            return currentDate
        }
        if(!analyteQueueDate){
            NodeManager nodeManager= ClarityWebTransaction.currentTransaction.requireNodeManager()
            String protocolName = StageUtility.getStepConfigurationNode(nodeManager, Stage.POOL_CREATION).protocolNode.name
            String workflowName = Stage.POOL_CREATION.workflow.value
            if(!artifactNodeService)
                artifactNodeService = (ArtifactNodeService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(ArtifactNodeService.class)
            analyteQueueDate = artifactNodeService.getArtifactsQueueDate(inputAnalytes.collect{it.id}, protocolName, workflowName)
        }
        return analyteQueueDate[analyte.id]
    }

    ExcelWorkbook uploadPoolingPrepSheet(List<LibraryInformation> poolPrepTableBeans, def fileNode){
        NodeManager nodeManager = ClarityWebTransaction.currentTransaction.requireNodeManager()
        PrePoolingWorksheet worksheet = new PrePoolingWorksheet(PoolCreation.POOLING_PREP_TEMPLATE, poolPrepTableBeans, libraryNameToFraction, testMode)
        worksheet.upload(nodeManager, fileNode.id as String)
    }

    LaneFraction findLaneFraction(String libraryLimsId, List<Analyte> novaSeqAnalytes){
        //LaneFractions fractions = getLaneFractions(novaSeqAnalytes)//inputAnalytes.findAll{ it.sequencerModelCv?.isIlluminaNovaSeq })
        return getLaneFractions(novaSeqAnalytes).libraries.find{ it.libraryLimsId == libraryLimsId }
    }

    Libraries getLaneFractions(List<Analyte> inputAnalytes) {
        if (laneFractionCached != null) {
            return laneFractionCached
        }
        if (inputAnalytes?.size() < 1)
            return null
        List<String> inputAnalyteIds = []
        inputAnalytes.each{ Analyte analyte ->
            inputAnalyteIds.addAll(analyte.libraryStockIds)
        }
        def responseJson = libraryPercentageService.laneFractionsResponseJson(inputAnalyteIds)
        Libraries units = new Libraries(responseJson, inputAnalytes)
        uploadFractionalLanePooling(units.toJson())
        laneFractionCached = units
        return laneFractionCached
    }

    void uploadFractionalLanePooling(def responseJson) {
        NodeManager clarityNodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        ArtifactNode fileNode = getFileNode(GENERATED_FRACTIONAL_LANE_POOLING)
        ClarityWebTransaction.logger.info "Uploading script generated file ${fileNode.id}..."
        String fileContents = convert(responseJson)
        FileUploadUtil.uploadTextFile(fileNode.id, fileContents, clarityNodeManager.nodeConfig, '.txt', false)
    }

    static final String convert(def responseJson) {
        return new JsonBuilder(responseJson) as String
    }

    Map<String, LaneFraction> getLibraryNameToFraction(List<Analyte> inputAnalytes){
        Map<String, LaneFraction> libraryNameToFraction = [:]
        if (inputAnalytes?.size() < 1)
            return libraryNameToFraction
        def errorMsg = StringBuilder.newInstance()
        inputAnalytes.each{ library ->
            LaneFraction laneFraction = findLaneFraction(library.id, inputAnalytes)
            if (!laneFraction) {
                errorMsg << "Library $library.name: cannot find lane fraction by lims id $library.id" << ClarityProcess.WINDOWS_NEWLINE
            }
            if (!laneFraction.laneFraction) {
                errorMsg << "Library $library.name: invalid lane fraction ${laneFraction.laneFraction}" << ClarityProcess.WINDOWS_NEWLINE
            }
            if (!errorMsg) {
                libraryNameToFraction[library.name] = laneFraction
            }
        }
        if (errorMsg?.length()) {
            throw new WebException(errorMsg as String, 422)
        }
        libraryNameToFraction
    }

    Map<String, LaneFraction> getLibraryNameToFraction() {
		if (libraryNameToFractionCached != null) {
			return libraryNameToFractionCached
		}
		libraryNameToFractionCached = getLibraryNameToFraction(inputAnalytes.findAll{ it.sequencerModelCv?.isIlluminaNovaSeq })
		libraryNameToFractionCached
	}

    String getUriToRoute(RunModeCv runModeCv){
        if(runModeCv.isPacBio)
            return ClarityWorkflow.PACBIO_SEQUEL_LIBRARY_ANNEALING.uri
        WorkflowNode workflowNode = nodeManager.getWorkflowNodeByName(runModeCv.sequencingWorkflowName)
        workflowNode.workflowUri
    }
}
