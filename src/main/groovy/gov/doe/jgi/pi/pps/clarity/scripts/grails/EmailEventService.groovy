package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.apache.commons.io.IOUtils

@Transactional
class EmailEventService {
    GrailsApplication grailsApplication

    /**
     * This method takes email purpose, base Url and a list of samples
     * and it POSTs the email events to appropriate webservice
     * For using this service one has to first implement EmailNotification Interface and add the class name to the EmailNotification.Purpose enum
     * Please refer to ShipSampleEmailNotification for details
     * @param notificationPurpose
     * @param samples
     * @param baseUrl
     */
    def sendNotificationEmail(EmailNotification notificationInstance, List<Analyte> samples, String processLink=null){
        notificationInstance.additionalInfo = processLink
        List<EmailEvent> emailEvents = notificationInstance.getEmailEvents(samples)
        emailEvents.each {
            postEmailEvent(it)
        }
        emailEvents
    }

    def postEmailEvent(EmailEvent ee){
        def config = grailsApplication?.config
        String endPointUrl = config?.getProperty('proposals.url') ?: config?.getProperty('proposalsStage.url')//'https://proposals-stage.jgi.doe.gov/'
        String emailJson = ee.marshallJson()
        if(!emailJson)
            return 422
        ClarityWebTransaction.logger.info "${endPointUrl}pmo_webservices/email_events.json"
        ClarityWebTransaction.logger.info emailJson
        def http = new HTTPBuilder(endPointUrl)
        http.request(Method.POST,ContentType.JSON){
            uri.path = 'pmo_webservices/email_events.json'
            body = emailJson
            response.success = {resp ->
                if(resp.status != 201){
                    throw new WebException("Unexpected response status: ${resp.status}", resp.status)
                }
                ClarityWebTransaction.currentTransaction.statusCode = resp.status
                return resp.status
            }
            response.failure = {resp ->
                def is = resp.entity?.content
                String errorStr = "Unexpected error: ${resp.status} : ${resp.statusLine.reasonPhrase}"
                ClarityWebTransaction.currentTransaction.statusCode = resp.status
                ClarityWebTransaction.currentTransaction.addErrorMessage(resp.statusLine.reasonPhrase)
                if(is)
                    errorStr += ": " + IOUtils.toString(is)
                ClarityWebTransaction.logger.error errorStr
                throw new WebException(errorStr, (Integer) resp.status)
            }
        }
    }
}
