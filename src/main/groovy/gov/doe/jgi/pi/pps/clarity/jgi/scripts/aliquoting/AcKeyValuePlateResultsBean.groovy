package gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class AcKeyValuePlateResultsBean {
	@FieldMapping(header = 'Aliquot Status', cellType = CellTypeEnum.DROPDOWN)
	public def  aliquotStatus
	@FieldMapping(header='Target Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER)
	public def targetAliquotMassNg
	@FieldMapping(header='Total Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public def totalVolumeUl

	def validate() {
		def errorMsg = "Please correct the errors below and upload spreadsheet again.$ClarityProcess.WINDOWS_NEWLINE Plate Overview Tab:"<<' '
		String labResult = aliquotStatus?.value
		if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
			return  errorMsg << "invalid Aliquot Status"
		}
 		if (targetAliquotMassNg == null) {
			return  errorMsg << "unspecified Target Aliquot Mass (ng) '$targetAliquotMassNg'"
		}
		if (totalVolumeUl == null) {
			return errorMsg << "unspecified Total Volume (ul) '$totalVolumeUl'"
		}
        return null
	}
}
