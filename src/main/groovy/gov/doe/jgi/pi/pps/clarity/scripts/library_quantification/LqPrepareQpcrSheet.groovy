package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.clarity.domain.LibraryQpcrFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.AttributeValues
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.PlatePassFailKeyValueBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.QpcrTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes

/**
 * Created by tlpaley on 3/2/15.
 */
class LqPrepareQpcrSheet extends ActionHandler {

    final static String QPCR_TEMPLATE = 'resources/templates/excel/qPCR_384_well_Worksheet'
    final static String TUBE_START_LOCATION = 'B1'
    final static String TUBE_ROW_COLUMN = 'B:1'
    final static String UNUSED_CELL = 'X'
    final static String[][] NINETYSIX_WELL_LOCATIONS = [
            [UNUSED_CELL, 'A:2', 'A:3', 'A:4', 'A:5', 'A:6', 'A:7', 'A:8', 'A:9', 'A:10', 'A:11', UNUSED_CELL],
            [TUBE_ROW_COLUMN, 'B:2', 'B:3', 'B:4', 'B:5', 'B:6', 'B:7', 'B:8', 'B:9', 'B:10', 'B:11', 'B:12'],
            ['C:1', 'C:2', 'C:3', 'C:4', 'C:5', 'C:6', 'C:7', 'C:8', 'C:9', 'C:10', 'C:11', 'C:12'],
            ['D:1', 'D:2', 'D:3', 'D:4', 'D:5', 'D:6', 'D:7', 'D:8', 'D:9', 'D:10', 'D:11', 'D:12'],
            ['E:1', 'E:2', 'E:3', 'E:4', 'E:5', 'E:6', 'E:7', 'E:8', 'E:9', 'E:10', 'E:11', 'E:12'],
            ['F:1', 'F:2', 'F:3', 'F:4', 'F:5', 'F:6', 'F:7', 'F:8', 'F:9', 'F:10', 'F:11', 'F:12'],
            ['G:1', 'G:2', 'G:3', 'G:4', 'G:5', 'G:6', 'G:7', 'G:8', 'G:9', 'G:10', 'G:11', 'G:12'],
            [UNUSED_CELL, 'H:2', 'H:3', 'H:4', 'H:5', 'H:6', 'H:7', 'H:8', 'H:9', 'H:10', 'H:11', UNUSED_CELL]
    ]

    private final static Map WELL_MAP = [:]
    static {
        Map wellCounts = [:].withDefault { 1 }
        List aToH = ('A'..'H') as List
        ('A'..'P').eachWithIndex { String row, Integer ri ->
            (1..24).eachWithIndex { Integer col, Integer ci ->
                Integer c = Math.ceil(col / 2) as Integer
                Integer r = Math.floor(ri / 2) as Integer
                String threeEightyFourPos = "$row$col"
                String ninetySixPos = "${aToH.get(r)}$c"
                Integer ninetySixPosCount = wellCounts[ninetySixPos]++
                WELL_MAP["$threeEightyFourPos"] = "$ninetySixPos.$ninetySixPosCount"
            }
        }
    }

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        validateNumberOfOutputContainers()
        ExcelWorkbook excelWorkbook = buildQpcrWorkbook()
        excelWorkbook.testMode = false
        ArtifactNode fileNode = process.getFileNode(LibraryQpcr.QPCR_DOWNLOAD_FILE)
        excelWorkbook.store(processNode.nodeManager.nodeConfig, fileNode.id)
        ClarityWebTransaction.logger.info "uploaded $QPCR_TEMPLATE to $fileNode"
    }

    def validateNumberOfOutputContainers(
            List<ContainerNode> outputContainerNodes = getContainerNodes(process.outputAnalytes)
    ) {
        def outputPlateContainers = outputContainerNodes.findAll { it.isNinetySixWellPlate }
        if (outputPlateContainers && outputPlateContainers.size() != 1) {
            process.postErrorMessage("""
                Output plate does not pass custom validation.
                Number of output containers with value '${outputPlateContainers.size()}' exceeds maximum value 1.
                Please select a single ${ContainerTypes.WELL_PLATE_96.value} as the output container.
            """)
        }
    }

    static List<ContainerNode> getContainerNodes(List<Analyte> analytes) {
        return analytes.collect { it.containerNode }.unique()
    }

    ExcelWorkbook buildQpcrWorkbook() {
        List<ArtifactNode> inputAnalytes = processNode.inputAnalytes
        List<ArtifactNode> outputAnalytes = processNode.outputAnalytes

        Stack<ArtifactNode> stack = new Stack<ArtifactNode>()
        for (int i=inputAnalytes.size()-1; i>=0; i--) {
            stack.push(outputAnalytes.get(i))
        }

        final String[] failureModeValues = LibraryQpcrFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
        final String [] passFailValues = [AttributeValues.PASS, AttributeValues.FAIL, AttributeValues.REQUEUE] as String[]
        Map<String, QpcrTableBean> populatedWells = new HashMap<String, QpcrTableBean>()
        (0..11).each { int col ->
            (0..7).each { int row ->
                String wellLocation = NINETYSIX_WELL_LOCATIONS[row][col]
                if (wellLocation.equals(UNUSED_CELL)) {
                    return
                }

                if (!stack) {
                    return
                }

                ArtifactNode outputArtifact = stack.pop()
                populatedWells.put(outputArtifact.getContainerLocation().getWellLocation(), outputArtifact)
            }
        }

        Map<String,String> sampleNamesMap = new HashMap<String,String>()
        Map<String,BigDecimal> fragmentSizeMap = new HashMap<String,BigDecimal>()
        Map<String,BigDecimal> totalDnaConcMap = new HashMap<String,BigDecimal>()
        Map<String, BigDecimal> dopMap = new HashMap<String, BigDecimal>()
        Map<String, QpcrTableBean> ninetySixPlateLocations = new HashMap<String, QpcrTableBean>()
        Map<String, QpcrTableBean> threeEightyFourPlateLocations = new HashMap<String, QpcrTableBean>()
        final QpcrTableBean emptyCells = new QpcrTableBean(passFailValues, failureModeValues)
        List<QpcrTableBean> nameTable = new ArrayList<QpcrTableBean>()
        ('A'..'P').each { String row ->
            (1..24).each { Integer col ->
                String numberedPos = WELL_MAP["$row$col"]
                String[] suffixes = numberedPos.split('\\.')
                if (populatedWells.containsKey(suffixes[0])) {
                    ArtifactNode outputArtifact = populatedWells.get(suffixes[0])
                    Analyte outputAnalyte = AnalyteFactory.analyteInstance(outputArtifact)
                    String wellLocation = outputArtifact.containerLocation.getWellLocation()
                    String notes = ''

                    List<Analyte> inputsForOutput = process.getInputAnalytes(outputAnalyte)
                    Analyte inputAnalyte = inputsForOutput.first()
                    if (inputAnalyte instanceof ClarityLibraryStock) {
                        ClarityLibraryStock libraryStock = inputAnalyte as ClarityLibraryStock
                        dopMap.put(wellLocation, libraryStock.getUdfDegreeOfPooling())
                        fragmentSizeMap.put(wellLocation, libraryStock.getUdfActualTemplateSizeBp())
                        notes = libraryStock.getUdfNotes()

                        BigDecimal libraryMolarityQcPm = libraryStock.getUdfLibraryMolarityQcPm()
                        if (libraryMolarityQcPm != null) {
                            totalDnaConcMap.put(wellLocation, libraryMolarityQcPm)
                        }
                    } else if (inputAnalyte instanceof ClarityLibraryPool) {
                        ClarityLibraryPool libraryPool = inputAnalyte as ClarityLibraryPool
                        dopMap.put(wellLocation, libraryPool.getUdfDegreeOfPooling())
                        fragmentSizeMap.put(wellLocation, libraryPool.getUdfActualTemplateSizeBp())
                        notes = libraryPool.getUdfNotes()

                        BigDecimal libraryMolarityQcPm = libraryPool.getUdfLibraryMolarityQcPm()
                        if (libraryMolarityQcPm != null) {
                            totalDnaConcMap.put(wellLocation, libraryMolarityQcPm)
                        }
                    }

                    String artifactName = "${inputAnalyte.name} $numberedPos"
                    QpcrTableBean qpcrTableBean = new QpcrTableBean(passFailValues, failureModeValues)
                    qpcrTableBean.name = artifactName
                    qpcrTableBean.library = artifactName
                    qpcrTableBean.notes = notes
                    nameTable.add(qpcrTableBean)

                    String artifactRowCol = outputArtifact.containerLocation.getRowColumn()
                    ninetySixPlateLocations.put(artifactRowCol, qpcrTableBean)
                    threeEightyFourPlateLocations.put("$row$col", qpcrTableBean)
                    sampleNamesMap.put(wellLocation, inputAnalyte.name)
                } else {
                    nameTable.add(emptyCells)
                }
            }
        }

        List<QpcrTableBean> runTemplateBeans = new ArrayList<QpcrTableBean>()
        (0..11).each { int col ->
            (0..7).each { int row ->
                String wellLocation = NINETYSIX_WELL_LOCATIONS[row][col]
                if (wellLocation.equals(UNUSED_CELL)) {
                    return
                }

                QpcrTableBean tableBean = ninetySixPlateLocations.get(wellLocation)
                if (tableBean) {
                    runTemplateBeans.add(tableBean)
                } else {
                    runTemplateBeans.add(emptyCells)
                }
            }
        }

        List<QpcrTableBean> dilutionTable = new ArrayList<QpcrTableBean>()
        (1..12).each { Integer col ->
            ('A'..'H').each { String row ->
                if ("$row$col".toString() in ['A1', 'A12', 'H1', 'H12']) {
                    return
                }
                (1..4).each { Integer index ->
                    QpcrTableBean tableBean = ninetySixPlateLocations["$row:$col"]
                    if (tableBean) {
                        dilutionTable.add(tableBean)
                    } else {
                        dilutionTable.add(emptyCells)
                    }
                }
            }
        }

        TableSection nameSection = new TableSection(1, QpcrTableBean.class, 'C1', 'C385', true)
        nameSection.setData(nameTable)

        TableSection dilutionSection = new TableSection(1, QpcrTableBean.class, 'S27', 'V395', true)
        dilutionSection.setData(dilutionTable)

        PlateSection sampleNamesSection = new PlateSection(0, CellTypeEnum.STRING, 'J4', null)
        sampleNamesSection.addDataValues(sampleNamesMap)

        PlateSection fragmentSizeSection = new PlateSection(0, CellTypeEnum.STRING, 'J14', null)
        fragmentSizeSection.addDataValues(fragmentSizeMap)

        PlateSection totalDnaConcSection = new PlateSection(0, CellTypeEnum.STRING, 'J24', null)
        //CellTypeEnum.STRING because of corners values: Std 1,2 /  Std 3,4 / Std 5,6 / neg/pos
        totalDnaConcSection.addDataValues(totalDnaConcMap)

        PlateSection dopSection = new PlateSection(0, CellTypeEnum.STRING, 'J50', null)
        dopSection.addDataValues(dopMap)

        Section platePassFailSection = new KeyValueSection(1, PlatePassFailKeyValueBean.class, 'H22')
        platePassFailSection.setData(new PlatePassFailKeyValueBean())

        ExcelWorkbook excelWorkbook = new ExcelWorkbook(QPCR_TEMPLATE, false)
        excelWorkbook.addSection(nameSection)
        excelWorkbook.addSection(dilutionSection)
        excelWorkbook.addSection(sampleNamesSection)
        excelWorkbook.addSection(fragmentSizeSection)
        excelWorkbook.addSection(totalDnaConcSection)
        excelWorkbook.addSection(dopSection)
        excelWorkbook.addSection(platePassFailSection)
        return excelWorkbook
    }
}
