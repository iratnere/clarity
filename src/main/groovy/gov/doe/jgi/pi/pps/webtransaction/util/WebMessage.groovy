package gov.doe.jgi.pi.pps.webtransaction.util

import org.springframework.context.MessageSourceResolvable

/**
 * Created by duncanscott on 6/27/15.
 */
class WebMessage implements MessageSourceResolvable {

    protected List<String> codeList = []
    protected List<Object> argList = []
    String defaultMessage = null

    protected codeToList(code) {
        List<String> codeList = []
        if (code instanceof Iterable) {
            code.each {
                codeList += codeToList(it)
            }
        } else if (code != null) {
            codeList << "${code}"
        }
        return codeList
    }

    WebMessage(String code, String defaultMessage) {
        this(code,null,defaultMessage)
    }

    WebMessage(Object code) {
        this(code,null)
    }

    WebMessage(Object code, Iterable args) {
        this(code,args,null)
    }

    WebMessage(Object code, Iterable args, Object defaultMessage) {
        codeList += codeToList(code)
        args?.each {
            argList << it
        }
        if (defaultMessage != null) {
            this.defaultMessage = "${defaultMessage}"
        }
    }

    @Override
    String[] getCodes() {
        return (String[]) codeList.toArray()
    }

    @Override
    Object[] getArguments() {
        return (Object[]) argList.toArray()
    }

}
