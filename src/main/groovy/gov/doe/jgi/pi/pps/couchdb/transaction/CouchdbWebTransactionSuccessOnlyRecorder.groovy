package gov.doe.jgi.pi.pps.couchdb.transaction

import gov.doe.jgi.pi.pps.couchdb.util.CouchDb
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransaction
import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransactionUtil

/**
 * Created by dscott on 2/11/2015.
 */
class CouchdbWebTransactionSuccessOnlyRecorder extends CouchdbWebTransactionRecorder {

    CouchdbWebTransactionSuccessOnlyRecorder(CouchDb couchDb) {
        super(couchDb)
    }

    @Override
    void recordFailure(WebTransaction webTransaction) {
        WebTransactionUtil.logger.debug "${webTransaction} failed with status code ${webTransaction.statusCode}"
    }

}
