package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.FileNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import org.apache.commons.io.IOUtils
import org.apache.poi.hssf.usermodel.HSSFDateUtil
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.*

import java.util.regex.Matcher

class ClarityAttachmentUtil {

    static Workbook downloadWorkbook(String artifactId) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        Workbook workbook = null
        InputStream inputStream = null
        try {
            inputStream = nodeManager.downloadFile(artifactId)
            workbook = new HSSFWorkbook(inputStream)
        }
        catch(Exception e){
            throw e
        }
        finally {
            inputStream?.close()
        }
        return workbook
    }

    static ExcelWorkbook downloadExcelWorkbook(String artifactId) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        ExcelWorkbook workbook = null
        InputStream inputStream = null
        try {
            inputStream = nodeManager.downloadFile(artifactId)
            workbook = new ExcelWorkbook(inputStream)
        }
        catch(Exception e){
            throw e
        }
        finally {
            inputStream?.close()
        }
        return workbook
    }

    static List<Map<Integer, Map<String, Object>>> downloadExcelAsCube(String artifactId) {
        return workbookToCube(downloadWorkbook(artifactId))
    }

    static List<Map<String, Object>> downloadExcelAsTable(String artifactId) {
        HSSFWorkbook workbook = downloadWorkbook(artifactId)
        FormulaEvaluator evaluator = workbook.creationHelper.createFormulaEvaluator()
        return workbookToTable(workbook, evaluator, 0)
    }

    static String downloadExcelAsCsv(String artifactId) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        InputStream inputStream = nodeManager.downloadFile(artifactId)
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos)
            new XLS2CSVmra(inputStream, ps, -1).process()
            return baos.toString('UTF-8')
        } finally {
            inputStream.close()
        }
    }


    static String downloadFileAsString(String artifactId) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        InputStream inputStream = null
        try {
            inputStream = nodeManager.downloadFile(artifactId)
            return IOUtils.toString(inputStream, "UTF-8");
        } finally {
            inputStream.close()
        }
    }

    static Map<String, List<String>> downloadFileAsMap(String artifactId, int index) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        InputStream inputStream = null
        try {
            inputStream = nodeManager.downloadFile(artifactId)
            Map<String, List<String>> indexValueToRow = [:]
            inputStream.splitEachLine('\t'){ items ->
                items[index] ? indexValueToRow.put(items[index], items) : null
            }
            return indexValueToRow
        } finally {
            inputStream.close()
        }
    }

    static String indexToExcelColumn(int index) {
        String columnName = ''
        int dividend = index
        while (dividend) {
            int modulo = (dividend - 1) % 26
            columnName = "${((65 + modulo) as Character)}${columnName}"
            dividend = (dividend - modulo) / 26
        }
        return columnName
    }

    static List<Map<Integer, Map<String, Object>>> workbookToCube(Workbook workbook) {
        List cube = [].withDefault{[:]}
        FormulaEvaluator evaluator = workbook.creationHelper.createFormulaEvaluator()
        for (int i=0; i<workbook.numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i)
            Map grid = [:].withDefault{[:]}
            for (Row row : sheet) {
                for (Cell cell : row) {
                    grid[row.rowNum + 1][indexToExcelColumn(cell.columnIndex + 1)] = cellData(cell, evaluator)
                }
            }
            cube[i] = grid
        }
        return cube
    }

    private static Object cellData(Cell cell, FormulaEvaluator evaluator) {
        def cellData = ''
        try {
            CellValue cellValue = evaluator.evaluate(cell)
            switch (cellValue?.cellType) {
                case Cell.CELL_TYPE_BOOLEAN:
                    cellData = cellValue.getBooleanValue()
                    break
                case Cell.CELL_TYPE_NUMERIC:
                    //workaround for PPS-799
                    if (cell.cellType != Cell.CELL_TYPE_FORMULA && HSSFDateUtil.isCellDateFormatted(cell)) {
                        cellData = HSSFDateUtil.getJavaDate(cellValue.getNumberValue())
                    } else {
                        cellData = cellValue.getNumberValue()
                    }
                    break
                case Cell.CELL_TYPE_STRING:
                    cellData = cellValue.getStringValue()
                    break
                case Cell.CELL_TYPE_BLANK:
                    break
                case Cell.CELL_TYPE_ERROR:
                    break
            // CELL_TYPE_FORMULA will never happen
                case Cell.CELL_TYPE_FORMULA:
                    break
            }
        } catch (Exception e) {
           ClarityAttachmentUtil.log.warn("${e.message} - ${cell.cellFormula}")
        }
        return cellData
    }

    static List<Map<String, Object>> workbookToTableColumnHeader(HSSFWorkbook xlsWorkbook, FormulaEvaluator evaluator = null, Integer sheetNumber = 0, Integer firstRow = null, Integer lastRow = null, Integer firstColumn = null, Integer lastColumn = null) {
        List<Map<String, Object>> table = [].withDefault{[:]}
        if (!evaluator) {
            evaluator = xlsWorkbook.creationHelper.createFormulaEvaluator()
        }
        HSSFSheet xlsSheet = xlsWorkbook.getSheetAt(sheetNumber)
        if (firstRow == null) {
            firstRow = xlsSheet.firstRowNum
        }
        if (lastRow == null) {
            lastRow = xlsSheet.lastRowNum
        }

        for (Integer i=firstRow; i<=lastRow; i++) {
            Row row = xlsSheet.getRow(i)
            if (!row) {
                break
            }

            int firstCell = row.firstCellNum
            int lastCell = row.lastCellNum

            if (firstColumn != null && firstColumn > firstCell) {
                if (firstColumn > lastCell) {
                    continue
                }
                firstCell = firstColumn
            }

            if (lastColumn != null && lastColumn < lastCell) {
                if (lastColumn < firstCell) {
                    continue
                }
                lastCell = lastColumn
            }

            Cell columnHeader = row.getCell(firstCell)
            Object headerRaw = cellData(columnHeader, evaluator)
            if (!headerRaw) {
                break
            }

            String header = null
            if (headerRaw) {
                Matcher m = headerRaw.toString() =~ /([^\s].+[^:\s])/
                if (m && m[0]) {
                    header = "${m[0][1]}"
                }
            }
            if (!header) {
                break
            }

            for (Integer j=firstCell + 1; j<=lastCell; j++) {
                Cell cell = row.getCell(j)
                Map<String, Object> data = table[j - 1]
                data[header] = cellData(cell, evaluator)
            }
        }

        return table
    }

    /*
     * Indexes are 1-based: the first row is 1, the first column is 1, and the first sheet is 1.  This is to maintain consistency with row-numbering in the cube.
     */
    static Map<String, Map<String, Object>> extract96WellPlateFromCube(
            List<Map<Integer, Map<String, Object>>> cube,
            Integer rowNumber,
            Integer columnNumber,
            Integer sheetNumber = 1) {

        Map rows = [:]
        Integer sheetIndex = sheetNumber - 1
        Map<Integer, Map<String, Object>> sheet = null
        if (sheetIndex != null && sheetIndex < cube.size()) {
            sheet = cube[sheetIndex]
        } else {
            throw new BundleAwareException([code:'sheet.number.notFound', args:[sheetNumber]])
        }

        ('A'..'H').toList().eachWithIndex { String rowName, Integer rowIndex ->
            Map<String, Object> cols = [:]
            rows[rowName] = cols
            Integer rowAddress = rowNumber + rowIndex
            Map<String, Object> row = sheet[rowAddress]
            (0..11).each { Integer colIndex ->
                String colAddress = indexToExcelColumn(columnNumber + colIndex)
                Integer plateAddress = colIndex + 1
                cols[plateAddress.toString()] = row?.get(colAddress)
            }
        }
        return rows
    }

    /*
     * Indexes are 1-based: the first row is 1, the first column is 1, and the first sheet is 1.  This is to maintain consistency with row-numbering in the cube.
     */
    static Map<String, Map<String, Object>> extract96WellPlateFromWorkbook(
            HSSFWorkbook xlsWorkbook,
            Integer rowIndex,
            Integer columnIndex,
            Integer sheetNumber = 1,
            FormulaEvaluator evaluator = null) {

        if (!evaluator) {
            evaluator = xlsWorkbook.creationHelper.createFormulaEvaluator()
        }

        HSSFSheet xlsSheet = xlsWorkbook.getSheetAt(sheetNumber - 1)
        int lastRow = xlsSheet.lastRowNum

        Map rows = [:]

        ('A'..'H').toList().eachWithIndex { String rowName, Integer rowNumber ->
            Map<String, Object> cols = [:]
            rows[rowName] = cols

            Integer rowAddress = rowNumber + rowIndex -1
            Row row = null
            if (rowAddress <= lastRow) {
                row = xlsSheet.getRow(rowAddress)
            }
            Integer firstCell = row?.firstCellNum
            Integer lastCell = row?.lastCellNum
            (0..11).each { Integer colNumber ->
                Integer plateColAddress = colNumber + 1
                cols[plateColAddress?.toString()] = null
                Integer colAddress = colNumber + columnIndex - 1
                if (firstCell != null && lastCell != null && colAddress >= firstCell && colAddress <= lastCell) {
                    Cell cell = row.getCell(colAddress.intValue())
                    cols[plateColAddress?.toString()] = cellData(cell, evaluator)
                }
            }
        }

        return rows

    }

    static List<Map<String, Object>> workbookToTable(
            HSSFWorkbook xlsWorkbook,
            FormulaEvaluator evaluator = null,
            Integer sheetNumber = 0,
            Integer firstRow = null,
            Integer lastRow = null,
            Integer firstColumn = null,
            Integer lastColumn = null) {

        List<Map<String, Object>> table = []
        if (!evaluator) {
            evaluator = xlsWorkbook.creationHelper.createFormulaEvaluator()
        }
        HSSFSheet xlsSheet = xlsWorkbook.getSheetAt(sheetNumber)
        if (firstRow == null) {
            firstRow = xlsSheet.firstRowNum
        }
        if (lastRow == null) {
            lastRow = xlsSheet.lastRowNum
        }

        Row headerRow = xlsSheet.getRow(firstRow)
        def headers = [:]
        for (Cell columnHeader : headerRow) {
            String columnName = cellData(columnHeader, evaluator)
            headers[columnHeader.columnIndex] = columnName
        }

        for (int i=firstRow+1; i<=lastRow; i++) {
            Map data = [:]
            Row row = xlsSheet.getRow(i)
            if (!row) {
                continue
            }

            int firstCell = row.firstCellNum
            int lastCell = row.lastCellNum

            //PlussLogger.logger.trace "first cell: ${firstCell}; last cell: ${lastCell}; first column: ${firstColumn}; last column: ${lastColumn}"

            if (firstColumn != null && firstColumn > firstCell) {
                if (firstColumn > lastCell) {
                    continue
                }
                firstCell = firstColumn
            }

            if (lastColumn != null && lastColumn < lastCell) {
                if (lastColumn < firstCell) {
                    continue
                }
                lastCell = lastColumn
            }

            for (int j=firstCell; j<=lastCell; j++) {
                Cell cell = row.getCell(j)
                if (headers.containsKey(j)) {
                    //PlussLogger.logger.trace "extracting from row ${i} column ${j} (${headers[j]}): ${cellData(cell, evaluator)}"
                    data[headers[j]] = cellData(cell, evaluator)
                }
            }
            table.add(data)
        }
        return table
    }

    static List<Map<String, Object>>  fetchTableData(ArtifactNode artifact) {
        if (artifact?.isResultFile) {
            FileNode fileNode = artifact.fileNode
            String contentUri = fileNode.contentUri
            return getContentAsTable(contentUri)
        }
        return null
    }




    /**
     * @param cube - 3-dimensional array {sheet,row,column}
     * @param sheet - sheet number
     * @param reference - Excel-style cell reference
     * @return List of Map representing cell range
     *
     * Example:
     * 	String reference = 'A1:E4' // Cells in columns A through E and rows 1 through 4
     * 	int sheet = 0	// Sheet number 0
     * 	def range = GlsAttachmentService.extractRangeFromCube(cube, sheet, reference)
     * 	range.each {
     * 		println it
     * 	}
     */
    static List<Map<Integer, Map<String, Object>>> extractRangeFromCube(List<Map<Integer, Map<String, Object>>> cube, int sheet, String reference) {
        def cells = reference =~ /([A-Z]+)([0-9]+):([A-Z]+)([0-9]+)/
        def rows = (cells[0][2] as Integer)..(cells[0][4] as Integer)
        def columns = (cells[0][1] as String)..(cells[0][3] as String)

        return cube[sheet].findAll { row -> row.key in rows }.collect { k,v ->
            v.findAll { column ->
                column.key in columns
            }
        }
    }

    /**
     * Translate Excel coordinates to 96-well plate coordinates
     *
     * @param plate
     * @return Map with rows={A..H} and columns={1..11}
     */
    static Map<String, Map<String, Object>> translateExcelTo96Well(List<Map<Integer, Map<String, Object>>> table) {
        Map<String, Map<String, Object>> wellPlate = [:].withDefault{[:]}
        table.eachWithIndex { Map<Integer, Map<String, Object>> map, Integer i ->
            String row = ((65 + i) as Character).toString()
            map.eachWithIndex { Integer k, Map<String, Object> v, Integer j ->
                String col = (j + 1) as String
                wellPlate[row][col] = v
            }
        }
        return wellPlate
    }

    /**
     * Translate 96-well plate coordinates to Excel coordinates
     *
     * @param row={A..H}, column={1..11}, startRow, startColumn
     * @return new Tuple<String>([excelRow, excelCol])
     */
    static Tuple plateLocationToExcel(Integer row, Integer col, Integer startRow, Integer startColumn) {
        Integer excelRow = row - 65 + startRow
        String excelCol = indexToExcelColumn(startColumn + col)
        return new Tuple([excelRow, excelCol])
    }

}
