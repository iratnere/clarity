package gov.doe.jgi.pi.pps.clarity.scripts.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Sheet

abstract class PoolingDualIndexesByDOP implements PoolingByDOP {
    int startPoolNumber
    Integer degreeOfPooling = null
    List<LibraryInformation> members
    int reqdPoolsCount

    PoolingDualIndexesByDOP(List<LibraryInformation> members){
        this.members = members
    }

    /**
     * 1. libraryGroupIndex i<- 0 to libraryGroupCount
     * 2. library Group lg <- libraryGroups[i]
     * 4. targetPoolsCount tpc <- lg.size()/dop
     * 5. if tpc < 1 go to step 1
     * 6. ActualMinimalPools amp <- empty
     * 7. unusedLibraries uul <- lg
     * 8. targetMinimalPoolSize tmps <- 2 to dop
     * 9. amp += minimalPools of size tmps
     * 10. uul -= amp
     * 11. if amp.size < tpc go to step 8
     * 12. ActualMinimalPoolIndex j <- 0 to ampCount
     * 13. if amp[j].size() = dop, go to step 12
     * 14. n <- dop - amp[j].size()
     * 15. amp[j] += uul[0..n-1]
     * */
    @Override
    void makePools() {
        reqdPoolsCount = members.size() / degreeOfPooling
        List<Set<LibraryInformation>> pools = []
        int i = 0
        while(pools.size() < reqdPoolsCount){
            List<List<LibraryInformation>> libGroups = splitMembers(members)
            if(i >= libGroups.size())
                break
            if(i > 0) {
                libGroups[0].addAll(libGroups[i])
                libGroups.remove(i)
            }
            pools = getMinimalPools(libGroups, reqdPoolsCount)
            i++
        }
        List<LibraryInformation> unUsedMembers = []
        pools.each{Set<LibraryInformation> pool ->
            unUsedMembers.addAll(pool)
        }
        unUsedMembers = members - unUsedMembers
        ClarityWebTransaction.logger.info "Found minimal pools $pools"
        unUsedMembers = unUsedMembers.sort{ a,b -> a.indexSequence <=> b.indexSequence}
        completePools(pools, unUsedMembers)
        pools.each { Set<LibraryInformation> pool ->
            if(pool.size() == degreeOfPooling)
                labelPool(pool)
        }
    }

    def getMinimalPools(List<List<LibraryInformation>> libGroups, int reqdCount){
        List<Set<LibraryInformation>> pools = []
        for(int i = 0; i < reqdCount; i++){
            Set<LibraryInformation> pool = [] as Set
            for(int j=1; j<libGroups.size(); j++){
                LibraryInformation li = libGroups[j].first()
                pool.add(li)
                libGroups[j].remove(li)
            }
            if(satisfyPool(pool, libGroups[0]))
                pools << pool
            else
                libGroups[0].addAll(pool)
        }
        return pools
    }

    /**
     * This method completes the minimal pools by making all pools of size DOP
     * @param members
     * @param validPools
     * @param dop
     * @param reqdPoolsCount
     */
    protected void completePools(List<Set<LibraryInformation>> minimalPools, List<LibraryInformation> remaining){
        int poolableCount = 0
        for(Set<LibraryInformation> pool : minimalPools){
            int poolSize = pool.size()
            while(poolSize < degreeOfPooling){
                List<String> poolIndexes = pool.collect{ArtifactIndexService.splitSequences(it.indexSequence)}.flatten()
                LibraryInformation candidate = remaining.find{!poolIndexes.intersect(ArtifactIndexService.splitSequences(it.indexSequence))}
                if(!candidate)
                    break
                pool << candidate
                remaining.remove(candidate)
                poolSize++
            }
            if(poolSize == degreeOfPooling) {
                poolableCount++
                if(poolableCount == reqdPoolsCount)
                    return
            }
        }
    }

    /**
     * Assigns the poolNumber to all the libraries if they are part of any pool
     * @param members
     * @param pool
     */
    protected void labelPool(Set<LibraryInformation> pool){
        if(startPoolNumber == 0)
            startPoolNumber++
        pool.each{ LibraryInformation member ->
            member.poolNumber = startPoolNumber
        }
        startPoolNumber++
    }

    boolean satisfyPool(Set<LibraryInformation> pool, List<LibraryInformation> options){
        if(pool.size() > degreeOfPooling)
            return false
        int max = Math.pow(2, pool[0].indexSequenceBits.split("\\+")[0].size())-1
        int andRes = max
        int orRes = 0
        pool.each{ LibraryInformation li ->
            li.indexSequenceBits.split("\\+").each {
                int sequence = Integer.parseInt(it,2)
                andRes &= sequence
                orRes |= sequence
            }
        }
        if(andRes == 0 && orRes == max)
            return true
        if(pool.size() == degreeOfPooling)
            return false
        LibraryInformation andBest, orBest
        int andBestSoFar = max, orBestSoFar = 0

        for(int i = 0; i < options.size() ; i++){
            LibraryInformation li = options[i]

            int currAndRes = andRes
            int currOrRes = orRes
            li.indexSequenceBits.split("\\+").each{
                int sequence = Integer.parseInt(it,2)
                currAndRes = currAndRes & sequence
                currOrRes = currOrRes | sequence
            }
            if(currAndRes == 0 && currOrRes == max){
                pool << li
                options.remove(li)
                return true
            }
            if(pool.size() > degreeOfPooling)
                return false
            if(andBestSoFar > currAndRes) {
                andBestSoFar = currAndRes
                andBest = li
            }
            if(orBestSoFar < currOrRes) {
                orBestSoFar = currOrRes
                orBest = li
            }
        }
        pool << andBest
        pool << orBest
        options.remove(andBest)
        options.remove(orBest)
        return satisfyPool(pool, options)
    }

    static List<String> extractActualSequences(List<String> indexSequences) {
        List<String> actualSequences = []
        indexSequences.each {String sequence ->
            actualSequences.addAll(ArtifactIndexService.splitSequences(sequence))
        }
        return actualSequences
    }

    static boolean validatePoolwithDualIndexes(List<String> indexSequences, List<List<Integer>> nonCompatibles = null){
        List<String> actualSequences = extractActualSequences(indexSequences)
        if(!actualSequences || actualSequences.size() < 2){
            return false
        }
        List<String> sequences = actualSequences*.replaceAll("A|C","0")*.replaceAll("G|T", "1")
        int index = sequences[0].indexOf("-")
        int leftExponent = index
        int rightExponent = sequences[0].size() - index - 1
        int leftMax = Math.pow(2, leftExponent)-1
        int rightMax = Math.pow(2, rightExponent)-1
        int leftAndResult = leftMax, leftOrResult = 0
        int rightAndResult = rightMax, rightOrResult = 0
        List<String> leftSequences = []
        List<String> rightSequences = []
        for(int i=0;i<sequences.size();i++){
            def sequenceSplit = sequences[i].split('-')
            int left = Integer.parseInt(sequenceSplit[0],2)
            int right = Integer.parseInt(sequenceSplit[1],2)
            leftAndResult&=left
            leftOrResult|=left
            rightAndResult&=right
            rightOrResult|=right
            leftSequences << left
            rightSequences << right
            if(leftAndResult == 0 && leftOrResult == leftMax && rightAndResult == 0 && rightOrResult == rightMax)
                return true
        }
        if(nonCompatibles != null) {
            if (leftAndResult != 0 || leftOrResult != leftMax)
                nonCompatibles << leftSequences
            if (rightAndResult != 0 || rightOrResult != rightMax)
                nonCompatibles << rightSequences
        }
        return false
    }

    @Override
    int getDop() {
        if(!degreeOfPooling)
            degreeOfPooling = members.first().dop
        return degreeOfPooling
    }

    @Override
    boolean getCanPool() {
        if(members.size() < dop)
            return false
        return true
    }

    List<List<LibraryInformation>> splitMembers(List<LibraryInformation> libraries){
        int maxPossibleCount = analyzeLibraries()
        if(reqdPoolsCount > maxPossibleCount)
            reqdPoolsCount = maxPossibleCount
        List<List<LibraryInformation>> libGroups = []
        libGroups[0] = []
        libGroups[0].addAll(libraries)
        int i = 1
        while(libGroups[0].size() > libGroups[1]?.size() && i < degreeOfPooling){
            List<LibraryInformation> group = makeGroup(libGroups[0],reqdPoolsCount)
            group = group.sort{a,b -> a.indexSequenceBits <=> b.indexSequenceBits}
            libGroups[i] = (i%2 == 0)? group : group.reverse()
            libGroups[0].removeAll(group)
            i++
        }
        for(i=1; i < libGroups.size(); i++){
            if(libGroups[i].size() <= reqdPoolsCount)
                continue
            List<LibraryInformation> extras = libGroups[i].subList(reqdPoolsCount,libGroups[i].size())
            libGroups[0].addAll(extras)
            libGroups[i].removeAll(extras)
        }
        return libGroups
    }

    int analyzeLibraries(){
        int[] rowSum = getRowSum(members)
        List<String> allIndexes = members.collect { it.indexSequenceBits.split("\\+") }.flatten()
        int n = allIndexes.size()
        int seqLength = allIndexes[0].size()
        (0..(seqLength-1)).each{int index ->
            if(rowSum[index] > n/2){
                rowSum[index] = n-rowSum[index]
            }
        }
        return GroovyCollections.min(rowSum)
    }

    /**
     * This method analyses the input libraries and provides the probable max pool count
     * This only provides the upper limit for the pool count but the libraries may or may not contain those many pools
     * @param indexSequences
     * @return
     */
    protected List<LibraryInformation> makeGroup(List<LibraryInformation> libraries, int valueToCompare){
        int[] rowSum = getRowSum(libraries)
        List<String> allIndexes = libraries.collect { it.indexSequenceBits.split("\\+") }.flatten()
        int min = allIndexes.size()
        int max = 0
        int minCol
        int maxCol
        int seqLength = libraries[0].indexSequenceBits.split("\\+")[0].size()
        (0..(seqLength-1)).each{int row ->
            if(rowSum[row] >= reqdPoolsCount) {
                if (rowSum[row] < min) {
                    min = rowSum[row]
                    minCol = row
                }
                if (rowSum[row] > max) {
                    max = rowSum[row]
                    maxCol = row
                }
            }
        }
        int colVal = 1
         max = allIndexes.size() - max
        if(max >= reqdPoolsCount && max < min){
            min = max
            minCol = maxCol
            colVal = 0
        }
        libraries.findAll{it.checkValueAt(colVal, minCol)}
    }

    def getRowSum(List<LibraryInformation> libraries){
        List<String> indexSequences = libraries.collect{it.indexSequenceBits.split("\\+")}.flatten()
        int n = indexSequences.size()
        int seqLength = indexSequences[0].size()
        int[][] indexMatrix = new int[seqLength][n]
        int[] rowSum = new int[seqLength]
        for(int col = 0; col < n ; col++){
            (0..(seqLength-1)).each{int row ->
                indexMatrix[row][col] = Integer.parseInt(indexSequences[col][row])
                rowSum[row] += indexMatrix[row][col]
            }
        }
        return rowSum
    }

    @Override
    int completePoolPrepBeans(int poolNumber)  {
        startPoolNumber = poolNumber
        if(!canPool)
            return startPoolNumber
        makePools()
        return startPoolNumber
    }

    @Override
    String getMessage() {
        return ""
    }

    @Override
    CellStyle getCellStyle(Sheet sheet) {
        def row = sheet.getRow(REGULAR_CELL_ROW)
        def cell = row.getCell(REGULAR_CELL)
        CellStyle cellStyle = cell.getCellStyle()
        return cellStyle
    }

    @Override
    List getActionBeans() {
        return []
    }

    @Override
    List getSummaryBeans() {
        return []
    }

    @Override
    List getCalculationBeans() {
        return []
    }

    def getIndexToCompare(LibraryInformation member){
        return member.indexSequences
    }

    int getPoolNumber(){
        members.first().poolNumber
    }

    @Override
    Map validate() {
        Map<String,List<String>> findings = [:].withDefault {[]}
        Map<String, List<String>> indexAnalytes = indexMembersMap
        def repIndexes = indexAnalytes.findAll{it.value.size() > 1}
        if(repIndexes)
            findings[ERROR] << "pool #$poolNumber has duplicate indexes: $repIndexes."
        if(members.size() < 6 && !validatePoolwithDualIndexes(indexAnalytes.keySet() as List))//PPS-5045 - Remove color balancing validation for pools above 6
            findings[WARNING] << "Pool #$poolNumber is not valid. Every valid pool must respect the rule that each of the two colors need to be represented at least once for each index base."
        findings
    }

    Map<String, List<LibraryInformation>> getIndexMembersMap(){
        Map<String, List<LibraryInformation>> indexMembers = [:].withDefault {[]}
        members.each{ LibraryInformation li ->
            def indexToCompare = getIndexToCompare(li)
            if(indexToCompare instanceof String)
                indexToCompare = [indexToCompare]
            indexToCompare.each { String index ->
                ArtifactIndexService.splitSequences(index).each {
                    indexMembers[it] << li
                }
            }
        }
        return indexMembers
    }
}