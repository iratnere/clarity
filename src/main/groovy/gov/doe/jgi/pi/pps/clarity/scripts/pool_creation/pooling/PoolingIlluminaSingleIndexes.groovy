package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.BasicPoolingAlgorithm
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation

class PoolingIlluminaSingleIndexes extends BasicPoolingAlgorithm {
    PoolingIlluminaSingleIndexes(List<LibraryInformation> members){
        super(members)
    }
}
