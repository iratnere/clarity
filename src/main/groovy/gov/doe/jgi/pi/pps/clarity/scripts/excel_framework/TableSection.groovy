package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import org.apache.poi.ss.formula.EvaluationWorkbook
import org.apache.poi.ss.formula.FormulaParser
import org.apache.poi.ss.formula.FormulaRenderer
import org.apache.poi.ss.formula.FormulaType
import org.apache.poi.ss.formula.ptg.AreaPtg
import org.apache.poi.ss.formula.ptg.Ptg
import org.apache.poi.ss.formula.ptg.RefPtgBase
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference

import java.lang.reflect.Field
import java.util.logging.Logger

/**
 * Table section corresponds to a Table in an excel section
 * One has to define a bean class that maps each field of the bean class to a header of the table in the excel file
 * store: Create a list of bean class instances and this class shall populate data from instances, one row per instance
 * load: This class reads the table section and creates one instance of bean class per row
 * 
 * Created by lvishwas on 10/4/14.
 */
class TableSection extends Section {
	def beanObjects
	Map<String, Closure> headerFieldMap
	Field rowHeaderField = null
	String beanLevelHeader = null
	boolean copyStyleFromFirstRow = false
	static final Logger logger = Logger.getLogger(TableSection.class.name)
	Map<Integer, RelativeFormula> columnFormula = [:]

	TableSection(int parentSheetIndex, Class cls, String startAddress, String endAddress = null, boolean copyStyleFromFirstRow=false){
		super(parentSheetIndex,cls,startAddress,endAddress)
		this.copyStyleFromFirstRow = copyStyleFromFirstRow
	}

	TableSection(int parentSheetIndex, Class cls, CellRangeAddress sectionRangeAddress, boolean copyStyleFromFirstRow=false){
		super(parentSheetIndex,cls,sectionRangeAddress)
		this.copyStyleFromFirstRow = copyStyleFromFirstRow
	}
	
	TableSection(TableSection srcSection, int parentSheetIndex, Class cls, String startAddress, boolean copyStyleFromFirstRow=false){
		super(srcSection,parentSheetIndex,cls,startAddress)
		this.copyStyleFromFirstRow = copyStyleFromFirstRow
	}

	void setData(def data){
		if(!data instanceof Collection){
			throw new IllegalArgumentException("Expecting collection of objects")
		}
		if(!headerFieldMap)
			populateHeaderFieldMap()
		if(rowHeaderField){
			beanObjects = [:]
			data.each{bean->
				beanObjects["${rowHeaderField.get(bean)}".toString()] = bean
			}
		}else{
			beanObjects = data
		}
	}

	void populateHeaderFieldMap(){
		Field[] fields = beanClass.fields
		headerFieldMap = [:]
		beanObjects = []
		fields.each{ Field field ->
			if(field.isAnnotationPresent(FieldMapping.class)){
				FieldMapping annotation = field.getAnnotation(FieldMapping.class);
				headerFieldMap[annotation.header()?.trim()] = field
				if(annotation.isRowHeader()){
					rowHeaderField = field
					beanLevelHeader = annotation.header()
					beanObjects = [:]
				}
			}
		}
	}
	
	boolean checkBeansMaps(TableSection tSection){
		for(Map.Entry<String, Object> entry : beanObjects){
			if(!entry.value.equals(tSection.beanObjects[entry.key]))
				return false
		}
		return true
	}
	
	private boolean checkBeansList(TableSection tSection){
		int i=0
		for(Object bean : beanObjects){
			if(!bean.equals(tSection.beanObjects[i++]))
				return false
		}
		return true
	}

	@Override
	void store() {
		logger.fine "Writing ${beanObjects.size()} rows to Table section: ${this.sectionKey}...."
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		int startRow = startCell.row
		int rowHeaderColIndex = -1
		String[] headers
		Row row = sheet.getRow(startRow)
		if(row){
			int lastCellNum = lastCell?lastCell.col+1:row.lastCellNum
			headers = new String[lastCellNum-startCell.col]
			int i=0
			while(startCell.col+i<lastCellNum){
				Cell cell = row.getCell(startCell.col+i)
				String header = readCell(cell,CellTypeEnum.STRING)?.trim()
				if(header == beanLevelHeader){
					rowHeaderColIndex = cell.columnIndex

				}
				headers[i++] = header
			}
			int k = 0
			while(k < beanObjects.size()) {
				++startRow
				row = sheet.getRow(startRow)?sheet.getRow(startRow):sheet.createRow(startRow)
				def beanObject = null
				if (rowHeaderColIndex >= 0) {
					Cell cell = row?.getCell(rowHeaderColIndex)
					beanObject = beanObjects[readCell(cell, CellTypeEnum.STRING)]
				} else {
					beanObject = beanObjects[k]
				}
				if(beanObject){
					if(copyStyleFromFirstRow)
						copyStyle(row)
					k++
					headers.eachWithIndex { String colHeader, int index ->
						if (colHeader != beanLevelHeader) {
							Field field = getFieldForHeader(colHeader)
							Cell cell = row.getCell(startCell.col + index)?row.getCell(startCell.col + index):row.createCell(startCell.col + index)
							if(field) {
                                if (((FieldMapping) field.annotations[0]).isCustomCellStyle() && beanObject.customCellStyle)
                                    cell.setCellStyle(beanObject.customCellStyle)
                                writeCell(cell, ((FieldMapping) field.annotations[0]).cellType(), field.get(beanObject))
                            }
						}
					}
				}
				if(lastCell && startRow > lastCell.row)
					break
			}
			if(!lastCell){
				ClarityWebTransaction.logger.info "Calculating lastcell.... "
                ClarityWebTransaction.logger.info "RowNum: ${row.rowNum} colNum ${startCell.col + headers.size()-1}"
				lastCell = new CellReference(row.rowNum, startCell.col + headers.size()-1)
			}

		}else{
			throw new IllegalArgumentException("Can't find the header row. Please check section configuration")
		}
	}

	@Override
	void load(boolean validate=true) {
		logger.fine "Reading section: ${this.readSectionName()}"
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		int startRow = startCell.row
		String[] headers
		Row row = sheet.getRow(startRow)
		if(row){
			int lastCellNum = lastCell?lastCell.col+1:row.lastCellNum
			headers = new String[lastCellNum-startCell.col]
			int i=0
			while(startCell.col+i<lastCellNum){
				Cell cell = row.getCell(startCell.col+i)
				String header = readCell(cell,CellTypeEnum.STRING)?.trim()
				headers[i++] = header
			}
			row = sheet.getRow(++startRow)
			while(startRow <= sheet.lastRowNum){
				if(!row) {
					row = sheet.getRow(++startRow)
					continue
				}
				boolean isBlank = true
				Field rowHeaderField = null
				def headerCellValue = null
				def beanObject = null
				headers.eachWithIndex { String colHeader, int index ->
					Field field = getFieldForHeader(colHeader)
					if(field){
						Cell cell =  row.getCell(startCell.col + index)
						FieldMapping fieldMapping = field.getAnnotation(FieldMapping.class)
						if(fieldMapping.isRowHeader() == true){
							rowHeaderField = field
							headerCellValue = readCell(cell, fieldMapping.cellType())
						}else{
							Object value = readCell(cell, fieldMapping.cellType())
							if(value != null && value != ''){
								isBlank = false
								beanObject = beanObject ? beanObject : beanClass.newInstance()
								field.set(beanObject,value)
							}
						}
					}
				}
				if(beanObject){
					rowHeaderField?.set(beanObject, headerCellValue)
					rowHeaderField?beanObjects[headerCellValue] = beanObject:beanObjects << beanObject
				}
				if(!isBlank && validate)
					validateBean(beanObject, row.rowNum)
				if(lastCell && startRow >= lastCell.row)
					break
				row = sheet.getRow(++startRow)
			}
		}else{
			throw new IllegalArgumentException("Can't find the header row. Please check section configuration")
		}
	}

	Field getFieldForHeader(String colHeader){
		if(!headerFieldMap){
			populateHeaderFieldMap()
		}
		return headerFieldMap[colHeader]
	}
	
	String getSectionConfig(String sectionDelim, String fieldDelim){
		StringBuilder sb = new StringBuilder()
		sb.append(sectionDelim)
		sb.append(this.class.name).append(fieldDelim)
		sb.append(this.beanClass.name).append(fieldDelim).append(fieldDelim)
		sb.append(parentSheetIndex).append(fieldDelim)
		sb.append(startCell.formatAsString()).append(fieldDelim)
		if(lastCell)sb.append(lastCell.formatAsString())
		sb.append(fieldDelim)
		return sb.toString()
	}
	
	def getData(){
		if(beanObjects instanceof Map){
			return beanObjects.values() as List
		}
		return beanObjects as List
	}

	void copyStyle(Row targetRow){
		Row srcRow = parentSheet.getRow(startCell.row+1)
		if(!srcRow || !targetRow)
			return
		int curCol = startCell.col
		int lastCol = lastCell?lastCell.col:srcRow.lastCellNum

		while(curCol <= lastCol) {
			Cell srcCell = srcRow.getCell(curCol)
			if (!srcCell){
				curCol++
				continue
			}
			RelativeFormula rf = columnFormula[srcCell.columnIndex]
			if(srcCell.cellTypeEnum == CellType.FORMULA && !rf){
				rf = new RelativeFormula(srcCell)
			}

			Cell targetCell = targetRow.getCell(srcCell.columnIndex)?targetRow.getCell(srcCell.columnIndex):targetRow.createCell(srcCell.columnIndex)
			if(srcCell.getCellStyle())
				targetCell.setCellStyle(srcCell.getCellStyle())
			if(srcCell.getCellComment())
				targetCell.setCellComment(srcCell.getCellComment())
			if(srcCell.getHyperlink())
				targetCell.setHyperlink(srcCell.getHyperlink())
			if(rf)
				targetCell.setCellFormula(rf.getRelativeFormula(targetCell))
			curCol++
		}
	}

	String toString(){
		StringBuilder sb = new StringBuilder()
		sb.append("Table Section: ").append(sectionKey).append("\n")
		getData().each{beanObject ->
			sb.append(beanObject.toString()).append("\n")
		}
		return sb.toString()
	}

	class RelativeFormula{
		Cell srcCell
		String originalFormula
		Ptg[] ptgs
		EvaluationWorkbook workbookWrapper

		RelativeFormula(Cell srcCell) {
			originalFormula = srcCell.getCellFormula()
			this.srcCell = srcCell
			workbookWrapper = TableSection.this.workbook.evaluationWorkbook
			ptgs = FormulaParser.parse(originalFormula, workbookWrapper, FormulaType.CELL, TableSection.this.parentSheetIndex);
		}

		String getRelativeFormula(Cell targetCell){
			int rowShift = targetCell.rowIndex - srcCell.rowIndex
			int colShift = targetCell.columnIndex - srcCell.columnIndex
			for (Ptg ptg : ptgs) {
				if (ptg instanceof RefPtgBase) // base class for cell references
				{
					RefPtgBase ref = (RefPtgBase) ptg;
					if (ref.isColRelative())
						ref.setColumn(ref.getColumn() + colShift);
					if (ref.isRowRelative())
						ref.setRow(ref.getRow() + rowShift);
				} else if (ptg instanceof AreaPtg) // base class for range references
				{
					AreaPtg ref = (AreaPtg) ptg;
					if (ref.isFirstColRelative())
						ref.setFirstColumn(ref.getFirstColumn() + colShift);
					if (ref.isLastColRelative())
						ref.setLastColumn(ref.getLastColumn() + colShift);
					if (ref.isFirstRowRelative())
						ref.setFirstRow(ref.getFirstRow() + rowShift);
					if (ref.isLastRowRelative())
						ref.setLastRow(ref.getLastRow() + rowShift);
				}
			}
			return ptgs?FormulaRenderer.toFormulaString(workbookWrapper, ptgs):originalFormula
		}
	}

	CellReference getCellAddress(String headerStr){
		if(sourceSection)
			return calculateCellAddressFromSourceSection(headerStr)
		CellReference cellReference = null
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		int currCol = startCell.col
		Row row = sheet.getRow(startCell.row);
		if(row){
			int lastCellNum = lastCell?lastCell.col+1:row.lastCellNum
			while(currCol<lastCellNum){
				Cell cell = row.getCell(currCol)
				String header = readCell(cell,CellTypeEnum.STRING)?.trim()
				if(header == headerStr){
					cellReference = new CellReference(cell.rowIndex, cell.columnIndex)
					break
				}
				currCol++
			}
		}
		return cellReference
	}

}
