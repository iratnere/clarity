package gov.doe.jgi.pi.pps.clarity.scripts.email_notification

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import grails.config.Config
import groovy.text.GStringTemplateEngine

/**
 * Created by lvishwas on 1/22/2015.
 */
abstract class EmailNotification {

    List<List<EmailDetails>> emailDetailsGroups = []
    List<EmailEvent> emailEvents = []
    String additionalInfo
    static final String SIGNATURE_TEMPLATE = "signature"
    static final String TEMPLATE_EXTENSION = ".template"
    static final String TEMPLATES_FOLDER = "templates/email/"

    protected EmailNotification(){}

    List<List<EmailDetails>> getEmailDetails(List<Analyte> analytes){
        if(!emailDetailsGroups)
            makeGroups(buildEmailDetails(analytes))
        return emailDetailsGroups
    }

    void makeGroups(List<EmailDetails> emailDetailsList) {
        def hashCodes = emailDetailsList.collect {
            it.hashCode
        }.unique()
        emailDetailsGroups = []
        hashCodes.each { hc ->
            emailDetailsGroups << emailDetailsList.findAll {
                it.hashCode == hc
            }
        }
    }

    protected Set<Integer> getEntityIds(List<EmailDetails> emailDetailsList){
        return emailDetailsList.collect{it.proposalId}
    }

    List<EmailEvent> getEmailEvents(List<Analyte> analytes){
        if(!emailEvents) {
            getEmailDetails(analytes).each { List<EmailDetails> emailDetailsList ->
                buildEmailEvent(emailDetailsList)
            }
        }
        return emailEvents
    }

    void buildEmailEvent(List<EmailDetails> emailDetailsList){
        EmailEvent event = new EmailEvent()
        event.subject = getSubjectLine(emailDetailsList)
        event.entity_id = getEntityIds(emailDetailsList)
        event.to = getToList(emailDetailsList)
        event.cc = getCcList(emailDetailsList)
        event.from = getFromList(emailDetailsList)
        event.body = getEmailBody(emailDetailsList)
        this.emailEvents << event
    }

    static String getDashboardIndexUrl() {
        Config config = ClarityWebTransaction.currentTransaction.grailsApplication.config
        String endPointUrl = config?.getProperty('proposalsMetadataDashboardIndex.url') ?: config?.getProperty('proposalsStageMetadataDashboardIndex.url')//'https://proposals-stage.jgi.doe.gov/metadata/dashboard/index'
        return endPointUrl
    }

    protected String getEmailBody(List<EmailDetails> emailDetailsList){
        GStringTemplateEngine engine = new GStringTemplateEngine()
        String templateFile = "${EmailNotification.TEMPLATES_FOLDER}$templateFileName${EmailNotification.TEMPLATE_EXTENSION}"
        Writable template = engine.createTemplate(EmailNotification.class.classLoader.getResource(templateFile)).make(getBinding(emailDetailsList))
        StringBuilder str = new StringBuilder("${template}")
        str << "\n\n"
        templateFile = "${EmailNotification.TEMPLATES_FOLDER}$SIGNATURE_TEMPLATE${EmailNotification.TEMPLATE_EXTENSION}"
        str << EmailNotification.class.classLoader.getResource(templateFile).getText('UTF-8')
        str.toString()
    }

    protected def getSampleList(List<EmailDetails> emailDetailsList){
        def info = []
        emailDetailsList.each{
            info.add(it.info)
        }
        info.join("\n")
    }

    /**
     * The rules defined for to, cc list:
     *
     * 1. send email for each plate to list of sample contacts, list of sequencing project contacts, list of proposal PIs.
     * CC list of project managers and mailing list.
     *
     *2. batch tubes together by sample contact and sequencing project manager. Send one email per batch to sample contact, list of sequencing project contact, list of proposal PI. CC project manager and mailing list.
     *
     * @return
     */
    abstract protected def getToList(List<EmailDetails> emailDetailsList)

    abstract protected def getCcList(List<EmailDetails> emailDetailsList)

    abstract protected String getSubjectLine(List<EmailDetails> emailDetailsList)

    abstract protected def getFromList(List<EmailDetails> emailDetailsList)

    abstract protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes)

    abstract protected def getBinding(List<EmailDetails> emailDetailsList)

    abstract protected String getTemplateFileName()
}
