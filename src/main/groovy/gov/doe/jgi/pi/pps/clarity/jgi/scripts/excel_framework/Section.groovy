package gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework

import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellRangeAddressList
import org.apache.poi.ss.util.CellReference

import java.lang.reflect.Field
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.logging.Logger

/**
 * This is an abstract class representing a section within a sheet of excel workbook
 * This is a generic class that accepts a bean object and populates the section accordingly
 * Every section holds the beginning and end of the section in terms of cell reference
 * end of section is optional whereas the beginning of the section is mandatory
 * if end of section is not defined then the section looks for a null row to end the section
 * 
 * Created by lvishwas on 10/4/14.
 */
abstract class Section {
   String name
   CellReference startCell, lastCell = null
   int parentSheetIndex
   Class beanClass
   ExcelWorkbook workbook
    Section sourceSection
   List<Field> requiredFields

   List<CellRangeAddress> regionsList = []
   def dvRecordsList = []

   static final Logger logger = Logger.getLogger(Section.class.name)

   Section(int parentSheetIndex, Class cls, String startAddress, String endAddress = null){
       startCell = new CellReference(startAddress)
       if(endAddress)
           lastCell = new CellReference(endAddress)
       this.beanClass = cls
       this.parentSheetIndex = parentSheetIndex
       this.sourceSection=null
       this.name = readSectionName()
   }

   Section(int parentSheetIndex, Class cls, CellRangeAddress sectionRangeAddress){
       startCell = new CellReference(sectionRangeAddress.getFirstRow(), sectionRangeAddress.getFirstColumn())
       lastCell = new CellReference(sectionRangeAddress.getLastRow(), sectionRangeAddress.getLastColumn())
       this.beanClass = cls
       this.parentSheetIndex = parentSheetIndex
       this.sourceSection=null
       this.name = readSectionName()
   }

   /**
    * This constructor is used to copy a section from a source section
    * This constructor is used when one needs to duplicate a section at the runtime
    * but this only copies the structure of the section and does not copy the values from it
    *
    * @param srcSection
    * @param parentSheetIndex
    * @param cls
    * @param startAddress
    */
   Section(Section srcSection, int parentSheetIndex, Class cls, String startAddress){
       this.parentSheetIndex = parentSheetIndex
       this.beanClass = cls
       this.startCell = new CellReference(startAddress)
       this.sourceSection = srcSection
       this.name = readSectionName()
   }

   Sheet getParentSheet(){
       return this.workbook?.getSheet(parentSheetIndex)
   }

   boolean hasSourceSection(){
       return sourceSection?true:false
   }

   String readSectionName(){
       if(startCell.row-1 >= 0)
           return getParentSheet()?.getRow(startCell.row-1)?.getCell(startCell.col)?.getStringCellValue()
       return null
   }

   void writeSectionName(){
       Cell srcSectionNameCell = sourceSection.getParentSheet().getRow(sourceSection.startCell.row-1)?.getCell(sourceSection.startCell.col)
       Row row = getParentSheet().getRow(startCell.row-1) ?getParentSheet().getRow(startCell.row-1):getParentSheet().createRow(startCell.row-1)
       Cell destSectionNameCell = row.createCell(startCell.col)
       copyCell(srcSectionNameCell,destSectionNameCell)
       if(name)
       writeCell(destSectionNameCell,CellTypeEnum.STRING,name)
   }

   String getSectionName(){
       return name
   }

   void setSectionName(String sectionName){
       this.name = sectionName
   }

   CellRangeAddress getMergedRegionForCell(Cell cell){
       if(!regionsList)
           regionsList = mergedRegions
       if(!regionsList)
           return null
       CellRangeAddress mergedRegion = null
       for(CellRangeAddress cellRangeAddress : regionsList){
           if(cellRangeAddress.isInRange(cell.rowIndex, cell.columnIndex)) {
               mergedRegion = cellRangeAddress
               break
           }
       }
       return mergedRegion
   }

   Cell getMergedCell(Cell cell) {
       CellRangeAddress mergedRegion = getMergedRegionForCell(cell)
       if(mergedRegion){
           return parentSheet.getRow(mergedRegion.firstRow)?.getCell(mergedRegion.firstColumn)
       }
   }

   Object readCell(Cell cell, CellTypeEnum type){
       if(!cell)
           return type == CellTypeEnum.DROPDOWN?new DropDownList(value: '',controlledVocabulary: []):''
       logger.fine "Reading Cell[${cell.rowIndex}, ${cell.columnIndex}]"
       Cell mergedCell = getMergedCell(cell)
       if(mergedCell)
           cell = mergedCell
       Object value
       switch(type) {
           case CellTypeEnum.DATE:
               DateFormat df = new SimpleDateFormat("MM/dd/yyyy")
               value = cell.getStringCellValue() ? df.parse(cell.getStringCellValue()) : ""

               break;
           case CellTypeEnum.STRING:
               if(cell.cellTypeEnum == CellType.STRING)
                   value = cell.getStringCellValue()
               else{
                   DataFormatter defaultFormat = new DataFormatter()
                   FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator()
                   evaluator.evaluate(cell)
                   value = defaultFormat.formatCellValue(cell, evaluator)
               }
               break
           case CellTypeEnum.NUMBER:
               if(cell.cellTypeEnum == CellType.NUMERIC) {
                   value = (cell.getNumericCellValue()!=null)?cell.getNumericCellValue() as BigDecimal:null
               }
               else if(cell.cellTypeEnum == CellType.STRING){
                   value = cell.getStringCellValue()? cell.getStringCellValue() as BigDecimal:null
               }
               break;
           case CellTypeEnum.FORMULA:
               value = getFormulaValue(cell)
               break
           case CellTypeEnum.FORMULA_NOT_IMPLEMENTED:
               try {
                   value = cell.getNumericCellValue()
               } catch (Exception e) {
                   /*FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator()
                    CellValue cellValue = evaluator.evaluate(cell)
                    returns
                    Caused by: org.apache.poi.ss.formula.eval.NotImplementedException: Error evaluating cell Sheet1!G20
                    NotImplementedFunctionException: LINEST
                    */
                   value = null
               }
               break
           case CellTypeEnum.DROPDOWN:
               DropDownList ddl = new DropDownList()
               ddl.setValue(cell.getStringCellValue())
               //Please do not uncomment the line below. This has been commented for a reason
//				ddl.controlledVocabulary = getControlledVocab(cell)
               value = ddl
               break
           default: value="";
       }
       return value
   }

   Object getFormulaValue(Cell cell){
       FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator()
       CellValue cellValue = evaluator.evaluate(cell)
       switch (cellValue?.cellTypeEnum) {
           case CellType.BOOLEAN:
               return cellValue.getBooleanValue()
           case CellType.NUMERIC:
               return (BigDecimal)cellValue.getNumberValue()
           case CellType.STRING:
               return cellValue.getStringValue()
           case CellType.BLANK:
               return null
           case CellType.ERROR:
               return null
           return null
       }
   }

   void writeCell(Cell cell, CellTypeEnum type, Object value){
        if(value == null)
           return
       logger.fine "Writing Cell[${cell.rowIndex}, ${cell.columnIndex}] "
       CellRangeAddress mergedRegion = getMergedRegionForCell(cell)
       if(mergedRegion){
           cell = getMergedCell(cell)
       }
       switch(type){
           case CellTypeEnum.DATE:cell.setCellType(CellType.STRING)
               DateFormat df = new SimpleDateFormat("MM/dd/yyyy")
               cell.setCellValue(df.format(value));
               break
           case CellTypeEnum.STRING:
               cell.setCellType(CellType.STRING);
               cell.setCellValue(value)
               break
           case CellTypeEnum.NUMBER:
               if(cell.cellTypeEnum != CellType.STRING)
                   cell.setCellType(CellType.NUMERIC)
               cell.setCellValue(value)
               break;
           case CellTypeEnum.FORMULA:
               if(value) {
                   try {
                       if (cell.cellTypeEnum != CellType.STRING)
                           cell.setCellType(CellType.FORMULA)
                       cell.setCellFormula(String.valueOf(value))
                   }
                   catch (Exception e){
                       if(Number.class.isInstance(value))
                           cell.setCellType(CellType.NUMERIC)
                       else if(String.class.isInstance(value))
                           cell.setCellType(CellType.STRING)
                       cell.setCellValue(value)
                   }
               }
               break
           case CellTypeEnum.DROPDOWN:
               CellRangeAddressList addressList = new CellRangeAddressList(cell.rowIndex, cell.rowIndex, cell.columnIndex, cell.columnIndex)
               DropDownList ddl = (DropDownList)value
               if(ddl.controlledVocabulary) {
                   DataValidation dataValidation = workbook.createDropdownList(parentSheet, addressList, ddl.controlledVocabulary)
                   //PPS-4733 - To allow custom values in drop down lists
                   dataValidation.setShowErrorBox(ddl.showErrorBox)
                   parentSheet.addValidationData(dataValidation)
               }
               String defaultValue = ddl.value
               if(defaultValue)
                   cell.setCellValue(defaultValue)
               break
       }
       /*if(mergedRegion){
           parentSheet.addMergedRegion(new CellRangeAddress(mergedRegion.firstRow, mergedRegion.lastRow, mergedRegion.firstColumn, mergedRegion.lastColumn))
       }*/
   }

   String getSectionKey(){
       return parentSheetIndex + startCell.formatAsString() + (lastCell?lastCell.formatAsString():'')
   }

   CellStyle getCellStyle(Cell srcCell, Cell destcell){
       if(srcCell.sheet.workbook == destcell.sheet.workbook)
           return srcCell.cellStyle
       return workbook.createCellStyle()
   }

   void copySection(){
       Sheet sourceSheet = sourceSection.getParentSheet()
       Sheet destSheet = getParentSheet()
       writeSectionName()
       int currentSrcRow = sourceSection.startCell.row
       int currentDestRow = startCell.row
       int currentSrcCol
       int currentDestCol
       Row srcRow = sourceSheet.getRow(currentSrcRow)
       while(srcRow){
           Row destRow = destSheet.getRow(currentDestRow)
           if(!destRow){
               destRow = destSheet.createRow(currentDestRow)
           }
           def height = srcRow.getHeight()
           height ? destRow.setHeight(height) : null
           int lastCellNum = sourceSection.lastCell?sourceSection.lastCell.col:srcRow.lastCellNum
           currentSrcCol = sourceSection.startCell.col
           currentDestCol = startCell.col
           while(currentSrcCol <= lastCellNum){
               Cell srcCell = srcRow.getCell(currentSrcCol)
               Cell destCell = destRow.createCell(currentDestCol)
               copyCell(srcCell, destCell)
               currentSrcCol++
               currentDestCol++
           }
           currentSrcRow++
           currentDestRow++
           if(sourceSection.lastCell && currentSrcRow > sourceSection.lastCell.row){
               break
           }
           srcRow = sourceSheet.getRow(currentSrcRow)
       }
       copyMergedRegions()
//		copyDropDownLists()
//		if(sourceSection.lastCell)
           lastCell = new CellReference(currentDestRow-1, currentDestCol-1)
   }

   def copyDropDownLists(){
       Sheet sourceSheet = sourceSection.getParentSheet()
       Sheet destSheet = getParentSheet()
       dvRecordsList = workbook.getDVRecords(sourceSheet)
       dvRecordsList.each{def dvRecord ->
           for(CellRangeAddress cellRangeAddress : dvRecord.getCellRangeAddress().getCellRangeAddresses()){
               if(sourceSection.isWithinSection(cellRangeAddress)){
                   int startRow = cellRangeAddress.firstRow - sourceSection.startCell.row + this.startCell.row
                   int endRow = cellRangeAddress.lastRow - sourceSection.startCell.row + this.startCell.row
                   int startCol = cellRangeAddress.firstColumn - sourceSection.startCell.col + this.startCell.col
                   int endCol = cellRangeAddress.lastColumn - sourceSection.startCell.col + this.startCell.col
                   CellRangeAddressList addressList = new CellRangeAddressList(startRow, endRow, startCol, endCol)
                   def value = dvRecord._formula1.tokens[0].toFormulaString()
                   value = value.substring(1, value.length()-1)
                   DataValidation dataValidation = workbook.createDropdownList(parentSheet, addressList,value.split(" "))
                   destSheet.addValidationData(dataValidation)
               }
           }
       }
   }

   String[] getControlledVocab(Cell cell){
       if(!dvRecordsList) {
           dvRecordsList = workbook.getDVRecords(getParentSheet())
       }
       String[] controlledVocab
       dvRecordsList.each{def dvRecord ->
           for(CellRangeAddress cellRangeAddress : dvRecord.getCellRangeAddress().getCellRangeAddresses()){
               if(isWithinSection(cellRangeAddress)){
                   if(cellRangeAddress.firstRow == cell.rowIndex && cellRangeAddress.firstColumn == cell.columnIndex
                   && cellRangeAddress.lastRow == cell.rowIndex && cellRangeAddress.lastColumn == cell.columnIndex) {
                       def value = dvRecord._formula1.tokens[0].toFormulaString()
                       if (value.contains(",") || value.contains('\u0000')) {
                           value = value.substring(1, value.length() - 1)
                           controlledVocab = value.split('\u0000')
                           break
                       }
                       else if (value.contains('!')){
                           //=Lists!$A$2:$A$12
                           String[] valueSplit = value.split("!")
                           String[] range = valueSplit[1].replaceAll('\\$',"").split(":")
                           OneDSection ods = new OneDSection(workbook.getSheetIndex(valueSplit[0]),CellTypeEnum.STRING,range[0],range[1])
                           workbook.addSection(ods)
                           ods.load()
                           controlledVocab = ods.data.toArray()
                           break
                       }
                       else{
                           controlledVocab = [value]
                           break
                       }
                   }
               }
           }
       }
       return controlledVocab
   }

   boolean isWithinSection(CellRangeAddress cellRangeAddress){
       if(cellRangeAddress.isInRange(startCell.row, startCell.col))
           return true
       if(lastCell){
           if(cellRangeAddress.isInRange(lastCell.row, lastCell.col))
               return true
           CellRangeAddress sectionRange = new CellRangeAddress(startCell.row, lastCell.row, startCell.col, lastCell.col)
           if(sectionRange.isInRange(cellRangeAddress.firstRow, cellRangeAddress.firstColumn))
               return true
           if(sectionRange.isInRange(cellRangeAddress.lastRow, cellRangeAddress.lastColumn))
               return true
       }
       return false
   }

   void copyMergedRegions(){
       // If there are are any merged regions in the source row, copy to new row
       Sheet sheet  = getParentSheet()
       sourceSection.getMergedRegions().each{CellRangeAddress cellRangeAddress ->
           int startRow = cellRangeAddress.firstRow - sourceSection.startCell.row + this.startCell.row
           int endRow = cellRangeAddress.lastRow - sourceSection.startCell.row + this.startCell.row
           int startCol = cellRangeAddress.firstColumn - sourceSection.startCell.col + this.startCell.col
           int endCol = cellRangeAddress.lastColumn - sourceSection.startCell.col + this.startCell.col
           CellRangeAddress newCellRangeAddress = new CellRangeAddress(startRow, endRow, startCol, endCol)
           sheet.addMergedRegion(newCellRangeAddress)
       }
   }

   List<CellRangeAddress> getMergedRegions(){
       Sheet sheet = getParentSheet()
       List<CellRangeAddress> mergedRegions = []
       for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
           CellRangeAddress cellRangeAddress = sheet.getMergedRegion(i);
           if(isWithinSection(cellRangeAddress))
               mergedRegions << cellRangeAddress
       }
       return mergedRegions
   }

   void copyCell(Cell srcCell, Cell destCell){
       if(!srcCell){
           destCell = null
           return
       }
       CellStyle newCellStyle = getCellStyle(srcCell, destCell)
       newCellStyle.cloneStyleFrom(srcCell.getCellStyle())
       destCell.setCellStyle(newCellStyle)
       // If there is a cell comment, copy
       if (srcCell.getCellComment()) {
           destCell.setCellComment(srcCell.getCellComment());
       }

       // If there is a cell hyperlink, copy
       if (srcCell.getHyperlink()) {
           destCell.setHyperlink(srcCell.getHyperlink());
       }

       // Set the cell data type
       destCell.setCellType(srcCell.cellTypeEnum);

       // Set the cell data value
       switch (srcCell.cellTypeEnum) {
           case CellType.BLANK:
               destCell.setCellValue(srcCell.getStringCellValue());
               break;
           case CellType.BOOLEAN:
               destCell.setCellValue(srcCell.getBooleanCellValue());
               break;
           case CellType.ERROR:
               destCell.setCellErrorValue(srcCell.getErrorCellValue());
               break;
           case CellType.FORMULA:
               destCell.setCellFormula(srcCell.getCellFormula());
               break;
           case CellType.NUMERIC:
               destCell.setCellValue(srcCell.getNumericCellValue());
               break;
           case CellType.STRING:
               destCell.setCellValue(srcCell.getRichStringCellValue());
               break;
       }
   }

   CellReference getCellReference(String fieldName){
       String fieldHeader = fieldName
       if(beanClass){
           FieldMapping fieldMapping = (FieldMapping)beanClass.getDeclaredField(fieldName)?.getAnnotation(FieldMapping.class)
           fieldHeader = fieldMapping.header()
       }
       return getCellAddress(fieldHeader)
   }

   CellReference calculateCellAddressFromSourceSection(String headerStr){
       CellReference cellReference  = sourceSection.getCellAddress(headerStr)
       if(cellReference){
           int rowOffset = cellReference.row - sourceSection.startCell.row
           int colOffset = cellReference.col - sourceSection.startCell.col
           return new CellReference(startCell.row+rowOffset, startCell.col+colOffset)
       }
       return cellReference
   }

   boolean isRequired(Field field){
       FieldMapping annotation = field.getAnnotation(FieldMapping.class)
       return annotation.required()
   }

   abstract String getSectionConfig(String sectionDelim, String fieldDelim)

   void validateBean(def bean, Integer row = null){
       if(!requiredFields){
           requiredFields = bean.class.fields.findAll{it.getAnnotation(FieldMapping.class)?.required()}
       }
       for(Field field : requiredFields){
           def value = field.get(bean)
           if(value == null || value == ''){
               String msg = "${field.getAnnotation(FieldMapping.class).header()} field is required."
               if(row != null)
                   msg += " Check row $row"
               throw new RuntimeException(msg)
           }
       }
   }

   abstract void store()

   abstract void load(boolean validate=true)

   abstract void setData(def data)

   abstract def getData()

   abstract protected CellReference getCellAddress(String headerStr)
}
