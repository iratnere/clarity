package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

import java.lang.annotation.*

/**
 * This is a custom annotations class meant to be used only with the fields of a bean class
 * Every bean created to populate an excel sheet has to define the annotation
 * 
 * Created by lvishwas on 10/4/14.
 */
@Target(ElementType.FIELD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
 @interface FieldMapping {
	//Specifies the field on which isRowHeader is true, specifies the row header in the table section. 
	//This is used only with the table section. This is optional
     boolean isRowHeader() default false
	//Specifies the header for the value the field holds. This specifies the header (matches the text exactly in the excel sheet)
	//the value of the field is displayed against the header in the excel sheet. Mandatory
     String header()
	//Specifies the cell type for the field. Mandatory
     CellTypeEnum cellType()
    //This is used only with the table section. This is optional
     boolean isCustomCellStyle() default false
    //Specifies if this is a required field
     boolean required() default false
}

