package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LcKeyValueResultsBean {
	@FieldMapping(header = 'Material Category', cellType = CellTypeEnum.STRING)
	public String materialCategory
	@FieldMapping(header = 'Input Plate Barcode', cellType = CellTypeEnum.STRING)
	public String inputPlateBarcode
	@FieldMapping(header = 'Output Plate Barcode', cellType = CellTypeEnum.STRING)
	public String outputPlateBarcode
	@FieldMapping(header = 'Plate Name', cellType = CellTypeEnum.STRING)
	public String plateName
	@FieldMapping(header = 'Capture Probe Set', cellType = CellTypeEnum.STRING)
	public String captureProbeSet
	@FieldMapping(header = '# samples', cellType = CellTypeEnum.NUMBER)
	public BigDecimal numberSamples
	@FieldMapping(header = 'Degree of Pooling', cellType = CellTypeEnum.NUMBER)
	public BigDecimal dop
	@FieldMapping(header = 'LC - Queue', cellType = CellTypeEnum.STRING)
	public String libraryCreationQueue
	@FieldMapping(header = 'LC - Instructions', cellType = CellTypeEnum.STRING)
	public String lcInstructions
	@FieldMapping(header = 'SM - Notes', cellType = CellTypeEnum.STRING)
	public String smNotes
	@FieldMapping(header = 'Target Template Size (bp)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal targetTemplateSize
	@FieldMapping(header = 'Target Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal targetAliquotMass
	@FieldMapping(header = 'Target Aliquot Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal targetAliquotVolume
	@FieldMapping(header = 'Freezer Path', cellType = CellTypeEnum.STRING)
	public String freezerPath
	@FieldMapping(header = 'Claimant', cellType = CellTypeEnum.STRING)
	public String claimant
	@FieldMapping(header = 'iTag Primer Set', cellType = CellTypeEnum.STRING)
	public String itagPrimerSet

	//Please Select,Pass,Fail
	@FieldMapping(header = 'PASS or FAIL?', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList plateResult
	//Sample Problem,Instrument Error,Operator Error,Informatics Error,Facility Problem,Protocol Deviation,Reagent Consumable Issue
	@FieldMapping(header = 'Failure Mode', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList failureMode

	@FieldMapping(header = 'PCR Cycles', cellType = CellTypeEnum.NUMBER)
	public BigDecimal numberPcrCycles
	@FieldMapping(header = 'Library Index Set', cellType = CellTypeEnum.DROPDOWN)
	public def libraryIndexSet
	@FieldMapping(header = 'LC - Plate Notes', cellType = CellTypeEnum.STRING)
	public String lcPlateNotes

	String getIndexContainerBarcode(){
		return libraryIndexSet?.value
	}
    LibraryStockFailureModesService libraryStockFailureModesService

	def validate() {
		def errorMsg = StringBuilder.newInstance()
        if (plateResult) { //Exome, iTag == null
            String labResult = plateResult.value
            if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
                errorMsg << "invalid PASS or FAIL? '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
				return buildErrorMessage(errorMsg)
            }
            Boolean passed = labResult == Analyte.PASS
            if (!passed && !failureMode?.value) {
                errorMsg << "unspecified Failure Mode $ClarityProcess.WINDOWS_NEWLINE"
				return buildErrorMessage(errorMsg)
            }
            if (!passed)
                return null
            if (failureMode?.value) {
                errorMsg << "incorrect failure mode '$failureMode.value' or status '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
            }
        }
		if (numberPcrCycles == null || numberPcrCycles < 0) {
			errorMsg << "unspecified Number of PCR Cycles $ClarityProcess.WINDOWS_NEWLINE"
		}
		return buildErrorMessage(errorMsg)
	}

    static def buildErrorMessage(def errorMsg) {
        def startErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << "Plate Results Section: $ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

	void populateRequiredFields(String libIndexSet, int passCount) {
		LibraryStockFailureModesService libraryStockFailureModesService = (LibraryStockFailureModesService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(LibraryStockFailureModesService)
		plateResult = libraryStockFailureModesService.dropDownPassFail
		failureMode = libraryStockFailureModesService.dropDownFailureModes
		if(passCount > 0)
			plateResult.value = Analyte.PASS
		else{
			plateResult.value = Analyte.FAIL
			failureMode.value = failureMode.controlledVocabulary[0]
		}
		numberPcrCycles = 1
		libraryIndexSet = new DropDownList(value: libIndexSet)
	}
}
