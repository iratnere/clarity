package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.sample_receipt.SampleTrackingTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import org.springframework.jdbc.core.JdbcTemplate

import javax.sql.DataSource
import java.sql.Timestamp

@Transactional
class SampleService {

    GrailsApplication grailsApplication
    JdbcTemplate jdbcTemplate
    DataSource dataSource

    def updateSamplesInPluss(List<SampleTrackingTableBean> sampleTrackingTableBeans) {
        if (!sampleTrackingTableBeans)
            return
        runJdbcTemplateQueuery(sampleTrackingTableBeans, SampleTrackingService.STATUS_TRASHED)
        runJdbcTemplateQueuery(sampleTrackingTableBeans, SampleTrackingService.STATUS_SHIPPED)
        runJdbcTemplateQueuery(sampleTrackingTableBeans, SampleTrackingService.STATUS_ONSITE)
    }

    def runJdbcTemplateQueuery(List<SampleTrackingTableBean> sampleTrackingTableBeans, String status){
        def query = buildUpdateQuery(sampleTrackingTableBeans.findAll{ it.status == status })
        if (!query)
            return
        ClarityWebTransaction.logger.warn "Execute the $status query $query"
        if (!jdbcTemplate)
            jdbcTemplate = new JdbcTemplate(dataSource)
        def result = jdbcTemplate.update(query)
        ClarityWebTransaction.logger.warn "Updated $result samples."
    }

    final static String buildUpdateQuery(List<SampleTrackingTableBean> beans) {
        if (!beans)
            return null
        ClarityWebTransaction.logger.warn "Planning to update ${beans.size()} samples."
        String containerBarcodes = "'${beans*.containerBarcode?.join("','")}'"
        String status = beans[0].status
        def date = new Timestamp(System.currentTimeMillis()).format('dd-MMM-yyyy')
        return """
UPDATE USS.DT_SAMPLE
SET CONTAINER_LOCATION     = '$status',
  CONTAINER_LOCATION_DATE  = '$date'
WHERE SAMPLE_TUBE_BARCODE IN ($containerBarcodes)
"""
    }

}
