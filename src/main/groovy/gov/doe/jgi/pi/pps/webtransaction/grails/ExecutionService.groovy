package gov.doe.jgi.pi.pps.webtransaction.grails

import gov.doe.jgi.pi.pps.webtransaction.transaction.WebTransaction
import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j
import org.hibernate.SessionFactory
import org.springframework.transaction.annotation.Propagation

@Slf4j
@Transactional
class ExecutionService {

    SessionFactory sessionFactory

    @org.springframework.transaction.annotation.Transactional(propagation = Propagation.REQUIRES_NEW)
    void execute(WebTransaction webTransaction, Closure action) {
        try {
            log.info "${webTransaction}  performing action"
            action()
            sessionFactory.currentSession.flush()
            webTransaction.generateJsonResponse()
            webTransaction.webTransactionRecorder?.recordSuccess(webTransaction)
        } catch (Throwable t) {
            throw new RuntimeException(t) //runtime exception, transaction rolls back
        }
    }

}
