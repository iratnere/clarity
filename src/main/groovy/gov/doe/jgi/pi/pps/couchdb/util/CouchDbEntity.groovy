package gov.doe.jgi.pi.pps.couchdb.util

import gov.doe.jgi.pi.pps.webtransaction.util.JsonEntity
import org.grails.web.json.JSONObject

/**
 * Created by dscott on 4/10/2015.
 */
interface CouchDbEntity extends JsonEntity {

    String getCouchDbName()
    void setCouchDbName(String name)

    String getId()
    void setId(String id)

    String getRevision()
    void setRevision(String rev)

    JSONObject getJson()
    void setJson(JSONObject json)

}