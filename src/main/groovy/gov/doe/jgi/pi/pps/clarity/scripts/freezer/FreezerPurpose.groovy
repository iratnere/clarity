package gov.doe.jgi.pi.pps.clarity.scripts.freezer

/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 3/8/11
 * Time: 7:47 PM
 * 
 * Enum corresponds to controlled vocabulary defined in table freezers.dt_purpose_cv.
 */
public enum FreezerPurpose {
	F454('454'),
	CLONING_TECHNOLOGY('Cloning Technology'),
	FINISHING('Finishing'),
	ILLUMINA('Illumina'),
	CLUSTER_GENERATION('Cluster Generation'),
	POOLING('Pooling'),
	QPCR('qPCR'),
	SAMPLE_MANAGEMENT('Sample Management')

	public final String purpose;

	/**
	 * The string purpose supplied to the constructor should be the string in the purpose field of the database
	 * CV table freezers.dt_purpose_cv.
	 * @param purpose
	 * @return
	 */
	public FreezerPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 * Method allow translation of the string defined in a database field to the corresponding enum value.
	 * @param name
	 * @return
	 */
	static FreezerPurpose toEnum(String name) {
		for (FreezerPurpose purpose : FreezerPurpose.values()) {
			if (name == purpose.purpose) {
				return purpose;
			}
		}
        return null;
	}
	
	public String toString() {
		return purpose
	}
	
}