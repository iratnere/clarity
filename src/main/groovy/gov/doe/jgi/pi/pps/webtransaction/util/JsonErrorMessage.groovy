package gov.doe.jgi.pi.pps.webtransaction.util

import org.grails.web.json.JSONObject

class JsonErrorMessage extends JSONObject {
	
	private static final String MESSAGE = 'message'
	private static final String INDEX = 'index'
	private static final String ENTITY = 'entity'
	private static final String KEY = 'key'
	
	public JsonErrorMessage() {}
	
	public JsonErrorMessage(Object message) {
		setMessage(message)
	}
	
	public JsonErrorMessage(Object message, Object entity, Object index, Object key = null) {
		setMessage(message)
		setEntity(entity)
		setIndex(index)
		setKey(key)
	}
	
	public JsonErrorMessage(Map message) {
		message.each() {key,value ->
			put(key?.toString(),value?.toString())
		}
		if (message[MESSAGE]) {
			remove('args')
			remove('code')
		}
	}
	
	
	public void setMessage(Object errorMessage) {
		put(MESSAGE, errorMessage?.toString())
	}
	
	public void setEntity(Object entity) {
		if (entity) {
			put(ENTITY,entity?.toString())
		}
	}
	
	public void setIndex(Object index) {
		if (index) {
			put(INDEX,index?.toString())
		}
	}
	
	public void setKey(Object key) {
		if (key) {
			put(KEY,key?.toString())
		}
	}
}
