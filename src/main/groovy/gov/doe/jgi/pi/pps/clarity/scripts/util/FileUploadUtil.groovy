package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity_node_manager.node.FilePoster
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig

/**
 * Created by datjandra on 9/17/2015.
 */
class FileUploadUtil {
    static Node uploadTextFile(String limsId,
                               String contents,
                               NodeConfig nodeConfig,
                               String extension='.txt',
                               Boolean testMode=true,
                               String fileName = limsId) {
        if (testMode) {
            return null
        }

        File tempFile = null
        try {
            tempFile = File.createTempFile(fileName, extension)
            tempFile.withWriter {
                it << contents
            }
            return FilePoster.attachFileReturnContentLocationNode(nodeConfig, limsId, tempFile)
        } finally {
            tempFile?.delete()
        }
    }
}
