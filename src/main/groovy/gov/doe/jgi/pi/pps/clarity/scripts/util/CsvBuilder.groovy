package gov.doe.jgi.pi.pps.clarity.scripts.util

import com.opencsv.CSVWriter

/**
 * Created by datjandra on 9/16/2015.
 */

class CsvBuilder {
    private Integer columns
    private StringWriter sw = new StringWriter()
    private CSVWriter writer = new CSVWriter(sw)
    boolean applyQuotesToAll

    CsvBuilder(Integer columns, applyQuotesToAll = true) {
        this.columns = columns
        this.applyQuotesToAll = applyQuotesToAll
    }

    void append(String[] strArray) {
        append(strArray as List)
    }

    void append(List strList) {
        if (strList.size() > columns) {
            throw new RuntimeException("List size must be <= $columns")
        }

        String[] strArray = new String[columns]
        strList.eachWithIndex { str, index ->
            strArray[index] = str as String
        }
        writer.writeNext(strArray, applyQuotesToAll)
    }

    void done() {
        writer.close()
    }

    String toString() {
        return sw.buffer.toString()
    }

    static String dataToCsvString(List<List> data) {
        StringWriter sw = new StringWriter()
        CSVWriter writer = new CSVWriter(sw)
        String[] emptyLine = null
        data.each { List row ->
            if (row) {
                String[] strArray = new String[row.size()]
                row.eachWithIndex { cell, index ->
                    strArray[index] = cell as String
                }
                writer.writeNext(strArray)
            } else {
                writer.writeNext(emptyLine)
            }
        }
        return sw.buffer.toString()
    }
}
