package gov.doe.jgi.pi.pps.couchdb.util

import groovy.util.logging.Slf4j
import org.grails.web.json.JSONObject

/**
 * Created by dscott on 4/13/2015.
 */
@Slf4j
class CouchDbEntityImpl implements CouchDbEntityBeforeAfter {

    String id
    String revision

    protected JSONObject jsonObject
    protected String dbname

    @Override
    String getCouchDbName() {
        dbname
    }

    @Override
    void setCouchDbName(String name) {
        this.dbname = name
    }

    JSONObject getJson() {
        jsonObject
    }

    void setJson(JSONObject json) {
        log.trace "${this} set json:\n${json?.toString(2)}"
        this.jsonObject = json
    }

    String toString() {
        return "${this.class.simpleName}[${couchDbName}:${id}]"
    }

    @Override
    void beforeInsert() {
        log.trace "${this} before insert"
    }

    @Override
    void afterInsert() {
        log.trace "${this} after insert"
    }

    @Override
    void beforeUpdate() {
        log.trace "${this} before update"
    }

    @Override
    void afterUpdate() {
        log.trace "${this} after update"
    }

    @Override
    void beforeDelete() {
        log.trace "${this} before delete"
    }

    @Override
    void afterDelete() {
        log.trace "${this} after delete"
    }

    @Override
    void beforeGet() {
        log.trace "${this} before get"
    }

    @Override
    void afterGet() {
        log.trace "${this} after get"
    }

}
