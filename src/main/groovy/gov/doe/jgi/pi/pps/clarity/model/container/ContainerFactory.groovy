package gov.doe.jgi.pi.pps.clarity.model.container

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

/**
 * Created by dscott on 3/3/2016.
 */
class ContainerFactory {

    private static Container generateContainer(ContainerNode containerNode) {
        Container container = null
        container = new Container(containerNode)
        container
    }

    static final Container containerInstance(ContainerNode containerNode) {
        if (!containerNode) {
            return null
        }
        if (!containerNode.cache) {
            containerNode.cache = generateContainer(containerNode)
        }
        return (Container) containerNode.cache
    }

}
