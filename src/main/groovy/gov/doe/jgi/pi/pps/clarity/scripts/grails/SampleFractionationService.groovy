package gov.doe.jgi.pi.pps.clarity.scripts.grails

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SampleFractionModel
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SamplesFractionationModel
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import org.grails.web.json.JSONObject
import org.springframework.http.HttpStatus

@Transactional
class SampleFractionationService {

    GrailsApplication grailsApplication
    static final int HTTP_422 = HttpStatus.UNPROCESSABLE_ENTITY.value()

    String getSubmissionUrl() {
        return grailsApplication.config.sampleFractionation.url
    }

    def createFractionSamples(List fractionsLimsIds, Long contactId){
        if (!fractionsLimsIds) {
            return
        }
        SamplesFractionationModel model = getSamplesFractionationModel(fractionsLimsIds, contactId)
        def responseJson = callHttp(submissionUrl, Method.POST, model)
        responseJson
    }

    SamplesFractionationModel getSamplesFractionationModel(List fractionsLimsIds, Long contactId) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        SamplesFractionationModel submission = new SamplesFractionationModel()
        submission.submittedBy = contactId
        submission.fractionSamples = fractionsLimsIds.collect{ limsId ->
            Analyte fraction = AnalyteFactory.analyteInstance(nodeManager.getArtifactNode(limsId))
            new SampleFractionModel(
                    originalSampleId: convertToLong(fraction.claritySample.name),
                    fractionDensityGMl: fraction.udfFractionDensity,
                    fractionDnaConcentrationNgUl: fraction.udfConcentrationNgUl,
                    fractionSampleBarcode: fraction.udfDestinationBarcode,
                    fractionSampleName: fraction.name,
                    fractionSampleVolumeUl: fraction.udfVolumeUl
            )
        }
        submission
    }

    static def convertToLong(def object) {
        try{
            object as Long
        } catch (e) {
            throw new WebException("""
Invalid original sample id: $object
${e.message}
""", HttpStatus.UNPROCESSABLE_ENTITY.value())
        }
        return object as Long
    }

//http://claritydev1.jgi-psf.org/sample-fractionation  //http://clarity-dev01.jgi-psf.org:60966/sample-fractionation
    def callHttp(String url, def method, SamplesFractionationModel samplesFractionationModel){
        String jsonBody = new ObjectMapper().writeValueAsString(samplesFractionationModel)
        def json = new JSONObject(jsonBody)
        def responseJson = null
        ClarityWebTransaction.logger.info "http request URL: ${url}"
        ClarityWebTransaction.logger.info "submission: ${json}"
        HTTPBuilder http = new HTTPBuilder(url)
        http.request(method as Method, ContentType.JSON) { req ->
            json ? body = json.toString() : null
            response.success = { HttpResponseDecorator resp, respJson ->
                ClarityWebTransaction.logger.info "response json: ${respJson}"
                responseJson = respJson
            }
            response.failure = { HttpResponseDecorator resp, respJson ->
                def errorMsg = StringBuilder.newInstance()
                errorMsg << "$url service response status: [${resp.status}]" << ClarityProcess.WINDOWS_NEWLINE
                def respError = JsonUtil.toJson(respJson)
                ClarityWebTransaction.logger.error("$url service response: status [${resp.status}], body [${respError}]")
                if (resp.status == HTTP_422 && respError.'errors'?.find{it.'index' != null}) {
                    respError.'errors'.each{
                        def index = it.'index' as int
                        def fractionSampleName = samplesFractionationModel.fractionSamples[index]?.fractionSampleName
                        errorMsg << "[$fractionSampleName] $it.key: $it.message" << ClarityProcess.WINDOWS_NEWLINE
                    }
                } else {
                    errorMsg << "[${respError}]" << ClarityProcess.WINDOWS_NEWLINE
                }
                throw new WebException(errorMsg.toString(), resp.status)
            }
        }
        responseJson
    }
}