package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by datjandra on 7/16/2015.
 */
class PlatePassFailKeyValueBean {

    PlatePassFailKeyValueBean() {
        passFail = new DropDownList()
        passFail.setControlledVocabulary([AttributeValues.DONE, AttributeValues.REWORK] as String[])
    }

    @FieldMapping(header = 'Plate Pass/Fail', cellType = CellTypeEnum.DROPDOWN)
    public DropDownList passFail;

    int populateRequiredFields(int platePassPercentage){
        passFail = new DropDownList()
        passFail.setControlledVocabulary([AttributeValues.DONE, AttributeValues.REWORK] as String[])
        passFail.value = platePassPercentage? AttributeValues.DONE: AttributeValues.REWORK
        return --platePassPercentage
    }
}
