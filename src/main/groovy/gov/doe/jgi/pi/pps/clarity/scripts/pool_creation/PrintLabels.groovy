package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

class PrintLabels extends ActionHandler {
    void execute() {
        process.writeLabels(process.analytesToPrintLabels)
    }
}
