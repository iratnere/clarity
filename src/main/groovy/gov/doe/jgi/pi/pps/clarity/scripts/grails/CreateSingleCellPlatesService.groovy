package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.grails.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SampleSubmission
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SamplesSubmission
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SequencingProjectSubmission
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SequencingProjectsSubmission
import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import gov.doe.jgi.pi.pps.clarity.model.researcher.ResearcherFactory
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProjectNode
import grails.converters.JSON
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import org.apache.commons.lang.RandomStringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate

@Transactional
class CreateSingleCellPlatesService {

    GrailsApplication grailsApplication
    def dataSource
    ArtifactNodeService artifactNodeService
    Logger logger = LoggerFactory.getLogger(this.class.name)

    static final Long TEST_FRAMEWORK_CONTACT_ID = 2633
    static final def CONTROL_TYPES = ['positive', 'negative']
    static final List<Long> INDEX_SET_IDS = [6, 7, 8, 9]

    Long submittedBy
    Long sequencingProductId = 2
    Long strategyId = 87

    int destContainersSize
    int inputContainersSize
    int batchSize

    def inputPlateLabels = []
    def inputPlateBarcodes = []
    def destContainerNames = []
    def wells
    def destWells

    static String buildSampleIdsQuery(List<Long> projectIds) {
        return """
SELECT SAMPLE_ID
FROM USS.DT_M2M_SAMPLESOWITEM
WHERE SOW_ITEM_ID IN
  (SELECT SOW_ITEM_ID
  FROM USS.DT_SOW_ITEM
  WHERE SEQUENCING_PROJECT_ID IN (${projectIds.join(',')})
  )
"""
    }

    static def getPlateWells() {
        def plateWells = []
        (24..1).each { column ->
            ('P'..'A').each { row ->
                plateWells << "$row$column"
            }
        }
        plateWells
    }

    static def getDestPlateWells() {
        def plateWells = []
        (12..1).each { column ->
            ('H'..'A').each { row ->
                plateWells << convert("$row$column")
            }
        }
        plateWells
    }

    static String convert(String well96){
        def row = well96.split(/\d+/)[0]
        def column = well96.tokenize(row)[0] as BigInteger
        def convertedRow = (2*(row.hashCode())-65) as Character
        def convertedColumn = 2*column-1
        return "$convertedRow$convertedColumn"
    }

    def init(int batchSize, int inputContainersSize, int destContainersSize, Long submittedBy) {
        this.submittedBy = submittedBy
        this.destContainersSize = destContainersSize
        this.inputContainersSize = inputContainersSize
        this.batchSize = batchSize

        assert batchSize > 0
        assert inputContainersSize > 0
        assert destContainersSize > 0
        assert batchSize >= destContainersSize
        assert batchSize >= inputContainersSize

        inputPlateLabels = (1..inputContainersSize).collect{ "T${new Date().time}${RandomStringUtils.randomAlphanumeric(4)}" }
        assert inputPlateLabels.size() == inputContainersSize

        inputPlateBarcodes = (1..inputContainersSize).collect{ "SrcBarcode${new Date().time}${RandomStringUtils.randomAlphanumeric(4)}" }
        assert inputPlateBarcodes.size() == inputContainersSize

        destContainerNames = (1..destContainersSize).collect{ "DestPlate$it" }
        assert destContainerNames.size() == destContainersSize

        wells = getPlateWells()
        assert wells.size() == 384
        destWells = getDestPlateWells()
        assert destWells.size() == 96
        assert batchSize <= 96
    }

    List<Long> create(int batchSize, int inputContainersSize, int destContainersSize, Long submittedBy) {
        init(batchSize, inputContainersSize, destContainersSize, submittedBy)
        String spSumbissionUrl = "${grailsApplication.config.getProperty('sequencingProjectSubmission.url')}"
        def responseJson = callHttp(spSumbissionUrl, getSequencingProjectsJson().toString())
        List<Long> projectIds = responseJson.'sequencing-projects' //response json: [sequencing-projects:[1249120, 1249121], submitted-by:2633]
        assert projectIds

        if (submittedBy == TEST_FRAMEWORK_CONTACT_ID) {
            Researcher researcher = ResearcherFactory.researcherForContactId(ClarityWebTransaction.requireCurrentTransaction().nodeManager, submittedBy)
            projectIds.each {updateProjectUdf(it, researcher.lastFirstName)}
        }

        List<Long> sampleIds = findSampleIds(projectIds)
        assert sampleIds

        String sampleSumbissionUrl = "${grailsApplication.config.getProperty('sampleSumbission.url')}"
        callHttp(sampleSumbissionUrl, getSamplesJson(sampleIds).toString())
        sampleIds
    }
    //TODO check?
    /*
        SequencingProduct sequencingProduct = SequencingProduct.get(sequencingProductId)
        SequencingStrategy sequencingStrategy = SequencingStrategy.get(strategyId)
        assert sequencingProduct?.id == sequencingProductId
        assert sequencingStrategy?.id == strategyId

        String mappingStrategy = sequencingStrategy.sampleMappingStrategy?.sampleMappingStrategy
        assert mappingStrategy == '1:All'
*/

    ProjectNode updateProjectUdf(Long projectId, def udfValue, ClarityUdf udf = ClarityUdf.PROJECT_CREATED_BY) {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().nodeManager
        String projectLimsId = artifactNodeService.getProjectIdByName(projectId as String)
        ProjectNode projectNode = nodeManager.getProjectNode(projectLimsId)
        projectNode.setUdf(udf.udf, udfValue)
        projectNode.httpPut()
        projectNode
    }

    List<Long> findSampleIds(List<Long> projectIds) {
        if (!projectIds)
            return null
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.queryForList(buildSampleIdsQuery(projectIds)).collect{it.'SAMPLE_ID' as Long}?.unique()
    }

    def callHttp(String url, def json){
        def responseJson = null
        logger.debug "http request URL: ${url}"
        HTTPBuilder http = new HTTPBuilder(url)
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = json.toString()
            response.success = { HttpResponseDecorator resp, respJson ->
                //println "response json: ${respJson}"
                responseJson = respJson
            }
            response.failure = {resp, reader ->
                logger.error "Errors: ${resp.status} : ${reader}"
                throw new RuntimeException("Errors: ${resp.status} : ${reader}")
            }
        }
        responseJson
    }

    String getPlateLocation() {
        return wells.pop() as String
    }

    String getDestPlateLocation() {
        return destWells.pop() as String
    }

    int getInputPlateIndex(int index) {
        return index.mod(inputContainersSize)
    }

    int getOutputPlateIndex(int index) {
        return index.mod(destContainersSize)
    }

    def getPoolNumber(int index) {
        return  ++(index % destContainersSize == 0 ? index : getOutputPlateIndex(index))
    }

    String getControlType(int index) {
        return (index % destContainersSize == 0 ? CONTROL_TYPES[0] : CONTROL_TYPES[1])
        //return (index in [0, 1] ? CONTROL_TYPES[index] : null)
    }

    SampleSubmission createSampleSubmission(Long sampleId, int index) {
        SampleSubmission sampleSubmission = new SampleSubmission()
        sampleSubmission.sampleId = sampleId
        sampleSubmission.label = inputPlateLabels[getInputPlateIndex(index)]
        sampleSubmission.barcode = inputPlateBarcodes[getInputPlateIndex(index)]
        sampleSubmission.plateLocation = plateLocation
        sampleSubmission.controlType = getControlType(index)
        sampleSubmission.poolNumber = getPoolNumber(index)
        sampleSubmission.destContainerName = destContainerNames[getOutputPlateIndex(index)]
        sampleSubmission.destContainerLocation = destPlateLocation

        return sampleSubmission
    }

    JSON getSamplesJson(List<Long> sampleIds) {
        SamplesSubmission submission = new SamplesSubmission()
        submission.submittedBy = submittedBy
        sampleIds.eachWithIndex {Long sampleId, int index ->
            submission.samples << createSampleSubmission(sampleId, index)
        }
        submission as JSON
    }

    JSON getSequencingProjectsJson() {
        SequencingProjectsSubmission submission = new SequencingProjectsSubmission()
        submission.submittedBy = submittedBy
        (1..batchSize).each {
            submission.sequencingProjects << getSequencingProjectSubmission()
        }
        submission as JSON
    }

    SequencingProjectSubmission getSequencingProjectSubmission() {
        SequencingProjectSubmission projectSubmission = new SequencingProjectSubmission()
        projectSubmission.sequencingProductId = sequencingProductId
        projectSubmission.strategyId = strategyId
        projectSubmission.submittedBy = submittedBy
        projectSubmission
    }
}
