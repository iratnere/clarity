package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPoolLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode

/**
 * Created by lvishwas on 12/18/2014.
 */
class LpRouteToWorkflow extends ActionHandler {

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        updateInputs((process as PoolCreation).getPoolCreationMemberBeans(PoolCreation.UPLOADED_POOL_CREATION_SHEET))
        performRouting()
    }

    void performRouting() {
        Map<String, List<ArtifactNode>> routeMap = [:].withDefault { [] }
        process.outputAnalytes?.each { ClarityLibraryPool pool ->
            routeMap[getRoutingUri(pool)] << pool.artifactNode
        }
        if (routeMap) {
            routeMap.each { String stageUri, List<ArtifactNode> poolNodes ->
                process.routeArtifactIdsToUri(stageUri, poolNodes*.id)
            }
        }
    }

    String getRoutingUri(ClarityLibraryPool pool){
        if(pool.udfLibraryQcResult == Analyte.PASS){
            if (pool.isInternalSingleCell && !(pool instanceof ClarityPoolLibraryPool))
                return Stage.SIZE_SELECTION.uri
            //PPS-4622: PacBio Multliplex Pools - Library not routed to PacBio Sequel Annealing when pool number zero is used
            //PPS-4497 - Route PacBio to Annealing queue
            if(pool.isPacBio)
                return Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING.uri
            return ((PoolCreation)process).getUriToRoute(pool.runModeCv)
        }
        return Stage.ABANDON_QUEUE.uri
    }

    static void updateInputs(def pools){
        pools.values().flatten().each{ PoolCreationTableBean bean ->
            def volume = bean.analyte.udfVolumeUl
            BigDecimal volumeUsed = bean.dilute == 'DILUTE' ? 2 : (bean.ulOfUndiluted as String)?.toBigDecimal()
            volume -= volumeUsed
            bean.analyte.udfVolumeUl = volume
            bean.analyte.setUdfLpActualWithSof(bean.lpActualWithSof as BigDecimal)
        }
    }
}
