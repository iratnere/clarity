package gov.doe.jgi.pi.pps.couchdb.util

import gov.doe.jgi.pi.pps.couchdb.query.changes.ChangesQueryResponseResult
import groovy.util.logging.Slf4j

import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Created by dscott on 4/13/2015.
 */
@Slf4j
class CouchDb implements Comparable<CouchDb> {

    String name
    Long loadChangesSince

    final ConcurrentLinkedQueue<ChangesQueryResponseResult> changesQueue = new ConcurrentLinkedQueue<>()


    String toString() {
        name
    }

    @Override
    int compareTo(CouchDb o) {
        this.name.compareTo(o.name)
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof CouchDb)) return false
        CouchDb couchDb = (CouchDb) o
        if (name != couchDb.name) return false
        return true
    }

    int hashCode() {
        return (name != null ? name.hashCode() : 0)
    }

}
