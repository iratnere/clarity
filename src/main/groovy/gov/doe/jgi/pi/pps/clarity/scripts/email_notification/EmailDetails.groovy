package gov.doe.jgi.pi.pps.clarity.scripts.email_notification

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample

/**
 * Created by lvishwas on 2/26/2015.
 */
abstract class EmailDetails {
    def proposalId
    def sampleContactId
    def sequencingProjectId
    def sequencingProjectName
    def sequencingProjectContactId
    def sequencingProjectPIName
    def sequencingProjectManagerId
    def sequencingProjectManagerName
    def sequencingProductName
    def pmoSampleId
    def sampleLimsId
    def sampleName
    def sampleMass
    def barcode
    def containerName
    def plateLocation
    def sampleContactName
    def containerNode

    EmailDetails(Analyte analyte) {
        ClaritySample sample = analyte.claritySample
        SequencingProject sqProject = sample.sequencingProject
        if (!sqProject)
            throw new RuntimeException("Sequencing Project not found for sample id ${sample.id}")
        proposalId = sqProject.udfSequencingProjectProposalId
        sampleContactId = sqProject.udfSampleContactId
        sequencingProjectId = sqProject.name
        sequencingProjectName = sqProject.udfSequencingProjectName//PPS-3082
        sequencingProjectContactId = sqProject.udfSequencingProjectPIContactId
        sequencingProjectPIName = EmailDetails.formatName(sqProject.udfSequencingProjectPI)
        sequencingProjectManagerId = sqProject.udfSequencingProjectManagerId
        sequencingProjectManagerName = EmailDetails.formatName(sqProject.udfSequencingProjectManager)
        sequencingProductName = sqProject.udfSequencingProductName
        pmoSampleId = sample.name
        PmoSample rootSample = (sample instanceof PmoSample)? sample : sample.pmoSample
        sampleLimsId = rootSample.id
        sampleName = rootSample.udfCollaboratorSampleName //PPS-3082
        sampleMass = rootSample.mass
        barcode = sample.containerName
        containerName = analyte.isOnPlate ? sample.containerUdfLabel : ""//PPS-3082
        plateLocation = analyte.isOnPlate ? sample.location.replace(":", "") : ""
        sampleContactName = EmailDetails.formatName(sqProject.udfSampleContact)
        containerNode = analyte.containerNode
    }

    static def formatName(String name=''){
        String[] nameSplit = name?.split(', ')
        if (nameSplit && nameSplit.length == 2)
            return "${nameSplit[1]} ${nameSplit[0]}"
        return name
    }

    abstract def getHashCode()

    abstract def getInfo()
}
