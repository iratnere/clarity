package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import grails.gorm.transactions.Transactional

@Transactional
class DropDownService {

    DropDownList getDropDownPassFail() {
        DropDownList dropDownList = new DropDownList()
        dropDownList.setControlledVocabulary(Analyte.PASS_FAIL_LIST)
        return dropDownList
    }
}
