package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.grails.ConfigurationService
import gov.doe.jgi.pi.pps.clarity.grails.PostProcessService
import gov.doe.jgi.pi.pps.clarity.grails.RoutingService
import gov.doe.jgi.pi.pps.clarity.grails.StatusService
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.sample_receipt.SampleTrackingTableBean
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Transactional
class SampleTrackingService {

    GrailsApplication grailsApplication
    ConfigurationService configurationService
    StatusService statusService
    PostProcessService postProcessService
    RoutingService routingService
    SampleService sampleService
    static final String STATUS_SHIPPED = 'Shipped'
    static final String STATUS_TRASHED = 'Trashed'
    static final String STATUS_ONSITE = 'On Site'
    private final static String RESULT_FILENAME = 'ContainerBarcodes.xls'
    static final int MAX_INPUT_SIZE = 300

    final static List<String> INVALID_STATUSES = [
            'Created',
            'Awaiting Collaborator Metadata',
            'Awaiting Shipping Approval',
            'Awaiting Sterility Document',
            'Sterility Document Review',
            'Awaiting Sample Receipt'
    ]
    private final static Logger logger = LoggerFactory.getLogger(this.class.name)

    File getSamFolder(){
        if (grailsApplication.config.sam.folder) {
            return new File(grailsApplication.config.sam.folder as String)
        } else {
            return new File("${System.getProperty('java.io.tmpdir')}${File.separator}data")
        }
    }

    def processFiles() {
        File samDir = samFolder
        logger.info "Reading ${samDir}....."
        if (!samDir.exists()) {
            samDir.mkdirs()
        }
        Set<File> archiveSet = new HashSet<File>()
        Set<File> deleteSet = new HashSet<File>()
        File[] fileList = samDir.listFiles()
        if (fileList) {
            ClarityWebTransaction webTransaction = new ClarityWebTransaction().setNodeManager().setTestMode().build()
            webTransaction.submittedBy = SampleTrackingService.name
            NodeConfig clarityNodeConfig = configurationService.getClarityNodeConfig()
            NodeManager nodeManager = new NodeManager(clarityNodeConfig)
            for (File file : fileList) {
                if (file.isDirectory()) {
                    continue
                }
                logger.info "Processing file ${file}...."
                FileInputStream fis = new FileInputStream(file)
                try {
                    processExcelWorkbook(new ExcelWorkbook(fis), nodeManager)
                    logger.info "Processing file ${file.name} complete."
                    deleteSet.add(file)
                } catch (Exception e) {
                    archiveSet.add(file)
                    logger.error("Error processing file ${file.name}. This file shall be archived.", e)
                } finally {
                    try {
                        fis.close()
                    } catch (Exception e) {
                        logger.error("Error ", e)
                    }
                }
            }
            delete(deleteSet)
            archive(archiveSet)
        }
    }

    void processExcelWorkbook(ExcelWorkbook excelWorkbook, NodeManager nodeManager){
        if(!excelWorkbook)
            return
        Section tableSection = new TableSection(0, SampleTrackingTableBean.class, 'A1', null, true)
        excelWorkbook.addSection(tableSection)
        excelWorkbook.load()
        List<SampleTrackingTableBean> sampleData = tableSection.getData() as List<SampleTrackingTableBean>
        List<SampleTrackingTableBean> sampleBeansOutsideClarity = []
        Map<Tuple, Map<ContainerNode, List<Analyte>>> analyteGroups = [:].withDefault{[:].withDefault{[]}}
        List<RoutingRequest> routingRequests = []
        sampleData?.each{ SampleTrackingTableBean bean ->
            String errors = bean.validateBean(nodeManager)
            if(errors)
                throw new IllegalArgumentException("File failed validation. $errors")
            prepareBean(bean, nodeManager)
            if(bean.isContainerInClarity){
                logger.info "Updating all containers...."
                bean.updateContainerUdfs()
                Tuple tuple = new Tuple([bean.stage, bean.researcher] as Object[])
                //PPS-4911 bean.beanAnalytes.groupBy{it.containerNode} returns LinkedHashMap<ContainerNode,ArrayList<Analyte>>>
                analyteGroups[tuple].putAll(bean.beanAnalytes.groupBy{it.containerNode})
                if(bean.routingRequests)
                    routingRequests.addAll(bean.routingRequests)
            } else {
                sampleBeansOutsideClarity << bean
                logger.warn "Cannot find container by barcode ${bean.barcode} in Clarity"
            }
        }
        logger.info "Processing samples that are not yet in Clarity...."
        sampleService.updateSamplesInPluss(sampleBeansOutsideClarity)
        nodeManager.httpPutDirtyNodes()
        if(routingRequests){
            logger.info "Removing analytes from respective queues....."
            routingService.submitRoutingRequests(routingRequests)
        }
        if(analyteGroups){
            postProcesses(analyteGroups, excelWorkbook, nodeManager)
        }
    }

    void prepareBean(SampleTrackingTableBean bean, NodeManager nodeManager){
        logger.info "Looking up containers by name ${bean.barcode}...."
        List<ContainerNode> containerNodes = nodeManager.getContainerNodesByName(bean.barcode) as List<ContainerNode>
        if(!containerNodes) {
            logger.info "Container ${bean.barcode} not found in clarity."
            return
        }
        logger.info "Found containers ${containerNodes} for name ${bean.barcode}"
        bean.setBeanContainers(containerNodes)
        Map<Long,String> sampleStatusMap = statusService.getSampleStatusBatch(bean.pmoSampleIds)
        logger.info "Validating contents of all containers....."
        validate(bean.beanAnalytes, sampleStatusMap)
    }

    void delete(Set<File> deleteSet) {
        if(!deleteSet)
            return
        deleteSet.each { File containerBarcodesFile ->
            if (!containerBarcodesFile?.isDirectory()) {
                containerBarcodesFile.delete()
            }
        }
        logger.info("Deleted ${deleteSet.size()} files - ${deleteSet.collect{it.name}}")
    }

    void archive(Set<File> archiveSet) {
        if(!archiveSet)
            return
        archiveSet.each{File  containerBarcodesFile ->
            if (!containerBarcodesFile?.isDirectory()) {
                String parentDirName = containerBarcodesFile?.parent
                if (parentDirName) {
                    String archiveDirName = "${parentDirName}${File.separator}archive"
                    File archiveDir = new File(archiveDirName)
                    if (!archiveDir.exists()) {
                        archiveDir.mkdir()
                    }

                    File destinationFile = new File(archiveDirName, containerBarcodesFile.name)
                    if (destinationFile.exists()) {
                        destinationFile.delete()
                    }
                    containerBarcodesFile.renameTo(destinationFile.absolutePath)
                }
            }
        }
        logger.info("Archived ${archiveSet.size()} files - ${archiveSet.collect{it.name}}")
    }

    void postProcesses(Map<Tuple, Map<ContainerNode,List<Analyte>>> analyteGroups, ExcelWorkbook excelWorkbook, NodeManager nodeManager) {
        for (Map.Entry<Tuple,Map<ContainerNode,List<Analyte>>> entry : analyteGroups.entrySet()) {
            Tuple tuple = entry.key
            Stage stage = tuple[0] as Stage
            Researcher researcher = tuple[1] as Researcher
            List<ArtifactNode> inputs = []
            entry.value.each{ContainerNode cNode, List<Analyte> analyteList ->
                def analytes = analyteList as Set<Analyte>
                if((inputs.size()+analytes.size()) > MAX_INPUT_SIZE){
                    postProcess(stage, inputs, nodeManager, researcher.researcherNode.id, excelWorkbook)
                    inputs.clear()
                }
                inputs.addAll(analytes.collect{it.artifactNode} as List<ArtifactNode>)
            }
            if(inputs){
                postProcess(stage,inputs,nodeManager, researcher.researcherNode.id, excelWorkbook)
            }
        }
    }

    void postProcess(Stage stage, List<ArtifactNode> inputs, NodeManager nodeManager, String researcherId, ExcelWorkbook excelWorkbook){
        String processId = postProcessService.postInputProcess(stage, inputs, nodeManager, researcherId)
        ProcessNode processNode = nodeManager.getProcessNode(processId)
        ClarityWebTransaction.logger.info "Process $processId posted successfully"
        if(excelWorkbook) {
            def artifactNode = processNode.outputResultFiles.find {
                it.name.contains(RESULT_FILENAME)
            }
            Node responseNode = excelWorkbook.store(nodeManager.nodeConfig, artifactNode.id, false)
            if (responseNode) {
                logger.info "uploaded $RESULT_FILENAME to $responseNode"
            }
        }
    }

    void validate(List<Analyte> analytes, Map sampleStatusMap) {
        analytes.each { Analyte analyte ->
            if (!(analyte instanceof SampleAnalyte)) {
                throw new IllegalArgumentException("Invalid input [${analyte.id}]: Expecting samples only.")
            }
            ClaritySample claritySample = analyte.claritySample
            if (claritySample instanceof PmoSample) {
                String status = sampleStatusMap[claritySample.pmoSampleId]
                if (!status || status in INVALID_STATUSES) {
                    throw new RuntimeException("Invalid status [${status}]. Samples must have a status >= [Sample Received].")
                }
            } else {
                logger.warn "Skipping validation of status of ${claritySample}: status validation only performed for PMO Samples only."
            }
        }
    }
}