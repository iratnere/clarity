package gov.doe.jgi.pi.pps.clarity.web_transaction

enum WebTransactionChildSessionKeys {
	NODE_MANAGER('node-manager')
	
	String value
	
	private WebTransactionChildSessionKeys(String value) {
		this.value = value
	}
	
	public String toString() {
		return value
	}
}
