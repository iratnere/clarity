package gov.doe.jgi.pi.pps.clarity.scripts.grails

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import grails.gorm.transactions.Transactional
import org.apache.commons.lang.StringUtils
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor
import org.springframework.jdbc.core.RowCallbackHandler

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by lvishwas on 2/27/15.
 */
@Transactional
class ArtifactIndexService {
    static final int BATCH_SIZE = 999
    static final String INDEX_CONCAT_DELIMITER = '-'
    static final String INDEX_NAME_N_A = 'n/a'

    DataSource dataSource

    static boolean isContainerBarcode(String indexSetName){
        String regex = "\\d{2}[A-Z]?[0-9]*-\\d{6}-[0-9]+"
        Pattern pattern = Pattern.compile(regex)
        Matcher matcher = pattern.matcher(indexSetName)
        return matcher.find()
    }

    static boolean isIndexPlateBarcode(String indexSetName){
        def indexSet = indexSetName.substring(0,indexSetName.indexOf("-"))
        isContainerBarcode(indexSetName) && StringUtils.isNumeric(indexSet)
    }

    static String getConstraints(String indexSetName, Map handShakeIdToSetName){
        String handShakeId = indexSetName
        StringBuilder sb = new StringBuilder()
        boolean closeParanthesis = false
        if(isContainerBarcode(indexSetName)){
            if(isIndexPlateBarcode(indexSetName))
                handShakeId = Index.parsePlateIndexBarcode(indexSetName).indexSet
            else{
                Index index = Index.parseTubeIndexBarcode(indexSetName)
                handShakeId = index.indexSet
                sb.append("(rt.NAME like '%${index.plateLocation}' AND ")
                closeParanthesis = true
            }
        }
        handShakeIdToSetName[handShakeId] << indexSetName
        sb.append("rc.NAME like '$handShakeId%'")
        return closeParanthesis? sb.append(")").toString():sb.toString()
    }

    private Map<String, List<Index>> getIndexesForSets(List indexContainerBarcodes, int batchSize, Map<String, List<String>> handShakeIdToSetName){
        Map<String, List<Index>> indexSetIndexes = [:].withDefault {[]}
        List<String> indexSets = []
        String baseQuery = """select rc.NAME as INDEXSET, rt.NAME as INDEXNAME,  rtrim(replace(rt.metadata,'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>') as SEQUENCE
	    from CLARITYLIMS.REAGENTCATEGORY rc, CLARITYLIMS.REAGENTTYPE rt
	    where rc.REAGENTCATEGORYID = rt.REAGENTCATEGORYID
	    and rt.ISVISIBLE=1
	    and rt.SPECIALTYPE='Index'
	    and ("""
        StringBuilder query = null
        int i=0
        indexContainerBarcodes.each { String indexSetName ->
            if(i == 0)
                query = new StringBuilder().append(baseQuery)
            if(i > 0)
                query.append("OR ")
            query.append(getConstraints(indexSetName, handShakeIdToSetName))
            indexSets << indexSetName
            i++
            if(i > 0 && i % batchSize == 0){
                query.append(")")
                indexSetIndexes.putAll(executeQueryAndProcessResultSet(query.toString(), handShakeIdToSetName))
                indexSets.clear()
                i = 0
                query = null
            }
        }
        if(query) {
            query.append(")")
            indexSetIndexes.putAll(executeQueryAndProcessResultSet(query.toString(), handShakeIdToSetName))
        }
        return indexSetIndexes
    }

    /**
     * This method is meant to return index names, sequences and plate locations of all indexes given the index container barcode or the index set name.
     * This method splits the list into multiple lists of 10 to avoid oracle exception, Since each index plate has 96 indexes.
     * If the client to this method is certain that all the items in the list are tube barcodes, then the "isIndexPlate" flag can be set to false.
     * This can speed up the retrieval.
     * @param indexContainerBarcodes
     * @param isIndexPlate
     * @return
     */
    Map<String, List<Index>> getIndexes(List indexContainerBarcodes, boolean isIndexPlate = true){
        Map<String, List<Index>> indexSetIndexes
        Map<String, List<String>> handShakeIdToSetName = [:].withDefault {[]}
        int batchSize = isIndexPlate? 10 : BATCH_SIZE
        indexSetIndexes = getIndexesForSets(indexContainerBarcodes, batchSize, handShakeIdToSetName)
        List<String> remaining = []
        indexSetIndexes.remove(indexSetIndexes.findAll{it.value.size() == 0})
        remaining.addAll(indexContainerBarcodes-indexSetIndexes.keySet())
        if(remaining) {
            Map<String, String> indexSequences = getSequences(remaining)
            indexSequences.each { String indexName, String indexSequence ->
                indexSetIndexes[indexName] << new Index(indexSet: indexName, indexName: indexName, indexSequence: indexSequence)
            }
        }
        return indexSetIndexes
    }

    private Map executeQueryAndProcessResultSet(String query, Map handShakeIdToSetName) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        Map indexMap = jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                Map map = [:].withDefault {[]}
                while (rs.next()) {
                    Index index = new Index(indexSet: rs.getString('INDEXSET'), indexSequence: rs.getString('SEQUENCE'))
                    String name = rs.getString('INDEXNAME')
                    if(name.contains(":")){
                        String[] nameSplit = name?.split(":")
                        index.indexName = nameSplit[0]
                        index.plateLocation = "${nameSplit[1]}:${nameSplit[2]}"
                    }
                    else{
                        index.indexName = name
                    }
                    index.getKey(handShakeIdToSetName).each { String setName ->
                        map[setName] << index
                    }
                }
                return map
            }
        })
        return indexMap
    }

    /**
     * This method takes a list of index names and returns a map of index names to the corresponding sequence
     * @param indexNames
     * @return
     */
    Map<String, String> getSequences(Collection<String> indexNames) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        Map<String, String> indexNameSequences = [:]
        indexNames?.collate(BATCH_SIZE)?.each{List<String> names ->
            List<String> constraints = names.collect{"rt.NAME like '$it%'"}
            String query = """SELECT rt.NAME as INDEXNAME, rtrim(replace(rt.metadata,'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>') as SEQUENCE  
	FROM CLARITYLIMS.REAGENTTYPE rt
	WHERE rt.SPECIALTYPE='Index'
	AND ${constraints.join(' OR ')}"""
            ClarityWebTransaction.logger.debug "Executing query $query"
            Map subMap = jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
                @Override
                Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                    Map map = [:]
                    while (rs.next()) {
                        String indexName = rs.getString('INDEXNAME')
                        if(indexName.contains(":"))
                            indexName = indexName.split(":")[0]
                        if(indexNames.contains(indexName))
                            map[indexName] = rs.getString('SEQUENCE')
                    }
                    return map
                }
            })
            List<String> missingIndexes = names.minus(subMap.keySet())
            if(missingIndexes)
                throw new IllegalArgumentException("Index names $missingIndexes are not registered with Clarity.")
            indexNameSequences.putAll(subMap)
        }
        return indexNameSequences
    }

    /**
     * THis method takes a list of index sequences and returns a map of index sequences to the corresponding names
     * This is mainly written for Onboarding Sequencing files.
     * @param indexSequences
     * @return
     */
    Map<String, Index> getIndexForSequences(Collection<String> indexSequences){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        Map<String, String> indexSequenceNames = [:]
        indexSequences?.collate(BATCH_SIZE)?.each{List<String> sequences ->
            List<String> constraints = sequences.collect{"rt.METADATA like '%\$$it]]%'"}
            String query = """SELECT rc.NAME AS INDEXSET,  rt.NAME as INDEXNAME, rtrim(replace(rt.metadata,'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>') as SEQUENCE 
	FROM CLARITYLIMS.REAGENTTYPE rt, CLARITYLIMS.REAGENTCATEGORY rc
	WHERE rc.REAGENTCATEGORYID = rt.REAGENTCATEGORYID
	and rt.SPECIALTYPE='Index'
	AND (${constraints.join(' OR ')})"""
            ClarityWebTransaction.logger.debug "Executing query $query"
            Map subMap = jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
                @Override
                Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                    Map map = [:]
                    while (rs.next()) {
                        Index index = new Index()
                        String indexName = rs.getString('INDEXNAME')
                        if(indexName.contains(":")) {
                            def nameSplit = indexName.split(":")
                            indexName = nameSplit[0]
                            index.plateLocation = "${nameSplit[1]}:${nameSplit[2]}"
                        }
                        index.indexName = indexName
                        index.indexSequence = rs.getString('SEQUENCE')
                        index.indexSet = rs.getString('INDEXSET')
                        map[index.indexSequence] = indexName
                    }
                    return map
                }
            })
            List<String> missingIndexes = sequences.minus(subMap.keySet())
            if(missingIndexes)
                throw new IllegalArgumentException("Indexes $missingIndexes are not registered with Clarity.")
            indexSequenceNames.putAll(subMap)
        }
        return indexSequenceNames
    }

    Map<String, String> getDapSeqIndexMap() {
        Map<String, String> indexMap = [:]
        ClarityWebTransaction.logger.info "Starting to load DAP indexes...."
        List<Integer> indexSetIds = getIndexSetsByDataStoreId(512).collect {it.indexSetId}
        String basicQuery = """SELECT NAME as INDEXNAME, rtrim(replace(metadata,'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>') as SEQUENCE  
	                            FROM CLARITYLIMS.REAGENTTYPE
	                            WHERE SPECIALTYPE='Index'
	                            AND REAGENTCATEGORYID IN (${indexSetIds.join(',')})"""
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        jdbcTemplate.setFetchSize(1000)
        jdbcTemplate.query(basicQuery, new RowCallbackHandler() {
            @Override
            void processRow(ResultSet rs) throws SQLException {
                if(rs.first) {
                    indexMap[rs.getString("INDEXNAME")] = rs.getString("SEQUENCE")
                }
                while (rs.next()) {
                    String indexName = rs.getString("INDEXNAME")
                    String sequence = rs.getString("SEQUENCE")
                    indexMap[indexName] = sequence
                }
            }
        })
        ClarityWebTransaction.logger.info "All DAP indexes loaded...."
        return indexMap
    }

    /**
     * This returns a DropDownList object
     * This is mainly written for Generating Library creation sheet for Plate library creation
     * The dropdown is for Library index set and has only those elements that need to be displayed in the library creation sheet
     * @return
     */
    DropDownList getIndexSetDropDownList(){
        return new DropDownList(controlledVocabulary: visibleIndexSets.collect{it.indexSet}?.toArray())
    }

    List<Index> getVisibleIndexSets(){
        String query = """SELECT DATASTOREID, NAME FROM CLARITYLIMS.REAGENTCATEGORY WHERE ISVISIBLE = 2 """
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<Index> sets = []
                while (rs.next()) {
                    sets << new Index(datastoreId:(rs.getString('DATASTOREID') as int), indexSet: rs.getString('NAME'))
                }
                return sets
            }
        })
    }

    String getIndexSets(String indexName, Boolean activeOnly = true){
        String query = """SELECT rc.NAME as INDEXSETNAME FROM CLARITYLIMS.REAGENTTYPE rt, CLARITYLIMS.REAGENTCATEGORY rc
	WHERE rc.REAGENTCATEGORYID=rt.REAGENTCATEGORYID
	AND rt.SPECIALTYPE='Index'
	AND rt.NAME like '$indexName%' """
        if(activeOnly)
            query = query + "AND rt.ISVISIBLE = 1"
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                if (rs.next()) {
                    return rs.getString('INDEXSETNAME')
                }
                return ''
            }
        })
    }

    boolean isDualIndex(String indexName, String indexSequence=null){
        if(!indexSequence){
            indexSequence = getSequences([indexName])[indexName]
        }
        return indexSequence?.contains(INDEX_CONCAT_DELIMITER)
    }

    static boolean isDualIndexSequence(String indexSequence){
        return indexSequence?.contains(INDEX_CONCAT_DELIMITER)
    }

    static boolean isCompoundSequence(String indexSequence) {
        return indexSequence?.contains("+") && indexSequence?.contains("-")
    }

    static List<String> splitSequences(String indexSequence) {
        if(!isCompoundSequence(indexSequence))
            return [indexSequence]
        String i7 = indexSequence.substring(indexSequence.lastIndexOf("-")+1)
        String i5s = indexSequence.replaceAll("-$i7", "")
        String[] i5Split = i5s.split("\\+")
        List<String> allSequences = []
        i5Split.each {
            allSequences << "$it-$i7"
        }
        return allSequences
    }

    List<Index> getSingleIndexes() {
        String query = """SELECT rc.DATASTOREID as DATASTOREID, rc.NAME as INDEXSET, rt.NAME as INDEXNAME, rtrim(replace(rt.metadata,'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>') as SEQUENCE
	        FROM CLARITYLIMS.REAGENTCATEGORY rc, CLARITYLIMS.REAGENTTYPE rt 
	        WHERE rc.ISVISIBLE > 0 
	        and rt.SPECIALTYPE='Index'
	        and rc.REAGENTCATEGORYID=rt.REAGENTCATEGORYID
	        and rt.METADATA not like '%-%]]%'
	        and rc.NAME not like 'PacBio%'
	        order by INDEXSET"""
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<Index> sets = []
                while (rs.next()) {
                    Index index = new Index()
                    index.datastoreId = (rs.getString('DATASTOREID') as int)
                    index.indexSet = rs.getString('INDEXSET')
                    String name = rs.getString('INDEXNAME')
                    if(name.contains(":")){
                        String[] nameSplit = name?.split(":")
                        index.indexName = nameSplit[0]
                        index.plateLocation = "${nameSplit[1]}:${nameSplit[2]}"
                    }
                    else{
                        index.indexName = name
                    }
                    index.indexSequence = rs.getString('SEQUENCE')
                    sets << index
                }
                return sets
            }
        })
    }

    Map<String, List<Index>> getPacBioIndexes() {
        List<String> indexSets = getIndexSetsByDataStoreId(128)?.collect{it.indexSet}
        return getIndexes(indexSets)
    }

    List<Index> getTubeIndexes(){
        String query = """select rc.NAME as INDEXSET, rt.NAME as INDEXNAME,  rtrim(replace(rt.metadata,'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>') as SEQUENCE
	        from CLARITYLIMS.REAGENTCATEGORY rc, CLARITYLIMS.REAGENTTYPE rt
	        where rc.REAGENTCATEGORYID = rt.REAGENTCATEGORYID
	        and rt.ISVISIBLE=1
	        and rt.SPECIALTYPE='Index'
	        and rc.REAGENTCATEGORYID=rt.REAGENTCATEGORYID
	        and rt.METADATA not like '%-%]]%'
	        and rc.NAME not like 'PacBio%'
	        order by INDEXSET"""
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<Index> sets = []
                while (rs.next()) {
                    Index index = new Index()
                    index.datastoreId = (rs.getString('DATASTOREID') as int)
                    index.indexSet = rs.getString('INDEXSET')
                    String name = rs.getString('INDEXNAME')
                    if(name.contains(":")){
                        String[] nameSplit = name?.split(":")
                        index.indexName = nameSplit[0]
                        index.plateLocation = "${nameSplit[1]}:${nameSplit[2]}"
                    }
                    else{
                        index.indexName = name
                    }
                    index.indexSequence = rs.getString('SEQUENCE')
                    sets << index
                }
                return sets
            }
        })
    }

    Map<String, List<Index>> getScannableIndexSets(){
        String query = """SELECT distinct rc.NAME FROM REAGENTCATEGORY rc, REAGENTTYPE rt
	WHERE rc.ISVISIBLE = 1
	AND rt.SPECIALTYPE='Index'
	AND rc.REAGENTCATEGORYID=rt.REAGENTCATEGORYID
	AND LENGTH(TRIM(TRANSLATE(rc.NAME, ' +-.0123456789', ' '))) is null
	order by rc.NAME"""
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        List<String> indexSets = jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<String> sets = []
                while (rs.next()) {
                    sets << "${rs.getString('NAME')}"
                }
                return sets
            }
        })
        if(indexSets)
            return getIndexes(indexSets, true)
        return null
    }

    List<Index> getIndexSetsByDataStoreId(int datastoreid) {
        String query = """SELECT rc.DATASTOREID as DATASTOREID, rc.NAME as INDEXSET, rc.REAGENTCATEGORYID as SETID
	        FROM CLARITYLIMS.REAGENTCATEGORY rc
	        where bitand(datastoreid, $datastoreid)>1
	        order by INDEXSET"""
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<Index> sets = []
                while (rs.next()) {
                    Index index = new Index()
                    index.datastoreId = rs.getString('DATASTOREID') as int
                    index.indexSet = rs.getString('INDEXSET')
                    index.indexSetId = rs.getInt('SETID')
                    sets << index
                }
                return sets
            }
        })
    }

    static List<String> validatePoolIndexes (String poolId, Collection<String> indexes) {
        List<Integer> seqLengths = []
        List<Boolean> seqTypes = []
        List<String> sequences = []
        List<String> errors = []
        indexes.each { String indexSequence ->
            def splitIndexes = splitSequences(indexSequence)
            seqLengths << splitIndexes[0].length()
            sequences.addAll(splitIndexes)
            seqTypes << isDualIndexSequence(indexSequence)
        }
        if(seqLengths.unique().size() > 1)
            errors << "$poolId indexes are not of same length. \n$indexes"
        if(seqTypes.unique().size() > 1)
            errors << "${poolId} combines single and dual indexes together. \n$indexes"
        if(sequences.size() > sequences.unique().size())
            errors << "$poolId has duplicate indexes. \n$indexes."
        return errors
    }
}