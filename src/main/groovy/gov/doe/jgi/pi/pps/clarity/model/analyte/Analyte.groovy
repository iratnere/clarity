package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.ActionType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.NextAction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorklowStageStatus
import gov.doe.jgi.pi.pps.clarity_node_manager.util.OnDemandCache
import gov.doe.jgi.pi.pps.clarity_node_manager.util.OnDemandCacheMapped
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.apache.commons.lang.builder.HashCodeBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by dscott on 2/10/14.
 */
class Analyte implements Comparable<Analyte> {

    Integer index
    
	static final Logger logger = LoggerFactory.getLogger(Analyte.class.name)
	static final String PASS = 'Pass'
	static final String FAIL = 'Fail'
    static final String[] PASS_FAIL_LIST = [PASS, FAIL]
	static final String SMALL_RNA_QUEUE = 'smRNA'
    static final String REAGENT_LABEL_PREFIX = 'SOW Item Id - '
    static final String INDEX_NAME_NA = 'N/A'

	ArtifactNodeInterface artifactNodeInterface
	protected OnDemandCacheMapped<ProcessType, List<ProcessNode>> cachedProcessesExecutedOn = new OnDemandCacheMapped<>()
	protected OnDemandCache<ProcessType> cachedParentProcessType = new OnDemandCache<>()
    protected OnDemandCache<ClaritySample> cachedClaritySample = new OnDemandCache<ClaritySample>()

	private def isItagTransient
	private def isExomeCaptureTransient
	private def isPacBioTransient
	private def isIlluminaTransient
	private def isSmallRnaTransient
    private def isSingleCellTransient
    private def isDapSeqTransient

    Analyte() {}

	Analyte(ArtifactNodeInterface artifactNodeInterface) {
        this.artifactNodeInterface = artifactNodeInterface
    }

    NextAction getNextAction(ActionType actionType, String stepUri = null){
        return new NextAction(action: actionType,artifactUri: artifactNodeInterface.uri,stepUri: stepUri)
    }

    ArtifactNodeInterface getArtifactNode() {
        artifactNodeInterface
    }

    void validate() {}

    ArtifactWorkflowStage artifactWorkflowStageInProgress(ProcessNode currentProcess, boolean staticMode = true) {
        artifactNodeInterface.getStageInProgress(currentProcess.stepsNode, staticMode)
    }

    Stage stageInProgress(ProcessNode currentProcess, boolean staticMode = true) {
        ArtifactWorkflowStage artifactWorkflowStage = artifactWorkflowStageInProgress(currentProcess,staticMode)
        if (!artifactWorkflowStage) {
            return null
        }
        Stage.artifactWorkflowStageToStage(artifactWorkflowStage)
    }


    Stage requireStageInProgress(ProcessNode currentProcess, boolean staticMode = true) {
        Stage stage = stageInProgress(currentProcess,staticMode)
        if (!stage) {
            throw new WebException("${this}: no stage in progress found for current process ${currentProcess}",422)
        }
        stage
    }

    Set<String> getSampleIds() {
        Set<String> ids = []
        artifactNodeInterface.sampleIds?.each{ids << it}
        ids
    }

    Analyte getParentAnalyte() {
        ArtifactNode parentArtifactNode = artifactNodeInterface.singleParentAnalyte
        if (parentArtifactNode) {
            return AnalyteFactory.analyteInstance(parentArtifactNode)
        }
        return null
    }

    List<Analyte> getParentAnalytes() {
        artifactNodeInterface.parentAnalytes?.collect{ AnalyteFactory.analyteInstance(it)}?.toList() ?: []
    }

    List<Analyte> ancestorsForProcessTypes(Collection<ProcessType> processTypes) {
        //List<ArtifactNode> parentLibraryNodes = artifactNode.findAncestorsForProcessTypes([ProcessType.LC_LIBRARY_CREATION.value,ProcessType.LP_POOL_CREATION.value])
        List<ArtifactNode> parentArtifactNodes = artifactNodeInterface.findAncestorsForProcessTypes(processTypes*.value)?.findAll()?.unique()
        if (parentArtifactNodes) {
            return parentArtifactNodes.collect{ AnalyteFactory.analyteInstance(it)}.toList()
        }
        return []
    }

    void setNewRunMode(String runMode) {
        if (isSupportedUdf(ClarityUdf.ANALYTE_RUN_MODE)) {
            artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_RUN_MODE.udf,runMode)
        }
    }

    String getUdfRunMode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_RUN_MODE.udf)
    }

    RunModeCv getRunModeCv() {
        String runModeName = udfRunMode
        if (runModeName) {
            return RunModeCv.findByRunMode(runModeName)
        }
        return null
    }

    boolean isSupportedUdf(ClarityUdf clarityUdf) {
        try {
            if (clarityUdf)
                return artifactNodeInterface.parentProcessNode?.processTypeNode?.outputAnalyteUdfSupported(clarityUdf.udf)
            return false
        } catch (Exception e) {
            ClarityWebTransaction.logger.warn "Analyte<$id>, ClarityUdf<$clarityUdf.udf>  isSupportedUdf exception: ${e?.message}"
            return false
        }
    }

    ProcessType getParentProcessType() {
        return cachedParentProcessType.fetch { ProcessType.toEnum(artifactNodeInterface.getParentProcessType()) }
    }

    boolean getIsSample() {
        return artifactNodeInterface.isSample
    }

    void validateIsSample() {
        if (!isSample) {
            throw new WebException([code:'analyte.sampleRequired', args:[this]],422)
        }
    }

    boolean getIsPmoSample(){
        return (isSample && claritySample instanceof PmoSample)
    }

    boolean getIsDerivedSample(){
        return (claritySample instanceof ScheduledSample)
    }

    void validateIsDerivedSample() {
        if (!isDerivedSample) {
            throw new WebException([code:"Invalid input analyte ${this.id}. Expecting Derived Sample as input.", args:[this, this.id]], 422)
        }
    }

    boolean getIsScheduledSample(){
        return (isSample && claritySample instanceof ScheduledSample)
    }

    void validateIsScheduledSample() {
        if (!isScheduledSample) {
            throw new WebException([code:'Invalid input analyte. Expecting Scheduled Sample as input.', args:[this, this.id]], 422)
        }
    }

    boolean getIsInProgressStages(){
        return (artifactNodeInterface.stagesInProgress?.size() > 0)
    }

    boolean validateIsInQueuedStages() {
        //Analytes is in a queue (workflow-stage status="QUEUED")
        List<ArtifactWorkflowStage> activeStages = artifactNodeInterface.getActiveStages()
        if (!activeStages) {
            throw new WebException([code:"Analyte is not in a queue.", args:[this, this.id]], 422)
        }
        if (activeStages.find{ ArtifactWorklowStageStatus.IN_PROGRESS == it.status }) {
            throw new WebException([code: "Analyte is part of active/started processes. ", args: [this, this.id]], 422)
        }
        return true
    }

    void validateParentProcessType(ProcessType processType) {
        if (parentProcessType != processType) {
            throw new WebException([code:'analyte.parentProcessType.invalid', args:[this, parentProcessType, processType]], 422)
        }
    }

    void validateParentProcessType(Collection<ProcessType> processTypes) {
        if (!(parentProcessType in processTypes)) {
            throw new WebException([code:'analyte.parentProcessType.invalid', args:[this, parentProcessType, processTypes?.collect{it.value}?.join(',')]], 422)
        }
    }

    String getId() {
        return artifactNodeInterface.id
    }

    String getContainerId() {
        return artifactNodeInterface.containerId
    }

	String getContainerName(){
		return artifactNodeInterface.containerName
	}

    String getContainerType(){
        return artifactNodeInterface.containerNode?.containerType
    }

    void setName(String name) {
		artifactNodeInterface.name = name
	}

	String getName() {
        return artifactNodeInterface.name
    }

    Set<SampleNode> getParentSampleNodes() {
        return artifactNodeInterface.parentSampleNodes
    }


    Set<ClaritySample> parentSamples() {
        parentSampleNodes.collect { SampleNode sampleNode ->
            SampleFactory.sampleInstance(sampleNode)
        }.findAll().toSet()
    }

    Set<ScheduledSample> getParentScheduledSamples() {
        parentSamples().findAll{it instanceof ScheduledSample} as Set<ScheduledSample>
    }

    Set<String> getParentScheduledSampleIds() {
        parentScheduledSamples*.id?.toSet()
    }

    Set<PmoSample> getParentPmoSamples() {
        parentSamples().collect{ ClaritySample claritySample -> claritySample.pmoSample}.findAll().toSet()
    }

    Set<Long> getParentPmoSampleIds() {
        parentPmoSamples*.pmoSampleId?.toSet()
    }

	SampleNode getSampleNode() {
		return artifactNodeInterface.sampleNode
	}

	boolean getIsOnPlate() {
        if (!artifactNodeInterface.containerNode) {
            throw new WebException([code:'analyte.containerNode.notFound', args:[this]], 422)
        }
        return artifactNodeInterface.containerNode.isPlate
	}

	boolean getIsItag() {
		if (isItagTransient != null)
			return isItagTransient
		isItagTransient = claritySample.isItag
		return isItagTransient
	}

	boolean getIsExomeCapture() {
		if (isExomeCaptureTransient != null)
			return isExomeCaptureTransient
		isExomeCaptureTransient = claritySample.isExomeCapture
		return isExomeCaptureTransient
	}

	boolean getIsPacBio() {
		if (isPacBioTransient != null)
			return isPacBioTransient
		isPacBioTransient = claritySample.isPacBio
		return isPacBioTransient
	}

    boolean getIsPacBioSequel() {
        if (claritySample instanceof ScheduledSample) {
            return claritySample.isPacBioSequel
        }
    }

    boolean getIsIllumina() {
		if (isIlluminaTransient != null)
			return isIlluminaTransient
		isIlluminaTransient = claritySample.isIllumina
		return isIlluminaTransient
	}

    boolean getIsSmallRna() {
		if (isSmallRnaTransient != null)
			return isSmallRnaTransient
		isSmallRnaTransient = claritySample.isSmallRna
		return isSmallRnaTransient
	}

    boolean getIsInternalSingleCell() {
        if (isSingleCellTransient != null)
            return isSingleCellTransient
        isSingleCellTransient = claritySample.isInternalSingleCell
        return isSingleCellTransient
    }

    boolean getIsDapSeq() {
        if (isDapSeqTransient != null)
            return isDapSeqTransient
        isDapSeqTransient = claritySample.isDapSeq
        return isDapSeqTransient
    }

    BigDecimal getUdfFractionDensity() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_DENSITY_G_ML.udf)
    }

    void setUdfFractionDensity(BigDecimal value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_DENSITY_G_ML.udf, value)
    }

    String getUdfDestinationBarcode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_DESTINATION_BARCODE.udf)
    }

    void setUdfDestinationBarcode(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_DESTINATION_BARCODE.udf, value)
    }

    public BigDecimal getUdfConcentrationNgUl() {
		return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_CONCENTRATION_NG_UL.udf)
	}

	public void setUdfConcentrationNgUl(BigDecimal concentration) {
		artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_CONCENTRATION_NG_UL.udf, concentration)
	}

	public BigDecimal getUdfVolumeUl() {
		return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_VOLUME_UL.udf)
	}

    public void setUdfVolumeUl(BigDecimal volume) {
		artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, volume)
	}

	BigDecimal getMassNg() {
		BigDecimal concentrationNgUl = getUdfConcentrationNgUl()
		BigDecimal volumeUl = getUdfVolumeUl()
		if (concentrationNgUl != null && volumeUl != null)
			return concentrationNgUl.multiply(volumeUl) //.setScale(2, RoundingMode.HALF_UP)
		return null
	}

    ClaritySample getClaritySample() {
        return cachedClaritySample.fetch {
            //logger.info "${this} fetching clarity sample"
            ClaritySample claritySample = SampleFactory.sampleInstance(artifactNodeInterface.sampleNode)
            claritySample
        }
    }

    List<ClaritySample> getClaritySamples() {
        NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
        return artifactNodeInterface.sampleIds.collect{ SampleFactory.sampleInstance(nodeManager.getSampleNode(it)) }
    }

    String getContainerUdfNotes() {
        return containerNode.getUdfAsString(ClarityUdf.CONTAINER_NOTES.udf)
    }

    void setContainerUdfNotes(String containerUdfNotes) {
        containerNode.setUdf(ClarityUdf.CONTAINER_NOTES.udf, containerUdfNotes)
    }

    String getContainerUdfLibraryQcFailureMode() {
        return containerNode.getUdfAsString(ClarityUdf.CONTAINER_LIBRARY_QC_FAILURE_MODE.udf)
    }

    void setContainerUdfLibraryQcFailureMode(String containerUdfLibraryQcFailureMode) {
        containerNode.setUdf(ClarityUdf.CONTAINER_LIBRARY_QC_FAILURE_MODE.udf, containerUdfLibraryQcFailureMode)
    }

    String getContainerUdfLibraryQcResult() {
        return containerNode.getUdfAsString(ClarityUdf.CONTAINER_LIBRARY_QC_RESULT.udf)
    }

    void setContainerUdfLibraryQcResult(String containerUdfLibraryQcResult) {
        containerNode.setUdf(ClarityUdf.CONTAINER_LIBRARY_QC_RESULT.udf, containerUdfLibraryQcResult)
    }

    void setContainerUdfFinalAliquotVolumeUl(BigDecimal containerUdfFinalAliquotVolumeUl) {
        containerNode.setUdf(ClarityUdf.CONTAINER_FINAL_ALIQUOT_VOLUME_UL.udf, containerUdfFinalAliquotVolumeUl)
    }

    BigDecimal getContainerUdfFinalAliquotVolumeUl() {
        return containerNode.getUdfAsBigDecimal(ClarityUdf.CONTAINER_FINAL_ALIQUOT_VOLUME_UL.udf)
    }

    void setContainerUdfFinalAliquotMassNg(BigDecimal containerUdfFinalAliquotMassNg) {
        containerNode.setUdf(ClarityUdf.CONTAINER_FINAL_ALIQUOT_MASS_NG.udf, containerUdfFinalAliquotMassNg)
    }

    BigDecimal getContainerUdfFinalAliquotMassNg() {
        return containerNode.getUdfAsBigDecimal(ClarityUdf.CONTAINER_FINAL_ALIQUOT_MASS_NG.udf)
    }

    void setContainerUdfLabel(String containerUdfLabel) {
        containerNode.setUdf(ClarityUdf.CONTAINER_LABEL.udf, containerUdfLabel)
    }

    String getContainerUdfLabel(){
        containerNode.getUdfAsString(ClarityUdf.CONTAINER_LABEL.udf)
    }

//some test
	String toString() {
		"${this.class.simpleName}(${this.id})"
	}

	List<ProcessNode> processesExecutedOn(ProcessType processType) {
		List<ProcessNode> processes = []
		processes += cachedProcessesExecutedOn.fetch(processType) { ProcessType pType ->
			NodeManager nodeManager = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager()
			nodeManager.lookupProcessNodesByInputArtifactIdAndProcessTypeName(artifactNodeInterface.id, pType.value)
		}
		return processes //return a copy of the cached list
	}

	List<ProcessNode> requireProcessesExecutedOn(ProcessType processType) {
		List<ProcessNode> processes = processesExecutedOn(processType)
		if (!processes) {
			throw new WebException([code:'analyte.executedProcess.notFound', args:[this.toString(), processType.value]], 422)
		}
		return null
	}
	
	ProcessNode lastProcessExecutedOn(ProcessType processType) {
		List<ProcessNode> processes = processesExecutedOn(processType)
		if (processes) {
			return processes[-1]
		}
		return null
	}
	
	ProcessNode requireLastProcessExecutedOn(ProcessType processType) {
		return requireProcessesExecutedOn(processType)[-1]
	}

    ContainerNode getContainerNode() {
        return artifactNodeInterface.containerNode
    }

    ContainerLocation getContainerLocation() {
        return artifactNodeInterface.containerLocation
    }

    //https://docs.google.com/document/d/12nMDl5KBM7R5tkHmQ5yQyGQkK1Q_jXhMLj0iT28ezPM/edit
    String getPrintLabelText() {
        throw new WebException([code:'Analyte.getPrintLabelText().notImplemented', args:[this.toString()]], 422)
    }

    LibraryCreationQueueCv getLibraryCreationQueue() {
        throw new WebException([code:'Analyte.getLibraryCreationQueue().notImplemented', args:[this.toString()]], 422)
    }

    String getSmrtbellTemplatePrepKit() {
        throw new WebException([code:'Analyte.getSmrtbellTemplatePrepKit().notImplemented', args:[this.toString()]], 422)
    }

    void setUdfLpActualWithSof(BigDecimal value) {
        throw new WebException([code:'Analyte.setUdfLpActualWithSof().notImplemented', args:[this.toString()]], 422)
    }

    BigInteger getReadLengthBp() {
        throw new WebException([code:'Analyte.getReadLengthBp().notImplemented', args:[this.toString()]], 422)
    }

    SequencerModelCv getSequencerModelCv() {
        throw new WebException([code:'Analyte.getReadLengthBp().notImplemented', args:[this.toString()]], 422)
    }

    BigDecimal getUdfLpActualWithSof() {
        throw new WebException([code:'Analyte.getUdfLpActualWithSof().notImplemented', args:[this.toString()]], 422)
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false
        Analyte analyte = (Analyte) o
        if (artifactNodeInterface != analyte.artifactNodeInterface) return false
        return true
    }

    int hashCode() {
        return artifactNodeInterface.hashCode()
    }

    @Override
    int compareTo(Analyte o) {
        if (!o) {
            return -1
        }
        if (this.is(o)) {
            return 0
        }
        int result = this.class.simpleName.compareTo(o.class.simpleName)
        if (result) {return result}
        return this.id.compareTo(o.id)
    }

    List<ClarityWorkflow> getActiveWorkflows(){
        List<ClarityWorkflow> workflows = []
        artifactNodeInterface.activeStages.each{ ArtifactWorkflowStage workflowStage ->
            workflows << ClarityWorkflow.toEnum(workflowStage.workflowNode.name)
        }
        workflows
    }

    List<ClarityWorkflow> getInProgressWorkflows(){
        List<ClarityWorkflow> workflows = []
        artifactNodeInterface.stagesInProgress.each{ ArtifactWorkflowStage workflowStage ->
            workflows << ClarityWorkflow.toEnum(workflowStage.workflowNode.name)
        }
        return workflows
    }


    void setReagentLabel(def sowItemId){
        artifactNodeInterface.setReagentLabels(REAGENT_LABEL_PREFIX + sowItemId)
    }

    String getReagentLabel(){
        String reagentLabel = artifactNodeInterface.reagentLabels?.find{it?.startsWith(REAGENT_LABEL_PREFIX)}
        if (reagentLabel) {
            return (reagentLabel - REAGENT_LABEL_PREFIX)
        }
        return null
    }

    def getReagentLabels(){
        return artifactNodeInterface.reagentLabels?.collect{(it - REAGENT_LABEL_PREFIX)}
    }

    void setFailureMode(String failureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_FAILURE_MODE.udf, failureMode)
    }

    String getFailureMode(){
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_FAILURE_MODE.udf)
    }

    void setClusterGenFailureMode(String clusterGenFailureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_CLUSTER_GEN_FAILURE_MODE.udf, clusterGenFailureMode)
    }

    String getClusterGenFailureMode(){
        artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_CLUSTER_GEN_FAILURE_MODE.udf)
    }

    BigDecimal getUdfVolumeUsedUl() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_VOLUME_USED_UL.udf)
    }

    void setUdfVolumeUsedUl(BigDecimal volumeUsed) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_VOLUME_USED_UL.udf, volumeUsed)
    }

    String getUdfNotes() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_NOTES.udf)
    }

    void setUdfNotes(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_NOTES.udf, value)
    }

    String getUdfExternal() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_EXTERNAL.udf)
    }

    void setUdfExternal(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_EXTERNAL.udf, value)
    }

    Boolean getSystemQcFlag() {
        return artifactNodeInterface.getSystemQcFlag()
    }

    void setSystemQcFlag(Boolean systemQcFlag) {
        artifactNodeInterface.systemQcFlag = systemQcFlag
    }

    List<RoutingRequest> getUnassignFromActiveStagesRequests() {
        List<RoutingRequest> routingRequests = []
        artifactNodeInterface.activeStages.collect{ ArtifactWorkflowStage stage ->
            new RoutingRequest(routingUri: stage.uri,artifactId: artifactNodeInterface.id,action: Routing.Action.unassign)
        }
        routingRequests
    }

    ArtifactWorkflowStage activeStage(ProcessNode processNode) {
        artifactNodeInterface.activeStage(processNode)
    }

    ArtifactWorkflowStage activeStage(StepsNode currentStep) {
        artifactNodeInterface.activeStage(currentStep)
    }

    boolean inActiveWorkflow(Collection<ClarityWorkflow> workflows) {
        Set workflowSet = new HashSet<ClarityWorkflow>(workflows)
        for (ClarityWorkflow activeWorkflow : activeWorkflows) {
            if (workflowSet.contains(activeWorkflow)) {
                return true
            }
        }
        return false
    }

    boolean inProgressWorkflow(Collection<ClarityWorkflow> workflows) {
        Set workflowSet = new HashSet<ClarityWorkflow>(workflows)
        for (ClarityWorkflow activeWorkflow : inProgressWorkflows) {
            if (workflowSet.contains(activeWorkflow)) {
                return true
            }
        }
        return false
    }

    def getHashCode(){
        return new HashCodeBuilder(17, 37).
                append(claritySample.sequencingProject.udfSampleContactId).
                append(claritySample.sequencingProject.udfSequencingProjectManagerId).
                append(claritySample.pmoSampleId).
                toHashCode()
    }

    Analyte getSingleParent(){
        AnalyteFactory.analyteInstance(artifactNodeInterface.singleParentAnalyte)
    }

    List<Analyte> getParents(){
        artifactNodeInterface.parentAnalytes.collect{ AnalyteFactory.analyteInstance(it)}
    }

    def validateIsLibraryInput() {
        throw new WebException("""
$this Invalid input analyte.
Input must be library stock, pool, pool of pools.
""",
                422
        )
    }

    Analyte getParentLibrary() {
        return null // not null for a child of PacBio library creation or PacBio pooling processes
    }

    List<String> getLibraryStockIds(){
        return null
    }
}
