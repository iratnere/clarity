package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation


import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPoolLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.ActionType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.NextAction

/**
 * This class is meant to read the pool creation sheet, update pool Udfs
 * Move PASSED pools to appropriate sequencing step and FAILED ones to Abandoned queue
 * for requirements refer 4d and 5 of the Clarity Library Pooling Requirements document
 * https://docs.google.com/a/lbl.gov/document/d/1itCb6lXyP_IM0Fl-o-8tMDdsVq-WgdmekOmN53JMfg4/edit#
 *
 * Created of the ticket PPS-1769
 * Created by lvishwas on 12/18/2014.
 */
class LpProcessPoolCreationSheet extends ActionHandler {
    def pools
    List<NextAction> poolsNextActions = []

    void execute() {
        ClarityWebTransaction.logger.info "Starting ${this.class.name} action...."
        validatePoolUdfs()
        pools = (process as PoolCreation).getPoolCreationMemberBeans(PoolCreation.UPLOADED_POOL_CREATION_SHEET)
        performLastStepActions()
        process.setArtifactActions(poolsNextActions)
    }

    void performLastStepActions(){
        pools.each{poolName, poolMembers ->
            ClarityLibraryPool pool = process.getOutputAnalytes(poolMembers.first().analyte).first() as ClarityLibraryPool
            ClarityWebTransaction.logger.info "Output Pool with poolname ${pool.name}"
            pool.artifactNode.httpRefresh()
            updatePoolUdfs(pool, poolMembers)
            poolsNextActions << pool.getNextAction(ActionType.COMPLETE)
        }
        process.nodeManager.httpPutDirtyNodes()
    }

    void validatePoolUdfs(List<ClarityLibraryPool> outputAnalytes = process.outputAnalytes){
        outputAnalytes.each { ClarityLibraryPool pool ->
            pool.validateVolumeUl()
            pool.validateActualTemplateSizeBp()
        }
    }

    void updatePoolUdfs(ClarityLibraryPool pool, def poolMemberBeans){
        ClarityWebTransaction.logger.info "Updating udfs for pool ${pool}"
        PoolCreationTableBean bean = poolMemberBeans.first()
        if(!bean.poolSize)
            throw new RuntimeException("undefined pool size for pool id '$pool.id'")
        pool.udfVolumeUl = pool.udfVolumeUl ?:bean.poolSize * ClarityLibraryPool.DEFAULT_VOLUME_UL
        pool.name = bean.poolName
        if(pool.isPacBio){
            //PPS-4497 - This field needs to be filled in for pacBio pools and should be saved to concentration udf
            if(!bean.pacBioPoolConcentration)
                throw new RuntimeException("undefined 'PacBio Pool Concentration ng/ul' for pool id '${bean.poolName}'")
            pool.udfConcentrationNgUl = bean.pacBioPoolConcentration
        }
        else {
            if (!bean.poolConcentrationpM)
                throw new RuntimeException("undefined 'Pool Concentration pM' for pool id '${bean.poolName}'")
            pool.udfLibraryMolarityPm = bean.poolConcentrationpM
        }
        def dop = pool.poolMembers.size()
        if(pool  instanceof ClarityPoolLibraryPool){
            dop = 0
            pool.poolMembers.each{ Analyte memberPool ->
                dop += memberPool.libraryStockIds.size()
            }
        }
        pool.udfDegreeOfPooling = dop
        pool.udfLibraryCreator = process.researcher?.fullName
//        pool.udfConcentrationNgUl = bean.poolConcentrationpM
        pool.containerUdfLibraryQcResult = bean.poolLabProcessResult.value
        pool.containerUdfLibraryQcFailureMode = bean.poolLabProcessFailureMode.value
        pool.udfLibraryQcResult = bean.poolLabProcessResult.value
        pool.systemQcFlag = bean.poolLabProcessResult.value == Analyte.PASS
        pool.udfLibraryQcFailureMode = bean.poolLabProcessFailureMode.value
        pool.udfNotes = bean.notification
    }
}
