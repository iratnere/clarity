package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

/**
 * This enum is defined to define the restricted values for the cell type 
 * 
 * @author lvishwas
 */
enum CellTypeEnum {
	STRING,
	NUMBER,//used for all numeric values int, float, double........
	DATE,
	FORMULA,
	DROPDOWN,
	FORMULA_NOT_IMPLEMENTED
}
