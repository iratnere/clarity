package gov.doe.jgi.pi.pps.clarity.scripts.util

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.grails.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.StepConfigurationNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.step.StepField
import gov.doe.jgi.pi.pps.clarity_node_manager.node.step.StepFieldAttachTo
import gov.doe.jgi.pi.pps.clarity_node_manager.util.*

class StageUtility {
    private static String getProtocolId(Node stageNode){
        String protocolUri = stageNode.'protocol'.'@uri'[0]
        return protocolUri.split('/')[-1] as Integer
    }

    private static String getStepId(Node stageNode){
        String stepUri = stageNode.'step'.'@uri'[0]
        return stepUri.split('/')[-1] as Integer
    }

    static StepConfigurationNode getStepConfigurationNode(NodeManager nodeManager, Stage stage){
        Node node = HttpUtils.httpGetAsNode(nodeManager.nodeConfig, stage.uri)
        return nodeManager.getStepConfigurationNode(getProtocolId(node), getStepId(node))
    }

    static List<UserDefinedField> getProcessUdfs(Stage stage, NodeManager nodeManager){
        StepConfigurationNode stepConfigurationNode = getStepConfigurationNode(nodeManager, stage)
        return getProcessUdfs(stepConfigurationNode)
    }

    static List<UserDefinedField> getProcessUdfs(StepConfigurationNode stepConfigurationNode){
        List<StepField> stepFields = stepConfigurationNode.stepFields.findAll {
            it.attachTo in [StepFieldAttachTo.ConfiguredProcess, StepFieldAttachTo.TransferProcess]
        }
        return stepFields.collect{ findUserDefinedField(it.name, stepConfigurationNode.processTypeName) }
    }

    static UserDefinedField findUserDefinedField(String udfName, String processTypeName) {
        def clarityUdf = ClarityUdf.toEnum(udfName, UdfAttachedTo.PROCESS)
        if (clarityUdf)
            return clarityUdf.udf
        def udfType = findUdfType(udfName, processTypeName)
        UdfFieldTypes udfFieldType = UdfFieldTypes.values().find {it.value == udfType}
        UserDefinedField userDefinedField = new UserDefinedField(
                name: udfName,
                attachedTo: UdfAttachedTo.PROCESS,
                type: udfFieldType,
                units: null
        )
        return userDefinedField
    }

    static String findUdfType(String udfName, String processTypeName) {
        NodeConfig clarityNodeConfig = ClarityWebTransaction.requireCurrentTransaction().requireNodeManager().nodeConfig
        //http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/544
        List<String> udfUris = ArtifactNodeService.getUdfUris(clarityNodeConfig, udfName, processTypeName)
        assert udfUris?.size() == 1
        def type = new XmlSlurper().parseText(HttpUtils.httpGet(clarityNodeConfig, udfUris[0])).@type
        return type
    }
}
