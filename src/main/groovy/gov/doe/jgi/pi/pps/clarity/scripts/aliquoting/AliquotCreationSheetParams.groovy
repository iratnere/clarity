package gov.doe.jgi.pi.pps.clarity.scripts.aliquoting

import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

class AliquotCreationSheetParams {
    List<Analyte> analytes
    LibraryCreationQueueCv plateLcQueueCached
    List<FreezerContainer> freezerContainers
    Researcher researcher
    DropDownList dropDownFailureModes
    DropDownList dropDownPassFail

    AliquotCreationSheetParams(ClarityProcess clarityProcess, List<Analyte> analytes=null) {
        this.analytes = analytes!=null?analytes:clarityProcess.outputAnalytes
        this.researcher = clarityProcess.researcher
        this.freezerContainers = clarityProcess.freezerContainers
        this.dropDownPassFail = clarityProcess.statusCv
        this.dropDownFailureModes = clarityProcess.failureModeCv
    }

    ContainerNode getOutputPlateContainer() {
        ContainerNode outputContainerNode = analytes[0]?.containerNode
        if (outputContainerNode?.isNinetySixWellPlate)
            return outputContainerNode
        return null
    }

    LibraryCreationQueueCv getPlateLcQueue() {
        if (plateLcQueueCached) {
            return plateLcQueueCached
        }
        if (!outputPlateContainer) {
            return null
        }
        if(analytes[0] instanceof SampleQcHamiltonAnalyte)
            plateLcQueueCached = analytes[0].claritySample.pmoSample.scheduledSamples[0].libraryCreationQueue
        else
            plateLcQueueCached = analytes[0].libraryCreationQueue
        plateLcQueueCached
    }
}
