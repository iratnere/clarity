package gov.doe.jgi.pi.pps.clarity.dto
/**
 * Created by dscott on 3/4/2016.
 */
class ContainerDto extends GeneusNodeDto {

    String type

    private Map<String, LocationDto> locations = [:]

    protected addLocation(LocationDto locationDto) {
        locations[locationDto.key] = locationDto
    }

    List<LocationDto> getLocations() {
        locations.values().sort()
    }

    List<ArtifactDto> getArtifacts() {
        getLocations().collect { LocationDto locationDto ->
            locationDto.artifact
        }
    }

    protected ContainerDto() {}

    LinkedList<String> getAllUdfNames() {
        Set<String> udfNameSet = []
        udfNames?.each {
            udfNameSet << it
        }
        newUdfNames?.each {
            udfNameSet << it
        }
        artifacts?.each { ArtifactDto artifactDto ->
            artifactDto.udfNames?.each {
                udfNameSet << it
            }
            artifactDto.newUdfNames?.each {
                udfNameSet << it
            }
            artifactDto.sample?.udfNames?.each {
                udfNameSet << it
            }
            artifactDto.sample?.newUdfNames?.each {
                udfNameSet << it
            }
        }
        LinkedList<String> nameList = new LinkedList<>()
        nameList.addAll(udfNameSet.sort(){it.trim().toLowerCase()})
        nameList
    }

}
