package gov.doe.jgi.pi.pps.clarity.scripts.excel_framework

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference

import java.util.logging.Logger

/**
 * This section enables us to populate cells of a one dimensional section
 * either vertical or horizontal
 *  one has to specify the data type the section holds 
 *  store: One has to populate a list of values that the section has to print into the excel file
 *  load: this section reads the excel file and populates the list with the values
 *  
 * @author lvishwas
 */
class OneDSection extends Section{
	private def values
	private boolean isColumnData
	private CellTypeEnum cellType
	static final Logger logger = Logger.getLogger(OneDSection.class.name)

	OneDSection(int parentSheetIndex, CellTypeEnum dataType, String startAddress, String endAddress){
		super(parentSheetIndex,null,startAddress,endAddress)
		if(!validate1DSection())
			throw new IllegalArgumentException('Section between ${startAddress} and ${endAddress} is not a 1-D Section')
		cellType = dataType
	}

	OneDSection(int parentSheetIndex, CellTypeEnum dataType, CellRangeAddress sectionRangeAddress){
		super(parentSheetIndex,null,sectionRangeAddress)
		if(!validate1DSection())
			throw new IllegalArgumentException('Section in ${sectionRangeAddress} is not a 1-D Section')
		cellType = dataType
	}

	OneDSection(OneDSection srcSection, int parentSheetIndex, CellTypeEnum dataType, String startAddress){
		super(srcSection,parentSheetIndex,null,startAddress)
		calculateLastCell(srcSection)
		cellType = dataType
	}

	OneDSection(int parentSheetIndex, CellTypeEnum dataType, String startAddress, boolean isColumnData){
		super(parentSheetIndex,null,startAddress, null)
		cellType = dataType
		this.isColumnData = isColumnData
	}

	void calculateLastCell(OneDSection srcSection){
		if(srcSection.startCell.row == srcSection.lastCell.row){
			int diff = srcSection.lastCell.col - srcSection.startCell.col
			lastCell = new CellReference(startCell.row, startCell.col + diff)
		}
		if(srcSection.startCell.col == srcSection.lastCell.col){
			int diff = srcSection.lastCell.row - srcSection.startCell.row
			lastCell = new CellReference(startCell.row + diff, startCell.col)
		}
	}

	boolean validate1DSection(){
		if(startCell.row == lastCell.row){
			isColumnData = false
			return true
		}
		if(startCell.col == lastCell.col){
			isColumnData = true
			return true
		}
		return false
	}

	@Override
	void store() {
		logger.fine "Writing section: ${this.sectionKey}"
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		int currentColumn = startCell.col
		int currentRow = startCell.row
		values.each{value ->
			Row row = sheet.getRow(currentRow)?sheet.getRow(currentRow):sheet.createRow(currentRow)
			Cell cell = row.getCell(currentColumn)?row.getCell(currentColumn):row.createCell(currentColumn)
			writeCell(cell, cellType,value)
			isColumnData?currentRow++:currentColumn++
		}
	}

	@Override
	void load(boolean validate=true) {
		logger.fine "Reading section: ${this.sectionKey}"
		Sheet sheet = workbook.getSheet(parentSheetIndex)
		int currentColumn = startCell.col
		int currentRow = startCell.row
		if(!lastCell){
			if(isColumnData)
				lastCell = new CellReference(sheet.lastRowNum, startCell.col)
			else{
				Row row = sheet.getRow(startCell.row)
				lastCell = new CellReference(startCell.row, row.lastCellNum)
			}
		}
		values = []
		while(currentColumn <= lastCell.col && currentRow <= lastCell.row){
			Row row = sheet.getRow(currentRow)
			Cell cell = row.getCell(currentColumn)
			values << readCell(cell, cellType)
			isColumnData?currentRow++:currentColumn++
		}
	}
	
	void setData(def data){
		if(!data instanceof Collection)
			throw new IllegalArgumentException("Expecting collection of objects")
		values = data
	}
	
	String getSectionConfig(String sectionDelim, String fieldDelim){
		StringBuilder sb = new StringBuilder()
		sb.append(sectionDelim)
		sb.append(this.class.name).append(fieldDelim)
		sb.append(fieldDelim).append(this.cellType.name()).append(fieldDelim)
		sb.append(parentSheetIndex).append(fieldDelim)
		sb.append(startCell.formatAsString()).append(fieldDelim)
		if(lastCell)sb.append(lastCell.formatAsString())
		sb.append(fieldDelim)
		return sb.toString()
	}
	
	def getData(){
		return values
	}

	String toString(){
		StringBuilder sb = new StringBuilder()
		sb.append("One D Section: ").append(sectionKey).append("\n")
		getData().each{value ->
			sb.append(value.toString()).append("\n")
		}
		return sb.toString()
	}

	CellReference getCellAddress(String headerStr){
		return startCell
	}
}
