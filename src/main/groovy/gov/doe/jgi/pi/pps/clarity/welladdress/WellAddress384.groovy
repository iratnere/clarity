package gov.doe.jgi.pi.pps.clarity.welladdress

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation

/**
 * Created by lvishwas on 7/13/17.
 */
class WellAddress384  extends gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress {

    static final Integer minRowIndex = 0
    static final Integer maxRowIndex = 15
    static final Integer minColIndex = 0
    static final Integer maxColIndex = 23

    static List<String> rows = ('A'..'P').toList()

    boolean getIsCorner() {
        return (minRowIndex == rowIndex || maxRowIndex == rowIndex) && (minColIndex == colIndex || maxColIndex == colIndex)
    }

    boolean getIsValid() {
        return rowIndex != null &&
                colIndex != null &&
                rowIndex >= minRowIndex &&
                rowIndex <= maxRowIndex &&
                colIndex >= minColIndex &&
                colIndex <= maxColIndex
    }

    WellAddress384() {}

    WellAddress384(ContainerLocation containerLocation) {
        this(containerLocation.rowColumn)
    }

    WellAddress384(String wellName) {
        String row
        String col
        if (wellName.contains(':')) {
            List<String> rowCol = wellName.split(':').toList()
            if (rowCol.size() == 2) {
                row = rowCol[0]
                col = rowCol[1]
            }
        } else if (wellName?.size() >= 2) {
            row = wellName[0]
            col = wellName[1..-1]
        }
        if (col?.isInteger()) {
            colIndex = col.toInteger() - 1
            rowIndex = rows.indexOf(row.toUpperCase())
        }
        if (!isValid) {
            throw new RuntimeException("invalid well name ${wellName}")
        }
    }

    String getRow() {
        return rows[rowIndex]
    }

    String getColumn() {
        return "${colIndex + 1}"
    }

}
