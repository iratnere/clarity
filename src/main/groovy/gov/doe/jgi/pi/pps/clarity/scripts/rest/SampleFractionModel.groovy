package gov.doe.jgi.pi.pps.clarity.scripts.rest

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming
import groovy.transform.Canonical

@Canonical
@JsonNaming(PropertyNamingStrategy.KebabCaseStrategy.class)
class SampleFractionModel {

    Long originalSampleId
    BigDecimal fractionDensityGMl
    BigDecimal fractionDnaConcentrationNgUl
    String fractionSampleBarcode
    String fractionSampleName
    BigDecimal fractionSampleVolumeUl

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        SampleFractionModel that = (SampleFractionModel) o

        if (fractionSampleName != that.fractionSampleName) return false
        if (originalSampleId != that.originalSampleId) return false

        return true
    }

    int hashCode() {
        int result
        result = originalSampleId.hashCode()
        result = 31 * result + fractionSampleName.hashCode()
        return result
    }
}
