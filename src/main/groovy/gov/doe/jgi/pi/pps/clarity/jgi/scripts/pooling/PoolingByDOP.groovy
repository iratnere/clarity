package gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling

interface PoolingByDOP extends Pooling {
    static final def REGULAR_CELL_ROW = 3 // == row 3 cell 11
    static final def REGULAR_CELL = 11

    int getDop()

    boolean getCanPool()
}