package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

/**
 * Created by datjandra on 7/7/2015.
 */
class AttributeValues {
    final static String PASS = Analyte.PASS
    final static String FAIL = Analyte.FAIL
    final static String DONE = 'Done'
    final static String REWORK = 'Rework'
    final static String REQUEUE = 'Requeue'
}