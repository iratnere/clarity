package gov.doe.jgi.pi.pps.clarity.jgi.scripts.util

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.grails.EmailEventService
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteSorter
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.web_transaction.ClarityWebTransaction

class ProcessUtility {
    ClarityProcess process

    ProcessUtility(ClarityProcess process){
        this.process = process
    }

    def sendEmailNotification(List<Analyte> analytes, EmailNotification emailNotification, String processLink = null){
        AnalyteSorter.sortInPlace(analytes)
        EmailEventService emailEventService = ClarityWebTransaction.currentTransaction.requireApplicationBean(EmailEventService.class)
        emailEventService.sendNotificationEmail(emailNotification, analytes, processLink)
    }
}
