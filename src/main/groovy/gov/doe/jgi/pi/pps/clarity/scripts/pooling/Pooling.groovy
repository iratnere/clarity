package gov.doe.jgi.pi.pps.clarity.scripts.pooling

import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Sheet

interface Pooling {
    String ERROR = "Error"
    String WARNING = "Warning"
    BigDecimal DEFAULT_THRESHOLD = 100.0

    void makePools()

    int completePoolPrepBeans(int poolNumber)

    String getMessage()

    CellStyle getCellStyle(Sheet sheet)

    List getActionBeans()

    List getSummaryBeans()

    List getCalculationBeans()

    Map validate()

    int getPoolNumber()

    def getIndexToCompare(LibraryInformation member)

    Map<String, List<LibraryInformation>> getIndexMembersMap()
}