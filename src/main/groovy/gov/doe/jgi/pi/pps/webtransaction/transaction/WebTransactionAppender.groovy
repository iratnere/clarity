package gov.doe.jgi.pi.pps.webtransaction.transaction

import ch.qos.logback.core.AppenderBase
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.event.LoggingEvent

/**
 * Created by dscott on 4/6/2016.
 */
class WebTransactionAppender extends AppenderBase<LoggingEvent> implements Closeable {

    Logger logger = LoggerFactory.getLogger(WebTransactionAppender.class.name)

    private final Map<String, WebTransaction> transactions = [:]

    private static String getThreadName() {
        Thread.currentThread().name
    }

    void addWebTransaction(WebTransaction webTransaction) {
       transactions[threadName] = webTransaction
    }

    void removeWebTransaction() {
        transactions.remove(threadName)
    }

    @Override
    protected void append(LoggingEvent event) {
        if (!event) {
            return
        }
        WebTransaction webTransaction = transactions[event.threadName]
        if(webTransaction && !webTransaction.closed) {
            webTransaction.addLoggingEvent(event)
        }
    }

    @Override
    void close() {
        clear()
    }

    void clear() {
        transactions.clear()
    }


}
