package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by datjandra on 1/8/2016.
 */
class SlopeKeyValueBean {

    @FieldMapping(header = 'If slope =', cellType = CellTypeEnum.NUMBER)
    public BigDecimal slope

    int populateRequiredFields(int platePassPercentage){
        slope = 1
        return platePassPercentage
    }
}
