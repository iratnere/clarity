package gov.doe.jgi.pi.pps.clarity.config


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class ClarityWorkflowSpec extends Specification {

    Logger logger = LoggerFactory.getLogger(ClarityWorkflowSpec.class.name)

    def setup() {
    }

    def cleanup() {
    }

    void "test ClarityWorkflow toEnum"() {
        when:
        ClarityWorkflow.values().each { ClarityWorkflow clarityWorkflow ->
            assert ClarityWorkflow.toEnum(clarityWorkflow.value) == clarityWorkflow
        }

        then:
        noExceptionThrown()

    }
}
