package gov.doe.jgi.pi.pps.clarity.domain

import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class SequencerModelCvUnitSpec extends Specification implements DomainUnitTest<SequencerModelCv> {

    def setup() {
    }

    def cleanup() {
    }

    void "test sequencerModelCv isSequel"() {
        setup:
        SequencerModelCv sequencerModelCv = new SequencerModelCv()
            sequencerModelCv.sequencerModel = SequencerModelTypeCv.SEQUEL_II.value
        expect:"sequencerModelCv"
            sequencerModelCv
            sequencerModelCv.isSequel
        when:
            sequencerModelCv.sequencerModel = SequencerModelTypeCv.SEQUEL.value
        then:
            sequencerModelCv.isSequel
        when:
            sequencerModelCv.sequencerModel = SequencerModelTypeCv.NOVASEQ_S4.value
        then:
            !sequencerModelCv.isSequel
    }

    void "test getFlowcellType" (String sModel, String flowcellType) {
        when:
        SequencerModelCv sm = new SequencerModelCv(sequencerModel: sModel)
        then:
        assert sm.flowcellType == flowcellType
        where:
        sModel                      | flowcellType
//        "HiSeq-1TB"                 | ""//PPS-5150: inactivate discontinued run modes
        "HiSeq-HO"                  | ""
        "HiSeq-Rapid"               | ""
        "MiSeq"                     | ""
        "NextSeq-HO"                | ""
        "NextSeq-MO"                | ""
        "NovaSeq"                   | ""
        "Sequel"                    | ""
        "Sequel II"                 | ""
        "X10"                       | ""
        "NovaSeq S2"                | "S2"
        "NovaSeq S4"                | "S4"
        "NovaSeq SP"                | "SP"
    }
}
