package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import org.grails.testing.GrailsUnitTest
import spock.lang.Specification

class ProcessTypeSpec extends Specification implements GrailsUnitTest {

    def setup() {
    }

    def cleanup() {
    }

    void "test ProcessType.toEnum"() {
        setup:
        ProcessType processType = ProcessType.values().first()

        expect:
        processType
        ProcessType.toEnum(processType.value) == processType
    }
}